<xsl:stylesheet version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="xml" indent="yes" />
    <xsl:param name="uid" />
    <xsl:param name="codeEdi" />
    <xsl:param name="emetteur" />
    <xsl:param name="destinataire" />
    
    <xsl:template match="/MGUN_DEMAT">
        <xsl:variable name="role">
	        <xsl:choose>
	            <xsl:when test="formalite/destinataires[codeEdiDestinataire=$codeEdi]/typeDestinataire = 'AC'"><xsl:value-of select="'TDR'" /></xsl:when>
	            <xsl:otherwise><xsl:value-of select="'CFE'" /></xsl:otherwise>
	        </xsl:choose>
	    </xsl:variable>
        <GUEN_DMTDU>
            <version>V2012.02</version>
            <emetteur><xsl:value-of select="$emetteur" /></emetteur>
            <destinataire><xsl:value-of select="$destinataire" /></destinataire>
            <dateHeureGenerationXml><xsl:value-of select="dateHeureGenerationXml" /></dateHeureGenerationXml>
            <commentaire>No comment</commentaire>
            <dossierUnique>
                <identificationDossierUnique>
                    <identifiantDossierUnique>
                        <codePartenaireEmetteur>FRONT-OFFICE</codePartenaireEmetteur>
                        <numeroDossierUnique><xsl:value-of select="$uid" /></numeroDossierUnique>
                    </identifiantDossierUnique>
                    <typeDossierUnique>D1</typeDossierUnique>
                    <nomDossier><xsl:value-of select="formalite/correspondant/identiteCorrespondant/nomCorrespondant" /></nomDossier>
                    <xsl:copy-of select="formalite/correspondant" />
                </identificationDossierUnique>
                <destinataireDossierUnique>
                    <roleDestinataire><xsl:value-of select="$role" /></roleDestinataire>
                    <codeDestinataire>
                        <codePartenaire><xsl:value-of select="formalite/destinataires[codeEdiDestinataire=$codeEdi]/codeEdiDestinataire" /></codePartenaire>
                        <codeEdi><xsl:value-of select="formalite/destinataires[codeEdiDestinataire=$codeEdi]/codeEdiDestinataire" /></codeEdi>
                        <libelleIntervenant><xsl:value-of select="formalite/destinataires[codeEdiDestinataire=$codeEdi]/codeEdiDestinataire" /></libelleIntervenant>
                    </codeDestinataire>
                </destinataireDossierUnique>
                <pieceJointe>
                    <indicePieceJointe>2</indicePieceJointe>
                    <typePieceJointe>LIASSE_<xsl:value-of select="$role" />_EDI</typePieceJointe>
                    <formatPieceJointe>XML</formatPieceJointe>
                    <fichierPieceJointe><xsl:value-of select="formalite/fichierLiasseXml" /></fichierPieceJointe>
                </pieceJointe>
                <xsl:for-each select="formalite/documentJustificatif">
                    <pieceJointe>
                        <indicePieceJointe><xsl:value-of select="position() + 2" /></indicePieceJointe>
                        <typePieceJointe>
                            <xsl:choose>
                                <xsl:when test="codeTypeDocumentJustificatif= 'LI'">LIASSE_<xsl:value-of select="$role" />_PDF</xsl:when>
                                <xsl:otherwise>AUTRE_PJ</xsl:otherwise>
                            </xsl:choose>
                        </typePieceJointe>
                        <formatPieceJointe><xsl:value-of select="substring-after(translate(fichierPieceJointe, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'),'.')" /></formatPieceJointe>
                        <fichierPieceJointe><xsl:value-of select="fichierPieceJointe" /></fichierPieceJointe>
                    </pieceJointe>
                </xsl:for-each>
                <dossierCfe>
                    <numeroDeLiasse><xsl:value-of select="formalite/numeroDeLiasse" /></numeroDeLiasse>
                    <dateHeureDepot><xsl:value-of select="dateHeureGenerationXml" /></dateHeureDepot>
                    <referenceLiasseFo><xsl:value-of select="$uid" /></referenceLiasseFo>
                    <destinataireDossierCfe>
                        <roleDestinataire><xsl:value-of select="$role" /></roleDestinataire>
                        <codeEdiDestinataire><xsl:value-of select="formalite/destinataires[codeEdiDestinataire=$codeEdi]/codeEdiDestinataire" /></codeEdiDestinataire>
                        <xsl:for-each select="formalite/documentJustificatif">
                            <indicePieceJointe><xsl:value-of select="position() + 2" /></indicePieceJointe>
                        </xsl:for-each>
                        <indiceLiasseXml>2</indiceLiasseXml>
                        <xsl:if test="formalite/destinataires[codeEdiDestinataire=$codeEdi]/paiement">
	                        <paiementDossier>
	                            <xsl:copy-of select="formalite/destinataires[codeEdiDestinataire=$codeEdi]/paiement/paiementCarteBleue/identitePayeur" />
	                            <xsl:copy-of select="formalite/destinataires[codeEdiDestinataire=$codeEdi]/paiement/paiementCarteBleue/adressePayeur" />
	                            <xsl:copy-of select="formalite/destinataires[codeEdiDestinataire=$codeEdi]/paiement/paiementCarteBleue/montantFormalite" />
	                            <xsl:copy-of select="formalite/destinataires[codeEdiDestinataire=$codeEdi]/paiement/paiementCarteBleue/referencePaiement" />
	                            <xsl:copy-of select="formalite/destinataires[codeEdiDestinataire=$codeEdi]/paiement/paiementCarteBleue/dateHeurePaiement" />
	                        </paiementDossier>
	                    </xsl:if>
                    </destinataireDossierCfe>
                </dossierCfe>
            </dossierUnique>
        </GUEN_DMTDU>
    </xsl:template>
</xsl:stylesheet>