package fr.ge.common.nash.batch.record.report.model.majdu;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.NONE)
public class IndiceTransmission {

    @XmlElement(name = "codePartenaireEmetteur", required = true)
    private String codePartenaireEmetteur;

    @XmlElement(name = "indiceTransmission", required = true)
    private Integer indiceTransmission;

    /**
     * @return the codePartenaireEmetteur
     */
    public String getCodePartenaireEmetteur() {
        return this.codePartenaireEmetteur;
    }

    /**
     * @return the indiceTransmission
     */
    public Integer getIndiceTransmission() {
        return this.indiceTransmission;
    }

}
