/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.batch.validator;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.github.jknack.handlebars.internal.lang3.StringUtils;

import fr.ge.common.nash.batch.bean.AuthorityNaming;
import fr.ge.common.nash.batch.bean.Errors;
import fr.ge.common.nash.batch.utils.ScanFixUtils;
import fr.ge.common.nash.batch.utils.XMLRegentQuery;
import fr.ge.common.nash.batch.utils.XPathBuilder;
import fr.ge.common.utils.ZipUtil;
import fr.ge.common.utils.bean.FileEntry;
import fr.ge.common.utils.exception.TechnicalException;

/**
 * Reset the typeDeVoie tag value into XMLTC.
 *
 * @author aolubi
 */
public class TypeDeVoieTagValidator implements IFunctionalValidator {

    private static final Logger LOGGER = LoggerFactory.getLogger(TypeDeVoieTagValidator.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean validateConditions(final byte[] resourceAsBytes) {
        final FileEntry xmlRegentEntry = ScanFixUtils.getXMLRegentEntry(resourceAsBytes);
        if (null == xmlRegentEntry) {
            LOGGER.warn("XML Regent file is missing");
            return false;
        }

        final String codeEdi = XMLRegentQuery.extractDestinataire(xmlRegentEntry).getTextContent();
        return codeEdi.startsWith(AuthorityNaming.GREFFE.getReseau());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Errors validate(final Properties appProperties, final byte[] resourceAsBytes) {
        return new Errors();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public byte[] fix(final Properties appProperties, final byte[] resourceAsBytes) {
        final FileEntry xmlRegentEntry = ScanFixUtils.getXMLRegentEntry(resourceAsBytes);
        if (null == xmlRegentEntry) {
            LOGGER.warn("XML Regent file is missing");
            return resourceAsBytes;
        }

        final FileEntry xmltcEntry = ScanFixUtils.getXMLTCEntry(resourceAsBytes);
        if (null == xmltcEntry) {
            LOGGER.warn("XMLTC file is missing");
            return resourceAsBytes;
        }

        final String codeEdi = XMLRegentQuery.extractDestinataire(xmlRegentEntry).getTextContent();
        if (!codeEdi.startsWith(AuthorityNaming.GREFFE.getReseau())) {
            return resourceAsBytes;
        }

        byte[] updateResourceAsBytes = resourceAsBytes;
        try {
            final Document document = XPathBuilder.buildDocument(xmltcEntry.asBytes());

            // -->Adresse correspondant block
            final String libelleDeVoieValueAC = Arrays.asList(ADRESSE_CORRESPONDANT_TYPE_DE_VOIE, ADRESSE_CORRESPONDANT_LIBELLE_DE_VOIE) //
                    .stream() //
                    .map(xpath -> XPathBuilder.queryNode(document.getDocumentElement(), xpath)) //
                    .filter(node -> null != node && StringUtils.isNotEmpty(node.getTextContent())) //
                    .map(Node::getTextContent) //
                    .collect(Collectors.joining(" ")) //
            ;

            if (StringUtils.isNotEmpty(libelleDeVoieValueAC)) {
                final Node libelleDeVoie = XPathBuilder.queryNode(document.getDocumentElement(), ADRESSE_CORRESPONDANT_LIBELLE_DE_VOIE);
                if (!libelleDeVoieValueAC.equals(libelleDeVoie.getTextContent())) {
                    libelleDeVoie.setTextContent(libelleDeVoieValueAC);
                    LOGGER.info(String.format("Fix the '%s' tag with %s value", ADRESSE_CORRESPONDANT_LIBELLE_DE_VOIE, libelleDeVoie.getTextContent()));

                    final Node typeDeVoie = XPathBuilder.queryNode(document.getDocumentElement(), ADRESSE_CORRESPONDANT_TYPE_DE_VOIE);
                    typeDeVoie.getParentNode().removeChild(typeDeVoie);
                    LOGGER.info("Remove the {} tag", ADRESSE_CORRESPONDANT_TYPE_DE_VOIE);

                    final Map<String, byte[]> filesToInsert = new HashMap<>();
                    filesToInsert.put(xmltcEntry.name(), XPathBuilder.asBytes(document));

                    updateResourceAsBytes = ZipUtil.entries(filesToInsert, resourceAsBytes);
                }
            }
            // <--

            // -->Adresse payeur block
            final String libelleDeVoieValueAP = Arrays.asList(ADRESSE_PAYEUR_TYPE_DE_VOIE, ADRESSE_PAYEUR_LIBELLE_DE_VOIE) //
                    .stream() //
                    .map(v -> String.format(v, codeEdi)) //
                    .map(xpath -> XPathBuilder.queryNode(document.getDocumentElement(), xpath)) //
                    .filter(node -> null != node && StringUtils.isNotEmpty(node.getTextContent())) //
                    .map(Node::getTextContent) //
                    .collect(Collectors.joining(" ")) //
            ;

            if (StringUtils.isNotEmpty(libelleDeVoieValueAP)) {
                final Node libelleDeVoie = XPathBuilder.queryNode(document.getDocumentElement(), String.format(ADRESSE_PAYEUR_LIBELLE_DE_VOIE, codeEdi));
                if (!libelleDeVoieValueAP.equals(libelleDeVoie.getTextContent())) {
                    libelleDeVoie.setTextContent(libelleDeVoieValueAP);
                    LOGGER.info(String.format("Fix the '%s' tag with %s value", String.format(ADRESSE_PAYEUR_LIBELLE_DE_VOIE, codeEdi), libelleDeVoie.getTextContent()));

                    final Node typeDeVoie = XPathBuilder.queryNode(document.getDocumentElement(), String.format(ADRESSE_PAYEUR_TYPE_DE_VOIE, codeEdi));
                    typeDeVoie.getParentNode().removeChild(typeDeVoie);
                    LOGGER.info("Remove the {} tag", ADRESSE_PAYEUR_TYPE_DE_VOIE);

                    final Map<String, byte[]> filesToInsert = new HashMap<>();
                    filesToInsert.put(xmltcEntry.name(), XPathBuilder.asBytes(document));

                    return ZipUtil.entries(filesToInsert, updateResourceAsBytes);
                }
            }
            // <--

        } catch (TechnicalException e) {
            LOGGER.error("Cannot fix the 'typeDeVoie' tag value", e);
            throw new TechnicalException("Cannot fix the 'typeDeVoie' tag value", e);
        }
        return updateResourceAsBytes;
    }
}
