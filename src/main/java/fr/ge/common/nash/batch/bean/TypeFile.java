/**
 * 
 */
package fr.ge.common.nash.batch.bean;

/**
 * Type file enumeration.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public enum TypeFile {

    XMLTC, //
    REGENT, //
    OTHER
}
