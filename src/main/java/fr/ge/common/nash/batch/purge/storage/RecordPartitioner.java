/**
 * 
 */
package fr.ge.common.nash.batch.purge.storage;

import fr.ge.common.nash.batch.common.AbstractPartitioner;
import fr.ge.common.storage.ws.model.StoredFile;
import fr.ge.common.utils.bean.search.SearchResult;

/**
 * Storage record batch partitionner.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class RecordPartitioner extends AbstractPartitioner<SearchResult<StoredFile>> {

    /**
     * {@inheritDoc}
     */
    @Override
    public SearchResult<StoredFile> search() {
        return this.storageRecordRestService.search(0L, 1L, this.searchQueryBuilder.getFilters(), this.searchQueryBuilder.getOrders(), null);
    }

}