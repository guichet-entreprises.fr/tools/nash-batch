/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.batch.utils;

import java.util.Arrays;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import fr.ge.common.nash.batch.validator.DenominationValidator.TypePersonne;
import fr.ge.common.utils.bean.FileEntry;
import fr.ge.common.utils.exception.TechnicalException;

/**
 * The XML Regent Query class.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public final class XMLRegentQuery {
    private static final Logger LOGGER = LoggerFactory.getLogger(XMLRegentQuery.class);

    /**
     * 
     * Constructeur de la classe.
     *
     */
    private XMLRegentQuery() {
    }

    /**
     * Extract tag 'Destinataire' from XML Regent
     * 
     * @param xmlRegentEntry
     *            The XML Regent file entry
     * @return the destinataire
     */
    public static Node extractDestinataire(final FileEntry xmlRegentEntry) {
        try {
            final Document document = XPathBuilder.buildDocument(xmlRegentEntry.asBytes());
            return XPathBuilder.queryNode(document.getDocumentElement(), "//REGENT-XML/Destinataire");

        } catch (TechnicalException e) {
            LOGGER.error("Cannot fix the 'destinataire' tag value", e);
            throw new TechnicalException("Cannot fix the 'destinataire' tag value", e);
        }
    }

    /**
     * Determine type personne from XML Regent entry.
     * 
     * @param xmlRegentEntry
     *            The file entry
     * @return The type personne as enum
     */
    @Deprecated
    public static TypePersonne getTypePersonne(final FileEntry xmlRegentEntry) {
        try {
            final Document document = XPathBuilder.buildDocument(xmlRegentEntry.asBytes());
            final String typePersonne = XPathBuilder.queryString(document.getDocumentElement(), "//REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10/C10.1");

            if (StringUtils.isEmpty(typePersonne)) {
                return null;
            }

            if (typePersonne.toUpperCase().endsWith("M")) {
                // -->Personne morale
                return TypePersonne.MORALE;
            } else if (typePersonne.toUpperCase().endsWith("P")) {
                // -->Personne physique
                return TypePersonne.PHYSIQUE;
            }
        } catch (TechnicalException e) {
            LOGGER.error("The XML Regent file cannot be read", e);
        }
        return null;
    }

    /**
     * Extract denomination from XML Regent file.
     * 
     * @param typePersonne
     *            The type personne enum
     * @param xmlRegentEntry
     *            The XML Regent file entry
     * @return the denomination
     */
    @Deprecated
    public static String extractDenominationAsString(final TypePersonne typePersonne, final FileEntry xmlRegentEntry) {
        String denomination = null;
        try {
            final Document document = XPathBuilder.buildDocument(xmlRegentEntry.asBytes());
            if (typePersonne.equals(TypePersonne.MORALE)) {
                // -->Personne morale
                denomination = XPathBuilder.queryString(document.getDocumentElement(), "//REGENT-XML/ServiceApplicatif/Liasse/PersonneMorale/ICM/M01/M01.1");

            } else if (typePersonne.equals(TypePersonne.PHYSIQUE)) {
                // -->Personne physique
                final String lastName = XPathBuilder.queryString(document.getDocumentElement(), "//REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P01/P01.2");
                final String firstName = XPathBuilder.queryString(document.getDocumentElement(), "//REGENT-XML/ServiceApplicatif/Liasse/PersonnePhysique/ICP/P01/P01.3[position() = 1]");

                denomination = StringUtils.join(Arrays.asList(lastName, firstName), " ");
            }
            return denomination;
        } catch (TechnicalException e) {
            LOGGER.error("Cannot extract denomination from XML Regent", e);
        }

        return null;
    }

    /**
     * Extract the liasse number from XML Regent.
     * 
     * @param regentEntry
     *            The XML regent as file entry
     * @return the liasse number
     */
    public static String extractNumeroLiasse(final FileEntry regentEntry) {
        try {
            final Document document = XPathBuilder.buildDocument(regentEntry.asBytes());
            return XPathBuilder.queryString(document.getDocumentElement(), "//REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C02");
        } catch (TechnicalException e) {
            LOGGER.error("The XML Regent cannot be read", e);
        }
        return null;
    }
}
