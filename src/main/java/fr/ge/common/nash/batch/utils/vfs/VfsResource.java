package fr.ge.common.nash.batch.utils.vfs;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;

public class VfsResource {

    private final FileObject targetResource;

    private final FileObject checksumResource;

    private final Collection<VfsResource> linkedResources = new ArrayList<>();

    public VfsResource(final FileObject targetResource) {
        this(targetResource, null);
    }

    public VfsResource(final FileObject targetResource, final FileObject checksumResource) {
        this.targetResource = targetResource;
        this.checksumResource = checksumResource;
    }

    public void moveTo(final FileObject targetBasePath) throws FileSystemException {
        moveTo(this.targetResource, targetBasePath);
        moveTo(this.checksumResource, targetBasePath);

        for (final VfsResource linkedResource : this.linkedResources) {
            linkedResource.delete();
        }
    }

    public void delete() throws FileSystemException {
        this.targetResource.deleteAll();
    }

    private static void moveTo(final FileObject src, final FileObject target) throws FileSystemException {
        if (null != src) {
            src.moveTo(target.resolveFile(src.getName().getBaseName()));
        }
    }

    public String getBasename() {
        return this.targetResource.getName().getBaseName();
    }

    public InputStream getInputStream() throws FileSystemException {
        return this.targetResource.getContent().getInputStream();
    }

    public FileObject getParent() throws FileSystemException {
        return this.targetResource.getParent();
    }

    public VfsResource attach(final VfsResource linkedResource) {
        this.linkedResources.add(linkedResource);
        return linkedResource;
    }

    public VfsResource attach(final FileObject linkedObject) {
        return this.attach(new VfsResource(linkedObject));
    }

}
