/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.batch.bean;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;

/**
 * The Class Errors.
 * 
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class Errors {

    /** The global errors. */
    private final Collection<Error> globalErrors = new LinkedHashSet<>();

    /** The object errors. */
    private final Map<String, Collection<Error>> objectErrors = new HashMap<>();

    /**
     * Adds the.
     *
     * @param error
     *            the error
     * @return the errors
     */
    public Errors add(final Error error) {
        this.globalErrors.add(error);
        return this;
    }

    /**
     * Adds the.
     *
     * @param objectName
     *            the object name
     * @param error
     *            the error
     * @return the errors
     */
    public Errors add(final String objectName, final Error error) {
        this.messages(objectName).add(error);
        return this;
    }

    /**
     * Merge.
     *
     * @param objectName
     *            the object name
     * @param errors
     *            the errors
     * @return the errors
     */
    public Errors merge(final String objectName, final Errors errors) {
        final Collection<Error> messages = null == errors ? null : errors.messages();
        if (CollectionUtils.isNotEmpty(messages)) {
            this.messages(objectName).addAll(messages);
        }
        return this;
    }

    /**
     * Gets the global errors.
     *
     * @return the globalErrors
     */
    public Collection<Error> getGlobalErrors() {
        return this.globalErrors;
    }

    /**
     * Gets the object errors.
     *
     * @return the objectErrors
     */
    public Map<String, Collection<Error>> getObjectErrors() {
        return this.objectErrors;
    }

    /**
     * Gets the object errors.
     *
     * @param objectName
     *            the object name
     * @return the object errors
     */
    public Collection<Error> getObjectErrors(final String objectName) {
        return this.objectErrors.get(objectName);
    }

    /**
     * Gets the objects.
     *
     * @return the objects
     */
    public Collection<String> getObjects() {
        return new ArrayList<>(this.objectErrors.keySet());
    }

    /**
     * Messages.
     *
     * @param objectName
     *            the object name
     * @return the list
     */
    public Collection<Error> messages(final String objectName) {
        Collection<Error> lst = this.objectErrors.get(objectName);
        if (null == lst) {
            lst = new LinkedHashSet<>();
            this.objectErrors.put(objectName, lst);
        }
        return lst;
    }

    /**
     * Messages.
     *
     * @return the list
     */
    public Collection<Error> messages() {
        final Collection<Error> msgs = new LinkedHashSet<>(this.globalErrors);
        this.objectErrors.forEach((k, v) -> msgs.addAll(v));
        return msgs;
    }

    /**
     * Checks for global errors.
     *
     * @return true, if successful
     */
    public boolean hasGlobalErrors() {
        return !this.globalErrors.isEmpty();
    }

    /**
     * Checks for object errors.
     *
     * @return true, if successful
     */
    public boolean hasObjectErrors() {
        return !this.objectErrors.isEmpty();
    }

    /**
     * Checks for errors.
     *
     * @return true, if successful
     */
    public boolean hasErrors() {
        return this.hasGlobalErrors() || this.hasObjectErrors();
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString() {
        return String.format("[ %s ]\n%s", //
                this.globalErrors.stream() //
                        .map(Error::toString) //
                        .collect(Collectors.joining(", ")), //
                this.objectErrors.entrySet().stream() //
                        .map( //
                                entry -> String.format("%s : [ %s ]", //
                                        entry.getKey(), entry.getValue().stream() //
                                                .map(Error::toString) //
                                                .collect(Collectors.joining(", ")) //
                                ) //
                        ) //
                        .collect(Collectors.joining("\n")) //
        );
    }

}
