package fr.ge.common.nash.batch.record.error;

import org.springframework.batch.item.ItemProcessor;

/**
 * Takes a list of SFTP record resource path, validate each one, and output only
 * those that are incorrect.
 *
 * @author Christian Cougourdan
 */
public class ScanItemProcessor implements ItemProcessor<String, String> {

    @Override
    public String process(final String item) throws Exception {
        return null;
    }

}
