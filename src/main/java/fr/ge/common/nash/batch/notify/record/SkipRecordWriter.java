package fr.ge.common.nash.batch.notify.record;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.stereotype.Component;

import fr.ge.common.nash.ws.v1.bean.RecordInfoBean;

/**
 * Skip record notify writer.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@Component(value = "skipRecordWriter")
public class SkipRecordWriter implements ItemWriter<RecordInfoBean> {

    private static final Logger LOGGER = LoggerFactory.getLogger(SkipRecordWriter.class);

    @Override
    public void write(final List<? extends RecordInfoBean> items) throws Exception {
        items.stream().forEach(record -> LOGGER.info("Skip notify record {}", record.getCode()));
    }
}
