/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.batch.bean;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * The error class definition.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class Error {

    /**
     * The Enum Level.
     */
    public enum Level {

        /** The debug. */
        DEBUG,
        /** The info. */
        INFO,
        /** The warn. */
        WARN,
        /** The error. */
        ERROR,
        /** The fatal. */
        FATAL
    }

    /** The level. */
    private final Level level;

    /** The message. */
    private final String message;

    /**
     * Constructor.
     */
    public Error() {
        this(Level.ERROR, StringUtils.EMPTY);
    }

    /**
     * Instantiates a new error.
     *
     * @param msg
     *            the msg
     */
    public Error(final String msg) {
        this(Level.ERROR, msg);
    }

    /**
     * Instantiates a new error.
     *
     * @param level
     *            the level
     * @param msg
     *            the msg
     */
    public Error(final Level level, final String msg) {
        this.level = level;
        this.message = msg;
    }

    /**
     * Gets the level.
     *
     * @return the level
     */
    public Level getLevel() {
        return this.level;
    }

    /**
     * Gets the message.
     *
     * @return the message
     */
    public String getMessage() {
        return this.message;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder() //
                .append(this.level) //
                .append(this.message) //
                .toHashCode();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        } else if (!(obj instanceof Error)) {
            return false;
        }

        final Error other = (Error) obj;

        return new EqualsBuilder() //
                .append(this.level, other.level) //
                .append(this.message, other.message) //
                .isEquals();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return String.format("[%s] %s", this.level, this.message);
    }

}
