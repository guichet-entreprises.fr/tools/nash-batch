/**
 *
 */
package fr.ge.common.nash.batch.common;

import org.slf4j.MDC;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;

/**
 * Logging job.
 *
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class LoggingJobExecutionListener implements JobExecutionListener {

    /**
     * {@inheritDoc}
     */
    @Override
    public void beforeJob(final JobExecution jobExecution) {
        MDC.put("job", jobExecution.getJobInstance().getJobName());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterJob(final JobExecution jobExecution) {
        MDC.remove("job");
    }

}
