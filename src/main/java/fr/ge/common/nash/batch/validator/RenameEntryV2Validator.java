/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.batch.validator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import fr.ge.common.nash.batch.bean.AuthorityNaming;
import fr.ge.common.nash.batch.bean.ReplacementAuthority;
import fr.ge.common.nash.batch.bean.TypeFile;
import fr.ge.common.nash.batch.utils.ScanFixUtils;
import fr.ge.common.nash.batch.utils.XMLRegentQuery;
import fr.ge.common.nash.batch.utils.XPathBuilder;
import fr.ge.common.utils.ZipUtil;
import fr.ge.common.utils.bean.FileEntry;
import fr.ge.common.utils.exception.TechnicalException;

/**
 * Control and rename entries into zip file if necessary for authories CCI, CMA,
 * URSSAF.
 *
 * @author aolubi
 */
public class RenameEntryV2Validator extends AbstractRenameEntryValidator {

    private static final Logger LOGGER = LoggerFactory.getLogger(RenameEntryV2Validator.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean validateConditions(final byte[] resourceAsBytes) {
        final FileEntry xmlRegentEntry = ScanFixUtils.getXMLRegentEntry(resourceAsBytes);
        if (null == xmlRegentEntry) {
            LOGGER.warn("XML Regent file is missing");
            return false;
        }

        final String codeEdi = XMLRegentQuery.extractDestinataire(xmlRegentEntry).getTextContent();
        return !codeEdi.startsWith(AuthorityNaming.GREFFE.getReseau());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public byte[] fix(final Properties appProperties, final byte[] resourceAsBytes) {
        if (this.validate(appProperties, resourceAsBytes).hasErrors()) {

            final FileEntry xmltcEntry = ScanFixUtils.getXMLTCEntry(resourceAsBytes);
            if (null == xmltcEntry) {
                LOGGER.warn("XMLTC file is missing");
                return resourceAsBytes;
            }

            final FileEntry xmlRegentEntry = ScanFixUtils.getXMLRegentEntry(resourceAsBytes);
            if (null == xmlRegentEntry) {
                LOGGER.warn("XML Regent file is missing");
                return resourceAsBytes;
            }

            final String codeEdi = XMLRegentQuery.extractDestinataire(xmlRegentEntry).getTextContent();
            final List<String> pjs = ZipUtil.entries(resourceAsBytes);

            // -->Rename all input files into archive and XMLTC file
            final Map<String, String> filesToUpdate = new HashMap<>();
            final AuthorityNaming naming = AuthorityNaming.getNaming(codeEdi);

            for (final String pj : pjs) {
                if (null != naming.getReplacements()) {
                    for (final ReplacementAuthority replacementAuthority : naming.getReplacements()) {
                        String replaceValue = pj;
                        if (StringUtils.isNotEmpty(filesToUpdate.get(replaceValue))) {
                            replaceValue = filesToUpdate.get(replaceValue);
                        }

                        if (REGEX_XMLTC.matcher(replaceValue).matches()) {
                            replaceValue = replacementAuthority.findAndReplace(TypeFile.XMLTC, replaceValue);
                        } else if (REGEX_REGENT.matcher(replaceValue).matches()) {
                            replaceValue = replacementAuthority.findAndReplace(TypeFile.REGENT, replaceValue);
                        } else if (REGEX_OTHER.matcher(replaceValue).matches()) {
                            replaceValue = replacementAuthority.findAndReplace(TypeFile.OTHER, replaceValue);
                        }

                        filesToUpdate.put(pj, replaceValue);
                    }
                } else {
                    filesToUpdate.put(pj, pj);
                }
            }

            // -->Replace attachment name into archive file
            final Map<String, byte[]> filesToAdd = new HashMap<>();

            filesToUpdate.forEach((search, replace) -> {
                if (!REGEX_XMLTC.matcher(search).matches()) {
                    final FileEntry attachmentEntry = ZipUtil.entry(search, resourceAsBytes);
                    filesToAdd.put(replace, attachmentEntry.asBytes());
                }
            });

            // -->Update XMLTC file
            final Entry<String, String> entryXml = filesToUpdate.entrySet().stream().filter(e -> REGEX_XMLTC.matcher(e.getKey()).matches()).findFirst().orElse(null);
            filesToAdd.put(entryXml.getValue(), this.updateXMLTCContent(filesToUpdate, xmltcEntry));

            return ZipUtil.create(filesToAdd);
        }
        return resourceAsBytes;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public byte[] updateXMLTCContent(final Map<String, String> filesToUpdate, final FileEntry xmltcEntry) {
        try {
            final Document document = XPathBuilder.buildDocument(xmltcEntry.asBytes());
            String expression = "//MGUN_DEMAT/formalite/fichierLiasseXml";
            final Node fichierLiasseXml = XPathBuilder.queryNode(document.getDocumentElement(), expression);
            final String oldLiasseName = fichierLiasseXml.getTextContent();
            final String newLiasseName = filesToUpdate.get(oldLiasseName);
            if (StringUtils.isNotEmpty(oldLiasseName) && StringUtils.isNotEmpty(newLiasseName) && !newLiasseName.equals(oldLiasseName)) {
                fichierLiasseXml.setTextContent(newLiasseName);
                LOGGER.debug("Rename entry file from {} to {} into XML TC file", oldLiasseName, newLiasseName);
            }

            expression = "//MGUN_DEMAT/formalite/documentJustificatif/fichierPieceJointe";
            final NodeList fichierPieceJointeList = XPathBuilder.queryNodeList(document.getDocumentElement(), expression);

            for (int idx = 0; idx < fichierPieceJointeList.getLength(); idx++) {
                final Node fichierPieceJointe = fichierPieceJointeList.item(idx);

                final String oldAttachmentName = fichierPieceJointe.getTextContent();
                final String newAttachmentName = filesToUpdate.get(oldAttachmentName);

                if (StringUtils.isNotEmpty(oldAttachmentName) && StringUtils.isNotEmpty(newAttachmentName) && !newAttachmentName.equals(oldAttachmentName)) {
                    fichierPieceJointe.setTextContent(newAttachmentName);
                    LOGGER.debug("Rename entry file from {} to {} into XML TC file", oldAttachmentName, newAttachmentName);
                }
            }

            return XPathBuilder.asBytes(document);
        } catch (TechnicalException e) {
            LOGGER.error("Cannot rename attachment document into XMLTC file", e);
        }

        return xmltcEntry.asBytes();
    }
}
