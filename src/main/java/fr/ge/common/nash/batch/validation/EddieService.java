/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.batch.validation;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Optional;
import java.util.Properties;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.ge.common.nash.batch.bean.FileState;
import fr.ge.common.nash.batch.bean.FileStateEnum;

/**
 * Eddie service.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@Service
public class EddieService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EddieService.class);

    @Autowired
    private Properties appProperties;

    public FileState uploadToEddie(final String inputFile, final String token, final String pathFolder, final String outputZip, final String outputFini) {
        try {
            for (final String absoluteFilePath : Arrays.asList(outputZip, outputFini)) {
                final Path path = Paths.get(absoluteFilePath);
                if (!Files.exists(path)) {
                    LOGGER.warn("Input file {} does not exist !!", absoluteFilePath);
                    return new FileState(FileStateEnum.SEND_KO, inputFile, outputZip, outputFini);
                }
                final String streamName = Optional.ofNullable(pathFolder) //
                        .filter(p -> StringUtils.isNotEmpty(p)) //
                        .map(p -> String.join(File.separator, p, path.getFileName().toString())) //
                        .orElse(String.join(File.separator, path.getFileName().toString()));

                final byte[] resourceAsBytes = FileUtils.readFileToByteArray(new File(absoluteFilePath));
                final Response response = ClientBuilder.newClient() //
                        .register(com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider.class) //
                        .target(this.appProperties.getProperty("eddie.gp.url.ws.private") + "/exchange") //
                        .queryParam("uid", token) //
                        .queryParam("streamName", streamName) //
                        .request() //
                        .post(Entity.entity(resourceAsBytes, javax.ws.rs.core.MediaType.APPLICATION_OCTET_STREAM));

                if (null != response && response.getStatus() != Status.OK.getStatusCode() && response.getStatus() != Status.CREATED.getStatusCode()) {
                    LOGGER.error("Output files {} are sent to EDDIE with failure", outputZip, outputFini);
                    return new FileState(FileStateEnum.SEND_KO, inputFile, outputZip, outputFini);
                }
            }
            LOGGER.info("Output files {} and {} are sent to EDDIE successfully", outputZip, outputFini);
            return new FileState(FileStateEnum.SEND_OK, inputFile, outputZip, outputFini);
        } catch (Exception e) {
            LOGGER.error(String.format("Output files %s and %s are sent to EDDIE with failure", outputZip, outputFini), e);
            return new FileState(FileStateEnum.SEND_KO, inputFile, outputZip, outputFini);
        }
    }
}
