package fr.ge.common.nash.batch.purge.storage;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import fr.ge.common.nash.batch.common.AbstractSearchQueryBuilder;
import fr.ge.common.storage.ws.model.StoredFile;
import fr.ge.common.storage.ws.rest.IRecordRestService;
import fr.ge.common.utils.bean.search.SearchResult;

/**
 * Storage record batch reader.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@Component("purgeStorageRecordReader")
public class RecordReader implements ItemReader<StoredFile>, InitializingBean {

    protected static final Logger LOGGER = LoggerFactory.getLogger(RecordReader.class);

    protected AbstractSearchQueryBuilder searchQueryBuilder;

    private IRecordRestService storageRecordRestService;

    protected Iterator<StoredFile> recordIterator;

    protected Long startIndex;

    protected Long maxResults;

    public RecordReader() {

    }

    /**
     * Mutateur sur l'attribut {@link #searchQueryBuilder}.
     *
     * @param searchQueryBuilder
     *            la nouvelle valeur de l'attribut searchQueryBuilder
     */
    public void setSearchQueryBuilder(final AbstractSearchQueryBuilder searchQueryBuilder) {
        this.searchQueryBuilder = searchQueryBuilder;
    }

    /**
     * Mutateur sur l'attribut {@link #storageRecordRestService}.
     *
     * @param storageRecordRestService
     *            la nouvelle valeur de l'attribut storageRecordRestService
     */
    public void setStorageRecordRestService(final IRecordRestService storageRecordRestService) {
        this.storageRecordRestService = storageRecordRestService;
    }

    /**
     * Mutateur sur l'attribut {@link #startIndex}.
     *
     * @param startIndex
     *            la nouvelle valeur de l'attribut startIndex
     */
    public void setStartIndex(final Long startIndex) {
        this.startIndex = startIndex;
    }

    /**
     * Mutateur sur l'attribut {@link #maxResults}.
     *
     * @param maxResults
     *            la nouvelle valeur de l'attribut maxResults
     */
    public void setMaxResults(final Long maxResults) {
        this.maxResults = maxResults;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        if (null != this.startIndex && null != this.maxResults) {
            LOGGER.info("Search with startIndex {}, maxResults {}, filters : {}, orders : {}", this.startIndex, this.maxResults, this.searchQueryBuilder.getFilters(),
                    this.searchQueryBuilder.getOrders());
            final SearchResult<StoredFile> searchResult = this.storageRecordRestService.search(this.startIndex, this.maxResults, this.searchQueryBuilder.getFilters(),
                    this.searchQueryBuilder.getOrders(), null);
            this.recordIterator = Optional.ofNullable(searchResult) //
                    .filter(r -> null != r.getContent()) //
                    .map(SearchResult::getContent) //
                    .orElse(new ArrayList<StoredFile>()) //
                    .iterator();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StoredFile read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
        if (null != this.recordIterator && this.recordIterator.hasNext()) {
            return this.recordIterator.next();
        }
        return null;
    }
}
