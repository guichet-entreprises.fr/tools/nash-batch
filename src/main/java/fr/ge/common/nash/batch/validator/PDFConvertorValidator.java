/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.batch.validator;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import fr.ge.common.nash.batch.bean.Error;
import fr.ge.common.nash.batch.bean.Error.Level;
import fr.ge.common.nash.batch.bean.Errors;
import fr.ge.common.nash.batch.utils.ScanFixUtils;
import fr.ge.common.nash.batch.utils.XPathBuilder;
import fr.ge.common.nash.core.util.MimeTypeUtil;
import fr.ge.common.nash.engine.adapter.v1_0.FileValueAdapter;
import fr.ge.common.nash.engine.adapter.v1_0.FileValueAdapter.Item;
import fr.ge.common.nash.engine.manager.processor.bridge.document.PdfDocument;
import fr.ge.common.utils.ZipUtil;
import fr.ge.common.utils.bean.FileEntry;
import fr.ge.common.utils.exception.TechnicalException;

/**
 * Converts .png files to pdf.
 *
 * @author aolubi
 */
public class PDFConvertorValidator implements IFunctionalValidator {

    private static final Logger LOGGER = LoggerFactory.getLogger(PDFConvertorValidator.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean validateConditions(final byte[] resourceAsBytes) {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Errors validate(final Properties appProperties, final byte[] resourceAsBytes) {
        final Errors errors = new Errors();
        final FileEntry xmltcEntry = ScanFixUtils.getXMLTCEntry(resourceAsBytes);
        if (null == xmltcEntry) {
            LOGGER.info("Missing XMLTC file");
            errors.add(new Error(Level.ERROR, "Missing XMLTC file"));
            return errors;
        }

        if (ZipUtil.entries(resourceAsBytes) //
                .stream() //
                .anyMatch(entry -> JPG_JPEG_PNG_EXTENSION.matcher(entry).matches())) {
            LOGGER.warn("The file contains entries which does not match .pdf or .xml extension");
            errors.add(new Error(Level.ERROR, "The file contains entries which does not match .pdf or .xml extension"));
        }

        return errors;
    }

    /**
     * Builds an item to pdf.
     *
     * @param item
     *            the item
     * @return the byte[]
     */
    public byte[] convert(final Item item) {
        try (PdfDocument doc = new PdfDocument()) {
            if (null != item) {
                doc.append(item);
            }

            LOGGER.info("item to merge : {}", item.getLabel());

            final Item out = doc.save("converted.pdf");
            return out.getContent();
        } catch (final IOException ex) {
            LOGGER.info("Merge files into PDF : {}", ex.getMessage());
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public byte[] fix(final Properties appProperties, final byte[] resourceAsBytes) {
        if (!this.validate(appProperties, resourceAsBytes).hasErrors()) {
            return resourceAsBytes;
        }

        final List<String> attachement = ZipUtil.entries(resourceAsBytes) //
                .stream() //
                .filter(entry -> JPG_JPEG_PNG_EXTENSION.matcher(entry).matches()) //
                .collect(Collectors.toList()) //
        ;

        // Replace png occurences to pdf into XMLTC if necessary
        try {
            final Map<String, byte[]> filesToInsert = new HashMap<>();
            final FileEntry xmltcEntry = ScanFixUtils.getXMLTCEntry(resourceAsBytes);
            final Document document = XPathBuilder.buildDocument(xmltcEntry.asBytes());
            final NodeList pieceJointelist = XPathBuilder.queryNodeList(document.getDocumentElement(), "//" + TAG_DOSSIER_UNIQUE + "/" + TAG_PIECE_JOINTE);
            for (int idx = 0; idx < pieceJointelist.getLength(); idx++) {
                final Node pieceJointe = pieceJointelist.item(idx);

                final Node fichierPieceJointe = XPathBuilder.queryNode(pieceJointe, "fichierPieceJointe");
                if (attachement.contains(fichierPieceJointe.getTextContent())) {
                    final String oldName = fichierPieceJointe.getTextContent();
                    final String newName = fichierPieceJointe.getTextContent().replaceFirst(JPG_JPEG_PNG_EXTENSION.pattern(), "$1.pdf");

                    final byte[] pdfAsBytes = this.convert(FileValueAdapter.createItem(-1, oldName, MimeTypeUtil.type(oldName), () -> ZipUtil.entry(oldName, resourceAsBytes).asBytes()));

                    filesToInsert.put(oldName, null);
                    LOGGER.info(String.format("Remove the content for %s", oldName));

                    fichierPieceJointe.setTextContent(newName);
                    LOGGER.info(String.format("Fix the 'fichierPieceJointe' tag with %s value", fichierPieceJointe.getTextContent()));

                    filesToInsert.put(newName, pdfAsBytes);
                    LOGGER.info(String.format("Add the new file %s", fichierPieceJointe.getTextContent()));

                    final Node formatPieceJointe = XPathBuilder.queryNode(pieceJointe, "formatPieceJointe");
                    formatPieceJointe.setTextContent("PDF");
                    LOGGER.info(String.format("Fix the 'formatPieceJointe' tag with %s value", formatPieceJointe.getTextContent().toUpperCase()));
                }
            }

            filesToInsert.put(xmltcEntry.name(), XPathBuilder.asBytes(document));

            return ZipUtil.entries(filesToInsert, resourceAsBytes);

        } catch (TechnicalException e) {
            LOGGER.error("Cannot convert PNG files to PDF", e);
            throw new TechnicalException("Cannot convert PNG files to PDF", e);
        }
    }
}
