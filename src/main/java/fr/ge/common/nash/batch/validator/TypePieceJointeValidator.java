/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.batch.validator;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.github.jknack.handlebars.internal.lang3.StringUtils;

import fr.ge.common.nash.batch.bean.AuthorityNaming;
import fr.ge.common.nash.batch.bean.Error;
import fr.ge.common.nash.batch.bean.Error.Level;
import fr.ge.common.nash.batch.bean.Errors;
import fr.ge.common.nash.batch.utils.ScanFixUtils;
import fr.ge.common.nash.batch.utils.XMLRegentQuery;
import fr.ge.common.nash.batch.utils.XMLTCQuery;
import fr.ge.common.nash.batch.utils.XPathBuilder;
import fr.ge.common.utils.ZipUtil;
import fr.ge.common.utils.bean.FileEntry;

/**
 * Scan & Fix typePieceJointe tag into the XMLTC file.
 *
 * @author aolubi
 */
public class TypePieceJointeValidator implements IFunctionalValidator {

    private static final Logger LOGGER = LoggerFactory.getLogger(TypePieceJointeValidator.class);

    private static final Map<String, Map<String, String>> CONVERTERS;

    static {
        final Map<String, Map<String, String>> map = new HashMap<>();

        // -->Role CFE
        final Map<String, String> cfe = new HashMap<>();
        cfe.put("PDF", "LIASSE_CFE_PDF");
        cfe.put("XML", "LIASSE_CFE_EDI");
        map.put("CFE", cfe);

        // -->Role TDR
        final Map<String, String> tdr = new HashMap<>();
        tdr.put("PDF", "LIASSE_TDR_PDF");
        tdr.put("XML", "LIASSE_TDR_EDI");
        map.put("TDR", tdr);

        // -->Role CFE+TDR
        map.put("CFE+TDR", cfe);

        CONVERTERS = Collections.unmodifiableMap(map);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Errors validate(final Properties appProperties, final byte[] resourceAsBytes) {
        final Errors errors = new Errors();

        final FileEntry xmltcEntry = ScanFixUtils.getXMLTCEntry(resourceAsBytes);
        if (null == xmltcEntry) {
            LOGGER.warn("Cannot compare entries where XMLTC file is missing");
            errors.add(new Error(Level.ERROR, "Cannot compare entries where XMLTC file is missing"));
            return errors;
        }
        final FileEntry xmlRegentEntry = ScanFixUtils.getXMLRegentEntry(resourceAsBytes);
        if (null == xmlRegentEntry) {
            LOGGER.warn("Cannot compare entries where XML Regent file is missing");
            errors.add(new Error(Level.ERROR, "Cannot compare entries where XML Regent file is missing"));
            return errors;
        }

        final String codeEdi = XMLRegentQuery.extractDestinataire(xmlRegentEntry).getTextContent();
        if (!codeEdi.startsWith(AuthorityNaming.GREFFE.getReseau())) {
            return errors;
        }

        final Node destinataireDossierCfeAsNode = XMLTCQuery.extractDestinataireDossierCfeAsNode(xmltcEntry, codeEdi);
        if (null == destinataireDossierCfeAsNode) {
            LOGGER.warn("Missing target authority {} description into XMLTC file", codeEdi);
            errors.add(new Error(Level.ERROR, String.format("Missing target authority %s description into XMLTC file", codeEdi)));
            return errors;
        }

        final String roleDestinataire = Optional.of(XPathBuilder.queryNode(destinataireDossierCfeAsNode, "roleDestinataire")) //
                .filter(node -> null != node) //
                .map(Node::getTextContent) //
                .orElse(null);
        if (StringUtils.isEmpty(roleDestinataire)) {
            LOGGER.warn("Cannot extract role for {}", codeEdi);
            errors.add(new Error(Level.ERROR, String.format("Cannot extract role for %s", codeEdi)));
            return errors;
        }

        final Document document = XPathBuilder.buildDocument(xmltcEntry.asBytes());
        final NodeList typePieceJointeList = XPathBuilder.queryNodeList(document, "//GUEN_DMTDU/dossierUnique/pieceJointe[starts-with(typePieceJointe,'LIASSE')]/typePieceJointe");
        for (int i = 0; i < typePieceJointeList.getLength(); i++) {
            final Node typePieceJointe = typePieceJointeList.item(i);
            if (!CONVERTERS.get(roleDestinataire).values().contains(typePieceJointe.getTextContent())) {
                LOGGER.warn("Invalid typePieceJointe {} for role {}", typePieceJointe.getTextContent(), roleDestinataire);
                errors.add(new Error(Level.ERROR, String.format("Invalid typePieceJointe %s for role %s", typePieceJointe.getTextContent(), roleDestinataire)));
            }
        }

        return errors;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public byte[] fix(final Properties appProperties, final byte[] resourceAsBytes) {
        if (!this.validate(appProperties, resourceAsBytes).hasErrors()) {
            return resourceAsBytes;
        }

        final FileEntry xmlRegentEntry = ScanFixUtils.getXMLRegentEntry(resourceAsBytes);
        if (null == xmlRegentEntry) {
            LOGGER.warn("Cannot compare entries where XML Regent file is missing");
            return resourceAsBytes;
        }

        final String codeEdi = XMLRegentQuery.extractDestinataire(xmlRegentEntry).getTextContent();
        if (!codeEdi.startsWith(AuthorityNaming.GREFFE.getReseau())) {
            return resourceAsBytes;
        }

        final FileEntry xmltcEntry = ScanFixUtils.getXMLTCEntry(resourceAsBytes);
        if (null == xmltcEntry) {
            LOGGER.warn("Cannot compare entries where XMLTC file is missing");
            return resourceAsBytes;
        }

        // -->Index of files declared for target authority
        final Node destinataireDossierCfeAsNode = XMLTCQuery.extractDestinataireDossierCfeAsNode(xmltcEntry, codeEdi);
        if (null == destinataireDossierCfeAsNode) {
            LOGGER.warn("Missing target authority {} description into XMLTC file", codeEdi);
            return resourceAsBytes;
        }

        final Document document = XPathBuilder.buildDocument(xmltcEntry.asBytes());

        final String roleDestinataire = Optional.of(XPathBuilder.queryNode(destinataireDossierCfeAsNode, "roleDestinataire")) //
                .filter(node -> null != node) //
                .map(Node::getTextContent) //
                .orElse(null);
        if (StringUtils.isEmpty(roleDestinataire)) {
            LOGGER.warn("Cannot extract role for {}", codeEdi);
            return resourceAsBytes;
        }

        final NodeList pieceJointeList = XPathBuilder.queryNodeList(document, "//GUEN_DMTDU/dossierUnique/pieceJointe[starts-with(typePieceJointe,'LIASSE')]");
        for (int i = 0; i < pieceJointeList.getLength(); i++) {
            final Node pieceJointeNode = pieceJointeList.item(i);
            final Node typePieceJointeNode = XPathBuilder.queryNode(pieceJointeNode, "typePieceJointe");
            final String oldValue = typePieceJointeNode.getTextContent();

            if (!CONVERTERS.get(roleDestinataire).values().contains(oldValue)) {

                // -->Get formatPieceJointe tag
                final String formatPieceJointe = XPathBuilder.queryString(pieceJointeNode, "formatPieceJointe");
                final String newValue = CONVERTERS.get(roleDestinataire).get(formatPieceJointe);
                if (!newValue.equals(oldValue)) {
                    typePieceJointeNode.setTextContent(newValue);
                    LOGGER.warn("Replace tag typePieceJointe from {} to {}", oldValue, newValue);
                }
            }
        }

        final Map<String, byte[]> filesToInsert = new HashMap<>();
        filesToInsert.put(xmltcEntry.name(), XPathBuilder.asBytes(document));

        return ZipUtil.entries(filesToInsert, resourceAsBytes);
    }
}
