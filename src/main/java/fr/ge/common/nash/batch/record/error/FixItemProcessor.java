package fr.ge.common.nash.batch.record.error;

import org.springframework.batch.item.ItemProcessor;

/**
 * Takes a list of SFTP resource path corresponding to record to fix, and send
 * it back to SFTP.
 *
 * @author Christian Cougourdan
 */
public class FixItemProcessor implements ItemProcessor<String, RecordResource> {

    @Override
    public RecordResource process(final String item) throws Exception {
        return null;
    }

}
