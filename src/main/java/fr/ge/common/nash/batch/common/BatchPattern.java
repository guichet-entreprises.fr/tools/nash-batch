/**
 * 
 */
package fr.ge.common.nash.batch.common;

import java.util.regex.Pattern;

/**
 * Batch pattern constant.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public interface BatchPattern {

    String NOTIFY_START = "notify-start-";
    String NOTIFY_START_REGEX = "(" + NOTIFY_START + ")(\\d{1,2})";
    String NOTIFY_STOP = "notify-stop";

    Pattern IS_NOTIFY_START = Pattern.compile(NOTIFY_START_REGEX);
    Pattern IS_NOTIFY_STOP = Pattern.compile(NOTIFY_STOP);
    Pattern IS_NOTIFY_START_OR_STOP = Pattern.compile("(" + NOTIFY_START_REGEX + "|" + NOTIFY_STOP + ")");

}
