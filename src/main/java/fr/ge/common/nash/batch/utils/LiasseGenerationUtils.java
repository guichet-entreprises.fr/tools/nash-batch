/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.batch.utils;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDateTime;

/**
 * Liasse generation.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class LiasseGenerationUtils {

    private String reseau;
    private String emetteur;
    private String type;
    private String version;
    private String extensionZip;
    private String extensionFini;

    /**
     * 
     * Constructeur de la classe.
     *
     */
    public LiasseGenerationUtils() {
    }

    /**
     * Accesseur sur l'attribut {@link #reseau}.
     *
     * @return String reseau
     */
    public String getReseau() {
        return reseau;
    }

    /**
     * Mutateur sur l'attribut {@link #reseau}.
     *
     * @param reseau
     *            la nouvelle valeur de l'attribut reseau
     */
    public void setReseau(final String reseau) {
        this.reseau = reseau;
    }

    /**
     * Accesseur sur l'attribut {@link #emetteur}.
     *
     * @return String emetteur
     */
    public String getEmetteur() {
        return emetteur;
    }

    /**
     * Mutateur sur l'attribut {@link #emetteur}.
     *
     * @param emetteur
     *            la nouvelle valeur de l'attribut emetteur
     */
    public LiasseGenerationUtils setEmetteur(final String emetteur) {
        this.emetteur = emetteur;
        return this;
    }

    /**
     * Accesseur sur l'attribut {@link #type}.
     *
     * @return String type
     */
    public String getType() {
        return type;
    }

    /**
     * Mutateur sur l'attribut {@link #type}.
     *
     * @param type
     *            la nouvelle valeur de l'attribut type
     */
    public LiasseGenerationUtils setType(final String type) {
        this.type = type;
        return this;
    }

    /**
     * Accesseur sur l'attribut {@link #version}.
     *
     * @return String version
     */
    public String getVersion() {
        return version;
    }

    /**
     * Mutateur sur l'attribut {@link #version}.
     *
     * @param version
     *            la nouvelle valeur de l'attribut version
     */
    public LiasseGenerationUtils setVersion(final String version) {
        this.version = version;
        return this;
    }

    /**
     * Accesseur sur l'attribut {@link #extensionZip}.
     *
     * @return String extensionZip
     */
    public String getExtensionZip() {
        return extensionZip;
    }

    /**
     * Mutateur sur l'attribut {@link #extensionZip}.
     *
     * @param extensionZip
     *            la nouvelle valeur de l'attribut extensionZip
     */
    public LiasseGenerationUtils setExtensionZip(final String extensionZip) {
        this.extensionZip = extensionZip;
        return this;
    }

    /**
     * Accesseur sur l'attribut {@link #extensionFini}.
     *
     * @return String extensionFini
     */
    public String getExtensionFini() {
        return extensionFini;
    }

    /**
     * Mutateur sur l'attribut {@link #extensionFini}.
     *
     * @param extensionFini
     *            la nouvelle valeur de l'attribut extensionFini
     */
    public LiasseGenerationUtils setExtensionFini(final String extensionFini) {
        this.extensionFini = extensionFini;
        return this;
    }

    /**
     * Generate zip file name based on liasse number.
     * 
     * @param numeroLiasse
     *            Liasse number
     * @return
     */
    public Map<String, String> generate(final String numeroLiasse) {
        final Map<String, String> outputFiles = new HashMap<>();
        final LocalDateTime now = LocalDateTime.now();
        final String year = String.valueOf(now.getYear());
        final String month = now.toString("MM");
        final String day = now.toString("dd");
        final String hour = now.toString("HH");
        final String minutes = now.toString("mm");
        final String seconds = now.toString("ss");

        final String dmtduFileName = StringUtils.join("C", this.getEmetteur(), "L", numeroLiasse.substring(6, 12), "D", year, month, day, "H", hour, minutes, seconds, "T", this.getType(), "P",
                this.getVersion());

        outputFiles.put("zip", StringUtils.join(dmtduFileName, this.getExtensionZip()));
        outputFiles.put("fini", StringUtils.join(dmtduFileName, this.getExtensionFini()));
        return outputFiles;
    }
}
