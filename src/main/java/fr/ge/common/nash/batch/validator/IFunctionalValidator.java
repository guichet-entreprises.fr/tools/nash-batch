/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.batch.validator;

import java.util.Properties;
import java.util.regex.Pattern;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;

import fr.ge.common.nash.batch.bean.AuthorityNaming;
import fr.ge.common.nash.batch.bean.Errors;
import fr.ge.common.nash.batch.utils.ScanFixUtils;
import fr.ge.common.nash.batch.utils.XMLRegentQuery;
import fr.ge.common.utils.bean.FileEntry;

/**
 * Functional validator interface.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public interface IFunctionalValidator {
    static final Logger LOGGER = LoggerFactory.getLogger(IFunctionalValidator.class);

    static final Pattern PDF_EXTENSION = Pattern.compile("(.*)\\.pdf$");
    static final Pattern JPG_JPEG_PNG_EXTENSION = Pattern.compile("(.*)\\.(jpg|jpeg|png)$");
    static final Pattern REGEX_XMLTC = Pattern.compile("^.{33}TXMLTC.*\\.xml$");
    static final Pattern REGEX_REGENT = Pattern.compile("^.{33}(TPIJTE|TFORMA).*\\.xml$");
    static final Pattern REGEX_OTHER = Pattern.compile("^.{33}(TPIJTE|TFORMA).*\\.(pdf|jpg|jpeg|png)$");
    static final Pattern REGEX_EXTENSION_UPPERCASE = Pattern.compile("^[A-Z]*$");
    static final Pattern REGEX_RECORD_FORMS_2 = Pattern.compile("[0-9]{4}\\-[0-9]{2}\\-[A-Z]{3}\\-[A-Z]{3}\\-[0-9]{2}");

    static final String TAG_DOSSIER_UNIQUE = "dossierUnique";
    static final String TAG_PIECE_JOINTE = "pieceJointe";
    static final String TAG_DESTINATAIRE = "destinataire";
    static final String TAG_EMETTEUR = "emetteur";

    static final String DESTINATAIRE_VALUE = "10000001";
    static final String EMETTEUR_VALUE = "10001001";

    static final String SEARCH = "search";
    static final String REPLACE = "replace";

    static final String TPIJTE = "TPIJTE";
    static final String TFORMA = "TFORMA";

    static final String PGUEN = "PGUEN";
    static final String PMGUN = "PMGUN";

    static final String RESEAU_GREFFE = "G";
    static final String RESEAU_CCI = "C";
    static final String RESEAU_CMA = "M";
    static final String RESEAU_CA = "X";
    static final String RESEAU_URSSAF = "U";

    static final String ADRESSE_CORRESPONDANT = "//GUEN_DMTDU/dossierUnique/identificationDossierUnique/correspondant/adresseCorrespondant";
    static final String ADRESSE_CORRESPONDANT_TYPE_DE_VOIE = ADRESSE_CORRESPONDANT + "/typeDeVoie";
    static final String ADRESSE_CORRESPONDANT_LIBELLE_DE_VOIE = ADRESSE_CORRESPONDANT + "/libelleDeVoie";

    static final String ADRESSE_PAYEUR = "//GUEN_DMTDU/dossierUnique/dossierCfe/destinataireDossierCfe[codeEdiDestinataire='%s']/paiementDossier/adressePayeur";
    static final String ADRESSE_PAYEUR_TYPE_DE_VOIE = ADRESSE_PAYEUR + "/typeDeVoie";
    static final String ADRESSE_PAYEUR_LIBELLE_DE_VOIE = ADRESSE_PAYEUR + "/libelleDeVoie";

    /**
     * Validate.
     *
     * @param messageFormatter
     *            message formatter
     * @param resourceAsByte
     *            the resource as byte
     * @return errors
     */
    Errors validate(final Properties appProperties, final byte[] resourceAsBytes);

    byte[] fix(final Properties appProperties, final byte[] resourceAsBytes);

    default boolean validateConditions(final byte[] resourceAsBytes) {
        final FileEntry xmlRegentEntry = ScanFixUtils.getXMLRegentEntry(resourceAsBytes);
        if (null == xmlRegentEntry) {
            LOGGER.warn("XML Regent file is missing");
            return false;
        }

        final String codeEdi = XMLRegentQuery.extractDestinataire(xmlRegentEntry).getTextContent();
        if (!codeEdi.startsWith(AuthorityNaming.GREFFE.getReseau())) {
            return false;
        }

        return true;
    }

    default JsonNode getCart(final Properties appProperties, final String uid) {
        final Response response = ClientBuilder.newClient() //
                .register(com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider.class) //
                .target(appProperties.getProperty("payment.ge.url.ws.private") + "/api/v1/cart/" + uid) //
                .request() //
                .get();

        if (null != response && response.getStatus() != Status.OK.getStatusCode()) {
            LOGGER.error("Cannot get proxy cart for the record {}", uid);
            return null;
        }

        return response.readEntity(JsonNode.class);
    }
}
