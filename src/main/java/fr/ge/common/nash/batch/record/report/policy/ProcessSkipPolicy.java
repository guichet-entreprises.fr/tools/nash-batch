package fr.ge.common.nash.batch.record.report.policy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.step.skip.SkipLimitExceededException;
import org.springframework.batch.core.step.skip.SkipPolicy;

/**
 * @author Christian Cougourdan
 */
public class ProcessSkipPolicy implements SkipPolicy {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProcessSkipPolicy.class);

    @Override
    public boolean shouldSkip(final Throwable t, final int skipCount) throws SkipLimitExceededException {
        LOGGER.warn("Skip item #{} : {}", skipCount, t.getMessage());
        return true;
    }

}
