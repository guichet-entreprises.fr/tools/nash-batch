package fr.ge.common.nash.batch.record.report.processor;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.io.IOUtils;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;
import org.slf4j.LoggerFactory;

import fr.ge.common.nash.batch.utils.vfs.VfsResource;

public abstract class AbstractMultipleReportProcessor<T> extends AbstractReportProcessor<T> {

    public AbstractMultipleReportProcessor(final Class<T> expectedDocumentClass) {
        super(expectedDocumentClass);
    }

    @Override
    public VfsResource process(final VfsResource item) throws Exception {
        final Collection<VfsResource> subItems = this.unzip(item);

        for (final VfsResource subItem : subItems) {
            this.processDocument(this.load(subItem));
        }

        return item;
    }

    private Collection<VfsResource> unzip(final VfsResource zip) throws FileSystemException {
        final Collection<VfsResource> items = new ArrayList<>();

        final FileObject basePath = zip.getParent().resolveFile(zip.getBasename().replaceAll("[.][^.]+$", ""));
        basePath.deleteAll();
        basePath.createFolder();
        zip.attach(basePath);

        try (ZipInputStream zin = new ZipInputStream(zip.getInputStream())) {
            ZipEntry entry;
            while (null != (entry = zin.getNextEntry())) {
                try (FileObject dst = basePath.resolveFile(entry.getName()); OutputStream out = dst.getContent().getOutputStream()) {
                    dst.createFile();
                    IOUtils.copy(zin, out);
                    items.add(new VfsResource(dst));
                }
            }
        } catch (final IOException ex) {
            LoggerFactory.getLogger(this.getClass()).warn("Reading {} input stream : {}", zip.getBasename(), ex.getMessage());
        }

        return items;
    }

}
