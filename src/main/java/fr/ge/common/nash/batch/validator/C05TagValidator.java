/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.batch.validator;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.jknack.handlebars.internal.lang3.ArrayUtils;

import fr.ge.common.nash.batch.bean.AuthorityNaming;
import fr.ge.common.nash.batch.bean.Error;
import fr.ge.common.nash.batch.bean.Error.Level;
import fr.ge.common.nash.batch.bean.Errors;
import fr.ge.common.nash.batch.utils.ScanFixUtils;
import fr.ge.common.nash.batch.utils.XMLRegentQuery;
import fr.ge.common.nash.batch.utils.XMLTCQuery;
import fr.ge.common.nash.batch.utils.XPathBuilder;
import fr.ge.common.utils.ZipUtil;
import fr.ge.common.utils.bean.FileEntry;
import fr.ge.common.utils.exception.TechnicalException;

/**
 * Remove the C05 tag value if necessary.
 *
 * @author aolubi
 */
public class C05TagValidator implements IFunctionalValidator {

    private static final Logger LOGGER = LoggerFactory.getLogger(C05TagValidator.class);

    private static final ObjectMapper mapper = new ObjectMapper();

    public enum ModelFormalityEnum {

        P0_PL_ME("Formalités SCN/ENT/Création/Déclaration de début d'activité libérale/Micro-entrepreneur", "X"), //

        P0_PL("Formalités SCN/ENT/Création/Déclaration de début d'activité libérale/Entreprise individuelle", "L"), //
        P2P4_MODIF("Formalités SCN/ENT/Modification/Déclaration de modification d'une entreprise libérale/Micro-entrepreneur", "L"), //
        P2PL("Formalités SCN/ENT/Modification/Déclaration de modification d'une entreprise libérale/Entreprise individuelle", "L"), //
        P2P4_RAD("Formalités SCN/ENT/Cessation/Déclaration de cessation d'activité d'une entreprise libérale/Micro-entrepreneur", "L"), //
        P4PL("Formalités SCN/ENT/Cessation/Déclaration de cessation d'activité libérale hors micro-entrepreneur", "L"), //

        AC0_PP("Formalités SCN/ENT/Création/Déclaration de début d'activité d'agent commercial", "R"), //
        AC2("Formalités SCN/ENT/Modification/Déclaration de modification d'un agent commercial", "R"), //
        AC4("Formalités SCN/ENT/Cessation/Déclaration de cessation d'activité d'un agent commercial", "R"), //
        ;

        private String referenceId;
        private String c05;

        ModelFormalityEnum(final String referenceId, final String c05) {
            this.referenceId = referenceId;
            this.c05 = c05;
        }

        /**
         * Accesseur sur l'attribut {@link #referenceId}.
         *
         * @return String referenceId
         */
        public String getReferenceId() {
            return referenceId;
        }

        /**
         * Accesseur sur l'attribut {@link #c05}.
         *
         * @return String c05
         */
        public String getC05() {
            return c05;
        }

        public static String getC05(final String referenceId) {
            for (final ModelFormalityEnum model : ModelFormalityEnum.values()) {
                if (model.getReferenceId().equals(referenceId)) {
                    return model.getC05();
                }
            }
            return "C";
        }
    }

    @Override
    public boolean validateConditions(final byte[] resourceAsBytes) {
        final FileEntry xmlRegentEntry = ScanFixUtils.getXMLRegentEntry(resourceAsBytes);
        if (null == xmlRegentEntry) {
            LOGGER.warn("XML Regent file is missing");
            return false;
        }

        final String codeEdi = XMLRegentQuery.extractDestinataire(xmlRegentEntry).getTextContent();
        return codeEdi.startsWith(AuthorityNaming.GREFFE.getReseau());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Errors validate(final Properties appProperties, final byte[] resourceAsBytes) {
        final Errors errors = new Errors();
        final FileEntry xmltcEntry = ScanFixUtils.getXMLTCEntry(resourceAsBytes);
        if (null == xmltcEntry) {
            LOGGER.info("Missing XMLTC file");
            errors.add(new Error(Level.ERROR, "Missing XMLTC file"));
            return errors;
        }

        // -->Exclude FORMS1 records
        final Node numeroDossier = XMLTCQuery.extractNumeroDossierUnique(xmltcEntry);
        if (null == numeroDossier || StringUtils.isEmpty(numeroDossier.getTextContent())) {
            return errors;
        }

        if (!REGEX_RECORD_FORMS_2.matcher(numeroDossier.getTextContent()).matches()) {
            return errors;
        }
        // <--

        final FileEntry xmlRegentEntry = ScanFixUtils.getXMLRegentEntry(resourceAsBytes);
        if (null == xmlRegentEntry) {
            LOGGER.warn("XML Regent file is missing");
            errors.add(new Error(Level.ERROR, "XML Regent file is missing"));
            return errors;
        }

        final Document document = XPathBuilder.buildDocument(xmlRegentEntry.asBytes());
        final Node c05 = XPathBuilder.queryNode(document.getDocumentElement(), "//REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C05");
        if (null == c05 || StringUtils.isEmpty(c05.getTextContent())) {
            errors.add(new Error(Level.ERROR, "Cannot get C05 tag into XML Regent"));
            return errors;
        }

        final String expected = this.expectedC05Tag(appProperties, numeroDossier.getTextContent());
        if (StringUtils.isNotEmpty(expected) && !expected.equals(c05.getTextContent())) {
            errors.add(new Error(Level.ERROR, "Invalid C05 tag into XML Regent"));
        }

        return errors;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public byte[] fix(final Properties appProperties, final byte[] resourceAsBytes) {
        if (!this.validate(appProperties, resourceAsBytes).hasErrors()) {
            return resourceAsBytes;
        }

        try {
            final FileEntry xmltcEntry = ScanFixUtils.getXMLTCEntry(resourceAsBytes);
            if (null == xmltcEntry) {
                LOGGER.warn("Missing XMLTC file");
                return resourceAsBytes;
            }

            final FileEntry xmlRegentEntry = ScanFixUtils.getXMLRegentEntry(resourceAsBytes);
            if (null == xmlRegentEntry) {
                LOGGER.warn("XML Regent file is missing");
                return resourceAsBytes;
            }

            final Node numeroDossier = XMLTCQuery.extractNumeroDossierUnique(xmltcEntry);

            if (null == numeroDossier || StringUtils.isEmpty(numeroDossier.getTextContent())) {
                return resourceAsBytes;
            }

            if (!REGEX_RECORD_FORMS_2.matcher(numeroDossier.getTextContent()).matches()) {
                return resourceAsBytes;
            }

            final Document document = XPathBuilder.buildDocument(xmlRegentEntry.asBytes());
            final Node c05 = XPathBuilder.queryNode(document.getDocumentElement(), "//REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C05");
            final String expected = this.expectedC05Tag(appProperties, numeroDossier.getTextContent());
            final String oldValue = c05.getTextContent();
            c05.setTextContent((!expected.equals(oldValue)) ? expected : oldValue);
            LOGGER.info(String.format("Replace tag '%s' value from %s to %s", "//REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C05", oldValue, c05.getTextContent()));

            final Map<String, byte[]> filesToInsert = new HashMap<>();
            filesToInsert.put(xmlRegentEntry.name(), XPathBuilder.asBytes(document));

            return ZipUtil.entries(filesToInsert, resourceAsBytes);
        } catch (TechnicalException e) {
            throw new TechnicalException("Cannot fix C05 tag", e);
        }

    }

    private String expectedC05Tag(final Properties appProperties, final String recordUid) {
        String headerRoles = null;
        try {
            final Map<String, List<String>> roles = new HashMap<>();
            roles.put("GE", Arrays.asList("*"));
            headerRoles = mapper.writeValueAsString(roles);
        } catch (JsonProcessingException e) {
            LOGGER.warn("Cannot parse header X-Roles to search record {}", recordUid);
            throw new TechnicalException(String.format("Cannot parse header X-Roles to search record %s", recordUid));
        }

        final Response response = ClientBuilder.newClient() //
                .register(com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider.class) //
                .target(String.format(appProperties.getProperty("ws.nash.url") + "/v1/Record/code/%s/file/description.xml", recordUid)) //
                .request() //
                .header("X-Roles", headerRoles) //
                .get();
        if (null != response && response.getStatus() != Status.OK.getStatusCode()) {
            LOGGER.error("Cannot download NASH record {}", recordUid);
            throw new TechnicalException(String.format("Cannot download NASH record %s", recordUid));
        }

        final byte[] descriptionAsBytes = response.readEntity(byte[].class);
        if (ArrayUtils.isEmpty(descriptionAsBytes)) {
            return null;
        }

        Node referenceId = null;
        try {
            final Document document = XPathBuilder.buildDocument(descriptionAsBytes);
            final String expression = "//description/reference-id";
            referenceId = XPathBuilder.queryNode(document.getDocumentElement(), expression);
            if (null == referenceId || StringUtils.isEmpty(referenceId.getTextContent())) {
                LOGGER.error("No referenceId into description file content");
                return null;
            }
        } catch (TechnicalException e) {
            LOGGER.warn("Cannot fix the 'C05' tag into XML Regent content", e);
        }

        return ModelFormalityEnum.getC05(referenceId.getTextContent());
    }
}
