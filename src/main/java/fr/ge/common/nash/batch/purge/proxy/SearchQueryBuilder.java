/**
 * 
 */
package fr.ge.common.nash.batch.purge.proxy;

import fr.ge.common.nash.batch.common.AbstractSearchQueryBuilder;

/**
 * Proxy search query builder.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class SearchQueryBuilder extends AbstractSearchQueryBuilder {

    /**
     * {@inheritDoc}
     */
    @Override
    public void build() {

    }
}
