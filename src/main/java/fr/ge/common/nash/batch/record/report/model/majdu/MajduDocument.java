package fr.ge.common.nash.batch.record.report.model.majdu;

import java.util.Collection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import fr.ge.common.nash.batch.record.report.model.common.ReportDocument;

@XmlRootElement(name = "GUEN_MAJDU")
@XmlAccessorType(XmlAccessType.NONE)
public class MajduDocument extends ReportDocument {

    @XmlElement(name = "dossierUnique")
    private Collection<DossierUnique> dossiersUnique;

    /**
     * @return the dossiersUnique
     */
    public Collection<DossierUnique> getDossiersUnique() {
        return this.dossiersUnique;
    }

}
