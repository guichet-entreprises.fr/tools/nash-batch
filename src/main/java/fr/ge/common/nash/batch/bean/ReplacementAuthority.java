/**
 * 
 */
package fr.ge.common.nash.batch.bean;

/**
 * Authority replacement object.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class ReplacementAuthority {

    private String search;
    private String replace;
    private TypeFile typeFile;

    /**
     * 
     * Constructeur de la classe.
     *
     */
    public ReplacementAuthority() {

    }

    /**
     * 
     * Constructeur de la classe.
     *
     * @param search
     * @param replace
     */
    public ReplacementAuthority(final TypeFile typeFile, final String search, final String replace) {
        this.search = search;
        this.replace = replace;
        this.setTypeFile(typeFile);
    }

    /**
     * Accesseur sur l'attribut {@link #search}.
     *
     * @return String search
     */
    public String getSearch() {
        return search;
    }

    /**
     * Mutateur sur l'attribut {@link #search}.
     *
     * @param search
     *            la nouvelle valeur de l'attribut search
     */
    public ReplacementAuthority setSearch(final String search) {
        this.search = search;
        return this;
    }

    /**
     * Accesseur sur l'attribut {@link #replace}.
     *
     * @return String replace
     */
    public String getReplace() {
        return replace;
    }

    /**
     * Mutateur sur l'attribut {@link #replace}.
     *
     * @param replace
     *            la nouvelle valeur de l'attribut replace
     */
    public ReplacementAuthority setReplace(final String replace) {
        this.replace = replace;
        return this;
    }

    /**
     * Accesseur sur l'attribut {@link #typeFile}.
     *
     * @return TypeFile typeFile
     */
    public TypeFile getTypeFile() {
        return typeFile;
    }

    /**
     * Mutateur sur l'attribut {@link #typeFile}.
     *
     * @param typeFile
     *            la nouvelle valeur de l'attribut typeFile
     */
    public ReplacementAuthority setTypeFile(final TypeFile typeFile) {
        this.typeFile = typeFile;
        return this;
    }

    /**
     * Find and replace string.
     * 
     * @param inputString
     * @return
     */
    public String findAndReplace(final TypeFile typeFile, final String inputString) {
        if (null == typeFile || !this.getTypeFile().equals(typeFile)) {
            return inputString;
        }
        return inputString.replace(this.getSearch(), this.getReplace());
    }
}
