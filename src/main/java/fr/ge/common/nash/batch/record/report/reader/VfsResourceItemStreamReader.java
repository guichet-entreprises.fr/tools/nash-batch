package fr.ge.common.nash.batch.record.report.reader;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.regex.Pattern;

import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.FileSystemOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemStreamException;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import fr.ge.common.nash.batch.utils.vfs.InDepthFileSelector;
import fr.ge.common.nash.batch.utils.vfs.VfsResource;
import fr.ge.common.nash.batch.utils.vfs.VfsResourceItemStream;

/**
 * @author Christian Cougourdan
 */
public abstract class VfsResourceItemStreamReader extends VfsResourceItemStream implements ItemReader<VfsResource> {

    private static final Logger LOGGER = LoggerFactory.getLogger(VfsResourceItemStreamReader.class);

    private Iterator<FileObject> resources;

    private String source;

    private Pattern filterPattern;

    private FileObject baseObject;

    @Override
    public void open(final ExecutionContext executionContext) throws ItemStreamException {
        super.open(executionContext);

        try {
            final URI resourceUri = this.buildUri('/' + this.source);
            this.baseObject = this.fs.resolveFile(resourceUri.toString(), this.getOptions());
        } catch (FileSystemException | URISyntaxException ex) {
            throw new ItemStreamException("Looking for VFS base object", ex);
        }
    }

    @Override
    public final VfsResource read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
        if (null == this.resources) {
            this.resources = this.find().iterator();
        }

        while (this.resources.hasNext()) {
            final FileObject resource = this.resources.next();
            if (null == this.filterPattern) {
                return new VfsResource(resource);
            } else {
                final String relativeChecksumResourcePath = this.baseObject.getName().getRelativeName(resource.getName());
                final String targetResourceLookup = this.filterPattern.matcher(relativeChecksumResourcePath).replaceFirst("");
                final FileObject targetResource = this.find(targetResourceLookup);
                if (null == targetResource) {
                    LOGGER.info("Found orphan trigger file : {}", resource.getName().getBaseName());
                } else {
                    return new VfsResource(targetResource, resource);
                }
            }
        }

        return null;
    }

    protected final Collection<FileObject> find() throws FileSystemException, URISyntaxException {
        final FileObject[] items = this.baseObject.findFiles(new InDepthFileSelector(0, this.filterPattern));

        return null == items ? Collections.emptyList() : Arrays.asList(items);
    }

    protected final FileObject find(final String resourcePath) throws FileSystemException, URISyntaxException {
        final FileObject resourceObject = this.baseObject.resolveFile(resourcePath);
        if (null == resourceObject || !resourceObject.exists()) {
            return null;
        } else {
            return resourceObject;
        }
    }

    public void setSource(final String source) {
        this.source = source;
    }

    public void setChecksumFileExtension(final String extension) {
        if (null == extension) {
            this.filterPattern = null;
        } else {
            this.filterPattern = Pattern.compile('.' + extension.replaceAll("[.]", "[.]") + '$');
        }
    }

    protected abstract URI buildUri(String path) throws URISyntaxException;

    protected FileSystemOptions getOptions() throws FileSystemException {
        return new FileSystemOptions();
    }

}
