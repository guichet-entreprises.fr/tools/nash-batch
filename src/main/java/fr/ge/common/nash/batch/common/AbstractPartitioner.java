/**
 * 
 */
package fr.ge.common.nash.batch.common;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.item.ExecutionContext;

import fr.ge.common.nash.ws.v1.service.IRecordService;
import fr.ge.common.storage.ws.rest.IRecordRestService;
import fr.ge.common.utils.bean.search.SearchResult;

/**
 * Abstract partitioner.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public abstract class AbstractPartitioner<T extends SearchResult> implements Partitioner {

    protected static final Logger LOGGER = LoggerFactory.getLogger(AbstractPartitioner.class);

    protected Integer maxResults;

    protected IRecordService recordServiceClient;

    protected IRecordRestService storageRecordRestService;

    protected AbstractSearchQueryBuilder searchQueryBuilder;

    public abstract T search();

    /**
     * Mutateur sur l'attribut {@link #maxResults}.
     *
     * @param maxResults
     *            la nouvelle valeur de l'attribut maxResults
     */
    public void setMaxResults(final Integer maxResults) {
        this.maxResults = maxResults;
    }

    /**
     * Mutateur sur l'attribut {@link #recordServiceClient}.
     *
     * @param recordServiceClient
     *            la nouvelle valeur de l'attribut recordServiceClient
     */
    public void setRecordServiceClient(final IRecordService recordServiceClient) {
        this.recordServiceClient = recordServiceClient;
    }

    /**
     * Mutateur sur l'attribut {@link #storageRecordRestService}.
     *
     * @param storageRecordRestService
     *            la nouvelle valeur de l'attribut storageRecordRestService
     */
    public void setStorageRecordRestService(final IRecordRestService storageRecordRestService) {
        this.storageRecordRestService = storageRecordRestService;
    }

    /**
     * Mutateur sur l'attribut {@link #searchQueryBuilder}.
     *
     * @param searchQueryBuilder
     *            la nouvelle valeur de l'attribut searchQueryBuilder
     */
    public void setSearchQueryBuilder(final AbstractSearchQueryBuilder searchQueryBuilder) {
        this.searchQueryBuilder = searchQueryBuilder;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, ExecutionContext> partition(final int gridSize) {
        final int totalRecords = Optional.ofNullable(this.search()) //
                .filter(r -> null != r) //
                .map(SearchResult::getTotalResults) //
                .map(Long::intValue) //
                .orElse(0);

        int partitions = (totalRecords / this.maxResults) + 1;
        int startIndex = 0;
        final Map<String, ExecutionContext> executionContextMap = new HashMap<String, ExecutionContext>();
        for (int i = 1; i <= partitions; i++) {
            final ExecutionContext executionContext = new ExecutionContext();
            executionContext.putString("name", "Thread_" + i);
            executionContext.putLong("startIndex", startIndex);
            executionContext.putLong("maxResults", this.maxResults);
            startIndex += this.maxResults;
            executionContextMap.put(String.valueOf("partition" + i), executionContext);
        }
        LOGGER.info("Number of partitions created : {}", executionContextMap.size());
        return executionContextMap;
    }
}
