package fr.ge.common.nash.batch.record.report;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Predicate;
import java.util.regex.Pattern;

import org.springframework.classify.Classifier;

import fr.ge.common.nash.batch.utils.vfs.VfsResource;

public class ResourcePatternClassifier<T> implements Classifier<VfsResource, T> {

    private Map<Predicate<String>, T> classifiers;

    @Override
    public T classify(final VfsResource classifiable) {
        for (final Entry<Predicate<String>, T> entry : this.classifiers.entrySet()) {
            if (entry.getKey().test(classifiable.getBasename())) {
                return entry.getValue();
            }
        }

        return null;
    }

    public void setProcessors(final Map<String, T> processors) {
        if (null == processors) {
            this.classifiers = Collections.emptyMap();
        } else {
            final Map<Predicate<String>, T> m = new HashMap<>();
            for (final Entry<String, T> entry : processors.entrySet()) {
                m.put(Pattern.compile("^" + entry.getKey() + "$", Pattern.CASE_INSENSITIVE).asPredicate(), entry.getValue());
            }
            this.classifiers = m;
        }
    }

}
