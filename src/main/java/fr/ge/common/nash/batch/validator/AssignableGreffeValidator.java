/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.batch.validator;

import java.util.ArrayList;
import java.util.Optional;
import java.util.Properties;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import fr.ge.common.nash.batch.bean.AuthorityNaming;
import fr.ge.common.nash.batch.bean.Error;
import fr.ge.common.nash.batch.bean.Error.Level;
import fr.ge.common.nash.batch.bean.Errors;
import fr.ge.common.nash.batch.utils.ScanFixUtils;
import fr.ge.common.nash.batch.utils.XMLRegentQuery;
import fr.ge.common.nash.batch.utils.XMLTCQuery;
import fr.ge.common.nash.batch.utils.XPathBuilder;
import fr.ge.common.utils.ZipUtil;
import fr.ge.common.utils.bean.FileEntry;
import fr.ge.common.utils.exception.TechnicalException;

/**
 * Check if the input DMTDU must be sent to GREFFE authority.
 *
 * @author aolubi
 */
@Deprecated
public class AssignableGreffeValidator implements IFunctionalValidator {

    private static final Logger LOGGER = LoggerFactory.getLogger(AssignableGreffeValidator.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public Errors validate(final Properties appProperties, final byte[] resourceAsBytes) {
        final Errors errors = new Errors();
        final FileEntry xmltcEntry = ScanFixUtils.getXMLTCEntry(resourceAsBytes);
        if (null == xmltcEntry) {
            LOGGER.info("Missing XMLTC file");
            errors.add(new Error(Level.ERROR, "Missing XMLTC file"));
            return errors;
        }

        final FileEntry xmlRegentEntry = ScanFixUtils.getXMLRegentEntry(resourceAsBytes);
        if (null == xmlRegentEntry) {
            LOGGER.warn("XML Regent file is missing");
            errors.add(new Error(Level.ERROR, "XML Regent file is missing"));
            return errors;
        }

        final String codeEdi = XMLRegentQuery.extractDestinataire(xmlRegentEntry).getTextContent();
        if (!codeEdi.startsWith(AuthorityNaming.GREFFE.getReseau())) {
            return errors;
        }

        final Node numeroDossier = XMLTCQuery.extractNumeroDossierUnique(xmltcEntry);

        if (null == numeroDossier || StringUtils.isEmpty(numeroDossier.getTextContent())) {
            errors.add(new Error(Level.ERROR, String.format("Invalid tag '%s' value", "numeroDossier")));
            return errors;
        }

        if (!REGEX_RECORD_FORMS_2.matcher(numeroDossier.getTextContent()).matches()) {
            return errors;
        }

        final Response response = ClientBuilder.newClient() //
                .register(com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider.class) //
                .target(appProperties.getProperty("ws.nash.url") + "/v1/Record/code/" + numeroDossier.getTextContent() + "/file") //
                .request() //
                .get();
        if (null != response && response.getStatus() != Status.OK.getStatusCode()) {
            LOGGER.error("Cannot download NASH record {}", numeroDossier.getTextContent());
            errors.add(new Error(Level.ERROR, String.format("Cannot download NASH record record %s", numeroDossier.getTextContent())));
            return errors;
        }

        final byte[] nashResouceAsBytes = response.readEntity(byte[].class);

        final FileEntry specData = Optional.of(nashResouceAsBytes) //
                .filter(r -> null != r) //
                .map(r -> ZipUtil.entries(r)) //
                .orElse(new ArrayList<String>()) //
                .stream() //
                .filter(e -> e.equals("1-data/data.xml")) //
                .findFirst() //
                .map(e -> ZipUtil.entry(e, nashResouceAsBytes)) //
                .orElse(null);

        if (null == specData) {
            return errors;
        }

        try {
            final Document document = XPathBuilder.buildDocument(specData.asBytes());
            final String expression = "//form/group[@id='cadre1RappelIdentificationGroup']/group[@id='cadre1RappelIdentification']/data[@id='immatriculation']/value/list/text[@id='immatRCS']";
            final Node immatRCS = XPathBuilder.queryNode(document.getDocumentElement(), expression);
            if (null == immatRCS || StringUtils.isEmpty(immatRCS.getTextContent())) {
                LOGGER.error("No immatriculation to GREFFE into data content");
                errors.add(new Error(Level.ERROR, "No immatriculation to GREFFE into data content"));
            }
        } catch (TechnicalException e) {
            LOGGER.warn("Cannot fix the 'immatRM' into data content", e);
        }
        return errors;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public byte[] fix(final Properties appProperties, final byte[] resourceAsBytes) {
        return resourceAsBytes;
    }
}
