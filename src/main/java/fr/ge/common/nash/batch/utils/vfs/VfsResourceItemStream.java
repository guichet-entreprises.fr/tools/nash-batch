package fr.ge.common.nash.batch.utils.vfs;

import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.FileSystemManager;
import org.apache.commons.vfs2.impl.StandardFileSystemManager;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemStream;
import org.springframework.batch.item.ItemStreamException;

/**
 * @author Christian Cougourdan
 */
public abstract class VfsResourceItemStream implements ItemStream {

    protected FileSystemManager fs;

    @Override
    public void open(final ExecutionContext executionContext) throws ItemStreamException {
        try {
            final StandardFileSystemManager stdFileSystem = new StandardFileSystemManager();
            stdFileSystem.init();
            this.fs = stdFileSystem;
        } catch (final FileSystemException ex) {
            throw new ItemStreamException("Virtual Filesystem initialization", ex);
        }
    }

    @Override
    public void update(final ExecutionContext executionContext) throws ItemStreamException {
    }

    @Override
    public void close() throws ItemStreamException {
        if (null != this.fs) {
            this.fs.close();
        }
    }

}
