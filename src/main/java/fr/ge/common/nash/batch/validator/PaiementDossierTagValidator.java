/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.batch.validator;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import fr.ge.common.nash.batch.bean.AuthorityNaming;
import fr.ge.common.nash.batch.bean.Error;
import fr.ge.common.nash.batch.bean.Error.Level;
import fr.ge.common.nash.batch.bean.Errors;
import fr.ge.common.nash.batch.utils.ScanFixUtils;
import fr.ge.common.nash.batch.utils.XMLRegentQuery;
import fr.ge.common.nash.batch.utils.XMLTCQuery;
import fr.ge.common.nash.batch.utils.XPathBuilder;
import fr.ge.common.utils.ZipUtil;
import fr.ge.common.utils.bean.FileEntry;
import fr.ge.common.utils.exception.TechnicalException;

/**
 * Add the payment infos with default values into XMLTC if necesseray.
 *
 * @author aolubi
 */
public class PaiementDossierTagValidator implements IFunctionalValidator {

    private static final Logger LOGGER = LoggerFactory.getLogger(PaiementDossierTagValidator.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public Errors validate(final Properties appProperties, final byte[] resourceAsBytes) {
        final Errors errors = new Errors();
        final FileEntry xmltcEntry = ScanFixUtils.getXMLTCEntry(resourceAsBytes);
        if (null == xmltcEntry) {
            LOGGER.info("Missing XMLTC file");
            errors.add(new Error(Level.ERROR, "Missing XMLTC file"));
            return errors;
        }

        final FileEntry xmlRegentEntry = ScanFixUtils.getXMLRegentEntry(resourceAsBytes);
        if (null == xmlRegentEntry) {
            LOGGER.warn("XML Regent file is missing");
            errors.add(new Error(Level.ERROR, "XML Regent file is missing"));
            return errors;
        }

        final String codeEdi = XMLRegentQuery.extractDestinataire(xmlRegentEntry).getTextContent();
        if (!codeEdi.startsWith(AuthorityNaming.GREFFE.getReseau())) {
            return errors;
        }

        try {
            final Node destinataireDossierCfe = XMLTCQuery.extractDestinataireDossierCfeAsNode(xmltcEntry, codeEdi);
            if (null == destinataireDossierCfe) {
                errors.add(new Error(Level.ERROR, String.format("Cannot find destinataireDossierCfe tag for target authority '%s'", codeEdi)));
            }
            if (null == XPathBuilder.queryNode(destinataireDossierCfe, "paiementDossier")) {
                errors.add(new Error(Level.ERROR, String.format("Cannot find paiementDossier tag for target authority '%s'", codeEdi)));
            }
        } catch (TechnicalException e) {
            LOGGER.error("The XMLTC file cannot be read", e);
            errors.add(new Error(Level.ERROR, "The XMLTC file cannot be read"));
        }
        return errors;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public byte[] fix(final Properties appProperties, final byte[] resourceAsBytes) {
        if (!this.validate(appProperties, resourceAsBytes).hasErrors()) {
            return resourceAsBytes;
        }

        final FileEntry xmlRegentEntry = ScanFixUtils.getXMLRegentEntry(resourceAsBytes);
        if (null == xmlRegentEntry) {
            LOGGER.warn("XML Regent file is missing");
            return resourceAsBytes;
        }

        final String codeEdi = XMLRegentQuery.extractDestinataire(xmlRegentEntry).getTextContent();
        final FileEntry xmltcEntry = ScanFixUtils.getXMLTCEntry(resourceAsBytes);
        try {
            final Document document = XPathBuilder.buildDocument(xmltcEntry.asBytes());
            final String expression = "//GUEN_DMTDU/dossierUnique/dossierCfe/destinataireDossierCfe[codeEdiDestinataire='" + codeEdi + "']/indicePieceJointe[last()]";
            final Node indicePieceJointe = XPathBuilder.queryNode(document.getDocumentElement(), expression);
            if (null == indicePieceJointe) {
                LOGGER.warn("Cannot find indicePieceJointe tag for target authority '{}'", codeEdi);
                return resourceAsBytes;
            }

            if (null != XPathBuilder.queryNode(indicePieceJointe.getParentNode(), "paiementDossier")) {
                LOGGER.warn("Find paiementDossier tag for target authority '%s'", codeEdi);
                return resourceAsBytes;
            }

            final Node paiementDossier = document.createElement("paiementDossier");
            // -->identitePayeur
            final Node identitePayeur = document.createElement("identitePayeur");
            final Node nomPayeur = document.createElement("nomPayeur");
            nomPayeur.setTextContent("unknown");
            identitePayeur.appendChild(nomPayeur);
            paiementDossier.appendChild(identitePayeur);
            // <--

            // -->adressePayeur
            final Node adressePayeur = document.createElement("adressePayeur");
            final Node numeroDeVoie = document.createElement("numeroDeVoie");
            numeroDeVoie.setTextContent("unknown");
            adressePayeur.appendChild(numeroDeVoie);

            final Node libelleDeVoie = document.createElement("libelleDeVoie");
            libelleDeVoie.setTextContent("unknown");
            adressePayeur.appendChild(libelleDeVoie);

            final Node codePostal = document.createElement("codePostal");
            codePostal.setTextContent("00000");
            adressePayeur.appendChild(codePostal);

            final Node bureauDistributeur = document.createElement("bureauDistributeur");
            bureauDistributeur.setTextContent("unknown");
            adressePayeur.appendChild(bureauDistributeur);

            final Node adresseEmail = document.createElement("adresseEmail");
            adresseEmail.setTextContent("no.mail@adresse.fr");
            adressePayeur.appendChild(adresseEmail);

            paiementDossier.appendChild(adressePayeur);
            // <--

            final Node montantFormalite = document.createElement("montantFormalite");
            montantFormalite.setTextContent("00,00");
            paiementDossier.appendChild(montantFormalite);

            final Node referencePaiement = document.createElement("referencePaiement");
            referencePaiement.setTextContent("xxxxxxxxxxxxxx");
            paiementDossier.appendChild(referencePaiement);

            // -->TODO
            final Node dateHeurePaiement = document.createElement("dateHeurePaiement");
            final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
            final LocalDateTime now = LocalDateTime.now();
            dateHeurePaiement.setTextContent(now.format(formatter));
            paiementDossier.appendChild(dateHeurePaiement);

            indicePieceJointe.getParentNode().insertBefore(paiementDossier, indicePieceJointe.getNextSibling());

            final Map<String, byte[]> filesToInsert = new HashMap<>();
            filesToInsert.put(xmltcEntry.name(), XPathBuilder.asBytes(document));

            LOGGER.info(String.format("Fix the '%s' tag for target authority %s value", "paiementDossier", codeEdi));

            return ZipUtil.entries(filesToInsert, resourceAsBytes);

        } catch (TechnicalException e) {
            LOGGER.error("Cannot fix the 'destinataire' tag value", e);
            throw new TechnicalException("Cannot fix the 'destinataire' tag value", e);
        }
    }
}
