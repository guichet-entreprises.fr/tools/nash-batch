package fr.ge.common.nash.batch.utils.monitor;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.ge.common.clicker.ws.v1.service.IEventRestService;

/**
 * @author Christian Cougourdan
 */
@Service
public class ClickerService extends AbstractExternalService {

    private static final DateTimeFormatter FORMAT = DateTimeFormatter.ofPattern("yyyyMMdd") //
            .withLocale(Locale.getDefault()) //
            .withZone(ZoneId.systemDefault());

    @Autowired
    private IEventRestService service;

    public void notify(final String code) {
        this.log( //
                () -> this.service.create(code, 1L, LocalDateTime.now().format(FORMAT), null), //
                "sending event", //
                code //
        );
    }

}
