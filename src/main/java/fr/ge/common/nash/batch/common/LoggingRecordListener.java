/**
 * 
 */
package fr.ge.common.nash.batch.common;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.MDC;
import org.springframework.batch.core.ItemWriteListener;

import fr.ge.common.nash.ws.v1.bean.RecordInfoBean;

/**
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class LoggingRecordListener implements ItemWriteListener<RecordInfoBean> {

    /**
     * {@inheritDoc}
     */
    @Override
    public void beforeWrite(final List<? extends RecordInfoBean> items) {
        items.stream().forEach(item -> {
            if (null != item && StringUtils.isNotEmpty(item.getCode())) {
                MDC.put("record", item.getCode());
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterWrite(final List<? extends RecordInfoBean> items) {
        // -->Do nothing here
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onWriteError(final Exception exception, final List<? extends RecordInfoBean> items) {
        // -->Do nothing here
    }

}
