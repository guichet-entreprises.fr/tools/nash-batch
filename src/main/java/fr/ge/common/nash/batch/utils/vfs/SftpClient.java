package fr.ge.common.nash.batch.utils.vfs;

import java.io.Closeable;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Collection;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.FileSystemOptions;
import org.apache.commons.vfs2.impl.StandardFileSystemManager;
import org.apache.commons.vfs2.provider.sftp.SftpFileSystemConfigBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;

/**
 * @author Christian Cougourdan
 */
public class SftpClient implements Closeable {

    /** folder local temporary. */
    /** cheminRelatifTmp */
    String FOLDER_LOCAL_TMP = "tmp";

    /** nom du répertoire contenant les archives sur le repertoire local. */
    /** cheminRelatifArchive */
    String FOLDER_LOCAL_ARCHIVE = "archive";

    /** folder local errors. */
    /** cheminRelatifErreurs */
    String FOLDER_LOCAL_ERRORS = "erreurs_traitement";

    /** folder sftp where your find dossier treated by batch GENZIP. */
    /** cheminRelatifATraiter */
    String FOLDER_SFTP_RECEIVED = "SES_RECEIVED";

    /** folder sftp where you find dossier treated by batch DMTDU. */
    /** cheminRelatifTraite */
    String FOLDER_SFTP_SEND = "SES_SEND";

    /** folder sftp where you find dossier forms. */
    /** cheminRelatifArchiveStp */
    String FOLDER_SFTP_ARCHIVE = "SES_ARCHIVE";

    private static final Logger LOGGER = LoggerFactory.getLogger(SftpClient.class);

    private String host;

    private String username;

    private String password;

    private StandardFileSystemManager fs;

    public void init() throws Exception {
        if (null == this.fs) {
            this.fs = new StandardFileSystemManager();
            this.fs.init();
        }
    }

    public Collection<Resource> findAll(final String baseDir) throws Exception {
        return this.find(baseDir, null);
    }

    public Collection<Resource> find(final String baseDir, final Pattern filter) throws Exception {
        final URI remoteUri = this.getRemoteUri("/" + baseDir);
        final FileObject base = this.fs.resolveFile(remoteUri.toString(), this.getOptions());
        final FileObject[] children = base.findFiles(new InDepthFileSelector(0, filter));

        return Arrays.stream(children).map(SftpResource::new).collect(Collectors.toList());
    }

    public Resource find(final String resourcePath) throws Exception {
        final URI remoteUri = this.getRemoteUri("/" + resourcePath);
        final FileObject resourceObject = this.fs.resolveFile(remoteUri.toString(), this.getOptions());
        return null == resourceObject || !resourceObject.exists() ? null : new SftpResource(resourceObject);
    }

    public URI getRemoteUri(final String remotePath) throws URISyntaxException {
        return new URI("sftp", this.username + ':' + this.password, this.host, -1, remotePath, null, null);
    }

    private FileSystemOptions getOptions() throws FileSystemException {
        final FileSystemOptions opts = new FileSystemOptions();

        final SftpFileSystemConfigBuilder builder = SftpFileSystemConfigBuilder.getInstance();
        builder.setStrictHostKeyChecking(opts, "no");
        builder.setUserDirIsRoot(opts, false);
        builder.setConnectTimeoutMillis(opts, 3000);

        return opts;
    }

    @Override
    public void close() throws IOException {
        if (null != this.fs) {
            this.fs.close();
        }
    }

    /**
     * @param host
     *            the host to set
     */
    public void setHost(final String host) {
        this.host = host;
    }

    /**
     * @param username
     *            the username to set
     */
    public void setUsername(final String username) {
        this.username = username;
    }

    /**
     * @param password
     *            the password to set
     */
    public void setPassword(final String password) {
        this.password = password;
    }

}
