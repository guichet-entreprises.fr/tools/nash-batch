/**
 * 
 */
package fr.ge.common.nash.batch.bean;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import fr.ge.common.nash.batch.utils.LiasseGenerationUtils;

/**
 * Authority naming enumeration.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public enum AuthorityNaming {

    GREFFE("G", //
            "10001001", //
            "10000001", //
            Pattern.compile("^.{33}TXMLTC.*PGUEN\\.xml$"), //
            Pattern.compile("^.{33}TPIJTE.*PGUEN\\.xml$"), //
            Pattern.compile("^.{33}TPIJTE.*PGUEN\\.(pdf|jpg|jpeg|png)$"), //
            Arrays.asList( //
                    new ReplacementAuthority(TypeFile.REGENT, "PGUEN", "PGUEN"), //
                    new ReplacementAuthority(TypeFile.REGENT, "TPIJTE", "TPIJTE"), //
                    new ReplacementAuthority(TypeFile.XMLTC, "PGUEN", "PGUEN"), //
                    new ReplacementAuthority(TypeFile.OTHER, "PGUEN", "PGUEN") //
            ), //
            new LiasseGenerationUtils().setEmetteur("1000A1001").setType("DMTDU").setVersion("GUEN").setExtensionZip(".zip").setExtensionFini(".zip.fini") //
    ),

    CCI("C", //
            "00161001", //
            "00161002", //
            Pattern.compile("^C0016A1002L.{22}TXMLTC.*PMGUN\\.xml$"), //
            Pattern.compile("^C0016A1002L.{22}TFORMA.*PMGUN\\.xml$"), //
            Pattern.compile("^C0016A1002L.{22}TPIJTE.*PMGUN\\.(pdf|jpg|jpeg|png)$"), Arrays.asList( //
                    new ReplacementAuthority(TypeFile.REGENT, "PGUEN", "PMGUN"), //
                    new ReplacementAuthority(TypeFile.REGENT, "TPIJTE", "TFORMA"), //
                    new ReplacementAuthority(TypeFile.XMLTC, "PGUEN", "PMGUN"), //
                    new ReplacementAuthority(TypeFile.OTHER, "PGUEN", "PMGUN"), //
                    new ReplacementAuthority(TypeFile.REGENT, "C1000A1001L", "C0016A1002L"), //
                    new ReplacementAuthority(TypeFile.XMLTC, "C1000A1001L", "C0016A1002L"), //
                    new ReplacementAuthority(TypeFile.OTHER, "C1000A1001L", "C0016A1002L") //
            ), //
            new LiasseGenerationUtils().setEmetteur("0016A1002").setType("DEMAT").setVersion("MGUN").setExtensionZip(".zip").setExtensionFini(".fini") //
    ),

    CMA("M", //
            "00161001", //
            "00161003", //
            Pattern.compile("^C0016A1003L.{22}TXMLTC.*PMGUN\\.xml$"), //
            Pattern.compile("^C0016A1003L.{22}TFORMA.*PMGUN\\.xml$"), //
            Pattern.compile("^C0016A1003L.{22}TPIJTE.*PMGUN\\.(pdf|xml|jpg|jpeg|png)$"), //
            Arrays.asList( //
                    new ReplacementAuthority(TypeFile.REGENT, "PGUEN", "PMGUN"), //
                    new ReplacementAuthority(TypeFile.REGENT, "TPIJTE", "TFORMA"), //
                    new ReplacementAuthority(TypeFile.XMLTC, "PGUEN", "PMGUN"), //
                    new ReplacementAuthority(TypeFile.OTHER, "PGUEN", "PMGUN"), //
                    new ReplacementAuthority(TypeFile.REGENT, "C1000A1001L", "C0016A1003L"), //
                    new ReplacementAuthority(TypeFile.XMLTC, "C1000A1001L", "C0016A1003L"), //
                    new ReplacementAuthority(TypeFile.OTHER, "C1000A1001L", "C0016A1003L") //
            ), //
            new LiasseGenerationUtils().setEmetteur("0016A1003").setType("DEMAT").setVersion("MGUN").setExtensionZip(".zip").setExtensionFini(".fini") //
    ),

    CA("X", //
            "00161001", //
            "00161004", //
            Pattern.compile("^.{33}TXMLTC.*PMGUN\\.xml$"), //
            Pattern.compile("^.{33}TPIJTE.*PMGUN\\.xml$"), //
            Pattern.compile("^.{33}TPIJTE.*PMGUN\\.(pdf|jpg|jpeg|png)$"), //
            Arrays.asList( //
                    new ReplacementAuthority(TypeFile.REGENT, "PGUEN", "PMGUN"), //
                    new ReplacementAuthority(TypeFile.REGENT, "TPIJTE", "TFORMA"), //
                    new ReplacementAuthority(TypeFile.XMLTC, "PGUEN", "PMGUN"), //
                    new ReplacementAuthority(TypeFile.OTHER, "PGUEN", "PMGUN") //
            ), //
            new LiasseGenerationUtils().setEmetteur("0016A1004").setType("DEMAT").setVersion("MGUN").setExtensionZip(".zip").setExtensionFini(".fini") //
    ),

    URSSAF("U", //
            "00161001", //
            "00161005", //
            Pattern.compile("^C0016A1005L.{22}TXMLTC.*PMGUN\\.xml$"), //
            Pattern.compile("^C0016A1005L.{22}TFORMA.*PMGUN\\.xml$"), //
            Pattern.compile("^C0016A1005L.{22}TPIJTE.*PMGUN\\.(pdf|jpg|jpeg|png)$"), //
            Arrays.asList( //
                    new ReplacementAuthority(TypeFile.REGENT, "PGUEN", "PMGUN"), //
                    new ReplacementAuthority(TypeFile.REGENT, "TPIJTE", "TFORMA"), //
                    new ReplacementAuthority(TypeFile.XMLTC, "PGUEN", "PMGUN"), //
                    new ReplacementAuthority(TypeFile.OTHER, "PGUEN", "PMGUN"), //
                    new ReplacementAuthority(TypeFile.REGENT, "C1000A1001L", "C0016A1005L"), //
                    new ReplacementAuthority(TypeFile.XMLTC, "C1000A1001L", "C0016A1005L"), //
                    new ReplacementAuthority(TypeFile.OTHER, "C1000A1001L", "C0016A1005L") //
            ), //
            new LiasseGenerationUtils().setEmetteur("0016A1005").setType("DEMAT").setVersion("MGUN").setExtensionZip(".zip").setExtensionFini(".zip.fini") //
    );

    private String reseau;

    private String codeEmetteur;

    private String codeDestinataire;

    private Pattern xmltcPattern;

    private Pattern xmlRegentPattern;

    private Pattern attachmentPattern;

    private List<ReplacementAuthority> replacements;

    private LiasseGenerationUtils liasseGeneration;

    AuthorityNaming(final String reseau, final String codeEmetteur, final String codeDestinataire, final Pattern xmltcPattern, final Pattern xmlRegentPattern, final Pattern attachmentPattern,
            final List<ReplacementAuthority> replacements, final LiasseGenerationUtils liasseGeneration) {
        this.reseau = reseau;
        this.codeEmetteur = codeEmetteur;
        this.codeDestinataire = codeDestinataire;
        this.xmltcPattern = xmltcPattern;
        this.xmlRegentPattern = xmlRegentPattern;
        this.attachmentPattern = attachmentPattern;
        this.replacements = replacements;
        this.liasseGeneration = liasseGeneration;
    }

    /**
     * Get authority naming based on reseau.
     *
     * @param reseau
     *            the reseau identifier
     * @return the authority naming
     */
    public static AuthorityNaming getNaming(final String codeEdi) {
        return Arrays.asList(AuthorityNaming.values()) //
                .stream() //
                .filter(s -> codeEdi.startsWith(s.getReseau())) //
                .findFirst() //
                .orElse(null) //
        ;
    }

    /**
     * 
     * @return
     */
    public String getReseau() {
        return this.reseau;
    }

    /**
     * Accesseur sur l'attribut {@link #codeEmetteur}.
     *
     * @return String codeEmetteur
     */
    public String getCodeEmetteur() {
        return codeEmetteur;
    }

    /**
     * Accesseur sur l'attribut {@link #codeDestinataire}.
     *
     * @return String codeDestinataire
     */
    public String getCodeDestinataire() {
        return codeDestinataire;
    }

    /**
     * Accesseur sur l'attribut {@link #xmltcPattern}.
     *
     * @return String xmltcPattern
     */
    public Pattern getXmltcPattern() {
        return xmltcPattern;
    }

    /**
     * Accesseur sur l'attribut {@link #xmlRegentPattern}.
     *
     * @return String xmlRegentPattern
     */
    public Pattern getXmlRegentPattern() {
        return xmlRegentPattern;
    }

    /**
     * Accesseur sur l'attribut {@link #attachmentPattern}.
     *
     * @return String attachmentPattern
     */
    public Pattern getAttachmentPattern() {
        return attachmentPattern;
    }

    /**
     * Accesseur sur l'attribut {@link #replacements}.
     *
     * @return List<ReplacementAuthority> replacements
     */
    public List<ReplacementAuthority> getReplacements() {
        return replacements;
    }

    /**
     * Accesseur sur l'attribut {@link #liasseGeneration}.
     *
     * @return LiasseGeneration liasseGeneration
     */
    public LiasseGenerationUtils getLiasseGeneration() {
        return liasseGeneration;
    }
}
