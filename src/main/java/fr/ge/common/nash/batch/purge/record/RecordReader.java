package fr.ge.common.nash.batch.purge.record;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import fr.ge.common.nash.batch.common.AbstractSearchQueryBuilder;
import fr.ge.common.nash.ws.v1.bean.RecordInfoBean;
import fr.ge.common.nash.ws.v1.bean.search.RecordSearchResult;
import fr.ge.common.nash.ws.v1.service.IRecordService;
import fr.ge.common.utils.bean.search.SearchResult;

/**
 * Record created batch reader.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@Component("purgeRecordReader")
public class RecordReader implements ItemReader<RecordInfoBean>, InitializingBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(RecordReader.class);

    private AbstractSearchQueryBuilder searchQueryBuilder;

    private IRecordService recordServiceClient;

    private Iterator<RecordInfoBean> recordIterator;

    private Long startIndex;

    private Long maxResults;

    /**
     * Mutateur sur l'attribut {@link #recordServiceClient}.
     *
     * @param recordServiceClient
     *            la nouvelle valeur de l'attribut recordServiceClient
     */
    public void setRecordServiceClient(final IRecordService recordServiceClient) {
        this.recordServiceClient = recordServiceClient;
    }

    /**
     * Mutateur sur l'attribut {@link #startIndex}.
     *
     * @param startIndex
     *            la nouvelle valeur de l'attribut startIndex
     */
    public void setStartIndex(final Long startIndex) {
        this.startIndex = startIndex;
    }

    /**
     * Mutateur sur l'attribut {@link #maxResults}.
     *
     * @param maxResults
     *            la nouvelle valeur de l'attribut maxResults
     */
    public void setMaxResults(final Long maxResults) {
        this.maxResults = maxResults;
    }

    /**
     * Mutateur sur l'attribut {@link #searchQueryBuilder}.
     *
     * @param searchQueryBuilder
     *            la nouvelle valeur de l'attribut searchQueryBuilder
     */
    public void setSearchQueryBuilder(final AbstractSearchQueryBuilder searchQueryBuilder) {
        this.searchQueryBuilder = searchQueryBuilder;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        if (null != this.startIndex && null != this.maxResults) {
            LOGGER.info("Search with startIndex {}, maxResults {}, filters : {}, orders : {}", this.startIndex, this.maxResults, this.searchQueryBuilder.getFilters(),
                    this.searchQueryBuilder.getOrders());
            final RecordSearchResult searchResult = this.recordServiceClient.search(this.startIndex, this.maxResults, this.searchQueryBuilder.getFilters(), this.searchQueryBuilder.getOrders());
            this.recordIterator = Optional.ofNullable(searchResult) //
                    .filter(r -> null != r.getContent()) //
                    .map(SearchResult::getContent) //
                    .orElse(new ArrayList<RecordInfoBean>()) //
                    .iterator();
        }
    }

    @Override
    public RecordInfoBean read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
        if (null != this.recordIterator && this.recordIterator.hasNext()) {
            return this.recordIterator.next();
        }
        return null;
    }
}
