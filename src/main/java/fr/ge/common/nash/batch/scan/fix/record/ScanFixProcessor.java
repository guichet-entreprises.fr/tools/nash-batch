/**
 * 
 */
package fr.ge.common.nash.batch.scan.fix.record;

import java.util.Arrays;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import com.github.jknack.handlebars.internal.lang3.StringUtils;

import fr.ge.common.storage.ws.model.StorageTrayEnum;
import fr.ge.common.storage.ws.model.StoredFile;
import fr.ge.common.storage.ws.rest.IRecordRestService;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.tracker.bean.Message;

/**
 * Scan & fix batch processor.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@Component(value = "scanFixMessageProcessor")
public class ScanFixProcessor implements ItemProcessor<Message, StoredFile> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ScanFixProcessor.class);

    /** Error message pattern. **/
    private static final Pattern ERROR_MESSAGE = Pattern.compile("^(.*)([0-9]{4}\\-[0-9]{2}\\-[A-Z]{3}\\-[A-Z]{3}\\-[0-9]{2})(.*)$");

    /** Storage record service. **/
    private IRecordRestService storageRecordRestService;

    /**
     * Mutateur sur l'attribut {@link #storageRecordRestService}.
     *
     * @param storageRecordRestService
     *            la nouvelle valeur de l'attribut storageRecordRestService
     */
    public void setStorageRecordRestService(final IRecordRestService storageRecordRestService) {
        this.storageRecordRestService = storageRecordRestService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StoredFile process(final Message message) throws Exception {
        if (null == message || StringUtils.isEmpty(message.getContent())) {
            LOGGER.error("Null or empty Tracker message");
            return null;
        }

        final Matcher matcher = ERROR_MESSAGE.matcher(message.getContent());
        if (!matcher.matches() && !matcher.find(2)) {
            LOGGER.error("The content \"{}\" does not match pattern : {}", message.getContent(), ERROR_MESSAGE.pattern());
            return null;
        }

        // -->Extract record uid from tracker message
        final String uid = matcher.group(2);
        LOGGER.debug("Extracting the record identifier from Tracker message : {}", uid);

        // -->Retrieve last STORAGE record in archived tray
        return Optional.ofNullable(this.storageRecordRestService.search( //
                0L, //
                1L, //
                Arrays.asList(new SearchQueryFilter("tray:" + StorageTrayEnum.ARCHIVED.getName().toLowerCase())), //
                Arrays.asList(new SearchQueryOrder("created:desc")), //
                uid) //
        ) //
                .filter(r -> null != r && r.getTotalResults() > 0L) //
                .map(r -> {
                    LOGGER.debug("Total archived related to record {} : {}", uid, r.getTotalResults());
                    return r;
                }) //
                .map(r -> r.getContent().iterator().next()) //
                .orElse(null);
    }

}
