package fr.ge.common.nash.batch.record.error;

import java.util.List;

import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Value;

/**
 * Write items to specified root destination path. Destination path parameter
 * HAS TO BE a correct URL, ie 'file:///...' or 'sftp:///...'.
 *
 * @author Christian Cougourdan
 */
public class RecordResourceItemWriter implements ItemWriter<RecordResource> {

    @Value("#{jobParameters['dst']:}")
    private String destinationRootPath;

    @Override
    public void write(final List<? extends RecordResource> items) throws Exception {
    }

}
