package fr.ge.common.nash.batch.record.report.reader;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Paths;

public class LocalResourceItemReader extends VfsResourceItemStreamReader {

    @Override
    protected URI buildUri(final String path) throws URISyntaxException {
        return Paths.get(path.substring(1)).toUri();
    }

}
