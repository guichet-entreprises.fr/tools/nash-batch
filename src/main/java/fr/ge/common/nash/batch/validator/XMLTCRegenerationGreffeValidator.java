/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.batch.validator;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.ge.common.nash.batch.bean.AuthorityNaming;
import fr.ge.common.nash.batch.bean.Error;
import fr.ge.common.nash.batch.bean.Error.Level;
import fr.ge.common.nash.batch.bean.Errors;
import fr.ge.common.nash.batch.utils.ScanFixUtils;
import fr.ge.common.nash.batch.utils.XMLRegentQuery;
import fr.ge.common.nash.batch.utils.XPathBuilder;
import fr.ge.common.nash.engine.manager.processor.bridge.service.ServiceRequest;
import fr.ge.common.nash.engine.manager.processor.bridge.service.ServiceResponse;
import fr.ge.common.storage.ws.model.StorageTrayEnum;
import fr.ge.common.storage.ws.model.StoredFile;
import fr.ge.common.utils.ResourceUtil;
import fr.ge.common.utils.ZipUtil;
import fr.ge.common.utils.bean.FileEntry;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.common.utils.exception.TechnicalException;

/**
 * Regenerate the XMLTC content from scratch if necessary.
 *
 * @author aolubi
 */
public class XMLTCRegenerationGreffeValidator implements IFunctionalValidator {

    private static final Logger LOGGER = LoggerFactory.getLogger(XMLTCRegenerationGreffeValidator.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean validateConditions(final byte[] resourceAsBytes) {
        final FileEntry xmlRegentEntry = ScanFixUtils.getXMLRegentEntry(resourceAsBytes);
        if (null == xmlRegentEntry) {
            LOGGER.warn("XML Regent file is missing");
            return false;
        }

        final String codeEdi = XMLRegentQuery.extractDestinataire(xmlRegentEntry).getTextContent();
        return codeEdi.startsWith(AuthorityNaming.GREFFE.getReseau());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Errors validate(final Properties appProperties, final byte[] resourceAsBytes) {
        final Errors errors = new Errors();
        final FileEntry xmltcEntry = ScanFixUtils.getXMLTCEntry(resourceAsBytes);
        if (null == xmltcEntry) {
            LOGGER.info("Missing XMLTC file");
            errors.add(new Error(Level.ERROR, "Missing XMLTC file"));
            return errors;
        }

        final FileEntry xmlRegentEntry = ScanFixUtils.getXMLRegentEntry(resourceAsBytes);
        if (null == xmlRegentEntry) {
            LOGGER.warn("XML Regent file is missing");
            errors.add(new Error(Level.ERROR, "XML Regent file is missing"));
            return errors;
        }

        final Document document = XPathBuilder.buildDocument(xmltcEntry.asBytes());
        final String expression = "//MGUN_DEMAT/version";
        final String version = XPathBuilder.queryString(document.getDocumentElement(), expression);
        if (StringUtils.isNotEmpty(version)) {
            errors.add(new Error(Level.ERROR, "XMLTC content is not eligible for GREFFE"));
            return errors;
        }

        return errors;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public byte[] fix(final Properties appProperties, final byte[] resourceAsBytes) {
        if (!this.validate(appProperties, resourceAsBytes).hasErrors()) {
            return resourceAsBytes;
        }

        // -->Extract liasse number
        final FileEntry xmlRegentEntry = ScanFixUtils.getXMLRegentEntry(resourceAsBytes);
        final Document document = XPathBuilder.buildDocument(xmlRegentEntry.asBytes());
        final Node c02 = XPathBuilder.queryNode(document.getDocumentElement(), "//REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C02");
        if (null == c02 || StringUtils.isEmpty(c02.getTextContent())) {
            return resourceAsBytes;
        }

        final String recordUid = this.loadStorageRecord(appProperties, c02.getTextContent());
        if (StringUtils.isEmpty(recordUid)) {
            LOGGER.warn("Cannot get record from storage using criteria liasse {}", c02.getTextContent());
            throw new TechnicalException(String.format("Cannot get record from storage using criteria liasse %s", c02.getTextContent()));
        }

        // -->Rebuild XMLTC from scratch
        final FileEntry xmltcEntry = ScanFixUtils.getXMLTCEntry(resourceAsBytes);
        try {
            final Source xslt = new StreamSource(new ByteArrayInputStream(ResourceUtil.resourceAsBytes("mgun-to-guen.xslt", this.getClass())));
            final TransformerFactory factory = TransformerFactory.newInstance();
            final Transformer transformer = factory.newTransformer(xslt);

            // -->Input arguments to XSLT
            transformer.setParameter("uid", recordUid);
            transformer.setParameter("emetteur", AuthorityNaming.GREFFE.getCodeEmetteur());
            transformer.setParameter("destinataire", AuthorityNaming.GREFFE.getCodeDestinataire());

            final String codeEdi = XMLRegentQuery.extractDestinataire(xmlRegentEntry).getTextContent();
            transformer.setParameter("codeEdi", codeEdi);

            final Source text = new StreamSource(new ByteArrayInputStream(xmltcEntry.asBytes()));
            final Map<String, byte[]> filesToInsert = new HashMap<>();
            try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
                final StreamResult streamResult = new StreamResult(out);
                transformer.transform(text, streamResult);
                filesToInsert.put(xmltcEntry.name(), out.toByteArray());

                LOGGER.info("Rebuild XMLTC with success");

                return ZipUtil.entries(filesToInsert, resourceAsBytes);
            }
        } catch (IOException | TransformerException e) {
            throw new TechnicalException(e);
        }
    }

    @SuppressWarnings("unchecked")
    private String loadStorageRecord(final Properties appProperties, final String liasse) {
        final ServiceResponse response = new ServiceRequest(appProperties.getProperty("mas.storage.service.url") + "/v1/Record") //
                .param("startIndex", 0L) //
                .param("maxResults", 1L) //
                .param("searchTerms", liasse) //
                .param("filters", "tray:" + StorageTrayEnum.ARCHIVED.getName()) //
                .accept("json") //
                .dataType(MediaType.APPLICATION_JSON) //
                .get();
        if (null == response || response.getStatus() != Status.OK.getStatusCode()) {
            LOGGER.error("Cannot get storage record from liasse {}", liasse);
            return null;
        }
        final SearchResult<StoredFile> result = response.asObject(SearchResult.class);
        if (null == result || result.getTotalResults() != 1L || CollectionUtils.isEmpty(result.getContent())) {
            return null;
        }
        final StoredFile record = new ObjectMapper().convertValue(result.getContent().iterator().next(), StoredFile.class);
        return Optional.of(record) //
                .map(StoredFile::getReferenceId) //
                .orElse(null) //
        ;
    }
}
