/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.batch.utils;

import java.util.ArrayList;
import java.util.Optional;

import fr.ge.common.nash.batch.validator.IFunctionalValidator;
import fr.ge.common.utils.ZipUtil;
import fr.ge.common.utils.bean.FileEntry;

/**
 * The ScanFix utility class.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public final class ScanFixUtils {

    /**
     * 
     * Constructeur de la classe.
     *
     */
    private ScanFixUtils() {
    }

    /**
     * Extract XMLTC from resource as file entry.
     * 
     * @param resourceAsBytes
     *            archive resource as list
     * @return XMLTC as entry
     */
    public static FileEntry getXMLTCEntry(final byte[] resourceAsBytes) {
        return Optional.of(resourceAsBytes) //
                .filter(r -> null != r) //
                .map(r -> ZipUtil.entries(r)) //
                .orElse(new ArrayList<String>()) //
                .stream() //
                .filter(e -> IFunctionalValidator.REGEX_XMLTC.matcher(e).matches()) //
                .findFirst() //
                .map(xml -> ZipUtil.entry(xml, resourceAsBytes)) //
                .orElse(null);
    }

    /**
     * Extract XML Regent from resource as file entry.
     * 
     * @param resourceAsBytes
     *            archive resource as list
     * @return XML Regent as entry
     */
    public static FileEntry getXMLRegentEntry(final byte[] resourceAsBytes) {
        return Optional.of(resourceAsBytes) //
                .filter(r -> null != r) //
                .map(r -> ZipUtil.entries(r)) //
                .orElse(new ArrayList<String>()) //
                .stream() //
                .filter(e -> IFunctionalValidator.REGEX_REGENT.matcher(e).matches()) //
                .findFirst() //
                .map(xml -> ZipUtil.entry(xml, resourceAsBytes)) //
                .orElse(null);
    }
}
