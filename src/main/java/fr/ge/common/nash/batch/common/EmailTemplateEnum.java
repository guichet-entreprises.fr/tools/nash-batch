/**
 * 
 */
package fr.ge.common.nash.batch.common;

import java.util.Arrays;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;

/**
 * Email Template enumeration.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public enum EmailTemplateEnum {

    WARNING_FR("fr_FR", "warning_fr", "Suppression de votre dossier n°%s", Locale.forLanguageTag("fr")), //
    WARNING_EN("en_EN", "warning_en", "Deletion of your application No.%s", Locale.forLanguageTag("en")), //
    WARNING_ES("es_ES", "warning_es", "Eliminación de su expendiente n.°%s", Locale.forLanguageTag("es")), //

    REMOVAL_FR("fr_FR", "removal_fr", "Suppression de votre dossier n°%s", Locale.forLanguageTag("fr")), //
    REMOVAL_EN("en_EN", "removal_en", "Deletion of your application No.%s", Locale.forLanguageTag("en")), //
    REMOVAL_ES("es_ES", "removal_es", "Eliminación de su expendiente n.°%s", Locale.forLanguageTag("es")) //
    ;

    private String language;

    private String name;

    private String object;

    private Locale locale;

    EmailTemplateEnum(final String language, final String name, final String object, final Locale locale) {
        this.language = language;
        this.name = name;
        this.object = object;
        this.locale = locale;
    }

    /**
     * Accesseur sur l'attribut {@link #language}.
     *
     * @return String language
     */
    public String getLanguage() {
        return language;
    }

    /**
     * Accesseur sur l'attribut {@link #name}.
     *
     * @return String name
     */
    public String getName() {
        return name;
    }

    /**
     * Accesseur sur l'attribut {@link #object}.
     *
     * @return String object
     */
    public String getObject() {
        return object;
    }

    /**
     * Accesseur sur l'attribut {@link #locale}.
     *
     * @return String locale
     */
    public Locale getLocale() {
        return locale;
    }

    public static EmailTemplateEnum getWarningTemplate(final String language) {
        return Optional.of(EmailTemplateEnum.values()) //
                .map(Arrays::stream) //
                .orElse(Stream.of(new EmailTemplateEnum[] {})) //
                .filter(template -> StringUtils.isNotEmpty(language) && template.getLanguage().equals(language)) //
                .findFirst() //
                .orElse(EmailTemplateEnum.WARNING_FR) //
        ;
    }

    public static EmailTemplateEnum getRemovalTemplate(final String language) {
        return Optional.of(EmailTemplateEnum.values()) //
                .map(Arrays::stream) //
                .orElse(Stream.of(new EmailTemplateEnum[] {})) //
                .filter(template -> StringUtils.isNotEmpty(language) && template.getLanguage().equals(language)) //
                .findFirst() //
                .orElse(EmailTemplateEnum.REMOVAL_FR) //
        ;
    }
}
