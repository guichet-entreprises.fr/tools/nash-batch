/**
 * 
 */
package fr.ge.common.nash.batch.purge.storage;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;

import fr.ge.common.nash.batch.common.AbstractSearchQueryBuilder;
import fr.ge.common.storage.ws.model.StorageTrayEnum;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;

/**
 * Storage record batch search query builder.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class SearchQueryBuilder extends AbstractSearchQueryBuilder {

    /**
     * {@inheritDoc}
     */
    @Override
    public void build() {
        this.filters.add(new SearchQueryFilter("tray:" + Arrays.asList( //
                StorageTrayEnum.ARCHIVED.getName(), //
                StorageTrayEnum.ARCHIVED_PARTNERS.getName()).stream().collect(Collectors.joining(","))) //
        );

        Optional.ofNullable(this.minusDaysMin) //
                .filter(v -> null != v) //
                .map(v -> LocalDate.now().minusDays(v)) //
                .map(LocalDate::toString) //
                .map(dateInf -> new SearchQueryFilter("created<=" + dateInf.toString())) //
                .map(this.filters::add) //
                .orElse(null) //
        ;

        Optional.ofNullable(this.minusDaysMax) //
                .filter(v -> null != v) //
                .map(v -> LocalDate.now().minusDays(v)) //
                .map(LocalDate::toString) //
                .map(dateInf -> new SearchQueryFilter("created>" + dateInf.toString())) //
                .map(this.filters::add) //
                .orElse(null) //
        ;

        this.orders = Arrays.asList("created:asc", "uid:asc").stream().map(SearchQueryOrder::new).collect(Collectors.toList());
    }
}
