/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.batch.validator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import fr.ge.common.nash.batch.bean.AuthorityNaming;
import fr.ge.common.nash.batch.bean.Error;
import fr.ge.common.nash.batch.bean.Error.Level;
import fr.ge.common.nash.batch.bean.Errors;
import fr.ge.common.nash.batch.utils.ScanFixUtils;
import fr.ge.common.nash.batch.utils.XMLRegentQuery;
import fr.ge.common.nash.batch.utils.XMLTCQuery;
import fr.ge.common.nash.batch.utils.XPathBuilder;
import fr.ge.common.utils.ZipUtil;
import fr.ge.common.utils.bean.FileEntry;

/**
 * Compare zip entries file and the attachment into the XMLTC file.
 *
 * @author aolubi
 */
public class CompareEntryZipValidator implements IFunctionalValidator {

    private static final Logger LOGGER = LoggerFactory.getLogger(CompareEntryZipValidator.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public Errors validate(final Properties appProperties, final byte[] resourceAsBytes) {
        final Errors errors = new Errors();

        final FileEntry xmltcEntry = ScanFixUtils.getXMLTCEntry(resourceAsBytes);
        if (null == xmltcEntry) {
            LOGGER.warn("Cannot compare entries where XMLTC file is missing");
            errors.add(new Error(Level.ERROR, "Cannot compare entries where XMLTC file is missing"));
            return errors;
        }
        final FileEntry xmlRegentEntry = ScanFixUtils.getXMLRegentEntry(resourceAsBytes);
        if (null == xmlRegentEntry) {
            LOGGER.warn("Cannot compare entries where XML Regent file is missing");
            errors.add(new Error(Level.ERROR, "Cannot compare entries where XML Regent file is missing"));
            return errors;
        }

        final String codeEdi = XMLRegentQuery.extractDestinataire(xmlRegentEntry).getTextContent();
        if (!codeEdi.startsWith(AuthorityNaming.GREFFE.getReseau())) {
            return errors;
        }

        final List<String> entries = ZipUtil.entries(resourceAsBytes);
        final Document document = XPathBuilder.buildDocument(xmltcEntry.asBytes());
        final NodeList fichierPieceJointeNodeList = XPathBuilder.queryNodeList(document, "//GUEN_DMTDU/dossierUnique/pieceJointe/fichierPieceJointe");
        for (int i = 0; i < fichierPieceJointeNodeList.getLength(); i++) {
            final Node fichierPieceJointe = fichierPieceJointeNodeList.item(i);
            if (!entries.contains(fichierPieceJointe.getTextContent())) {
                errors.add(new Error(Level.ERROR, String.format("The file %s does not exist into Zip content", fichierPieceJointe.getTextContent())));
            }
        }

        if (errors.hasErrors()) {
            return errors;
        }

        // -->Index of files declared for target authority
        final Node destinataireDossierCfeAsNode = XMLTCQuery.extractDestinataireDossierCfeAsNode(xmltcEntry, codeEdi);
        if (null == destinataireDossierCfeAsNode) {
            LOGGER.warn("Missing target authority {} description into XMLTC file", codeEdi);
            errors.add(new Error(Level.ERROR, String.format("Missing target authority %s description into XMLTC file", codeEdi)));
            return errors;
        }

        final List<String> indexPjAuthority = new ArrayList<>();
        final NodeList indicePieceJointeNodeList = XPathBuilder.queryNodeList(destinataireDossierCfeAsNode, "indicePieceJointe");
        for (int i = 0; i < indicePieceJointeNodeList.getLength(); i++) {
            final Node indicePieceJointe = indicePieceJointeNodeList.item(i);
            indexPjAuthority.add(indicePieceJointe.getTextContent());
        }
        final String indiceLiasseXml = XPathBuilder.queryString(destinataireDossierCfeAsNode, "indiceLiasseXml");
        indexPjAuthority.add(indiceLiasseXml);
        // <--

        // -->Index of all files declared in the tag 'indicePieceJointe'
        final List<String> indexAllPjs = new ArrayList<>();
        final NodeList pieceJointeIndicePieceJointeAsNodeList = XMLTCQuery.extractPieceJointeIndicePieceJointeAsNodeList(xmltcEntry);
        for (int idx = 0; idx < pieceJointeIndicePieceJointeAsNodeList.getLength(); idx++) {
            final String indicePieceJointeAsString = pieceJointeIndicePieceJointeAsNodeList.item(idx).getTextContent();
            indexAllPjs.add(indicePieceJointeAsString);
        }

        if (!CollectionUtils.isEqualCollection(indexAllPjs, indexPjAuthority)) {
            LOGGER.warn("Detailled index files : {}", indexAllPjs);
            LOGGER.warn("Detailled index files for target authority {} : {}", codeEdi, indexPjAuthority);
            LOGGER.warn("Index files are not matching between {} and {}", indexAllPjs, indexPjAuthority);
            errors.add(new Error(Level.ERROR, String.format("Index files are not matching between %s and %s for target authority %s", indexAllPjs, indexPjAuthority, codeEdi)));
        }

        return errors;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public byte[] fix(final Properties appProperties, final byte[] resourceAsBytes) {
        if (!this.validate(appProperties, resourceAsBytes).hasErrors()) {
            return resourceAsBytes;
        }

        final FileEntry xmlRegentEntry = ScanFixUtils.getXMLRegentEntry(resourceAsBytes);
        if (null == xmlRegentEntry) {
            LOGGER.warn("Cannot compare entries where XML Regent file is missing");
            return resourceAsBytes;
        }

        final String codeEdi = XMLRegentQuery.extractDestinataire(xmlRegentEntry).getTextContent();
        if (!codeEdi.startsWith(AuthorityNaming.GREFFE.getReseau())) {
            return resourceAsBytes;
        }

        final FileEntry xmltcEntry = ScanFixUtils.getXMLTCEntry(resourceAsBytes);
        if (null == xmltcEntry) {
            LOGGER.warn("Cannot compare entries where XMLTC file is missing");
            return resourceAsBytes;
        }

        // -->Index of files declared for target authority
        final Node destinataireDossierCfeAsNode = XMLTCQuery.extractDestinataireDossierCfeAsNode(xmltcEntry, codeEdi);
        if (null == destinataireDossierCfeAsNode) {
            LOGGER.warn("Missing target authority {} description into XMLTC file", codeEdi);
            return resourceAsBytes;
        }

        final List<String> indexPjAuthority = new ArrayList<>();
        final NodeList indicePieceJointeNodeList = XPathBuilder.queryNodeList(destinataireDossierCfeAsNode, "indicePieceJointe");
        for (int i = 0; i < indicePieceJointeNodeList.getLength(); i++) {
            final Node indicePieceJointe = indicePieceJointeNodeList.item(i);
            indexPjAuthority.add(indicePieceJointe.getTextContent());
        }
        final String indiceLiasseXml = XPathBuilder.queryString(destinataireDossierCfeAsNode, "indiceLiasseXml");
        indexPjAuthority.add(indiceLiasseXml);
        // <--

        final Document document = XPathBuilder.buildDocument(xmltcEntry.asBytes());
        final NodeList pieceJointeNodeList = XPathBuilder.queryNodeList(document, "//GUEN_DMTDU/dossierUnique/pieceJointe");

        // -->Remove unused 'pieceJointe' tag
        final List<String> entries = ZipUtil.entries(resourceAsBytes);
        for (int idx = 0; idx < pieceJointeNodeList.getLength(); idx++) {
            final Node pieceJointeNode = pieceJointeNodeList.item(idx);
            final String indicePieceJointe = XPathBuilder.queryString(pieceJointeNode, "indicePieceJointe");
            if (!indexPjAuthority.contains(indicePieceJointe) || !entries.contains(XPathBuilder.queryString(pieceJointeNode, "fichierPieceJointe"))) {
                pieceJointeNode.getParentNode().removeChild(pieceJointeNode);
                LOGGER.warn("Removing unused 'pieceJointe' tag at index {}", indicePieceJointe);
            }
        }

        final Node pieceJointeLiasseNode = XPathBuilder.queryNode(document, "//GUEN_DMTDU/dossierUnique/pieceJointe[fichierPieceJointe='" + xmlRegentEntry.name() + "']");
        final String indiceLiasse = XPathBuilder.queryString(pieceJointeLiasseNode, "indicePieceJointe");
        if (!indiceLiasseXml.equals(indiceLiasse)) {
            XPathBuilder.queryNode(document, "//GUEN_DMTDU/dossierUnique/dossierCfe/destinataireDossierCfe[codeEdiDestinataire='" + codeEdi + "']/indiceLiasseXml").setTextContent(indiceLiasse);
            LOGGER.info("Update indice liasse from {} to {}", indiceLiasseXml, indiceLiasse);

            final String expression = "//GUEN_DMTDU/dossierUnique/dossierCfe/destinataireDossierCfe[codeEdiDestinataire='" + codeEdi + "' and indicePieceJointe='" + indiceLiasse
                    + "']/indicePieceJointe";
            final NodeList indicePieceJointeList = XPathBuilder.queryNodeList(document, expression);
            for (int i = 0; i < indicePieceJointeList.getLength(); i++) {
                final Node indicePieceJointe = indicePieceJointeList.item(i);
                if (indicePieceJointe.getTextContent().equals(indiceLiasse)) {
                    indicePieceJointe.getParentNode().removeChild(indicePieceJointe).getTextContent();
                    LOGGER.info("Remove indicePieceJointe {}", indiceLiasse);
                }
            }

            XPathBuilder.queryNode(pieceJointeLiasseNode, "typePieceJointe").setTextContent( //
                    Optional.of(XPathBuilder.queryNode(destinataireDossierCfeAsNode, "roleDestinataire")) //
                            .filter(node -> null != node) //
                            .map(Node::getTextContent) //
                            .filter(role -> "TDR".equals(role)) //
                            .map(role -> "LIASSE_TDR_EDI") //
                            .orElse("LIASSE_CFE_EDI"));
        }

        final Map<String, byte[]> filesToInsert = new HashMap<>();
        filesToInsert.put(xmltcEntry.name(), XPathBuilder.asBytes(document));

        return ZipUtil.entries(filesToInsert, resourceAsBytes);
    }
}
