/**
 * 
 */
package fr.ge.common.nash.batch.bean;

/**
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class FileState {

    private FileStateEnum state;
    private String inputFile;
    private String outputZipFileName;
    private String outputZipFiniFileName;

    public FileState() {
    }

    public FileState(final FileStateEnum state, final String inputFile) {
        this(state, inputFile, null, null);
    }

    public FileState(final FileStateEnum state, final String inputFile, final String outputZipFileName, final String outputZipFiniFileName) {
        this.state = state;
        this.inputFile = inputFile;
        this.setOutputZipFileName(outputZipFileName);
        this.setOutputZipFiniFileName(outputZipFiniFileName);
    }

    /**
     * Accesseur sur l'attribut {@link #state}.
     *
     * @return FileStateEnum state
     */
    public FileStateEnum getState() {
        return state;
    }

    /**
     * Mutateur sur l'attribut {@link #state}.
     *
     * @param state
     *            la nouvelle valeur de l'attribut state
     */
    public FileState setState(final FileStateEnum state) {
        this.state = state;
        return this;

    }

    /**
     * Accesseur sur l'attribut {@link #inputFile}.
     *
     * @return String inputFile
     */
    public String getInputFile() {
        return inputFile;
    }

    /**
     * Mutateur sur l'attribut {@link #inputFile}.
     *
     * @param inputFile
     *            la nouvelle valeur de l'attribut inputFile
     */
    public FileState setInputFile(final String inputFile) {
        this.inputFile = inputFile;
        return this;
    }

    /**
     * Accesseur sur l'attribut {@link #outputZipFileName}.
     *
     * @return String outputZipFileName
     */
    public String getOutputZipFileName() {
        return outputZipFileName;
    }

    /**
     * Mutateur sur l'attribut {@link #outputZipFileName}.
     *
     * @param outputZipFileName
     *            la nouvelle valeur de l'attribut outputZipFileName
     */
    public FileState setOutputZipFileName(final String outputZipFileName) {
        this.outputZipFileName = outputZipFileName;
        return this;
    }

    /**
     * Accesseur sur l'attribut {@link #outputZipFiniFileName}.
     *
     * @return String outputZipFiniFileName
     */
    public String getOutputZipFiniFileName() {
        return outputZipFiniFileName;
    }

    /**
     * Mutateur sur l'attribut {@link #outputZipFiniFileName}.
     *
     * @param outputZipFiniFileName
     *            la nouvelle valeur de l'attribut outputZipFiniFileName
     */
    public FileState setOutputZipFiniFileName(final String outputZipFiniFileName) {
        this.outputZipFiniFileName = outputZipFiniFileName;
        return this;
    }

}
