package fr.ge.common.nash.batch.notify.record;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.stereotype.Component;

import com.github.jknack.handlebars.Handlebars;
import com.github.jknack.handlebars.io.ClassPathTemplateLoader;

import fr.ge.common.nash.batch.common.BatchTrackerEnum;
import fr.ge.common.nash.batch.common.EmailTemplateEnum;
import fr.ge.common.nash.batch.validation.AccountService;
import fr.ge.common.nash.ws.v1.bean.RecordInfoBean;
import fr.ge.core.exception.TechniqueException;
import fr.ge.exchange.email.ws.v1.bean.EmailSendBean;
import fr.ge.exchange.email.ws.v1.service.ISendEmailRestService;
import fr.ge.tracker.service.v1.IUidRestService;

/**
 * Record created batch writer.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@Component(value = "notifyRecordWriter")
public class RecordWriter implements ItemWriter<RecordInfoBean> {

    private static final Logger LOGGER = LoggerFactory.getLogger(RecordWriter.class);

    private static final String DATE_FORMAT = "dd MMMM yyyy";

    private AccountService accountRestService;

    private ISendEmailRestService sendEmailRestService;

    private IUidRestService uidRestService;

    private String supportEmailAddress;

    private String sender;

    private String remainingDays;

    /**
     * Mutateur sur l'attribut {@link #accountRestService}.
     *
     * @param accountRestService
     *            la nouvelle valeur de l'attribut accountRestService
     */
    public void setAccountRestService(final AccountService accountRestService) {
        this.accountRestService = accountRestService;
    }

    /**
     * Mutateur sur l'attribut {@link #sendEmailRestService}.
     *
     * @param sendEmailRestService
     *            la nouvelle valeur de l'attribut sendEmailRestService
     */
    public void setSendEmailRestService(final ISendEmailRestService sendEmailRestService) {
        this.sendEmailRestService = sendEmailRestService;
    }

    /**
     * Mutateur sur l'attribut {@link #uidRestService}.
     *
     * @param uidRestService
     *            la nouvelle valeur de l'attribut uidRestService
     */
    public void setUidRestService(final IUidRestService uidRestService) {
        this.uidRestService = uidRestService;
    }

    /**
     * Mutateur sur l'attribut {@link #supportEmailAddress}.
     *
     * @param supportEmailAddress
     *            la nouvelle valeur de l'attribut supportEmailAddress
     */
    public void setSupportEmailAddress(String supportEmailAddress) {
        this.supportEmailAddress = supportEmailAddress;
    }

    /**
     * Mutateur sur l'attribut {@link #sender}.
     *
     * @param sender
     *            la nouvelle valeur de l'attribut sender
     */
    public void setSender(final String sender) {
        this.sender = sender;
    }

    /**
     * Accesseur sur l'attribut {@link #remainingDays}.
     *
     * @return String remainingDays
     */
    public String getRemainingDays() {
        return remainingDays;
    }

    /**
     * Mutateur sur l'attribut {@link #remainingDays}.
     *
     * @param remainingDays
     *            la nouvelle valeur de l'attribut remainingDays
     */
    public void setRemainingDays(final String remainingDays) {
        this.remainingDays = remainingDays;
    }

    @Override
    public void write(final List<? extends RecordInfoBean> items) throws Exception {
        for (final RecordInfoBean record : items) {
            try {
                final Map<String, String> userInfo = this.accountRestService.readUser(record.getAuthor());
                userInfo.put("civility", userInfo.getOrDefault("civility", null));
                userInfo.put("firstName", userInfo.getOrDefault("firstName", null));
                userInfo.put("lastName", userInfo.getOrDefault("lastName", null));

                final String userLanguage = userInfo.getOrDefault("language", EmailTemplateEnum.WARNING_FR.getLanguage());
                LOGGER.debug("User language : {}", userLanguage);
                final EmailTemplateEnum template = EmailTemplateEnum.getWarningTemplate(userLanguage);

                userInfo.put("code", record.getCode());

                final LocalDate createdDateRecord = LocalDateTime.ofInstant(record.getCreated().toInstant(), record.getCreated().getTimeZone().toZoneId()).toLocalDate();
                final LocalDate updatedDateRecord = LocalDateTime.ofInstant(record.getCreated().toInstant(), record.getCreated().getTimeZone().toZoneId()).toLocalDate();
                userInfo.put("created", createdDateRecord.format(DateTimeFormatter.ofPattern(DATE_FORMAT, template.getLocale())));
                userInfo.put("updated", updatedDateRecord.format(DateTimeFormatter.ofPattern(DATE_FORMAT, template.getLocale())));

                userInfo.put("support", this.supportEmailAddress);
                userInfo.put("remainingDays", this.remainingDays);

                final EmailSendBean body = new EmailSendBean();
                body.setSender(this.sender);
                body.setObject(String.format(template.getObject(), record.getCode()));
                body.setRecipient(userInfo.getOrDefault("email", null));

                body.setContent(new Handlebars(new ClassPathTemplateLoader("/templates", ".html")).compile(template.getName()).apply(userInfo));

                this.sendEmailRestService.sendEmail(body);

                this.uidRestService.addMessage(record.getCode(), //
                        String.format(BatchTrackerEnum.ENGAGE_REMOVAL_PROCEDURE_UNLESS_X_DAYS.getMessage(), this.remainingDays,
                                LocalDate.now().format(DateTimeFormatter.ofPattern(DATE_FORMAT, EmailTemplateEnum.WARNING_FR.getLocale()))), //
                        String.format(BatchTrackerEnum.ENGAGE_REMOVAL_PROCEDURE_UNLESS_X_DAYS.getAuthor(), this.remainingDays) //
                );
                LOGGER.info("The removable procedure is engaged");
            } catch (TechniqueException | IOException e) {
                LOGGER.warn(String.format("Cannot notify user for the record %s", record.getCode()), e);
            }
        }
    }
}
