/**
 * 
 */
package fr.ge.common.nash.batch.launcher;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.ge.common.nash.batch.validation.ScanFixService;

/**
 * Scan and fix launcher;
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public final class ScanFixLauncher {

    /**
     * Private constructor for utility class.
     */
    private ScanFixLauncher() {
        // Nothing to do.
    }

    /**
     * @param args
     */
    @SuppressWarnings("resource")
    public static void main(final String[] args) throws Exception {
        final Option batchTypeOption = Option.builder("t").argName("type").hasArg().desc("The batch type (scan or fix)").build();
        final Option inputAbsoluteFileOpt = Option.builder("f").argName("file").hasArg().desc("The input absolute file path to scan or fix").build();
        final Option inputAbsoluteDirectoryOpt = Option.builder("d").argName("input_dir").hasArg().desc("The input absolute directory path to scan or fix").build();
        final Option inputAsboluteCsvFileOpt = Option.builder("csv").argName("csv_file").hasArg().desc("The input absolute CSV file describing files to be fixed").build();
        final Option outputAsboluteDirectoryOpt = Option.builder("o").argName("output_dir").hasArg().desc("The output directory to write the output files fixed").build();
        final Option tokenOpt = Option.builder("token").argName("token").hasArg().desc("The target EDDIE token to push the output files fixed").build();
        final Option pathFolderOpt = Option.builder("pathFolder").argName("pathFolder").hasArg().desc("The target EDDIE path folder token to push the output files fixed").build();
        final Options options = new Options();

        final CommandLineParser parser = new DefaultParser();

        options.addOption(batchTypeOption);
        options.addOption(inputAsboluteCsvFileOpt);
        options.addOption(inputAbsoluteDirectoryOpt);
        options.addOption(inputAbsoluteFileOpt);
        options.addOption(outputAsboluteDirectoryOpt);
        options.addOption(tokenOpt);
        options.addOption(pathFolderOpt);

        final String header = "               [<arg1> [<arg2> [<arg3> [<arg4> [<arg5> [<arg6> [<arg6> ...\n       Options, flags and arguments may be in any order";
        final HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("Scan & Fix Launcher", header, options, null, true);

        try {
            final CommandLine commandLine = parser.parse(options, args);
            if (Arrays.asList("t", "f", "d", "o").stream().allMatch(opt -> !commandLine.hasOption(opt))) {
                System.out.print("All options are missing");
                return;
            }

            final Map<String, String> params = new HashMap<>();
            Arrays.asList("t", "f", "d", "o", "csv", "token", "pathFolder").stream().forEach(param -> {
                if (commandLine.hasOption(param)) {
                    System.out.print("Option " + param + " is present.  The value is: ");
                    System.out.print(commandLine.getOptionValue(param));
                    System.out.println();
                    params.put(param, commandLine.getOptionValue(param));
                }
            });

            final ApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring/validator-context.xml");
            final ScanFixService service = (ScanFixService) context.getBean(ScanFixService.class);
            service.run(params);

        } catch (ParseException exception) {
            System.out.print("Parse error: ");
            System.out.println(exception.getMessage());
        }
    }

}
