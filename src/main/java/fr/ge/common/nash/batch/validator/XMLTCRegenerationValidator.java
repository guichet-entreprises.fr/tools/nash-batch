/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.batch.validator;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import fr.ge.common.nash.batch.bean.AuthorityNaming;
import fr.ge.common.nash.batch.bean.Error;
import fr.ge.common.nash.batch.bean.Error.Level;
import fr.ge.common.nash.batch.bean.Errors;
import fr.ge.common.nash.batch.utils.ScanFixUtils;
import fr.ge.common.nash.batch.utils.XMLRegentQuery;
import fr.ge.common.nash.batch.utils.XMLTCQuery;
import fr.ge.common.nash.batch.utils.XPathBuilder;
import fr.ge.common.utils.ZipUtil;
import fr.ge.common.utils.bean.FileEntry;
import fr.ge.common.utils.exception.TechnicalException;

/**
 * Regenerate the XMLTC content from scratch if necessary.
 *
 * @author aolubi
 */
public class XMLTCRegenerationValidator implements IFunctionalValidator {

    private static final Logger LOGGER = LoggerFactory.getLogger(XMLTCRegenerationValidator.class);
    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean validateConditions(final byte[] resourceAsBytes) {
        final FileEntry xmlRegentEntry = ScanFixUtils.getXMLRegentEntry(resourceAsBytes);
        if (null == xmlRegentEntry) {
            LOGGER.warn("XML Regent file is missing");
            return false;
        }

        final String codeEdi = XMLRegentQuery.extractDestinataire(xmlRegentEntry).getTextContent();
        return !codeEdi.startsWith(AuthorityNaming.GREFFE.getReseau());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Errors validate(final Properties appProperties, final byte[] resourceAsBytes) {
        final Errors errors = new Errors();
        final FileEntry xmltcEntry = ScanFixUtils.getXMLTCEntry(resourceAsBytes);
        if (null == xmltcEntry) {
            LOGGER.info("Missing XMLTC file");
            errors.add(new Error(Level.ERROR, "Missing XMLTC file"));
            return errors;
        }

        final FileEntry xmlRegentEntry = ScanFixUtils.getXMLRegentEntry(resourceAsBytes);
        if (null == xmlRegentEntry) {
            LOGGER.warn("XML Regent file is missing");
            errors.add(new Error(Level.ERROR, "XML Regent file is missing"));
            return errors;
        }

        final Document document = XPathBuilder.buildDocument(xmltcEntry.asBytes());
        final String expression = "//MGUN_DEMAT/version";
        final String version = XPathBuilder.queryString(document.getDocumentElement(), expression);
        if (StringUtils.isEmpty(version)) {
            errors.add(new Error(Level.ERROR, "Invalid XMLTC content"));
            return errors;
        }

        return errors;
    }

    /**
     * Calcule l'émetteur en V1 en fonction du code partenaire.
     * 
     * @param codePartenaire
     *            le code partenaire
     * @return le code de l'émetteur
     */
    private String determineEmetteur(final String codeEdi) {
        if (!StringUtils.isBlank(codeEdi)) {
            if (codeEdi.startsWith(AuthorityNaming.CCI.getReseau())) {
                return "00161002";
            }
            if (codeEdi.startsWith(AuthorityNaming.CMA.getReseau())) {
                return "00161003";
            }
            if (codeEdi.startsWith(AuthorityNaming.CA.getReseau())) {
                return "00161004";
            }
            if (codeEdi.startsWith(AuthorityNaming.URSSAF.getReseau())) {
                return "00161005";
            }
        }
        return "00160001";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public byte[] fix(final Properties appProperties, final byte[] resourceAsBytes) {
        if (!this.validate(appProperties, resourceAsBytes).hasErrors()) {
            return resourceAsBytes;
        }

        // -->Rebuild XMLTC from scratch
        try {
            final String sysDateStr = LocalDateTime.now().format(FORMATTER);
            final FileEntry xmltcEntry = ScanFixUtils.getXMLTCEntry(resourceAsBytes);
            final FileEntry xmlRegentEntry = ScanFixUtils.getXMLRegentEntry(resourceAsBytes);
            final String codeEdi = XMLRegentQuery.extractDestinataire(xmlRegentEntry).getTextContent();
            final Node destinataireDossierCfe = XMLTCQuery.extractDestinataireDossierCfeAsNode(xmltcEntry, codeEdi);
            final Document docXMLTC = XPathBuilder.buildDocument(xmltcEntry.asBytes());

            final DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            builderFactory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
            final DocumentBuilder builder = builderFactory.newDocumentBuilder();
            final Document document = builder.newDocument();
            document.setXmlStandalone(true);

            final Element root = (Element) document.createElement("MGUN_DEMAT");
            root.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            root.setAttribute("xsi:noNamespaceSchemaLocation", "mgun_demat.xsd");

            final Element version = (Element) document.createElement("version");
            version.setTextContent("V2009.01");
            root.appendChild(version);

            final Element emetteur = (Element) document.createElement("emetteur");
            emetteur.setTextContent(AuthorityNaming.getNaming(codeEdi).getCodeEmetteur());
            root.appendChild(emetteur);

            final Element destinataire = (Element) document.createElement("destinataire");
            destinataire.setTextContent(AuthorityNaming.getNaming(codeEdi).getCodeDestinataire());
            root.appendChild(destinataire);

            final Element dateHeureGenerationXml = (Element) document.createElement("dateHeureGenerationXml");
            dateHeureGenerationXml.setTextContent(sysDateStr);
            root.appendChild(dateHeureGenerationXml);

            // -->formalite
            final Element formalite = (Element) document.createElement("formalite");

            final Element numeroDeLiasse = (Element) document.createElement("numeroDeLiasse");
            numeroDeLiasse.setTextContent(XMLRegentQuery.extractNumeroLiasse(xmlRegentEntry));
            formalite.appendChild(numeroDeLiasse);

            final Element dateDepot = (Element) document.createElement("dateDepot");
            dateDepot.setTextContent(sysDateStr);
            formalite.appendChild(dateDepot);

            final Element destinataires = (Element) document.createElement("destinataires");

            final Element codeEdiDestinataire = (Element) document.createElement("codeEdiDestinataire");
            codeEdiDestinataire.setTextContent(codeEdi);
            destinataires.appendChild(codeEdiDestinataire);

            final Element typeDestinataire = (Element) document.createElement("typeDestinataire");
            typeDestinataire.setTextContent(XPathBuilder.queryString(destinataireDossierCfe, "roleDestinataire"));
            destinataires.appendChild(typeDestinataire);

            // -->paiement tag
            final Node paiementDossierNode = XPathBuilder.queryNode(destinataireDossierCfe, "paiementDossier");
            if (null != paiementDossierNode) {
                final Element paiementDossier = (Element) document.createElement("paiement");
                final Element paiementCarteBleue = (Element) document.createElement("paiementCarteBleue");
                final Element identitePayeur = (Element) document.createElement("identitePayeur");

                final String tagValue = XPathBuilder.queryString(paiementDossierNode, "identitePayeur/nomPayeur");
                final Element nomPayeur = (Element) document.createElement("nomPayeur");
                nomPayeur.setTextContent(tagValue);
                identitePayeur.appendChild(nomPayeur);

                paiementCarteBleue.appendChild(identitePayeur);

                final Element adressePayeur = (Element) document.createElement("adressePayeur");
                Arrays.asList("numeroDeVoie", "typeDeVoie", "libelleDeVoie", "localite", "codePostal", "bureauDistributeur", "codePays", "adresseEmail").forEach(tag -> {
                    final String adresseTagValue = XPathBuilder.queryString(paiementDossierNode, "adressePayeur/" + tag);
                    final Element tagElement = (Element) document.createElement(tag);
                    if (StringUtils.isEmpty(adresseTagValue)) {
                        if ("bureauDistributeur".equals(tag)) {
                            tagElement.setTextContent("BUREAUDISTR");
                        } else if ("codePays".equals(tag)) {
                            tagElement.setTextContent("FR");
                        }
                    } else {
                        tagElement.setTextContent(adresseTagValue);
                    }

                    if (StringUtils.isNotEmpty(tagElement.getTextContent())) {
                        adressePayeur.appendChild(tagElement);
                    }
                });

                if (adressePayeur.hasChildNodes()) {
                    paiementCarteBleue.appendChild(adressePayeur);
                }

                Arrays.asList("montantFormalite", "referencePaiement", "dateHeurePaiement").forEach(tag -> {
                    final String paiementTagValue = XPathBuilder.queryString(paiementDossierNode, tag);
                    if (StringUtils.isNotEmpty(paiementTagValue)) {
                        final Element tagElement = (Element) document.createElement(tag);
                        tagElement.setTextContent(paiementTagValue);
                        paiementCarteBleue.appendChild(tagElement);
                    }
                });
                paiementDossier.appendChild(paiementCarteBleue);
                destinataires.appendChild(paiementDossier);
            }
            // <--

            formalite.appendChild(destinataires);

            final Element fichierLiasseXml = (Element) document.createElement("fichierLiasseXml");
            String expression = "//GUEN_DMTDU/dossierUnique/pieceJointe[indicePieceJointe=//GUEN_DMTDU/dossierUnique/dossierCfe/destinataireDossierCfe[codeEdiDestinataire='" + codeEdi
                    + "']/indiceLiasseXml/text()]/fichierPieceJointe";
            final String liasseName = XPathBuilder.queryString(docXMLTC.getDocumentElement(), expression);
            fichierLiasseXml.setTextContent(liasseName);
            formalite.appendChild(fichierLiasseXml);

            // -->LIASSE documentJustificatif
            final Element documentJustificatif = (Element) document.createElement("documentJustificatif");
            final Element codeTypeDocumentJustificatif = (Element) document.createElement("codeTypeDocumentJustificatif");
            codeTypeDocumentJustificatif.setTextContent("LI");
            documentJustificatif.appendChild(codeTypeDocumentJustificatif);

            final Element fichierPieceJointe = (Element) document.createElement("fichierPieceJointe");
            expression = "//GUEN_DMTDU/dossierUnique/pieceJointe[contains(typePieceJointe, '_PDF')]/fichierPieceJointe";
            final String cerfaName = XPathBuilder.queryString(docXMLTC.getDocumentElement(), expression);
            fichierPieceJointe.setTextContent(cerfaName);
            documentJustificatif.appendChild(fichierPieceJointe);
            formalite.appendChild(documentJustificatif);
            // <--

            // -->Others attachment
            expression = "//GUEN_DMTDU/dossierUnique/pieceJointe[ not(contains(typePieceJointe,'_PDF') or contains(typePieceJointe,'_EDI')) ]/fichierPieceJointe";
            final NodeList othersAttachmentList = XPathBuilder.queryNodeList(docXMLTC.getDocumentElement(), expression);
            for (int idx = 0; idx < othersAttachmentList.getLength(); idx++) {
                final Node othersAttachment = othersAttachmentList.item(idx);

                final Element otherDocumentJustificatif = (Element) document.createElement("documentJustificatif");
                final Element otherCodeTypeDocumentJustificatif = (Element) document.createElement("codeTypeDocumentJustificatif");
                otherCodeTypeDocumentJustificatif.setTextContent("PJ");
                otherDocumentJustificatif.appendChild(otherCodeTypeDocumentJustificatif);

                final Element otherFichierPieceJointe = (Element) document.createElement("fichierPieceJointe");
                otherFichierPieceJointe.setTextContent(othersAttachment.getTextContent());
                otherDocumentJustificatif.appendChild(otherFichierPieceJointe);
                formalite.appendChild(otherDocumentJustificatif);
            }
            // <--

            root.appendChild(formalite);
            // <--

            document.appendChild(root);

            final Map<String, byte[]> filesToInsert = new HashMap<>();
            filesToInsert.put(xmltcEntry.name(), XPathBuilder.asBytes(document));

            LOGGER.info("Rebuild XMLTC from scratch with success");
            return ZipUtil.entries(filesToInsert, resourceAsBytes);

        } catch (TechnicalException | ParserConfigurationException e) {
            LOGGER.warn("Cannot rebuild XMLTC from scratch", e);
        }
        // <--
        return resourceAsBytes;
    }
}
