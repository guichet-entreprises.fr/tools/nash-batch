package fr.ge.common.nash.batch.record.report.model.majdu;

import java.util.Collection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * @author Christian Cougourdan
 */
@XmlAccessorType(XmlAccessType.NONE)
public class MajEtatSes {

    @XmlElement(name = "typeDossierMisAJour", required = true)
    private String typeDossierMisAJour;

    @XmlElement(name = "etatDossier", required = true)
    private Collection<EtatDossier> etatsDossier;

    /**
     * @return the typeDossierMisAJour
     */
    public String getTypeDossierMisAJour() {
        return this.typeDossierMisAJour;
    }

    /**
     * @return the etatsDossier
     */
    public Collection<EtatDossier> getEtatsDossier() {
        return this.etatsDossier;
    }

}
