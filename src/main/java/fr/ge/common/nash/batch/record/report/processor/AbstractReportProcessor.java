package fr.ge.common.nash.batch.record.report.processor;

import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.Collection;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.vfs2.FileSystemException;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import fr.ge.common.nash.batch.record.report.model.common.Erreur;
import fr.ge.common.nash.batch.utils.monitor.ClickerService;
import fr.ge.common.nash.batch.utils.monitor.TrackerService;
import fr.ge.common.nash.batch.utils.vfs.VfsResource;
import fr.ge.common.nash.core.util.JaxbFactoryImpl;
import fr.ge.common.utils.CoreUtil;
import fr.ge.common.utils.exception.TechnicalException;

/**
 * @author Christian Cougourdan
 */
public abstract class AbstractReportProcessor<T> implements ItemProcessor<VfsResource, VfsResource>, InitializingBean {

    @Value("${ws.tracker.support.path:{yyyy-MM}.SCNGE/{yyyy-MM}.SPT}")
    private String trackerSupportPath;

    /** TRACKER info reference for support teams */
    protected String trackerSupportRefInfo;

    /** TRACKER error reference for support teams */
    protected String trackerSupportRefError;

    @Autowired
    protected TrackerService tracker;

    @Autowired
    protected ClickerService clicker;

    private final Class<T> expectedDocumentClass;

    public AbstractReportProcessor(final Class<T> expectedDocumentClass) {
        this.expectedDocumentClass = expectedDocumentClass;
    }

    private void initTrackerSupportPath() {
        if (StringUtils.isEmpty(this.trackerSupportPath)) {
            return;
        }

        final TemporalAccessor now = LocalDateTime.now();
        final String refTemplate = (null == this.trackerSupportPath || this.trackerSupportPath.indexOf("{") < 0) ? "{yyyy-MM}.SCNGE/{yyyy-MM}.SPT" : this.trackerSupportPath;
        final String refPath = CoreUtil.searchAndReplace(refTemplate, "\\{([^}]+)\\}", m -> {
            final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(m.group(1));
            return formatter.format(now);
        });

        final String trackerSupportRef = this.tracker.create(refPath);
        if (null == trackerSupportRef) {
            throw new TechnicalException("Unable to initialize TRACKER support references");
        } else {
            this.trackerSupportRefInfo = this.tracker.create(trackerSupportRef + ".INFO", trackerSupportRef);
            this.trackerSupportRefError = this.tracker.create(trackerSupportRef + ".ERROR", trackerSupportRef);
        }
    }

    protected T load(final VfsResource item) throws FileSystemException {
        return JaxbFactoryImpl.instance().unmarshal(item.getInputStream(), this.expectedDocumentClass);
    }

    @Override
    public abstract VfsResource process(final VfsResource item) throws Exception;

    protected abstract boolean processDocument(T doc) throws Exception;

    @Override
    public void afterPropertiesSet() throws Exception {
        this.initTrackerSupportPath();
    }

    protected int errorsAsString(final StringBuilder builder, final Collection<Erreur> erreurs) {
        if (CollectionUtils.isNotEmpty(erreurs)) {
            final StringBuilder msg = null == builder ? new StringBuilder() : builder;
            for (final Erreur err : erreurs) {
                msg.append(String.format("\n    - [%s] %s", err.getCodeErreur(), err.getLibelleErreur()));
                if (StringUtils.isNotEmpty(err.getFichierEnErreur())) {
                    msg.append("\n        * fichier : ").append(err.getFichierEnErreur());
                }
                if (StringUtils.isNotEmpty(err.getRubriqueEnErreur())) {
                    msg.append("\n        * rubrique : ").append(err.getRubriqueEnErreur());
                }
                if (StringUtils.isNotEmpty(err.getValeurEnErreur())) {
                    msg.append("\n        * valeur : ").append(err.getValeurEnErreur());
                }
            }

            return erreurs.size();
        } else {
            return 0;
        }
    }

    protected <R> R assertNotNull(final R value, final String message, final Object... params) {
        if (null == value) {
            final String formattedMessage = MessageFormat.format(message, params);
            LoggerFactory.getLogger(this.getClass()).info(formattedMessage);
            throw new TechnicalException(formattedMessage);
        } else {
            return value;
        }
    }

}
