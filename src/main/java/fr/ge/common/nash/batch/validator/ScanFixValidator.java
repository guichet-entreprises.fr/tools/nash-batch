/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.batch.validator;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.apache.commons.collections4.ListUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import fr.ge.common.nash.batch.bean.Error;
import fr.ge.common.nash.batch.bean.Error.Level;
import fr.ge.common.nash.batch.bean.Errors;
import fr.ge.common.utils.exception.TechnicalException;

/**
 * XMLTC process validation.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@Service
public class ScanFixValidator extends AbstractValidator {

    private static final Logger LOGGER = LoggerFactory.getLogger(ScanFixValidator.class);

    /** Consistency validators. */
    private final List<IFunctionalValidator> CONSISTENCY_HIGHER_VALIDATORS = Arrays.asList( //
            new XMLRegentEntryValidator(), //
            new XMLTCEntryValidator() //
    // new AssignableGreffeValidator() //
    );

    /** Consistency validators. */
    private final List<IFunctionalValidator> CONSISTENCY_MAJOR_VALIDATORS = Arrays.asList( //
            new XMLTCRegenerationGreffeValidator(), //
            new DestinataireTagValidator(), //
            new EmetteurTagValidator(), //
            new TypeDestinataireTagValidator(), //
            new MissingBlocPaiementValidator() //
    );

    /** Functional validators. */
    private final List<IFunctionalValidator> FUNCTIONAL_VALIDATORS = Arrays.asList( //
            new XMLTCRegenerationValidator(), //
            new CompareEntryZipValidator(), //
            new ExtensionFileValidator(), //
            new RenameEntryValidator(), //
            new RenameEntryV2Validator(), //
            new DestinataireDossierCfeTagValidator(), //
            new DestinataireTagValidator(), //
            new EmetteurTagValidator(), //
            new CodePaysTagValidator(), //
            new RemovePaiementTagValidator(), //
            new TotalAmountValidator(), //
            new C05TagValidator(), //
            new PDFConvertorValidator(), //
            new TypeDeVoieTagValidator(), //
            new TypePieceJointeValidator(), //
            new TypePieceJointeRCSBEValidator() //
    );

    /**
     * Constructor.
     */
    public ScanFixValidator() {
        // Nothing to do.
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Errors validate(final Properties appProperties, final byte[] resourceAsBytes) {
        final Errors errors = new Errors();
        ListUtils.union(CONSISTENCY_HIGHER_VALIDATORS, CONSISTENCY_MAJOR_VALIDATORS).forEach(validator -> {
            if (validator.validateConditions(resourceAsBytes)) { //
                try {
                    errors.merge("DMTDU", validator.validate(appProperties, resourceAsBytes));
                } catch (TechnicalException e) {
                    LOGGER.error("Error executing validation", e);
                    errors.add(new Error(Level.ERROR, "Error executing validation"));
                }
            }
        });

        if (!errors.hasErrors()) {
            FUNCTIONAL_VALIDATORS.forEach(validator -> {
                if (validator.validateConditions(resourceAsBytes)) { //
                    try {
                        errors.merge("DMTDU", validator.validate(appProperties, resourceAsBytes));
                    } catch (TechnicalException e) {
                        LOGGER.error("Error executing validation", e);
                        errors.add(new Error(Level.ERROR, "Error executing validation"));
                    }
                }
            });
        }

        if (errors.hasErrors()) {
            LOGGER.error("Scan KO");
            errors.getObjectErrors().forEach((key, values) -> values.forEach(err -> LOGGER.error(err.toString())));
        } else {
            LOGGER.info("Scan OK");
        }
        return errors;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public byte[] fix(final Properties appProperties, final byte[] resourceAsBytes) {
        final Errors errors = new Errors();
        CONSISTENCY_HIGHER_VALIDATORS.forEach(validator -> {
            if (validator.validateConditions(resourceAsBytes)) { //
                errors.merge("DMTDU", validator.validate(appProperties, resourceAsBytes));
            }
        });

        if (errors.hasErrors()) {
            LOGGER.error("The input file has consistency errors. The batch cannot fix that file.");
            return null;
        }

        byte[] outputAsBytes = resourceAsBytes;
        for (final IFunctionalValidator validator : ListUtils.union(CONSISTENCY_MAJOR_VALIDATORS, FUNCTIONAL_VALIDATORS)) {
            if (validator.validateConditions(resourceAsBytes)) {
                try {
                    outputAsBytes = validator.fix(appProperties, outputAsBytes);
                } catch (TechnicalException e) {
                    LOGGER.error("Error during fixing file", e);
                    return null;
                }
            }
        }
        return outputAsBytes;
    }

}
