package fr.ge.common.nash.batch.record.report.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import fr.ge.common.nash.batch.record.report.model.arfic.ARFicDocument;
import fr.ge.common.nash.batch.utils.RecordUtils;

/**
 * @author Christian Cougourdan
 */
public class AckProcessor extends AbstractSingleReportProcessor<ARFicDocument> {

    private final Logger logger = LoggerFactory.getLogger(AckProcessor.class);

    @Value("${ws.clicker.events.arfic:SES_ARFIC}")
    private String clickerEvent;

    public AckProcessor() {
        super(ARFicDocument.class);
    }

    @Override
    public boolean processDocument(final ARFicDocument doc) throws Exception {
        final String nomFichierRecu = this.assertNotNull(doc.getNomFichierRecu(), "Received file name must be specified");
        final String numeroLiasse = this.assertNotNull(RecordUtils.findNumeroLiasseFromFileName(nomFichierRecu), "Record number not found from filename {0}", nomFichierRecu);

        this.clicker.notify(this.clickerEvent);

        this.tracker.notify( //
                numeroLiasse, //
                "Le fichier %s sous la référence [%s] a été reçu par le SES.", //
                nomFichierRecu, numeroLiasse //
        );

        this.tracker.notify( //
                this.trackerSupportRefInfo, //
                "Le fichier %s sous la référence [%s] a été reçu par le SES.", //
                nomFichierRecu, numeroLiasse //
        );

        return false;
    }

}
