package fr.ge.common.nash.batch.record.report.listener;

import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.FileSystemManager;
import org.apache.commons.vfs2.impl.StandardFileSystemManager;
import org.jboss.logging.MDC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ItemProcessListener;
import org.springframework.beans.factory.InitializingBean;

import fr.ge.common.nash.batch.utils.RecordUtils;
import fr.ge.common.nash.batch.utils.vfs.VfsResource;
import fr.ge.common.utils.exception.TechnicalException;

public class ErrorListener implements ItemProcessListener<VfsResource, VfsResource>, InitializingBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(ErrorListener.class);

    protected FileSystemManager fs;

    private String target;

    private FileObject targetObject;

    @Override
    public void beforeProcess(final VfsResource item) {
        MDC.put("record", RecordUtils.findNumeroLiasseFromFileName(item.getBasename()));
    }

    @Override
    public void afterProcess(final VfsResource item, final VfsResource result) {
        MDC.remove("record");
    }

    @Override
    public void onProcessError(final VfsResource item, final Exception e) {
        LOGGER.debug("Error processing resource {} : {}", item.getBasename(), e.getMessage());
        try {
            item.moveTo(this.targetObject);
        } catch (final FileSystemException ex) {
            LOGGER.warn("Writing error item {} to {} : {}", item.getBasename(), this.targetObject.getName().getBaseName(), ex.getMessage());
        }
        MDC.remove("record");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        try {
            final StandardFileSystemManager stdFileSystem = new StandardFileSystemManager();
            stdFileSystem.init();
            this.fs = stdFileSystem;
            this.targetObject = this.fs.resolveFile(this.target);
        } catch (final FileSystemException ex) {
            throw new TechnicalException("Virtual Filesystem initialization", ex);
        }
    }

    /**
     * @param target
     *            the target to set
     */
    public void setTarget(final String target) {
        this.target = target;
    }

}
