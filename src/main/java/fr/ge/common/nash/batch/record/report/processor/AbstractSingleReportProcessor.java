package fr.ge.common.nash.batch.record.report.processor;

import fr.ge.common.nash.batch.utils.vfs.VfsResource;

/**
 * @author Christian Cougourdan
 * @param <T>
 */
public abstract class AbstractSingleReportProcessor<T> extends AbstractReportProcessor<T> {

    public AbstractSingleReportProcessor(final Class<T> expectedDocumentClass) {
        super(expectedDocumentClass);
    }

    @Override
    public VfsResource process(final VfsResource item) throws Exception {
        this.processDocument(this.load(item));

        return item;
    }

}
