package fr.ge.common.nash.batch.utils.vfs;

import org.springframework.beans.factory.annotation.Value;

public final class SftpConfiguration {

    @Value("${vfs.sftp.host:null}")
    private final String host = "localhost";

    @Value("${vfs.sftp.port:-1}")
    private final int port = -1;

    @Value("${vfs.sftp.username:null}")
    private final String username = "geadmin";

    @Value("${vfs.sftp.password:null}")
    private final String password = "gepassword";

    private final String strictHostKeyChecking = "no";

    private final boolean userDirIsRoot = false;

    private final int connectTimeout = 3000;

    /**
     * @return the host
     */
    public String getHost() {
        return this.host;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return this.username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return this.password;
    }

    /**
     * @return the port
     */
    public int getPort() {
        return this.port;
    }

    /**
     * @return the strictHostKeyChecking
     */
    public String getStrictHostKeyChecking() {
        return this.strictHostKeyChecking;
    }

    /**
     * @return the userDirIsRoot
     */
    public boolean isUserDirIsRoot() {
        return this.userDirIsRoot;
    }

    /**
     * @return the connectTimeout
     */
    public int getConnectTimeout() {
        return this.connectTimeout;
    }

}
