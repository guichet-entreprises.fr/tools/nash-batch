/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.batch.utils;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import fr.ge.common.nash.batch.validator.IFunctionalValidator;
import fr.ge.common.utils.bean.FileEntry;
import fr.ge.common.utils.exception.TechnicalException;

/**
 * The XMLTC Query class.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public final class XMLTCQuery {

    private static final Logger LOGGER = LoggerFactory.getLogger(XMLTCQuery.class);

    /**
     * 
     * Constructeur de la classe.
     *
     */
    private XMLTCQuery() {
    }

    /**
     * Extract file name as list.
     * 
     * @param xmltcEntry
     *            The XMLTC file entry
     * @return file name as list
     */
    public static List<String> extractFichierPieceJointe(final FileEntry xmltcEntry) {
        final List<String> pjs = new ArrayList<String>();
        try {
            final Document document = XPathBuilder.buildDocument(xmltcEntry.asBytes());
            final NodeList pieceJointelist = XPathBuilder.queryNodeList(document.getDocumentElement(), "//" + IFunctionalValidator.TAG_DOSSIER_UNIQUE + "/" + IFunctionalValidator.TAG_PIECE_JOINTE);

            for (int idx = 0; idx < pieceJointelist.getLength(); idx++) {
                final Node pieceJointe = pieceJointelist.item(idx);
                final String fichierPieceJointe = XPathBuilder.queryString(pieceJointe, "fichierPieceJointe");
                pjs.add(fichierPieceJointe);
            }

            return pjs;

        } catch (TechnicalException e) {
            LOGGER.warn("Cannot extract list of tag 'fichierPieceJointe' from document", e);
        }
        return pjs;
    }

    /**
     * Extract denomination from XML TC file as string.
     * 
     * @param xmlRegentEntry
     *            The XML TC file entry
     * @return the denomination
     */
    public static String extractDenominationAsString(final FileEntry xmltcEntry) {
        final Node node = extractDenominationAsNode(xmltcEntry);
        return (null != node && StringUtils.isNotEmpty(node.getTextContent())) ? node.getTextContent() : null;
    }

    /**
     * Extract denomination from XML TC file as node.
     * 
     * @param xmlRegentEntry
     *            The XML TC file entry
     * @return the denomination
     */
    public static Node extractDenominationAsNode(final FileEntry xmltcEntry) {
        try {
            final Document document = XPathBuilder.buildDocument(xmltcEntry.asBytes());
            return XPathBuilder.queryNode(document.getDocumentElement(), "//GUEN_DMTDU/dossierUnique/identificationDossierUnique/nomDossier");
        } catch (TechnicalException e) {
            LOGGER.warn("Cannot extract denomination from XML TC", e);
        }

        return null;
    }

    /**
     * Extract destinataireDossierCfe from XML TC file as node.
     * 
     * @param xmlRegentEntry
     *            The XML TC file entry
     * @return the destinataireDossierCfe as node
     */
    public static Node extractDestinataireDossierCfeAsNode(final FileEntry xmltcEntry, final String codeEdi) {
        try {
            final Document document = XPathBuilder.buildDocument(xmltcEntry.asBytes());
            final String expression = "//GUEN_DMTDU/dossierUnique/dossierCfe/destinataireDossierCfe[codeEdiDestinataire='" + codeEdi + "']";
            return XPathBuilder.queryNode(document.getDocumentElement(), expression);
        } catch (TechnicalException e) {
            LOGGER.warn("Cannot extract denomination from XML TC", e);
        }

        return null;
    }

    public static NodeList extractPieceJointeIndicePieceJointeAsNodeList(final FileEntry xmltcEntry) {
        try {
            final Document document = XPathBuilder.buildDocument(xmltcEntry.asBytes());
            return XPathBuilder.queryNodeList(document.getDocumentElement(), "//GUEN_DMTDU/dossierUnique/pieceJointe/indicePieceJointe");
        } catch (TechnicalException e) {
            LOGGER.warn("Cannot extract denomination from XML TC", e);
        }

        return null;
    }

    /**
     * Extract numeroDossierUnique from XML TC file as node.
     * 
     * @param xmlRegentEntry
     *            The XML TC file entry
     * @return the record identifier
     */
    public static Node extractNumeroDossierUnique(final FileEntry xmltcEntry) {
        try {
            final Document document = XPathBuilder.buildDocument(xmltcEntry.asBytes());
            return XPathBuilder.queryNode(document.getDocumentElement(), "//GUEN_DMTDU/dossierUnique/identificationDossierUnique/identifiantDossierUnique/numeroDossierUnique");
        } catch (TechnicalException e) {
            LOGGER.warn("Cannot extract denomination from XML TC", e);
        }
        return null;
    }

    /**
     * Extract codeDestinataire from XML TC file as node.
     * 
     * @param xmlRegentEntry
     *            The XML TC file entry
     * @return the code destinataire as node
     */
    public static Node extractCodeDestinataireAsNode(final FileEntry xmltcEntry, final String ediCode) {
        try {
            final Document document = XPathBuilder.buildDocument(xmltcEntry.asBytes());
            return XPathBuilder.queryNode(document.getDocumentElement(), "//GUEN_DMTDU/dossierUnique/destinataireDossierUnique/codeDestinataire[codeEdi='" + ediCode + "']");
        } catch (TechnicalException e) {
            LOGGER.warn("Cannot extract codeDestinataire tag from XML TC", e);
        }

        return null;
    }

    /**
     * Extract libelleIntervenant tag from XML TC file as string.
     * 
     * @param xmlRegentEntry
     *            The XML TC file entry
     * @param ediCode
     *            The EDI identifier
     * @return the libelle intervenant as string
     */
    public static String extractLibelleIntervenantAsString(final FileEntry xmltcEntry, final String ediCode) {
        try {
            final Node codeDestinataire = extractCodeDestinataireAsNode(xmltcEntry, ediCode);
            return XPathBuilder.queryString(codeDestinataire, "libelleIntervenant");
        } catch (TechnicalException e) {
            LOGGER.warn("Cannot extract libelleIntervenant tag from XML TC", e);
        }
        return null;
    }

    /**
     * Extract code pas value from XML TC file as node.
     * 
     * @param xmlRegentEntry
     *            The XML TC file entry
     * @return the code destinataire as node
     */
    public static Node extractCorrespondantCodePaysAsNode(final FileEntry xmltcEntry) {
        try {
            final Document document = XPathBuilder.buildDocument(xmltcEntry.asBytes());
            return XPathBuilder.queryNode(document.getDocumentElement(), "//GUEN_DMTDU/dossierUnique/identificationDossierUnique/correspondant/adresseCorrespondant/codePays");
        } catch (TechnicalException e) {
            LOGGER.warn("Cannot extract codeDestinataire tag from XML TC", e);
        }

        return null;
    }

    /**
     * Extract referencePaiement tag from XML TC file as string.
     * 
     * @param xmlRegentEntry
     *            The XML TC file entry
     * @param ediCode
     *            The EDI identifier
     * @return the paiement reference as string
     */
    public static String extractReferencePaiementAsString(final FileEntry xmltcEntry, final String ediCode) {
        try {
            final Document document = XPathBuilder.buildDocument(xmltcEntry.asBytes());
            final String expression = "//GUEN_DMTDU/dossierUnique/dossierCfe/destinataireDossierCfe[codeEdiDestinataire='" + ediCode + "']/paiementDossier/referencePaiement/text()";
            return XPathBuilder.queryString(document.getDocumentElement(), expression);
        } catch (TechnicalException e) {
            LOGGER.warn("Cannot extract libelleIntervenant tag from XML TC", e);
        }
        return null;
    }
}
