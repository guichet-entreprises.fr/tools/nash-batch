package fr.ge.common.nash.batch.record.report.model.majdu;

import java.util.Collection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * @author Christian Cougourdan
 */
@XmlAccessorType(XmlAccessType.NONE)
public class DossierUnique {

    @XmlElement(name = "identifiantDossierUnique", required = true)
    private IdentifiantDossierUnique identifiantDossierUnique;

    @XmlElement(name = "emetteurMiseAJour", required = true)
    private EmetteurMiseAJour emetteurMiseAJour;

    @XmlElement(name = "indiceTransmission")
    private IndiceTransmission indiceTransmission;

    @XmlElement(name = "nomDossier")
    private String nomDossier;

    @XmlElement(name = "majEtatSes")
    private Collection<MajEtatSes> majEtatsSes;

    /**
     * @return the identifiantDossierUnique
     */
    public IdentifiantDossierUnique getIdentifiantDossierUnique() {
        return this.identifiantDossierUnique;
    }

    /**
     * @return the emetteurMiseAJour
     */
    public EmetteurMiseAJour getEmetteurMiseAJour() {
        return this.emetteurMiseAJour;
    }

    /**
     * @return the indiceTransmission
     */
    public IndiceTransmission getIndiceTransmission() {
        return this.indiceTransmission;
    }

    /**
     * @return the nomDossier
     */
    public String getNomDossier() {
        return this.nomDossier;
    }

    /**
     * @return the majEtatSes
     */
    public Collection<MajEtatSes> getMajEtatsSes() {
        return this.majEtatsSes;
    }

}
