/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.batch.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import fr.ge.common.utils.exception.TechnicalException;

/**
 * The XPath class builder.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public final class XPathBuilder {

    /**
     * 
     * Constructeur de la classe.
     *
     */
    private XPathBuilder() {
    }

    public static Document buildDocument(final byte[] resourceXmlAsBytes) throws TechnicalException {
        try {
            final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
            factory.setFeature("http://xml.org/sax/features/external-general-entities", false);
            factory.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
            final DocumentBuilder builder = factory.newDocumentBuilder();
            return builder.parse(new ByteArrayInputStream(resourceXmlAsBytes));
        } catch (ParserConfigurationException | SAXException | IOException e) {
            throw new TechnicalException("Cannot read resource", e);
        }
    }

    public static XPath buildXPath() {
        final XPathFactory xpf = XPathFactory.newInstance();
        return xpf.newXPath();
    }

    public static NodeList queryNodeList(final Object item, final String expression) throws TechnicalException {
        try {
            return (NodeList) buildXPath().evaluate(expression, item, XPathConstants.NODESET);
        } catch (XPathExpressionException e) {
            throw new TechnicalException(String.format("Cannot evaluate expression %s", expression), e);
        }
    }

    public static Node queryNode(final Object item, final String expression) throws TechnicalException {
        try {
            return (Node) buildXPath().evaluate(expression, item, XPathConstants.NODE);
        } catch (XPathExpressionException e) {
            throw new TechnicalException(String.format("Cannot evaluate expression %s", expression), e);
        }
    }

    public static String queryString(final Object item, final String expression) throws TechnicalException {
        try {
            return (String) buildXPath().evaluate(expression, item, XPathConstants.STRING);
        } catch (XPathExpressionException e) {
            throw new TechnicalException(String.format("Cannot evaluate expression %s", expression), e);
        }
    }

    public static byte[] asBytes(final Document document) throws TechnicalException {
        final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        final Source xmlSource = new DOMSource(document);
        final Result outputTarget = new StreamResult(outputStream);
        try {
            TransformerFactory.newInstance().newTransformer().transform(xmlSource, outputTarget);
        } catch (TransformerException | TransformerFactoryConfigurationError e) {
            throw new TechnicalException("Cannot convert document to byte array", e);
        }

        return outputStream.toByteArray();
    }

    public static Integer queryInt(final Object item, final String expression) throws TechnicalException {
        try {
            return ((Double) buildXPath().evaluate(expression, item, XPathConstants.NUMBER)).intValue();
        } catch (XPathExpressionException e) {
            throw new TechnicalException(String.format("Cannot evaluate expression %s", expression), e);
        }
    }
}
