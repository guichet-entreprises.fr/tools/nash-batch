/**
 * 
 */
package fr.ge.common.nash.batch.purge.proxy;

import java.time.LocalDate;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import fr.ge.common.nash.batch.common.AbstractSearchQueryBuilder;
import fr.ge.common.proxy.payment.ws.v1.service.ICartService;
import fr.ge.common.proxy.service.IProxySessionService;

/**
 * Signature Tasklet.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class ProxyTasklet implements Tasklet {

    private AbstractSearchQueryBuilder searchQueryBuilder;

    private IProxySessionService proxySignatureSessionService;

    private ICartService cartService;

    /**
     * Mutateur sur l'attribut {@link #searchQueryBuilder}.
     *
     * @param searchQueryBuilder
     *            la nouvelle valeur de l'attribut searchQueryBuilder
     */
    public void setSearchQueryBuilder(final AbstractSearchQueryBuilder searchQueryBuilder) {
        this.searchQueryBuilder = searchQueryBuilder;
    }

    /**
     * Mutateur sur l'attribut {@link #proxySignatureSessionService}.
     *
     * @param proxySignatureSessionService
     *            la nouvelle valeur de l'attribut proxySignatureSessionService
     */
    public void setProxySignatureSessionService(final IProxySessionService proxySignatureSessionService) {
        this.proxySignatureSessionService = proxySignatureSessionService;
    }

    /**
     * Mutateur sur l'attribut {@link #cartService}.
     *
     * @param cartService
     *            la nouvelle valeur de l'attribut cartService
     */
    public void setCartService(final ICartService cartService) {
        this.cartService = cartService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RepeatStatus execute(final StepContribution contribution, final ChunkContext chunkContext) throws Exception {
        final String fromDate = LocalDate.now().minusDays(this.searchQueryBuilder.getMinusDaysMin()).toString();
        this.proxySignatureSessionService.remove(fromDate, null);
        this.cartService.remove(fromDate, null);
        return RepeatStatus.FINISHED;
    }

}
