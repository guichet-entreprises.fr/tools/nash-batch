package fr.ge.common.nash.batch.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Christian Cougourdan
 */
public final class RecordUtils {

    private static final Pattern SOURCE_FILE_PATTERN = Pattern.compile("^C.{4}A.{4}L(.{6})D.{8}H.{6}T(.{5})P(.{4})\\.(?:[^.]+)$");

    public static String findNumeroLiasseFromFileName(final String nomFichierRecu) {
        final Matcher m = SOURCE_FILE_PATTERN.matcher(nomFichierRecu);
        if (m.matches()) {
            return "H10000" + m.group(1);
        } else {
            return null;
        }
    }

}
