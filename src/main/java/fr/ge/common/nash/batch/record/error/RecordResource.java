package fr.ge.common.nash.batch.record.error;

/**
 *
 * @author Christian Cougourdan
 */
public class RecordResource {

    private final String path;

    public RecordResource(final String path) {
        this.path = path;
    }

    /**
     * @return the path
     */
    public String getPath() {
        return this.path;
    }

}
