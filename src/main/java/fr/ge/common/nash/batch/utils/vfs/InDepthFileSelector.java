package fr.ge.common.nash.batch.utils.vfs;

import java.util.function.Predicate;
import java.util.regex.Pattern;

import org.apache.commons.vfs2.FileSelectInfo;
import org.apache.commons.vfs2.FileSelector;

/**
 * @author Christian Cougourdan
 */
public class InDepthFileSelector implements FileSelector {

    private final int maxDepth;

    private final Predicate<String> filter;

    public InDepthFileSelector(final int maxDepth) {
        this(maxDepth, (Predicate<String>) null);
    }

    public InDepthFileSelector(final int maxDepth, final Pattern pattern) {
        this(maxDepth, null == pattern ? (str) -> true : pattern.asPredicate());
    }

    public InDepthFileSelector(final int maxDepth, final Predicate<String> filter) {
        this.maxDepth = maxDepth;
        this.filter = filter;
    }

    @Override
    public boolean traverseDescendents(final FileSelectInfo fileInfo) throws Exception {
        return -1 == this.maxDepth || fileInfo.getDepth() <= this.maxDepth;
    }

    @Override
    public boolean includeFile(final FileSelectInfo fileInfo) throws Exception {
        return fileInfo.getFile().isFile() && this.filter.test(fileInfo.getFile().getName().getBaseName());
    }

}