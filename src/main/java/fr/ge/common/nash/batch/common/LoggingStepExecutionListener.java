/**
 *
 */
package fr.ge.common.nash.batch.common;

import org.slf4j.MDC;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;

/**
 * Logging step.
 *
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class LoggingStepExecutionListener implements StepExecutionListener {

    /**
     * {@inheritDoc}
     */
    @Override
    public void beforeStep(final StepExecution stepExecution) {
        MDC.put("job", stepExecution.getJobExecution().getJobInstance().getJobName());
        MDC.put("step", stepExecution.getStepName());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExitStatus afterStep(final StepExecution stepExecution) {
        MDC.remove("step");
        return ExitStatus.COMPLETED;
    }

}
