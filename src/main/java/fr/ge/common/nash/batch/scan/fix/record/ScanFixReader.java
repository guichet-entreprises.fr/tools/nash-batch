package fr.ge.common.nash.batch.scan.fix.record;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.tracker.bean.Message;
import fr.ge.tracker.service.v2.IMessageService;

/**
 * Scan & fix batch reader.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@Component(value = "scanFixMessageReader")
public class ScanFixReader implements ItemReader<Message>, InitializingBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(ScanFixReader.class);

    /** Tracker message service. **/
    private IMessageService messageService;

    /** Tracker message iterator. **/
    private Iterator<Message> recordIterator;

    /** Tracker messages references to search. **/
    private List<String> references;

    /** Tracker date before. **/
    private String borneInf;

    /** Tracker date after. **/
    private String borneSup;

    /** Tracker start index. **/
    private Long startIndex;

    /** Tracker max results. **/
    private Long maxResults;

    /**
     * Mutateur sur l'attribut {@link #references}.
     *
     * @param references
     *            la nouvelle valeur de l'attribut references
     */
    public void setReferences(final List<String> references) {
        this.references = references;
    }

    /**
     * Mutateur sur l'attribut {@link #messageService}.
     *
     * @param messageService
     *            la nouvelle valeur de l'attribut messageService
     */
    public void setMessageService(final IMessageService messageService) {
        this.messageService = messageService;
    }

    /**
     * Mutateur sur l'attribut {@link #borneInf}.
     *
     * @param borneInf
     *            la nouvelle valeur de l'attribut borneInf
     */
    public void setBorneInf(final String borneInf) {
        this.borneInf = borneInf;
    }

    /**
     * Mutateur sur l'attribut {@link #borneSup}.
     *
     * @param borneSup
     *            la nouvelle valeur de l'attribut borneSup
     */
    public void setBorneSup(final String borneSup) {
        this.borneSup = borneSup;
    }

    /**
     * Mutateur sur l'attribut {@link #startIndex}.
     *
     * @param startIndex
     *            la nouvelle valeur de l'attribut startIndex
     */
    public void setStartIndex(final Long startIndex) {
        this.startIndex = startIndex;
    }

    /**
     * Mutateur sur l'attribut {@link #maxResults}.
     *
     * @param maxResults
     *            la nouvelle valeur de l'attribut maxResults
     */
    public void setMaxResults(final Long maxResults) {
        this.maxResults = maxResults;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        if (null != this.startIndex && null != this.maxResults) {
            LOGGER.info("Search Tracker message with references {}", this.references);
            LOGGER.info("Start index : {}", this.startIndex);
            LOGGER.info("Max results : {}", this.maxResults);
            LOGGER.info("Borne inf : {}", this.borneInf);
            LOGGER.info("Borne supp : {}", this.borneSup);

            // -->Build filters
            final List<SearchQueryFilter> filters = new ArrayList<SearchQueryFilter>();
            filters.add(new SearchQueryFilter("value", ":", this.references.stream().collect(Collectors.joining(", "))));

            if (StringUtils.isNotEmpty(this.borneInf)) {
                filters.add(new SearchQueryFilter("created", ">=", this.borneInf));
            }
            if (StringUtils.isNotEmpty(this.borneSup)) {
                filters.add(new SearchQueryFilter("created", "<=", this.borneSup));
            }

            LOGGER.info("Search Tracker messages with startIndex : {}, max results : {}, filters : {}", this.startIndex, this.maxResults, filters);
            final SearchResult<Message> searchResult = this.messageService.searchMessage(this.startIndex, //
                    this.maxResults, //
                    filters, //
                    Arrays.asList(new SearchQueryOrder("created", "asc")) //
            );
            this.recordIterator = Optional.ofNullable(searchResult) //
                    .filter(r -> null != r.getContent()) //
                    .map(s -> {
                        LOGGER.debug("Total messages : {}", s.getTotalResults());
                        return s;
                    }) //
                    .map(SearchResult::getContent) //
                    .orElse(new ArrayList<Message>()) //
                    .stream() //
                    .iterator();
        }
    }

    @Override
    public Message read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
        if (null != this.recordIterator && this.recordIterator.hasNext()) {
            return this.recordIterator.next();
        }
        return null;
    }
}
