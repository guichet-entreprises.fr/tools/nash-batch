package fr.ge.common.nash.batch.record.report.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import fr.ge.common.nash.batch.record.report.model.majdu.DossierUnique;
import fr.ge.common.nash.batch.record.report.model.majdu.EtatDossier;
import fr.ge.common.nash.batch.record.report.model.majdu.MajEtatSes;
import fr.ge.common.nash.batch.record.report.model.majdu.MajduDocument;

/**
 * @author Christian Cougourdan
 */
public class FunctionalControlProcessor extends AbstractMultipleReportProcessor<MajduDocument> {

    private final Logger logger = LoggerFactory.getLogger(FunctionalControlProcessor.class);

    @Value("${ws.clicker.events.majdu.ok:SES_MAJDU_OK}")
    private String clickerEventOk;

    @Value("${ws.clicker.events.majdu.ko:SES_MAJDU_KO}")
    private String clickerEventKo;

    public FunctionalControlProcessor() {
        super(MajduDocument.class);
    }

    @Override
    protected boolean processDocument(final MajduDocument doc) throws Exception {
        for (final DossierUnique dossierUnique : doc.getDossiersUnique()) {
            this.assertNotNull(dossierUnique, "Unique record not found");

            final String numeroDossier = this.assertNotNull(dossierUnique.getIdentifiantDossierUnique().getNumeroDossierUnique().replaceFirst("^DOSSIER_", ""), "Record number not found");

            for (final MajEtatSes etatSes : dossierUnique.getMajEtatsSes()) {
                this.assertNotNull(etatSes, "SES status is mandatory");

                for (final EtatDossier etatDossier : etatSes.getEtatsDossier()) {
                    this.assertNotNull(etatDossier, "Record status is mandatory");

                    final StringBuilder msgSupport = new StringBuilder();
                    msgSupport.append( //
                            String.format( //
                                    "[%s] [%s] %s", //
                                    etatDossier.getCodeEtat(), //
                                    numeroDossier, //
                                    String.join(", ", etatDossier.getCommentairesEtat()) //
                            ) //
                    );

                    this.tracker.notify( //
                            numeroDossier, //
                            "[%s] %s", //
                            etatDossier.getCodeEtat(), String.join(", ", etatDossier.getCommentairesEtat()) //
                    );

                    final int cnt = this.errorsAsString(msgSupport, etatDossier.getErreursDossier());

                    if (0 == cnt) {
                        this.clicker.notify(this.clickerEventOk);
                        this.tracker.notify(this.trackerSupportRefInfo, msgSupport.toString());
                    } else {
                        this.clicker.notify(this.clickerEventKo);
                        this.tracker.notify(this.trackerSupportRefError, msgSupport.toString());
                    }
                }
            }
        }

        return true;
    }

}
