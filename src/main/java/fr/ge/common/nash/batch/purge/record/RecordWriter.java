package fr.ge.common.nash.batch.purge.record;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.stereotype.Component;

import com.github.jknack.handlebars.Handlebars;
import com.github.jknack.handlebars.io.ClassPathTemplateLoader;

import fr.ge.common.nash.batch.common.EmailTemplateEnum;
import fr.ge.common.nash.batch.validation.AccountService;
import fr.ge.common.nash.ws.v1.bean.RecordInfoBean;
import fr.ge.common.nash.ws.v1.service.IRecordService;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.core.exception.TechniqueException;
import fr.ge.exchange.email.ws.v1.bean.EmailSendBean;
import fr.ge.exchange.email.ws.v1.service.ISendEmailRestService;

/**
 * Record batch writer.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@Component(value = "purgeRecordWriter")
public class RecordWriter implements ItemWriter<RecordInfoBean> {

    private static final Logger LOGGER = LoggerFactory.getLogger(RecordWriter.class);

    private static final String DATE_FORMAT = "dd MMMM yyyy";

    private IRecordService recordServiceClient;

    private AccountService accountRestService;

    private ISendEmailRestService sendEmailRestService;

    private String supportEmailAddress;

    private String sender;

    /**
     * Mutateur sur l'attribut {@link #recordServiceClient}.
     *
     * @param recordServiceClient
     *            la nouvelle valeur de l'attribut recordServiceClient
     */
    public void setRecordServiceClient(final IRecordService recordServiceClient) {
        this.recordServiceClient = recordServiceClient;
    }

    /**
     * Mutateur sur l'attribut {@link #accountRestService}.
     *
     * @param accountRestService
     *            la nouvelle valeur de l'attribut accountRestService
     */
    public void setAccountRestService(final AccountService accountRestService) {
        this.accountRestService = accountRestService;
    }

    /**
     * Mutateur sur l'attribut {@link #sendEmailRestService}.
     *
     * @param sendEmailRestService
     *            la nouvelle valeur de l'attribut sendEmailRestService
     */
    public void setSendEmailRestService(final ISendEmailRestService sendEmailRestService) {
        this.sendEmailRestService = sendEmailRestService;
    }

    /**
     * Mutateur sur l'attribut {@link #supportEmailAddress}.
     *
     * @param supportEmailAddress
     *            la nouvelle valeur de l'attribut supportEmailAddress
     */
    public void setSupportEmailAddress(String supportEmailAddress) {
        this.supportEmailAddress = supportEmailAddress;
    }

    /**
     * Mutateur sur l'attribut {@link #sender}.
     *
     * @param sender
     *            la nouvelle valeur de l'attribut sender
     */
    public void setSender(final String sender) {
        this.sender = sender;
    }

    @Override
    public void write(final List<? extends RecordInfoBean> items) throws Exception {
        final List<String> uids = items.stream() //
                .map(RecordInfoBean::getCode) //
                .collect(Collectors.toList());
        this.recordServiceClient.remove(Arrays.asList(new SearchQueryFilter("uid", ":", uids)));
        LOGGER.info("Removing nash record uids : {}", uids);

        for (final RecordInfoBean record : items) {
            try {
                final Map<String, String> userInfo = this.accountRestService.readUser(record.getAuthor());
                userInfo.put("civility", userInfo.getOrDefault("civility", null));
                userInfo.put("firstName", userInfo.getOrDefault("firstName", null));
                userInfo.put("lastName", userInfo.getOrDefault("lastName", null));

                final String userLanguage = userInfo.getOrDefault("language", EmailTemplateEnum.REMOVAL_FR.getLanguage());
                LOGGER.debug("User language : {}", userLanguage);
                final EmailTemplateEnum template = EmailTemplateEnum.getRemovalTemplate(userLanguage);

                userInfo.put("code", record.getCode());

                final LocalDate createdDateRecord = LocalDateTime.ofInstant(record.getCreated().toInstant(), record.getCreated().getTimeZone().toZoneId()).toLocalDate();
                final LocalDate updatedDateRecord = LocalDateTime.ofInstant(record.getCreated().toInstant(), record.getCreated().getTimeZone().toZoneId()).toLocalDate();
                userInfo.put("created", createdDateRecord.format(DateTimeFormatter.ofPattern(DATE_FORMAT, template.getLocale())));
                userInfo.put("updated", updatedDateRecord.format(DateTimeFormatter.ofPattern(DATE_FORMAT, template.getLocale())));

                userInfo.put("support", this.supportEmailAddress);

                final EmailSendBean body = new EmailSendBean();
                body.setSender(this.sender);
                body.setObject(String.format(template.getObject(), record.getCode()));
                body.setRecipient(userInfo.getOrDefault("email", null));
                body.setContent(new Handlebars(new ClassPathTemplateLoader("/templates", ".html")).compile(template.getName()).apply(userInfo));

                this.sendEmailRestService.sendEmail(body);
            } catch (TechniqueException | IOException e) {
                LOGGER.warn(String.format("Cannot notify user for the record %s", record.getCode()), e);
            }
        }
    }
}
