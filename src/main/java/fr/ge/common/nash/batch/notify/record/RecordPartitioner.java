/**
 * 
 */
package fr.ge.common.nash.batch.notify.record;

import fr.ge.common.nash.batch.common.AbstractPartitioner;
import fr.ge.common.nash.ws.v1.bean.search.RecordSearchResult;

/**
 * Record created batch partitioner.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class RecordPartitioner extends AbstractPartitioner<RecordSearchResult> {

    /**
     * {@inheritDoc}
     */
    @Override
    public RecordSearchResult search() {
        return this.recordServiceClient.search(0L, 1L, this.searchQueryBuilder.getFilters(), this.searchQueryBuilder.getOrders());
    }

}
