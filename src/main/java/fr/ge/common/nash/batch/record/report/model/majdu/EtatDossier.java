package fr.ge.common.nash.batch.record.report.model.majdu;

import java.util.Collection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import fr.ge.common.nash.batch.record.report.model.common.Erreur;

@XmlAccessorType(XmlAccessType.NONE)
public class EtatDossier {

    @XmlElement(name = "indiceEtat", required = true)
    private Integer indiceEtat;

    @XmlElement(name = "dateHeureEtat", required = true)
    private String dateHeureEtat;

    @XmlElement(name = "typeEtat", required = true)
    private String typeEtat;

    @XmlElement(name = "codeEtat", required = true)
    private String codeEtat;

    @XmlElement(name = "commentaireEtat")
    private Collection<String> commentairesEtat;

    @XmlElement(name = "erreurDossier")
    private Collection<Erreur> erreursDossier;

    /**
     * @return the indiceEtat
     */
    public Integer getIndiceEtat() {
        return this.indiceEtat;
    }

    /**
     * @return the dateHeureEtat
     */
    public String getDateHeureEtat() {
        return this.dateHeureEtat;
    }

    /**
     * @return the typeEtat
     */
    public String getTypeEtat() {
        return this.typeEtat;
    }

    /**
     * @return the codeEtat
     */
    public String getCodeEtat() {
        return this.codeEtat;
    }

    /**
     * @return the commentairesEtat
     */
    public Collection<String> getCommentairesEtat() {
        return this.commentairesEtat;
    }

    /**
     * @return the erreursDossier
     */
    public Collection<Erreur> getErreursDossier() {
        return this.erreursDossier;
    }

    /**
     * @param erreursDossier
     *            the erreursDossier to set
     */
    public void setErreursDossier(final Collection<Erreur> erreursDossier) {
        this.erreursDossier = erreursDossier;
    }

}
