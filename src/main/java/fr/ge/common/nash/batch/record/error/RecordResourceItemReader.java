package fr.ge.common.nash.batch.record.error;

import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.beans.factory.annotation.Value;

/**
 * Write items from specified root source path. Source parameter HAS TO BE a
 * correct URL, ie 'file:///...' or 'sftp:///...'.
 *
 * @author Christian Cougourdan
 */
public class RecordResourceItemReader implements ItemReader<String> {

    @Value("#{jobParameters['src']:}")
    private String sourceRootPath;

    @Override
    public String read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
        return null;
    }

}
