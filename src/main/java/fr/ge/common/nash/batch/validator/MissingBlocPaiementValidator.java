/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.batch.validator;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.TimeZone;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.fasterxml.jackson.databind.JsonNode;

import fr.ge.common.nash.batch.bean.Error;
import fr.ge.common.nash.batch.bean.Error.Level;
import fr.ge.common.nash.batch.bean.Errors;
import fr.ge.common.nash.batch.utils.ScanFixUtils;
import fr.ge.common.nash.batch.utils.XMLRegentQuery;
import fr.ge.common.nash.batch.utils.XMLTCQuery;
import fr.ge.common.nash.batch.utils.XPathBuilder;
import fr.ge.common.utils.ZipUtil;
import fr.ge.common.utils.bean.FileEntry;
import fr.ge.common.utils.exception.TechnicalException;

/**
 * Remove paiement tag from XMLTC if necessary.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class MissingBlocPaiementValidator implements IFunctionalValidator {

    private static final Logger LOGGER = LoggerFactory.getLogger(MissingBlocPaiementValidator.class);

    private static final Pattern C10_1_TAG = Pattern.compile("^[0-9]{2}M$");

    /**
     * {@inheritDoc}
     */
    @Override
    public Errors validate(final Properties appProperties, final byte[] resourceAsBytes) {
        final Errors errors = new Errors();

        final FileEntry xmlRegentEntry = ScanFixUtils.getXMLRegentEntry(resourceAsBytes);
        if (null == xmlRegentEntry) {
            LOGGER.warn("XML Regent file is missing");
            errors.add(new Error(Level.ERROR, "XML Regent file is missing"));
            return errors;
        }

        final Document document = XPathBuilder.buildDocument(xmlRegentEntry.asBytes());
        final Node c05 = XPathBuilder.queryNode(document.getDocumentElement(), "//REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C05");
        final Node c101 = XPathBuilder.queryNode(document.getDocumentElement(), "//REGENT-XML/ServiceApplicatif/Liasse/Service/EDF/C10/C10.1[1]");

        if ((null != c101 && StringUtils.isNotEmpty(c101.getTextContent()) && C10_1_TAG.matcher(c101.getTextContent()).matches()) || //
                (null != c05 && StringUtils.isNotEmpty(c05.getTextContent()) && "R".equals(c05.getTextContent()))) {
            // -->Checking if payment block is missing
            final String codeEdi = XMLRegentQuery.extractDestinataire(xmlRegentEntry).getTextContent();
            final String expression = "//GUEN_DMTDU/dossierUnique/dossierCfe/destinataireDossierCfe[codeEdiDestinataire='" + codeEdi + "']/paiementDossier";

            final FileEntry xmltcEntry = ScanFixUtils.getXMLTCEntry(resourceAsBytes);
            if (null == xmltcEntry) {
                LOGGER.info("Missing XMLTC file");
                errors.add(new Error(Level.ERROR, "Missing XMLTC file"));
                return errors;
            }

            final Document documentXMLTC = XPathBuilder.buildDocument(xmltcEntry.asBytes());
            final Node paiementDossier = XPathBuilder.queryNode(documentXMLTC.getDocumentElement(), expression);
            if (null == paiementDossier) {
                LOGGER.info("Missing payment block into XMLTC");
                errors.add(new Error(Level.ERROR, "Missing payment block into XMLTC"));
            }
            // <--
        }

        return errors;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public byte[] fix(final Properties appProperties, final byte[] resourceAsBytes) {
        if (!this.validate(appProperties, resourceAsBytes).hasErrors()) {
            return resourceAsBytes;
        }

        final FileEntry xmlRegentEntry = ScanFixUtils.getXMLRegentEntry(resourceAsBytes);
        if (null == xmlRegentEntry) {
            LOGGER.warn("XML Regent file is missing");
            return resourceAsBytes;
        }

        final FileEntry xmltcEntry = ScanFixUtils.getXMLTCEntry(resourceAsBytes);
        if (null == xmltcEntry) {
            LOGGER.info("Missing XMLTC file");
            return resourceAsBytes;
        }

        final String codeEdi = XMLRegentQuery.extractDestinataire(xmlRegentEntry).getTextContent();
        final String recordUid = Optional.ofNullable(XMLTCQuery.extractNumeroDossierUnique(xmltcEntry)) //
                .filter(node -> null != node) //
                .map(Node::getTextContent) //
                .orElse(null);
        if (!IFunctionalValidator.REGEX_RECORD_FORMS_2.matcher(recordUid).matches()) {
            return resourceAsBytes;
        }

        // -->Update total amount from proxy
        final JsonNode carts = this.getCart(appProperties, recordUid);
        if (null == carts) {
            LOGGER.error("Cannot find payment into proxy payment for record '{}", recordUid);
            return resourceAsBytes;
        }

        BigDecimal totalAmount = new BigDecimal("0.00");
        for (int cartIdx = 0; cartIdx < carts.size(); cartIdx++) {
            final JsonNode cart = carts.get(cartIdx);
            final JsonNode items = cart.path("items");
            for (int itemIdx = 0; itemIdx < items.size(); itemIdx++) {
                final JsonNode item = items.get(itemIdx);
                if (item.path("ediCode").asText().equals(codeEdi)) {
                    totalAmount = totalAmount.add(new BigDecimal(item.path("price").textValue()));
                }
            }
        }

        final DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator(',');
        final DecimalFormat formatter = new DecimalFormat("0.00", symbols);
        final String newAmount = formatter.format(totalAmount);

        try {
            final Document document = XPathBuilder.buildDocument(xmltcEntry.asBytes());
            final String expression = "//GUEN_DMTDU/dossierUnique/dossierCfe/destinataireDossierCfe[codeEdiDestinataire='" + codeEdi + "']/indicePieceJointe[last()]";
            final Node indicePieceJointe = XPathBuilder.queryNode(document.getDocumentElement(), expression);
            if (null == indicePieceJointe) {
                LOGGER.warn("Cannot find indicePieceJointe tag for target authority '{}'", codeEdi);
                return resourceAsBytes;
            }

            if (null != XPathBuilder.queryNode(indicePieceJointe.getParentNode(), "paiementDossier")) {
                LOGGER.warn("Find paiementDossier tag for target authority '%s'", codeEdi);
                return resourceAsBytes;
            }

            final JsonNode cart = carts.get(0);
            final Node paiementDossier = document.createElement("paiementDossier");
            // -->identitePayeur
            final Node identitePayeur = document.createElement("identitePayeur");
            final Node nomPayeur = document.createElement("nomPayeur");
            nomPayeur.setTextContent(String.join(" ", cart.path("userLastName").asText(), cart.path("userFirstName").asText()));
            identitePayeur.appendChild(nomPayeur);
            paiementDossier.appendChild(identitePayeur);
            // <--

            // -->adressePayeur
            final Node adressePayeur = document.createElement("adressePayeur");
            final Node adresseCorrespondant = XPathBuilder.queryNode(document.getDocumentElement(), ADRESSE_CORRESPONDANT);
            final NodeList adresseCorrespondantChilds = adresseCorrespondant.getChildNodes();
            for (int i = 0; i < adresseCorrespondantChilds.getLength(); i++) {
                adressePayeur.appendChild(adresseCorrespondantChilds.item(i).cloneNode(true));
            }
            paiementDossier.appendChild(adressePayeur);
            // <--

            final Node montantFormalite = document.createElement("montantFormalite");
            montantFormalite.setTextContent(newAmount);
            paiementDossier.appendChild(montantFormalite);

            final Node referencePaiement = document.createElement("referencePaiement");
            referencePaiement.setTextContent(cart.path("orderId").asText().substring(0, 14));
            paiementDossier.appendChild(referencePaiement);

            // -->TODO
            final Node dateHeurePaiement = document.createElement("dateHeurePaiement");
            final DateTimeFormatter dtFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
            final LocalDateTime localDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(cart.path("created").asLong()), TimeZone.getDefault().toZoneId());
            dateHeurePaiement.setTextContent(localDateTime.format(dtFormatter));

            paiementDossier.appendChild(dateHeurePaiement);

            indicePieceJointe.getParentNode().insertBefore(paiementDossier, indicePieceJointe.getNextSibling());

            final Map<String, byte[]> filesToInsert = new HashMap<>();
            filesToInsert.put(xmltcEntry.name(), XPathBuilder.asBytes(document));

            LOGGER.info(String.format("Fix the '%s' tag for target authority %s value", "paiementDossier", codeEdi));

            return ZipUtil.entries(filesToInsert, resourceAsBytes);

        } catch (TechnicalException e) {
            LOGGER.error("Cannot fix the 'destinataire' tag value", e);
            throw new TechnicalException("Cannot fix the 'destinataire' tag value", e);
        }
    }
}
