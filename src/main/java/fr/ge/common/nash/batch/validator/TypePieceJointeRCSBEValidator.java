/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.batch.validator;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.github.jknack.handlebars.internal.lang3.StringUtils;

import fr.ge.common.nash.batch.bean.AuthorityNaming;
import fr.ge.common.nash.batch.bean.Error;
import fr.ge.common.nash.batch.bean.Error.Level;
import fr.ge.common.nash.batch.bean.Errors;
import fr.ge.common.nash.batch.utils.ScanFixUtils;
import fr.ge.common.nash.batch.utils.XMLRegentQuery;
import fr.ge.common.nash.batch.utils.XMLTCQuery;
import fr.ge.common.nash.batch.utils.XPathBuilder;
import fr.ge.common.utils.ZipUtil;
import fr.ge.common.utils.bean.FileEntry;

/**
 * Scan & Fix typePieceJointe tag into the XMLTC file.
 *
 * @author aolubi
 */
public class TypePieceJointeRCSBEValidator implements IFunctionalValidator {

    private static final Logger LOGGER = LoggerFactory.getLogger(TypePieceJointeRCSBEValidator.class);

    private final List<String> INVALID_TYPE_PIECE_JOINTE = Arrays.asList("RCS-FORMULAIRE_BE", "RCS-FORM_BE_SOCIETE");

    private final String AUTRE_PJ = "AUTRE_PJ";

    /**
     * {@inheritDoc}
     */
    @Override
    public Errors validate(final Properties appProperties, final byte[] resourceAsBytes) {
        final Errors errors = new Errors();

        final FileEntry xmltcEntry = ScanFixUtils.getXMLTCEntry(resourceAsBytes);
        if (null == xmltcEntry) {
            LOGGER.warn("Cannot compare entries where XMLTC file is missing");
            errors.add(new Error(Level.ERROR, "Cannot compare entries where XMLTC file is missing"));
            return errors;
        }
        final FileEntry xmlRegentEntry = ScanFixUtils.getXMLRegentEntry(resourceAsBytes);
        if (null == xmlRegentEntry) {
            LOGGER.warn("Cannot compare entries where XML Regent file is missing");
            errors.add(new Error(Level.ERROR, "Cannot compare entries where XML Regent file is missing"));
            return errors;
        }

        final String codeEdi = XMLRegentQuery.extractDestinataire(xmlRegentEntry).getTextContent();
        if (!codeEdi.startsWith(AuthorityNaming.GREFFE.getReseau())) {
            return errors;
        }

        final Node destinataireDossierCfeAsNode = XMLTCQuery.extractDestinataireDossierCfeAsNode(xmltcEntry, codeEdi);
        if (null == destinataireDossierCfeAsNode) {
            LOGGER.warn("Missing target authority {} description into XMLTC file", codeEdi);
            errors.add(new Error(Level.ERROR, String.format("Missing target authority %s description into XMLTC file", codeEdi)));
            return errors;
        }

        final String roleDestinataire = Optional.of(XPathBuilder.queryNode(destinataireDossierCfeAsNode, "roleDestinataire")) //
                .filter(node -> null != node) //
                .map(Node::getTextContent) //
                .orElse(null);
        if (StringUtils.isEmpty(roleDestinataire)) {
            LOGGER.warn("Cannot extract role for {}", codeEdi);
            errors.add(new Error(Level.ERROR, String.format("Cannot extract role for %s", codeEdi)));
            return errors;
        }

        final Document document = XPathBuilder.buildDocument(xmltcEntry.asBytes());
        final NodeList pieceJointeList = XPathBuilder.queryNodeList(document,
                "//GUEN_DMTDU/dossierUnique/pieceJointe[typePieceJointe = 'RCS-FORMULAIRE_BE' or typePieceJointe = 'RCS-FORM_BE_SOCIETE']");

        if (null != pieceJointeList && pieceJointeList.getLength() > 0) {
            LOGGER.warn("Invalid typePieceJointe {}", INVALID_TYPE_PIECE_JOINTE);
            errors.add(new Error(Level.ERROR, String.format("Invalid typePieceJointe %s", INVALID_TYPE_PIECE_JOINTE)));
        }

        return errors;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public byte[] fix(final Properties appProperties, final byte[] resourceAsBytes) {
        if (!this.validate(appProperties, resourceAsBytes).hasErrors()) {
            return resourceAsBytes;
        }

        final FileEntry xmlRegentEntry = ScanFixUtils.getXMLRegentEntry(resourceAsBytes);
        if (null == xmlRegentEntry) {
            LOGGER.warn("Cannot compare entries where XML Regent file is missing");
            return resourceAsBytes;
        }

        final String codeEdi = XMLRegentQuery.extractDestinataire(xmlRegentEntry).getTextContent();
        if (!codeEdi.startsWith(AuthorityNaming.GREFFE.getReseau())) {
            return resourceAsBytes;
        }

        final FileEntry xmltcEntry = ScanFixUtils.getXMLTCEntry(resourceAsBytes);
        if (null == xmltcEntry) {
            LOGGER.warn("Cannot compare entries where XMLTC file is missing");
            return resourceAsBytes;
        }

        // -->Index of files declared for target authority
        final Node destinataireDossierCfeAsNode = XMLTCQuery.extractDestinataireDossierCfeAsNode(xmltcEntry, codeEdi);
        if (null == destinataireDossierCfeAsNode) {
            LOGGER.warn("Missing target authority {} description into XMLTC file", codeEdi);
            return resourceAsBytes;
        }

        final Document document = XPathBuilder.buildDocument(xmltcEntry.asBytes());

        final String roleDestinataire = Optional.of(XPathBuilder.queryNode(destinataireDossierCfeAsNode, "roleDestinataire")) //
                .filter(node -> null != node) //
                .map(Node::getTextContent) //
                .orElse(null);
        if (StringUtils.isEmpty(roleDestinataire)) {
            LOGGER.warn("Cannot extract role for {}", codeEdi);
            return resourceAsBytes;
        }

        final NodeList pieceJointeList = XPathBuilder.queryNodeList(document,
                "//GUEN_DMTDU/dossierUnique/pieceJointe[typePieceJointe = 'RCS-FORMULAIRE_BE' or typePieceJointe = 'RCS-FORM_BE_SOCIETE']");
        for (int idx = 0; idx < pieceJointeList.getLength(); idx++) {
            final Node pieceJointeNode = pieceJointeList.item(idx);
            final Node typePieceJointeNode = XPathBuilder.queryNode(pieceJointeNode, "typePieceJointe");
            final String oldValue = typePieceJointeNode.getTextContent();
            typePieceJointeNode.setTextContent(AUTRE_PJ);
            LOGGER.warn("Replace tag typePieceJointe from {} to {}", oldValue, AUTRE_PJ);
        }

        final Map<String, byte[]> filesToInsert = new HashMap<>();
        filesToInsert.put(xmltcEntry.name(), XPathBuilder.asBytes(document));

        return ZipUtil.entries(filesToInsert, resourceAsBytes);
    }
}
