package fr.ge.common.nash.batch.record.report.writer;

import java.util.List;

import org.apache.commons.vfs2.FileObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;

import fr.ge.common.nash.batch.utils.vfs.VfsResource;
import fr.ge.common.nash.batch.utils.vfs.VfsResourceItemStream;

public class LocalResourceItemWriter extends VfsResourceItemStream implements ItemWriter<VfsResource> {

    private static final Logger LOGGER = LoggerFactory.getLogger(LocalResourceItemWriter.class);

    private String target;

    @Override
    public void write(final List<? extends VfsResource> items) throws Exception {
        final FileObject targetBasePath = this.fs.resolveFile(this.target);
        for (final VfsResource item : items) {
            LOGGER.debug("Moving resource \"{}\" to \"{}\" local folder", item.getBasename(), targetBasePath.getName().getBaseName());
            item.moveTo(targetBasePath);
        }
    }

    /**
     * @param target
     *            the target to set
     */
    public void setTarget(final String target) {
        this.target = target;
    }

}
