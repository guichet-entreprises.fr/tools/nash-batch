/**
 * 
 */
package fr.ge.common.nash.batch.scan.fix.record;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.item.ExecutionContext;

import fr.ge.common.utils.CoreUtil;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.tracker.service.v2.IMessageService;

/**
 * Scan & fix batch partitioner.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class ScanFixPartitioner implements Partitioner {

    private static final Logger LOGGER = LoggerFactory.getLogger(ScanFixPartitioner.class);

    /** Tracker message service. **/
    private IMessageService messageService;

    /** Tracker max results. **/
    private Integer maxResults;

    /** Tracker date before. **/
    private String borneInf;

    /** Tracker date after. **/
    private String borneSup;

    /** Tracker reference to search. **/
    private String trackerReference;

    /**
     * Mutateur sur l'attribut {@link #messageService}.
     *
     * @param messageService
     *            la nouvelle valeur de l'attribut messageService
     */
    public void setMessageService(final IMessageService messageService) {
        this.messageService = messageService;
    }

    /**
     * Mutateur sur l'attribut {@link #maxResults}.
     *
     * @param maxResults
     *            la nouvelle valeur de l'attribut maxResults
     */
    public void setMaxResults(final Integer maxResults) {
        this.maxResults = maxResults;
    }

    /**
     * Mutateur sur l'attribut {@link #borneInf}.
     *
     * @param borneInf
     *            la nouvelle valeur de l'attribut borneInf
     */
    public void setBorneInf(final String borneInf) {
        this.borneInf = borneInf;
    }

    /**
     * Mutateur sur l'attribut {@link #borneSup}.
     *
     * @param borneSup
     *            la nouvelle valeur de l'attribut borneSup
     */
    public void setBorneSup(final String borneSup) {
        this.borneSup = borneSup;
    }

    /**
     * Accesseur sur l'attribut {@link #trackerReference}.
     *
     * @return String trackerReference
     */
    public String getTrackerReference() {
        return trackerReference;
    }

    /**
     * Mutateur sur l'attribut {@link #trackerReference}.
     *
     * @param trackerReference
     *            la nouvelle valeur de l'attribut trackerReference
     */
    public void setTrackerReference(String trackerReference) {
        this.trackerReference = trackerReference;
    }

    /**
     * Return TRACKER error reference names.
     * 
     * @return
     */
    private List<String> references() {
        final List<String> references = new ArrayList<String>();
        if (StringUtils.isEmpty(this.borneInf) && StringUtils.isEmpty(this.borneSup)) {
            references.add(CoreUtil.searchAndReplace(this.trackerReference, "\\{([^}]+)\\}", m -> {
                final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(m.group(1));
                return formatter.format(LocalDate.now());
            }));
            return references;
        }
        final LocalDate inf = LocalDate.parse(this.borneInf);
        final LocalDate sup = LocalDate.parse(this.borneSup);

        LocalDate start = inf;
        while (start.isBefore(sup) || start.isEqual(sup)) {
            references.add(CoreUtil.searchAndReplace(this.trackerReference, "\\{([^}]+)\\}", m -> {
                final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(m.group(1));
                return formatter.format(LocalDate.now());
            }));
            start = start.plusMonths(1L);
        }
        return references;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, ExecutionContext> partition(final int gridSize) {
        final List<String> references = references();
        LOGGER.info("Start partitionning with references : {}", references);

        // -->Build filters
        final List<SearchQueryFilter> filters = new ArrayList<SearchQueryFilter>();
        filters.add(new SearchQueryFilter("value", ":", references.stream().collect(Collectors.joining(", "))));

        if (StringUtils.isNotEmpty(this.borneInf)) {
            filters.add(new SearchQueryFilter("created", ">=", this.borneInf));
        }
        if (StringUtils.isNotEmpty(this.borneSup)) {
            filters.add(new SearchQueryFilter("created", "<=", this.borneSup));
        }

        LOGGER.info("Search Tracker messages with filters : {}", filters);
        final int totalResults = Optional.ofNullable(this.messageService.searchMessage( //
                0L, //
                1L, //
                filters, //
                null //
        )) //
                .filter(r -> null != r) //
                .map(SearchResult::getTotalResults) //
                .map(Long::intValue) //
                .orElse(0);

        int partitions = (totalResults / this.maxResults) + 1;
        int startIndex = 0;
        final Map<String, ExecutionContext> executionContextMap = new HashMap<String, ExecutionContext>();
        for (int i = 1; i <= partitions; i++) {
            final ExecutionContext executionContext = new ExecutionContext();
            executionContext.putString("name", "Thread_" + i);
            executionContext.putLong("startIndex", startIndex);
            executionContext.putLong("maxResults", this.maxResults);
            executionContext.putLong("totalResults", totalResults);
            executionContext.put("references", references);

            if (StringUtils.isEmpty(this.borneInf)) {
                executionContext.putString("borneInf", this.borneInf);
            }

            if (StringUtils.isEmpty(this.borneSup)) {
                executionContext.putString("borneSup", this.borneSup);
            }
            startIndex += this.maxResults;
            executionContextMap.put(String.valueOf("partition" + i), executionContext);
        }
        LOGGER.info("Number of partitions created : {}", partitions);
        return executionContextMap;
    }
}
