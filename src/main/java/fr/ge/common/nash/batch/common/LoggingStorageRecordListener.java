/**
 * 
 */
package fr.ge.common.nash.batch.common;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.MDC;
import org.springframework.batch.core.ItemWriteListener;

import fr.ge.common.storage.ws.model.StoredFile;

/**
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class LoggingStorageRecordListener implements ItemWriteListener<StoredFile> {

    /**
     * {@inheritDoc}
     */
    @Override
    public void beforeWrite(final List<? extends StoredFile> items) {
        items.stream().forEach(item -> {
            if (null != item && StringUtils.isNotEmpty(item.getId())) {
                MDC.put("record", item.getId());
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterWrite(final List<? extends StoredFile> items) {
        // -->Do nothing here
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onWriteError(final Exception exception, final List<? extends StoredFile> items) {
        // -->Do nothing here
    }

}
