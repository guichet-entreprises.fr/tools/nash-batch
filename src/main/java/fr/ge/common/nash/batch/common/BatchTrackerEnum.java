/**
 * 
 */
package fr.ge.common.nash.batch.common;

/**
 * Tracker enumeration.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public enum BatchTrackerEnum {

    ENGAGE_REMOVAL_PROCEDURE_UNLESS_X_DAYS("Sans action de votre part, votre dossier sera supprimé dans %s jours à compter du %s", "notify-start-%s"), //
    STOP_REMOVAL_PROCEDURE("Votre dossier est de nouveau actif et ne sera donc pas supprimé.", "notify-stop") //
    ;

    private String message;

    private String author;

    BatchTrackerEnum(final String message, final String author) {
        this.message = message;
        this.author = author;
    }

    /**
     * Accesseur sur l'attribut {@link #message}.
     *
     * @return String message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Accesseur sur l'attribut {@link #author}.
     *
     * @return String author
     */
    public String getAuthor() {
        return author;
    }
}
