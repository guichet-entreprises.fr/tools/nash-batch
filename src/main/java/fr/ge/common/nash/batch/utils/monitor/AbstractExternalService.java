package fr.ge.common.nash.batch.utils.monitor;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.function.Supplier;

import javax.ws.rs.ProcessingException;

import org.slf4j.LoggerFactory;

/**
 * @author Christian Cougourdan
 */
public abstract class AbstractExternalService {

    protected void log(final Exception ex, final String what, final String subject, final Object... args) {
        final Throwable cause = ex.getCause();
        final String msg = 0 == args.length ? subject : String.format(subject, args);
        if (cause instanceof SocketTimeoutException) {
            LoggerFactory.getLogger(this.getClass()).warn("Service timeout on {} : {}", what, msg);
        } else if (cause instanceof ConnectException) {
            LoggerFactory.getLogger(this.getClass()).warn("Connection refused on {} : {}", what, msg);
        } else {
            LoggerFactory.getLogger(this.getClass()).warn("Unknown error on {} : {}", what, msg, ex);
        }
    }

    protected <R> R log(final Supplier<R> fn, final String what, final String subject, final Object... args) {
        try {
            return fn.get();
        } catch (final ProcessingException ex) {
            this.log(ex, what, subject, args);
        }

        return null;
    }

}
