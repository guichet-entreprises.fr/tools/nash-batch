package fr.ge.common.nash.batch.record.report.model.majdu;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.NONE)
public class EmetteurMiseAJour {

    @XmlElement(name = "codePartenaire", required = true)
    private String codePartenaire;

    /**
     * @return the codePartenaire
     */
    public String getCodePartenaire() {
        return this.codePartenaire;
    }

}
