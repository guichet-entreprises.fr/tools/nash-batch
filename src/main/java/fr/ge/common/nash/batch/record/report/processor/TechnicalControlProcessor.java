package fr.ge.common.nash.batch.record.report.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import fr.ge.common.nash.batch.record.report.model.ctfic.CTFicDocument;
import fr.ge.common.nash.batch.utils.RecordUtils;

/**
 * @author Christian Cougourdan
 */
public class TechnicalControlProcessor extends AbstractSingleReportProcessor<CTFicDocument> {

    private final Logger logger = LoggerFactory.getLogger(TechnicalControlProcessor.class);

    @Value("${ws.clicker.events.ctfic.tc:SES_CTFIC_OK}")
    private String clickerEventOk;

    @Value("${ws.clicker.events.ctfic.rpd:SES_CTFIC_ERR_RECORD_PARTIAL}")
    private String clickerEventRecordErrorPartial;

    @Value("${ws.clicker.events.ctfic.rtd:SES_CTFIC_ERR_RECORD_ALL}")
    private String clickerEventRecordErrorAll;

    @Value("${ws.clicker.events.ctfic.rtf:SES_CTFIC_KO}")
    private String clickerEventKo;

    public TechnicalControlProcessor() {
        super(CTFicDocument.class);
    }

    @Override
    protected boolean processDocument(final CTFicDocument doc) throws Exception {
        final String receivedFilename = this.assertNotNull(doc.getNomFichierRecu(), "Received file name must be specified");
        final String numeroDeLiasse = this.assertNotNull(RecordUtils.findNumeroLiasseFromFileName(receivedFilename), "Record number not found from filename {0}", receivedFilename);
        final String status = this.assertNotNull(doc.getEtatTraitementFichier(), "Record processing status must be specified");

        if ("TC".equals(status)) {
            this.clicker.notify(this.clickerEventOk);
            this.tracker.notify( //
                    numeroDeLiasse, //
                    "Le fichier %s sous la référence [%s] a été contrôlé par le SES.", //
                    receivedFilename, numeroDeLiasse //
            );
            this.tracker.notify( //
                    this.trackerSupportRefInfo, "[%s] Le fichier %s sous la référence [%s] a été contrôlé par le SES.", //
                    status, receivedFilename, numeroDeLiasse //
            );
        } else {
            final StringBuilder msg = new StringBuilder();
            msg.append( //
                    String.format( //
                            "[%s] Le fichier %s sous la référence [%s] a été contrôlé par le SES.", //
                            status, receivedFilename, numeroDeLiasse //
                    ) //
            );

            this.tracker.notify(numeroDeLiasse, msg.toString() + " Une ou plusieurs erreurs sont survenues lors du traitement.");

            switch (status) {
            case "RPD": // L'identifiant d'un ou plusieurs dossiers sont erronés
                this.clicker.notify(this.clickerEventRecordErrorPartial);
                break;
            case "RTD": // Tous les identifiants des dossiers sont erronés
                this.clicker.notify(this.clickerEventRecordErrorAll);
                break;
            case "RTF": // Rejet total du fichier : le fichier n'a pu être exploité
            case "RT":
            default:
                this.clicker.notify(this.clickerEventKo);
                break;
            }

            this.errorsAsString(msg, doc.getErreurs());

            this.tracker.notify(this.trackerSupportRefError, msg.toString());
        }

        return true;
    }

}
