package fr.ge.common.nash.batch.notify.record;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.stereotype.Component;

import fr.ge.common.nash.batch.common.BatchTrackerEnum;
import fr.ge.common.nash.ws.v1.bean.RecordInfoBean;
import fr.ge.tracker.service.v1.IUidRestService;

/**
 * Stop removal procedure batch writer.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@Component(value = "stopRemovalProcedureRecordWriter")
public class StopRemovalProcedureRecordWriter implements ItemWriter<RecordInfoBean> {

    private static final Logger LOGGER = LoggerFactory.getLogger(StopRemovalProcedureRecordWriter.class);

    private IUidRestService uidRestService;

    /**
     * Mutateur sur l'attribut {@link #uidRestService}.
     *
     * @param uidRestService
     *            la nouvelle valeur de l'attribut uidRestService
     */
    public void setUidRestService(final IUidRestService uidRestService) {
        this.uidRestService = uidRestService;
    }

    @Override
    public void write(final List<? extends RecordInfoBean> items) throws Exception {
        items.stream().forEach(record -> {
            this.uidRestService.addMessage(record.getCode(), //
                    BatchTrackerEnum.STOP_REMOVAL_PROCEDURE.getMessage(), //
                    BatchTrackerEnum.STOP_REMOVAL_PROCEDURE.getAuthor() //
            );
            LOGGER.info("Stopping removal procedure for record : {}", record.getCode());
        });
    }
}
