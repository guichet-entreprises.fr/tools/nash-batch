/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.batch.validator;

import java.util.ArrayList;
import java.util.Optional;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ge.common.nash.batch.bean.Error;
import fr.ge.common.nash.batch.bean.Error.Level;
import fr.ge.common.nash.batch.bean.Errors;
import fr.ge.common.nash.batch.utils.ScanFixUtils;
import fr.ge.common.utils.ZipUtil;
import fr.ge.common.utils.bean.FileEntry;

/**
 * Check if the XMLTC file is present.
 *
 * @author aolubi
 */
public class XMLTCEntryValidator implements IFunctionalValidator {

    /** La constante LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(XMLTCEntryValidator.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean validateConditions(final byte[] resourceAsBytes) {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Errors validate(final Properties appProperties, final byte[] resourceAsBytes) {
        final Errors errors = new Errors();

        if (null == Optional.of(resourceAsBytes) //
                .filter(r -> null != r) //
                .map(r -> ZipUtil.entries(r)) //
                .orElse(new ArrayList<String>()) //
                .stream() //
                .filter(entry -> REGEX_XMLTC.matcher(entry).matches()) //
                .findFirst() //
                .orElse(null)) {
            LOGGER.info("The XMLTC file is missing or does not match the pattern : {}", REGEX_XMLTC.pattern());
            errors.add(new Error(Level.ERROR, String.format("The XMLTC file is missing or does not match the pattern : %s", REGEX_XMLTC.pattern())));
        }

        // -->Count number of XML TC files
        final Long total = Optional.of(resourceAsBytes) //
                .filter(r -> null != r) //
                .map(r -> ZipUtil.entries(r)) //
                .orElse(new ArrayList<String>()) //
                .stream() //
                .filter(entry -> REGEX_XMLTC.matcher(entry).matches()) //
                .count();
        if (1L != total) {
            LOGGER.warn("{} XMLTC file(s) in a Zip content", total);
            errors.add(new Error(Level.ERROR, String.format("%d XMLTC file(s) in a Zip content", total)));
        }

        final FileEntry xmltcEntry = ScanFixUtils.getXMLTCEntry(resourceAsBytes);
        if (null == xmltcEntry) {
            LOGGER.warn("Cannot compare entries where XMLTC file is missing");
            errors.add(new Error(Level.ERROR, "Cannot compare entries where XMLTC file is missing"));
            return errors;
        }

        return errors;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public byte[] fix(final Properties appProperties, final byte[] resourceAsBytes) {
        // -->Do nothing here
        return resourceAsBytes;
    }
}
