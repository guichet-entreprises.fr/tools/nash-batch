package fr.ge.common.nash.batch.utils.monitor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import fr.ge.tracker.bean.Reference;
import fr.ge.tracker.service.v1.IUidRestService;
import fr.ge.tracker.service.v2.IReferenceService;

/**
 * @author Christian Cougourdan
 */
@Service
public class TrackerService extends AbstractExternalService {

    private static final String RECORD_CODE_PREFIX = "DOSSIER_";

    @Autowired
    private IUidRestService uidRestService;

    @Autowired
    private IReferenceService serviceV2;

    @Value("${mas.tracker.service.author}")
    private String author;

    /**
     * Send a new message using tracker service.
     *
     * @param code
     *            target reference
     * @param message
     *            message
     * @param args
     *            message arguments
     */
    public void notify(final String code, final String message, final Object... args) {
        final String msg = String.format(message, args);
        this.log( //
                () -> this.uidRestService.addMessage(code, msg, this.author), //
                "sending message", //
                "[%s] %s", code, msg //
        );
    }

    /**
     * Retrieve a recursive reference. If not exists, create it.
     *
     * @param reference
     *            reference to retrieve/create
     * @return reference ending leaf
     */
    public String create(final String reference) {
        final String[] splitted = reference.split("/", 2);
        final Reference nfo = this.load(splitted[0]);

        if (null == nfo) {
            try {
                this.uidRestService.getOrCreateUid(splitted[0], this.author);
            } catch (final Exception ex) {
                this.log(ex, "creating new reference", splitted[0]);
                return null;
            }
            if (splitted.length > 1) {
                return this.create(splitted[1], splitted[0]);
            } else {
                return splitted[0];
            }
        } else if (splitted.length > 1) {
            return this.create(splitted[1], nfo);
        }

        return splitted[0];
    }

    public String create(final String child, final String parent) {
        final String[] splitted = child.split("/", 2);
        try {
            this.uidRestService.addReference(parent, splitted[0], this.author);
        } catch (final Exception ex) {
            this.log(ex, "link child reference", "%s -> %s", splitted[0], parent);
            return null;
        }

        if (splitted.length > 1) {
            return this.create(splitted[1], splitted[0]);
        } else {
            return splitted[0];
        }
    }

    private String create(final String child, final Reference root) {
        final String[] splitted = child.split("/", 2);

        final Reference subRefNfo = root.getReferences().stream().filter(ref -> splitted[0].equals(ref.getValue())).findFirst().orElse(null);

        if (null == subRefNfo) {
            return this.create(child, root.getValue());
        } else if (splitted.length > 1) {
            return this.create(splitted[1], subRefNfo);
        } else {
            return splitted[0];
        }
    }

    public Reference load(final String code) {
        final String cleanedCode = code.replaceFirst("^" + RECORD_CODE_PREFIX, "");
        return this.log(() -> this.serviceV2.load(cleanedCode), "loading reference", cleanedCode);
    }

}
