package fr.ge.common.nash.batch.record.report.model.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * @author Christian Cougourdan
 */
@XmlAccessorType(XmlAccessType.NONE)
public class Erreur {

    @XmlElement(name = "codeErreur", required = true)
    private String codeErreur;

    @XmlElement(name = "libelleErreur", required = true)
    private String libelleErreur;

    @XmlElement(name = "fichierEnErreur", required = true)
    private String fichierEnErreur;

    @XmlElement(name = "rubriqueEnErreur")
    private String rubriqueEnErreur;

    @XmlElement(name = "valeurEnErreur")
    private String valeurEnErreur;

    /**
     * @return the codeErreur
     */
    public String getCodeErreur() {
        return this.codeErreur;
    }

    /**
     * @return the libelleErreur
     */
    public String getLibelleErreur() {
        return this.libelleErreur;
    }

    /**
     * @return the fichierEnErreur
     */
    public String getFichierEnErreur() {
        return this.fichierEnErreur;
    }

    /**
     * @return the rubriqueEnErreur
     */
    public String getRubriqueEnErreur() {
        return this.rubriqueEnErreur;
    }

    /**
     * @return the valeurEnErreur
     */
    public String getValeurEnErreur() {
        return this.valeurEnErreur;
    }

}
