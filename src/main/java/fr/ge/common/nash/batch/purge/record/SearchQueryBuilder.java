/**
 * 
 */
package fr.ge.common.nash.batch.purge.record;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;

import fr.ge.common.nash.batch.common.AbstractSearchQueryBuilder;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;

/**
 * Record batch search query builder.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class SearchQueryBuilder extends AbstractSearchQueryBuilder {

    /**
     * {@inheritDoc}
     */
    @Override
    public void build() {
        Optional.ofNullable(this.minusDaysMin) //
                .filter(v -> null != v) //
                .map(v -> LocalDate.now().minusDays(v)) //
                .map(LocalDate::toString) //
                .map(dateInf -> new SearchQueryFilter("created<=" + dateInf.toString())) //
                .map(this.filters::add) //
                .orElse(null) //
        ;

        Optional.ofNullable(this.minusDaysMax) //
                .filter(v -> null != v) //
                .map(v -> LocalDate.now().minusDays(v)) //
                .map(LocalDate::toString) //
                .map(dateInf -> new SearchQueryFilter("created>" + dateInf.toString())) //
                .map(this.filters::add) //
                .orElse(null) //
        ;

        this.orders = Arrays.asList("created:asc", "uid:asc").stream().map(SearchQueryOrder::new).collect(Collectors.toList());
    }
}
