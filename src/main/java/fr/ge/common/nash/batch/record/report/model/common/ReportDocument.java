package fr.ge.common.nash.batch.record.report.model.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * @author Christian Cougourdan
 */
@XmlAccessorType(XmlAccessType.NONE)
public class ReportDocument {

    @XmlElement(name = "version", required = true)
    private String version;

    @XmlElement(name = "emetteur", required = true)
    private String emetteur;

    @XmlElement(name = "destinataire", required = true)
    private String destinataire;

    /**
     * @return the version
     */
    public String getVersion() {
        return this.version;
    }

    /**
     * @return the emetteur
     */
    public String getEmetteur() {
        return this.emetteur;
    }

    /**
     * @return the destinataire
     */
    public String getDestinataire() {
        return this.destinataire;
    }

}
