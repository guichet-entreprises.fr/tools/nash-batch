/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.batch.validation;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.jboss.logging.MDC;
import org.joda.time.LocalDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Node;

import fr.ge.common.nash.batch.bean.AuthorityNaming;
import fr.ge.common.nash.batch.bean.Errors;
import fr.ge.common.nash.batch.bean.FileState;
import fr.ge.common.nash.batch.bean.FileStateEnum;
import fr.ge.common.nash.batch.utils.ScanFixUtils;
import fr.ge.common.nash.batch.utils.XMLRegentQuery;
import fr.ge.common.nash.batch.utils.XMLTCQuery;
import fr.ge.common.nash.batch.validator.IFunctionalValidator;
import fr.ge.common.nash.batch.validator.ScanFixValidator;
import fr.ge.common.utils.bean.FileEntry;
import fr.ge.common.utils.exception.TechnicalException;

/**
 * Scan and fix service.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@Service
public class ScanFixService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ScanFixService.class);

    @Autowired
    private ScanFixValidator validator;

    @Autowired
    private EddieService eddieService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private NashService nashService;

    @Autowired
    private Properties appProperties;

    public static final String[] CSV_HEADERS = { "event", "edi", "authority", "input", "source", "liasse", "uid", "reference_paiement", "output_zip", "output_fini" };

    /**
     * The batch type enum.
     * 
     * @author $Author: aolubi $
     * @version $Revision: 0 $
     */
    public enum BatchTypeEnum {

        SCAN("scan"),

        FIX("fix"),

        SEND("send");

        private String code;

        BatchTypeEnum(final String code) {
            this.code = code;
        }

        public void setCode(final String code) {
            this.code = code;
        }

        public String getCode() {
            return this.code;
        }

        /**
         * Get type from code.
         *
         * @param code
         *            the code identifier
         * @return the status enum
         */
        public static BatchTypeEnum getType(final String code) {
            return Arrays.asList(BatchTypeEnum.values()) //
                    .stream() //
                    .filter(s -> s.getCode().equalsIgnoreCase(code)) //
                    .findFirst() //
                    .orElse(null) //
            ;
        }
    }

    /**
     * Constructor.
     */
    public ScanFixService() {
        // Nothing to do.
    }

    /**
     * Call the validator based on batch type.
     * 
     * @param batchType
     *            The batch type
     * @param absoluteFilePath
     *            The absolute path file
     * @param targetDirectory
     *            The target directory
     */

    /**
     * Run Scan and fix service.
     * 
     * @param batchType
     *            The batch type
     * @param absoluteFilePath
     *            The input absolute file path to scan or fix
     * @param inputDirectory
     *            The input absolute directory path to scan or fix
     * @param absoluteCsvFilePath
     *            The input absolute CSV file describing files to be fixed
     * @param outputDirectory
     *            The target directory
     * @param token
     *            The EDDIE token
     */
    public void run(final Map<String, String> params) {
        final String batchType = params.getOrDefault("t", null);
        final String absoluteFilePath = params.getOrDefault("f", null);
        final String inputDirectory = params.getOrDefault("d", null);
        final String absoluteCsvFilePath = params.getOrDefault("csv", null);
        final String outputDirectory = params.getOrDefault("o", null);
        final String token = params.getOrDefault("token", null);
        final String pathFolder = params.getOrDefault("pathFolder", null);

        if (null == BatchTypeEnum.getType(batchType)) {
            LOGGER.warn("Cannot determine the batch type to execute. Use scan or fix value");
            return;
        }

        if (StringUtils.isNotEmpty(absoluteCsvFilePath) && !Files.exists(Paths.get(absoluteCsvFilePath))) {
            LOGGER.warn("Input CSV file {} does not exist !!", absoluteCsvFilePath);
            return;
        }

        if (StringUtils.isNotEmpty(outputDirectory) && !Files.isDirectory(Paths.get(outputDirectory))) {
            LOGGER.warn("Output directory {} does not exist !!", outputDirectory);
            return;
        }

        if (BatchTypeEnum.SEND.getCode().equals(batchType)) {
            if (StringUtils.isEmpty(token)) {
                LOGGER.warn("Cannot send files to EDDIE without passing a token parameter {}", token);
                return;
            }
        }

        Map<FileStateEnum, List<FileState>> statusFiles = new LinkedHashMap<>();

        if (StringUtils.isNotEmpty(absoluteCsvFilePath)) {
            // -->Read CSV file
            try (final Reader in = new FileReader(absoluteCsvFilePath)) {
                final Iterable<CSVRecord> records = CSVFormat.DEFAULT.withDelimiter(';').withHeader(CSV_HEADERS).withFirstRecordAsHeader().parse(in);
                for (final CSVRecord record : records) {
                    if (BatchTypeEnum.SEND.getCode().equals(batchType)) {
                        if (null != record.get("output_zip") && StringUtils.isNotEmpty(record.get("output_zip")) && //
                                null != record.get("output_fini") && StringUtils.isNotEmpty(record.get("output_fini")) && //
                                null != record.get("input") && StringUtils.isNotEmpty(record.get("input")) //
                        ) {
                            final FileState fileState = this.eddieService.uploadToEddie(record.get("input"), token, pathFolder, record.get("output_zip"), record.get("output_fini"));
                            statusFiles = this.addFileStatus(statusFiles, fileState);
                        }
                    } else if (BatchTypeEnum.SCAN.getCode().equals(batchType) || BatchTypeEnum.FIX.getCode().equals(batchType)) {
                        if (null != record.get("input") && StringUtils.isNotEmpty(record.get("input"))) {
                            final String inputFile = record.get("input");
                            final FileState fileState = this.scanOrFix(batchType, inputFile, outputDirectory);
                            statusFiles = this.addFileStatus(statusFiles, fileState);
                        }
                    }
                }
            } catch (IOException e) {
                LOGGER.error(String.format("Cannot read input csv file : %s", absoluteCsvFilePath), e);
                return;
            }
        } else if (StringUtils.isNotEmpty(absoluteFilePath)) {
            final Path path = Paths.get(absoluteFilePath);
            if (!Files.exists(path)) {
                LOGGER.warn("Input file {} does exist !!", absoluteFilePath);
                return;
            }

            final String fileExt = FilenameUtils.getExtension(absoluteFilePath);
            if (!"zip".equals(fileExt.toLowerCase())) {
                LOGGER.warn("Invalid '{}' file extension !! It is not a zip file.", fileExt);
                return;
            }

            final FileState fileState = this.scanOrFix(batchType, absoluteFilePath, outputDirectory);

            statusFiles = this.addFileStatus(statusFiles, fileState);

        } else if (StringUtils.isNotEmpty(inputDirectory)) {
            if (!Files.isDirectory(Paths.get(inputDirectory))) {
                LOGGER.warn("The input directory {} does not exist !!", inputDirectory);
                return;
            }

            if (Paths.get(inputDirectory).toFile().listFiles(new FilenameFilter() {
                public boolean accept(File directory, String fileName) {
                    return fileName.endsWith(".zip");
                }
            }).length == 0) {
                LOGGER.warn("The input directory {} has no files !", inputDirectory);
                return;
            }

            for (final File file : Paths.get(inputDirectory).toFile().listFiles(new FilenameFilter() {

                @Override
                public boolean accept(final File dir, final String name) {
                    return name.endsWith(".zip");
                }
            })) {
                if (file.isFile() && file.exists() && file.canRead()) {
                    final FileState fileState = this.scanOrFix(batchType, file.getAbsolutePath(), outputDirectory);
                    statusFiles = this.addFileStatus(statusFiles, fileState);
                }
            }
        }

        MDC.put("filename", "");
        LOGGER.info("#####################################");
        LOGGER.info("BATCH SUMMARY");
        LOGGER.info("#####################################");
        final LocalDateTime now = LocalDateTime.now();
        Optional.ofNullable(statusFiles) //
                .filter(m -> null != m) //
                .orElse(new HashMap<>()) //
                .forEach((state, files) -> {
                    LOGGER.info("{} : {}", state.getLabel(), files.size());
                    try {
                        this.generateReport(outputDirectory, now, state, files);
                    } catch (TechnicalException e) {
                        LOGGER.error("Cannot build CSV for state : {}", state.getCode());
                    }
                });

        LOGGER.info("#####################################");
    }

    private Map<FileStateEnum, List<FileState>> addFileStatus(final Map<FileStateEnum, List<FileState>> statusFiles, final FileState fileState) {
        Optional.of(fileState.getState()) //
                .map(state -> {
                    if (FileStateEnum.FIX_OK.equals(state)) {
                        return Arrays.asList(FileStateEnum.SCAN_OK, FileStateEnum.FIX_OK, FileStateEnum.PREPARE_MASS_MAILING);
                    } else if (FileStateEnum.FIX_KO.equals(state)) {
                        return Arrays.asList(FileStateEnum.SCAN_KO, FileStateEnum.FIX_KO);
                    } else if (FileStateEnum.UNECESSARY_FIX.equals(state)) {
                        return Arrays.asList(FileStateEnum.SCAN_OK, FileStateEnum.UNECESSARY_FIX);
                    }
                    return Arrays.asList(state);
                }) //
                .orElse(new ArrayList<>()) //
                .forEach(s -> {
                    final List<FileState> files = Optional.of(statusFiles).filter(map -> null != map.get(s)).map(map -> map.get(s)).orElse(new ArrayList<>());
                    files.add(fileState);
                    statusFiles.put(s, files);
                });
        return statusFiles;
    }

    private FileState scanOrFix(final String batchType, final String absoluteFilePath, final String outputDirectory) {
        final Path path = Paths.get(absoluteFilePath);
        if (!Files.exists(path)) {
            LOGGER.warn("Input file {} does exist !!", absoluteFilePath);
            return new FileState(FileStateEnum.FILE_NOT_FOUND, absoluteFilePath);
        }

        MDC.put("dmtdu", absoluteFilePath);
        byte[] resourceAsBytes = null;
        try {
            resourceAsBytes = FileUtils.readFileToByteArray(new File(absoluteFilePath));
        } catch (IOException e) {
            LOGGER.error("Unable to read input zip file");
            return new FileState(FileStateEnum.FILE_NOT_FOUND, absoluteFilePath);
        }

        final BatchTypeEnum type = BatchTypeEnum.getType(batchType);
        if (BatchTypeEnum.SCAN.equals(type)) {
            final Errors errors = this.validator.validate(this.appProperties, resourceAsBytes);
            final FileStateEnum state = Optional.of(errors).filter(e -> e.hasErrors()).map(e -> FileStateEnum.SCAN_KO).orElse(FileStateEnum.SCAN_OK);
            return new FileState(state, absoluteFilePath);
        } else if (BatchTypeEnum.FIX.equals(type)) {
            final byte[] outResourceAsBytes = this.validator.fix(this.appProperties, resourceAsBytes);

            if (Arrays.equals(resourceAsBytes, outResourceAsBytes)) {
                LOGGER.info("No fix applied to the input file");
                return new FileState(FileStateEnum.UNECESSARY_FIX, absoluteFilePath);
            } else if (ArrayUtils.isNotEmpty(outResourceAsBytes)) {
                final Map<String, String> zipFiles = this.generateFiles(outResourceAsBytes);
                if (MapUtils.isNotEmpty(zipFiles)) {
                    final String zipFileName = zipFiles.get("zip");
                    final String zipFiniFileName = zipFiles.get("fini");

                    if (StringUtils.isNotEmpty(zipFileName) && StringUtils.isNotEmpty(zipFiniFileName)) {
                        // -->Generate zip and fini file into output directory
                        try {
                            final String absoluteZipFileName = outputDirectory + File.separator + zipFileName;
                            final String absoluteZipFiniFileName = outputDirectory + File.separator + zipFiniFileName;
                            FileUtils.writeByteArrayToFile(new File(absoluteZipFileName), outResourceAsBytes);
                            FileUtils.writeByteArrayToFile(new File(absoluteZipFiniFileName), StringUtils.EMPTY.getBytes());
                            return new FileState(FileStateEnum.FIX_OK, absoluteFilePath, absoluteZipFileName, absoluteZipFiniFileName);
                        } catch (IOException e) {
                            LOGGER.error(String.format("Unable to write file into target directory %s.", outputDirectory), e);
                            return new FileState(FileStateEnum.FIX_KO, absoluteFilePath);
                        }
                    }
                }
            }
            LOGGER.warn("The batch will not fix the file");
            return new FileState(FileStateEnum.CANNOT_FIX_FILE, absoluteFilePath);

        } else if (BatchTypeEnum.SEND.equals(type)) {

        }
        return new FileState(FileStateEnum.CANNOT_FIX_FILE, absoluteFilePath);
    }

    public Map<String, String> generateFiles(final byte[] resourceAsBytes) {
        final FileEntry regentEntry = ScanFixUtils.getXMLRegentEntry(resourceAsBytes);
        if (null == regentEntry) {
            return null;
        }

        final String numeroLiasse = XMLRegentQuery.extractNumeroLiasse(regentEntry);
        final String codeEdi = XMLRegentQuery.extractDestinataire(regentEntry).getTextContent();
        final AuthorityNaming naming = AuthorityNaming.getNaming(codeEdi);

        return naming.getLiasseGeneration().generate(numeroLiasse);
    }

    @SuppressWarnings("resource")
    public void generateReport(final String outputDirectory, final LocalDateTime now, final FileStateEnum state, final List<FileState> files) throws TechnicalException {
        if (null == files || files.isEmpty()) {
            return;
        }
        final String filename = this.generateCSVName(now, state);
        try {
            final FileWriter out = new FileWriter(String.join(File.separator, outputDirectory, filename));
            final CSVPrinter printer = new CSVPrinter(out, CSVFormat.DEFAULT.withDelimiter(';').withHeader(CSV_HEADERS));

            printer.flush();
            out.close();

            if (!state.equals(FileStateEnum.PREPARE_MASS_MAILING)) {
                this.buildReport(outputDirectory, filename, now, state, files);
            } else {
                this.buildMassMailing(outputDirectory, filename, now, state, files);
            }

        } catch (IOException e) {
            LOGGER.error(String.format("Cannot build CSV for state : %s", state.getCode()), e);
            throw new TechnicalException(String.format("Cannot build CSV for state : %s", state.getCode()), e);
        }
    }

    private String generateCSVName(final LocalDateTime now, final FileStateEnum state) {
        return String.format("%s-%s%s%s-%s%s%s.csv", state.getCode(), //

                now.getYear(), //
                StringUtils.leftPad(String.valueOf(now.getMonthOfYear()), 2, "0"), //
                StringUtils.leftPad(String.valueOf(now.getDayOfMonth()), 2, "0"), //

                StringUtils.leftPad(String.valueOf(now.getHourOfDay()), 2, "0"), //
                StringUtils.leftPad(String.valueOf(now.getMinuteOfHour()), 2, "0"), //
                StringUtils.leftPad(String.valueOf(now.getSecondOfMinute()), 2, "0") //
        );
    }

    @SuppressWarnings("resource")
    private void buildReport(final String outputDirectory, final String filename, final LocalDateTime now, final FileStateEnum state, final List<FileState> files) {
        try {
            final FileWriter out = new FileWriter(String.join(File.separator, outputDirectory, filename));
            final CSVPrinter printer = new CSVPrinter(out, CSVFormat.DEFAULT.withDelimiter(';').withHeader(CSV_HEADERS));
            for (final FileState f : files) {
                byte[] resourceAsBytes = null;
                try {
                    resourceAsBytes = FileUtils.readFileToByteArray(new File(f.getInputFile()));
                } catch (IOException e) {
                    LOGGER.error("Unable to read input zip file");
                    continue;
                }

                if (ArrayUtils.isEmpty(resourceAsBytes)) {
                    LOGGER.error("Input file {} is not exist to print status", f.getInputFile());
                    continue;
                }

                final String uid = Optional.ofNullable(ScanFixUtils.getXMLTCEntry(resourceAsBytes)) //
                        .filter(entry -> null != entry) //
                        .map(e -> XMLTCQuery.extractNumeroDossierUnique(e)) //
                        .filter(n -> null != n) //
                        .map(Node::getTextContent) //
                        .orElse(StringUtils.EMPTY);

                final String ediCode = Optional.ofNullable(ScanFixUtils.getXMLRegentEntry(resourceAsBytes)) //
                        .filter(entry -> null != entry) //
                        .map(e -> XMLRegentQuery.extractDestinataire(e)) //
                        .map(Node::getTextContent) //
                        .orElse(StringUtils.EMPTY);
                ;

                final String authorityLabel = Optional.ofNullable(ScanFixUtils.getXMLTCEntry(resourceAsBytes)) //
                        .filter(entry -> null != entry) //
                        .map(e -> XMLTCQuery.extractLibelleIntervenantAsString(e, ediCode)) //
                        .orElse(StringUtils.EMPTY);

                final String source = Optional.of(uid) //
                        .filter(u -> StringUtils.isNotEmpty(u) && IFunctionalValidator.REGEX_RECORD_FORMS_2.matcher(u).matches()) //
                        .map(u -> "NASH") //
                        .orElse("FORMS") //
                ;
                final String liasse = Optional.ofNullable(ScanFixUtils.getXMLRegentEntry(resourceAsBytes)) //
                        .filter(entry -> null != entry) //
                        .map(e -> XMLRegentQuery.extractNumeroLiasse(e)) //
                        .orElse(StringUtils.EMPTY);

                final String outputZip = Optional.ofNullable(f) //
                        .filter(fs -> null != fs && StringUtils.isNotEmpty(fs.getOutputZipFileName())) //
                        .map(FileState::getOutputZipFileName) //
                        .orElse(StringUtils.EMPTY);

                final String outputFini = Optional.ofNullable(f) //
                        .filter(fs -> null != fs && StringUtils.isNotEmpty(fs.getOutputZipFiniFileName())) //
                        .map(FileState::getOutputZipFiniFileName) //
                        .orElse(StringUtils.EMPTY);

                final String referencePaiement = Optional.ofNullable(ScanFixUtils.getXMLTCEntry(resourceAsBytes)) //
                        .filter(entry -> null != entry) //
                        .map(e -> XMLTCQuery.extractReferencePaiementAsString(e, ediCode)) //
                        .orElse(StringUtils.EMPTY);

                try {
                    printer.printRecord(state.getCode(), //
                            ediCode, //
                            authorityLabel, //
                            f.getInputFile(), //
                            source, //
                            liasse, //
                            uid, //
                            referencePaiement, //
                            outputZip, //
                            outputFini //
                    );
                } catch (IOException e) {
                    LOGGER.error("Cannot print value into CSV for file {} related to state : {}", f.getInputFile(), state.getCode());
                    continue;
                }
            }
            printer.flush();
            out.close();
        } catch (IOException e) {
            LOGGER.error(String.format("Cannot build CSV for state : %s", state.getCode()), e);
            throw new TechnicalException(String.format("Cannot build CSV for state : %s", state.getCode()), e);
        }
    }

    @SuppressWarnings("resource")
    private void buildMassMailing(final String outputDirectory, final String filename, final LocalDateTime now, final FileStateEnum state, final List<FileState> files) {
        try {
            final FileWriter out = new FileWriter(String.join(File.separator, outputDirectory, filename));
            final CSVPrinter printer = new CSVPrinter(out, CSVFormat.DEFAULT.withDelimiter(';'));
            for (final FileState f : files) {
                byte[] resourceAsBytes = null;
                try {
                    resourceAsBytes = FileUtils.readFileToByteArray(new File(f.getInputFile()));
                } catch (IOException e) {
                    LOGGER.error("Unable to read input zip file");
                    continue;
                }

                if (ArrayUtils.isEmpty(resourceAsBytes)) {
                    LOGGER.error("Input file {} is not exist to print status", f.getInputFile());
                    continue;
                }

                final String ediCode = Optional.ofNullable(ScanFixUtils.getXMLRegentEntry(resourceAsBytes)) //
                        .filter(entry -> null != entry) //
                        .map(e -> XMLRegentQuery.extractDestinataire(e)) //
                        .map(Node::getTextContent) //
                        .orElse(StringUtils.EMPTY);
                ;

                final String authorityLabel = Optional.ofNullable(ScanFixUtils.getXMLTCEntry(resourceAsBytes)) //
                        .filter(entry -> null != entry) //
                        .map(e -> XMLTCQuery.extractLibelleIntervenantAsString(e, ediCode)) //
                        .orElse(StringUtils.EMPTY);

                final String uid = Optional.ofNullable(ScanFixUtils.getXMLTCEntry(resourceAsBytes)) //
                        .filter(entry -> null != entry) //
                        .map(e -> XMLTCQuery.extractNumeroDossierUnique(e)) //
                        .filter(n -> null != n) //
                        .map(Node::getTextContent) //
                        .orElse(StringUtils.EMPTY);

                final String source = Optional.of(uid) //
                        .filter(u -> StringUtils.isNotEmpty(u) && IFunctionalValidator.REGEX_RECORD_FORMS_2.matcher(u).matches()) //
                        .map(u -> "NASH") //
                        .orElse("FORMS") //
                ;

                final Map<String, String> userInfo = Optional.of(source) //
                        .filter(s -> "NASH".equals(s)) //
                        .map(s -> this.nashService.getAuthor(uid)) //
                        .filter(StringUtils::isNotEmpty) //
                        .map(a -> this.accountService.readUser(a)) //
                        .orElse(new HashMap<>());

                try {
                    printer.printRecord(//
                            userInfo.getOrDefault("email", null), //
                            userInfo.getOrDefault("firstName", null), //
                            userInfo.getOrDefault("lastName", null), //
                            userInfo.getOrDefault("civility", null), //
                            authorityLabel, //
                            uid //
                    );
                } catch (IOException e) {
                    LOGGER.error("Cannot print value into CSV for file {} related to state : {}", f.getInputFile(), state.getCode());
                    continue;
                }
            }
            printer.flush();
            out.close();
        } catch (IOException e) {
            LOGGER.error(String.format("Cannot build CSV for state : %s", state.getCode()), e);
            throw new TechnicalException(String.format("Cannot build CSV for state : %s", state.getCode()), e);
        }
    }

    public byte[] fix(final byte[] resourceAsBytes) {
        return this.validator.fix(this.appProperties, resourceAsBytes);
    }
}
