package fr.ge.common.nash.batch.utils.vfs;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;
import org.springframework.core.io.Resource;

/**
 * @author Christian Cougourdan
 */
public class SftpResource implements Resource {

    private final FileObject fileObject;

    private File file;

    public SftpResource(final FileObject fileObject) {
        this.fileObject = fileObject;
    }

    @Override
    public InputStream getInputStream() throws IOException {
        return this.fileObject.getContent().getInputStream();
    }

    @Override
    public long contentLength() throws IOException {
        return this.fileObject.getContent().getSize();
    }

    @Override
    public Resource createRelative(final String relativePath) throws IOException {
        final FileObject newFileObject = this.fileObject.resolveFile(relativePath);
        newFileObject.createFile();
        return new SftpResource(newFileObject);
    }

    @Override
    public boolean exists() {
        try {
            return this.fileObject.exists();
        } catch (final FileSystemException ex) {
            return false;
        }
    }

    @Override
    public String getDescription() {
        return null;
    }

    @Override
    public File getFile() throws IOException {
        if (null == this.file) {
            this.file = new VfsFile(this.fileObject);
        }
        return this.file;
    }

    @Override
    public String getFilename() {
        return this.fileObject.getName().getBaseName();
    }

    @Override
    public URI getURI() throws IOException {
        try {
            return this.fileObject.getURL().toURI();
        } catch (FileSystemException | URISyntaxException ex) {
            throw new IOException(ex);
        }
    }

    @Override
    public URL getURL() throws IOException {
        return this.fileObject.getURL();
    }

    @Override
    public boolean isOpen() {
        return this.fileObject.isContentOpen();
    }

    @Override
    public boolean isReadable() {
        try {
            return this.fileObject.isReadable();
        } catch (final FileSystemException ex) {
            return false;
        }
    }

    @Override
    public long lastModified() throws IOException {
        return this.fileObject.getContent().getLastModifiedTime();
    }

    private static class VfsFile extends File {

        private static final long serialVersionUID = 1L;

        private final FileObject fileObject;

        public VfsFile(final FileObject fileObject) throws FileSystemException {
            super(fileObject.getName().getRoot().getRelativeName(fileObject.getName()));
            this.fileObject = fileObject;
        }

        @Override
        public boolean delete() {
            try {
                return this.fileObject.delete();
            } catch (final FileSystemException e) {
                return false;
            }
        }

    }
}