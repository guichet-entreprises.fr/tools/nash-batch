package fr.ge.common.nash.batch.record.report.model.arfic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import fr.ge.common.nash.batch.record.report.model.common.ReportDocument;

/**
 * @author Christian Cougourdan
 */
@XmlRootElement(name = "GUEN_ARFIC")
@XmlAccessorType(XmlAccessType.NONE)
public class ARFicDocument extends ReportDocument {

    @XmlElement(name = "nomFichierRecu", required = true)
    private String nomFichierRecu;

    @XmlElement(name = "dateHeureFichierRecu", required = true)
    private String dateHeureFichierRecu;

    /**
     * @return the nomFichierRecu
     */
    public String getNomFichierRecu() {
        return this.nomFichierRecu;
    }

    /**
     * @return the dateHeureFichierRecu
     */
    public String getDateHeureFichierRecu() {
        return this.dateHeureFichierRecu;
    }

}
