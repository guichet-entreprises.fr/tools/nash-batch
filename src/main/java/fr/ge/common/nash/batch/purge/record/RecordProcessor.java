/**
 * 
 */
package fr.ge.common.nash.batch.purge.record;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Optional;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import com.github.jknack.handlebars.internal.lang3.StringUtils;

import fr.ge.common.nash.batch.common.BatchPattern;
import fr.ge.common.nash.ws.v1.bean.RecordInfoBean;
import fr.ge.tracker.bean.Message;
import fr.ge.tracker.bean.UidNfo;
import fr.ge.tracker.service.v1.IUidRestService;

/**
 * Record purge batch processor.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@Component(value = "purgeRecordProcessor")
public class RecordProcessor implements ItemProcessor<RecordInfoBean, RecordInfoBean> {

    private static final Logger LOGGER = LoggerFactory.getLogger(RecordProcessor.class);

    private IUidRestService uidRestService;

    /**
     * Mutateur sur l'attribut {@link #uidRestService}.
     *
     * @param uidRestService
     *            la nouvelle valeur de l'attribut uidRestService
     */
    public void setUidRestService(final IUidRestService uidRestService) {
        this.uidRestService = uidRestService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RecordInfoBean process(final RecordInfoBean record) throws Exception {
        // -->Get Tracker messages
        final UidNfo nfo = this.uidRestService.getUid(record.getCode());
        if (null == nfo || CollectionUtils.isEmpty(nfo.getMessages())) {
            return null;
        }

        final Message lastRecordMessage = Optional.of(nfo.getMessages()) //
                .filter(CollectionUtils::isNotEmpty) //
                .orElse(new ArrayList<Message>()) //
                .stream() //
                .sorted(Comparator.comparing(Message::getCreated, (m1, m2) -> {
                    return m2.compareTo(m1);
                })) //
                .filter(m -> !BatchPattern.IS_NOTIFY_START.matcher(m.getClient()).matches()) //
                .findFirst() //
                .orElse(null) //
        ;

        if (null == lastRecordMessage || null == lastRecordMessage.getCreated() || StringUtils.isEmpty(lastRecordMessage.getClient())) {
            LOGGER.warn("Cannot get last message for record : {}", record.getCode());
            return null;
        }

        // -->Find there is a notification already sent with same
        // author from last record activity
        if (Optional.of(nfo.getMessages()) //
                .filter(CollectionUtils::isNotEmpty) //
                .orElse(new ArrayList<Message>()) //
                .stream() //
                .sorted(Comparator.comparing(Message::getCreated, (m1, m2) -> {
                    return m2.compareTo(m1);
                })) //
                .filter(m -> BatchPattern.IS_NOTIFY_START.matcher(m.getClient()).matches() && m.getCreated().compareTo(lastRecordMessage.getCreated()) >= 0) //
                .count() >= 2) {
            return record;
        }
        return null;
    }

}
