/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.batch.validator;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import fr.ge.common.nash.batch.bean.AuthorityNaming;
import fr.ge.common.nash.batch.bean.Error;
import fr.ge.common.nash.batch.bean.Error.Level;
import fr.ge.common.nash.batch.bean.Errors;
import fr.ge.common.nash.batch.utils.ScanFixUtils;
import fr.ge.common.nash.batch.utils.XMLRegentQuery;
import fr.ge.common.nash.batch.utils.XPathBuilder;
import fr.ge.common.utils.ZipUtil;
import fr.ge.common.utils.bean.FileEntry;
import fr.ge.common.utils.exception.TechnicalException;

/**
 * Remove the tag e if necesseray.
 *
 * @author aolubi
 */
public class TypeDestinataireTagValidator implements IFunctionalValidator {

    private static final Logger LOGGER = LoggerFactory.getLogger(TypeDestinataireTagValidator.class);

    public enum TypeDestinataire {

        CFE("CFE", "CFE"), //
        TDR("TDR", "AC"), //
        CFE_TDR("CFE+TDR", "CFE+AC");

        private String typeV1;
        private String typeV2;

        TypeDestinataire(final String typeV1, final String typeV2) {
            this.typeV1 = typeV1;
            this.typeV2 = typeV2;
        }

        /**
         * Accesseur sur l'attribut {@link #typeV1}.
         *
         * @return String typeV1
         */
        public String getTypeV1() {
            return typeV1;
        }

        /**
         * Accesseur sur l'attribut {@link #typeV2}.
         *
         * @return String typeV2
         */
        public String getTypeV2() {
            return typeV2;
        }

        public static TypeDestinataire getTypeV1(final String typeV1) {
            for (TypeDestinataire typeDestinataire : TypeDestinataire.values()) {
                if (typeDestinataire.getTypeV1().equals(typeV1)) {
                    return typeDestinataire;
                }
            }
            return null;
        }

        public static TypeDestinataire getTypeV2(final String typeV2) {
            for (TypeDestinataire typeDestinataire : TypeDestinataire.values()) {
                if (typeDestinataire.getTypeV2().equals(typeV2)) {
                    return typeDestinataire;
                }
            }
            return null;
        }
    }

    @Override
    public boolean validateConditions(final byte[] resourceAsBytes) {
        final FileEntry xmlRegentEntry = ScanFixUtils.getXMLRegentEntry(resourceAsBytes);
        if (null == xmlRegentEntry) {
            LOGGER.warn("XML Regent file is missing");
            return false;
        }

        final String codeEdi = XMLRegentQuery.extractDestinataire(xmlRegentEntry).getTextContent();
        return codeEdi.startsWith(AuthorityNaming.CMA.getReseau());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Errors validate(final Properties appProperties, final byte[] resourceAsBytes) {
        final Errors errors = new Errors();
        final FileEntry xmltcEntry = ScanFixUtils.getXMLTCEntry(resourceAsBytes);
        if (null == xmltcEntry) {
            LOGGER.info("Missing XMLTC file");
            errors.add(new Error(Level.ERROR, "Missing XMLTC file"));
            return errors;
        }

        final FileEntry xmlRegentEntry = ScanFixUtils.getXMLRegentEntry(resourceAsBytes);
        if (null == xmlRegentEntry) {
            LOGGER.warn("XML Regent file is missing");
            errors.add(new Error(Level.ERROR, "XML Regent file is missing"));
            return errors;
        }

        final String codeEdi = XMLRegentQuery.extractDestinataire(xmlRegentEntry).getTextContent();
        if (!codeEdi.startsWith(AuthorityNaming.CMA.getReseau())) {
            return errors;
        }

        try {
            final Document document = XPathBuilder.buildDocument(xmltcEntry.asBytes());
            final String typeDestinataire = XPathBuilder.queryString(document.getDocumentElement(), "//MGUN_DEMAT/formalite/destinataires/typeDestinataire");

            if (StringUtils.isEmpty(typeDestinataire)) {
                errors.add(new Error(Level.ERROR, String.format("Invalid tag '%s' value", "typeDestinataire")));
                return errors;
            }

            if (null == TypeDestinataire.getTypeV2(typeDestinataire)) {
                errors.add(new Error(Level.ERROR, String.format("Incorrect tag '%s' value : %s", "typeDestinataire", typeDestinataire)));
                return errors;
            }
        } catch (TechnicalException e) {
            LOGGER.error("The XMLTC file cannot be read", e);
            errors.add(new Error(Level.ERROR, "The XMLTC file cannot be read"));
        }
        return errors;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public byte[] fix(final Properties appProperties, final byte[] resourceAsBytes) {
        if (!this.validate(appProperties, resourceAsBytes).hasErrors()) {
            return resourceAsBytes;
        }

        try {
            final FileEntry xmltcEntry = ScanFixUtils.getXMLTCEntry(resourceAsBytes);
            final Document document = XPathBuilder.buildDocument(xmltcEntry.asBytes());
            final Node typeDestinataire = XPathBuilder.queryNode(document.getDocumentElement(), "//MGUN_DEMAT/formalite/destinataires/typeDestinataire");
            if (null == typeDestinataire || StringUtils.isEmpty(typeDestinataire.getTextContent()) || null == TypeDestinataire.getTypeV1(typeDestinataire.getTextContent())) {
                return resourceAsBytes;
            }

            final String actual = typeDestinataire.getTextContent();
            final String typeV2 = TypeDestinataire.getTypeV1(actual).getTypeV2();
            typeDestinataire.setTextContent(typeV2);
            LOGGER.info(String.format("Replace tag '%s' value from %s to %s", "typeDestinataire", actual, typeV2));

            final Map<String, byte[]> filesToInsert = new HashMap<>();
            filesToInsert.put(xmltcEntry.name(), XPathBuilder.asBytes(document));

            return ZipUtil.entries(filesToInsert, resourceAsBytes);
        } catch (TechnicalException e) {
            LOGGER.error("Cannot fix typeDestinataire", e);
            throw new TechnicalException("Cannot fix typeDestinataire", e);
        }

    }
}
