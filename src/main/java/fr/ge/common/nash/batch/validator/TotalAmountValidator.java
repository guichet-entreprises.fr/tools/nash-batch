/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.batch.validator;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.fasterxml.jackson.databind.JsonNode;

import fr.ge.common.nash.batch.bean.AuthorityNaming;
import fr.ge.common.nash.batch.bean.Error;
import fr.ge.common.nash.batch.bean.Error.Level;
import fr.ge.common.nash.batch.bean.Errors;
import fr.ge.common.nash.batch.utils.ScanFixUtils;
import fr.ge.common.nash.batch.utils.XMLRegentQuery;
import fr.ge.common.nash.batch.utils.XMLTCQuery;
import fr.ge.common.nash.batch.utils.XPathBuilder;
import fr.ge.common.utils.ZipUtil;
import fr.ge.common.utils.bean.FileEntry;
import fr.ge.common.utils.exception.TechnicalException;

/**
 * Scan and fix total amount from XMLTC if necessary.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class TotalAmountValidator implements IFunctionalValidator {

    private static final Logger LOGGER = LoggerFactory.getLogger(TotalAmountValidator.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean validateConditions(final byte[] resourceAsBytes) {
        final FileEntry xmlRegentEntry = ScanFixUtils.getXMLRegentEntry(resourceAsBytes);
        if (null == xmlRegentEntry) {
            LOGGER.warn("XML Regent file is missing");
            return false;
        }

        final String codeEdi = XMLRegentQuery.extractDestinataire(xmlRegentEntry).getTextContent();
        return codeEdi.startsWith(AuthorityNaming.GREFFE.getReseau());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Errors validate(final Properties appProperties, final byte[] resourceAsBytes) {
        final Errors errors = new Errors();
        final FileEntry xmltcEntry = ScanFixUtils.getXMLTCEntry(resourceAsBytes);
        if (null == xmltcEntry) {
            LOGGER.info("Missing XMLTC file");
            errors.add(new Error(Level.ERROR, "Missing XMLTC file"));
            return errors;
        }

        final FileEntry xmlRegentEntry = ScanFixUtils.getXMLRegentEntry(resourceAsBytes);
        if (null == xmlRegentEntry) {
            LOGGER.warn("XML Regent file is missing");
            errors.add(new Error(Level.ERROR, "XML Regent file is missing"));
            return errors;
        }

        final String codeEdi = XMLRegentQuery.extractDestinataire(xmlRegentEntry).getTextContent();
        if (!codeEdi.startsWith(AuthorityNaming.GREFFE.getReseau())) {
            return errors;
        }

        final String recordUid = Optional.ofNullable(XMLTCQuery.extractNumeroDossierUnique(xmltcEntry)) //
                .filter(node -> null != node) //
                .map(Node::getTextContent) //
                .orElse(null);
        if (!IFunctionalValidator.REGEX_RECORD_FORMS_2.matcher(recordUid).matches()) {
            return errors;
        }

        try {
            final Document document = XPathBuilder.buildDocument(xmltcEntry.asBytes());
            final String expression = "//GUEN_DMTDU/dossierUnique/dossierCfe/destinataireDossierCfe[codeEdiDestinataire='" + codeEdi
                    + "']/paiementDossier[montantFormalite!='0,00' and montantFormalite!='00,00']/montantFormalite";

            final String montantFormalite = XPathBuilder.queryString(document.getDocumentElement(), expression);
            if (StringUtils.isEmpty(montantFormalite)) {
                return errors;
            }

            // -->Comparing total amount from XMLTC and total amount from proxy
            final JsonNode carts = this.getCart(appProperties, recordUid);
            if (null == carts) {
                errors.add(new Error(Level.ERROR, String.format("Cannot find payment into proxy payment for record '%s", recordUid)));
                return errors;
            }

            BigDecimal totalAmount = new BigDecimal("0.00");
            for (int cartIdx = 0; cartIdx < carts.size(); cartIdx++) {
                final JsonNode cart = carts.get(cartIdx);
                final JsonNode items = cart.path("items");
                for (int itemIdx = 0; itemIdx < items.size(); itemIdx++) {
                    final JsonNode item = items.get(itemIdx);
                    if (item.path("ediCode").asText().equals(codeEdi)) {
                        totalAmount = totalAmount.add(new BigDecimal(item.path("price").textValue()));
                    }
                }
            }

            if (!totalAmount.equals(this.formatAmount(montantFormalite))) {
                errors.add(new Error(Level.ERROR, String.format("Amount from XMLTC file %s does not match the total amount from proxy %s", montantFormalite, totalAmount)));
            }
        } catch (TechnicalException e) {
            LOGGER.error("The XMLTC file cannot be read", e);
            errors.add(new Error(Level.ERROR, "The XMLTC file cannot be read"));
        }
        return errors;
    }

    private BigDecimal formatAmount(final String amount) {
        try {
            final DecimalFormat df = (DecimalFormat) NumberFormat.getNumberInstance(Locale.FRANCE);
            df.setParseBigDecimal(true);
            return (BigDecimal) df.parse(amount);
        } catch (ParseException e) {
            LOGGER.error(String.format("Cannot format amount %s", amount), e);
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public byte[] fix(final Properties appProperties, final byte[] resourceAsBytes) {
        if (!this.validate(appProperties, resourceAsBytes).hasErrors()) {
            return resourceAsBytes;
        }

        final FileEntry xmlRegentEntry = ScanFixUtils.getXMLRegentEntry(resourceAsBytes);
        if (null == xmlRegentEntry) {
            LOGGER.warn("XML Regent file is missing");
            return resourceAsBytes;
        }

        final String codeEdi = XMLRegentQuery.extractDestinataire(xmlRegentEntry).getTextContent();
        final String expression = "//GUEN_DMTDU/dossierUnique/dossierCfe/destinataireDossierCfe[codeEdiDestinataire='" + codeEdi
                + "']/paiementDossier[montantFormalite!='0,00' and montantFormalite!='00,00']/montantFormalite";

        final FileEntry xmltcEntry = ScanFixUtils.getXMLTCEntry(resourceAsBytes);
        try {
            final String recordUid = Optional.ofNullable(XMLTCQuery.extractNumeroDossierUnique(xmltcEntry)) //
                    .filter(node -> null != node) //
                    .map(Node::getTextContent) //
                    .orElse(null);

            final Document document = XPathBuilder.buildDocument(xmltcEntry.asBytes());
            final Node montantFormalite = XPathBuilder.queryNode(document.getDocumentElement(), expression);
            if (null == montantFormalite) {
                LOGGER.warn("Cannot find paiementDossier tag for target authority '{}'", codeEdi);
                return resourceAsBytes;
            }

            // -->Update total amount from proxy
            final JsonNode carts = this.getCart(appProperties, recordUid);
            if (null == carts) {
                LOGGER.error("Cannot find payment into proxy payment for record '{}", recordUid);
                return resourceAsBytes;
            }

            BigDecimal totalAmount = new BigDecimal("0.00");
            for (int cartIdx = 0; cartIdx < carts.size(); cartIdx++) {
                final JsonNode cart = carts.get(cartIdx);
                final JsonNode items = cart.path("items");
                for (int itemIdx = 0; itemIdx < items.size(); itemIdx++) {
                    final JsonNode item = items.get(itemIdx);
                    if (item.path("ediCode").asText().equals(codeEdi)) {
                        totalAmount = totalAmount.add(new BigDecimal(item.path("price").textValue()));
                    }
                }
            }

            final DecimalFormatSymbols symbols = new DecimalFormatSymbols();
            symbols.setDecimalSeparator(',');
            final DecimalFormat formatter = new DecimalFormat("0.00", symbols);
            final String newAmount = formatter.format(totalAmount);

            final String oldAmount = montantFormalite.getTextContent();
            if (null != totalAmount && !totalAmount.equals(new BigDecimal("0.00"))) {
                montantFormalite.setTextContent(newAmount);

                final Map<String, byte[]> filesToInsert = new HashMap<>();
                filesToInsert.put(xmltcEntry.name(), XPathBuilder.asBytes(document));

                LOGGER.info(String.format("Update the total amount from %s to %s tag for target authority %s", oldAmount, newAmount, codeEdi));

                return ZipUtil.entries(filesToInsert, resourceAsBytes);
            }

            return resourceAsBytes;
        } catch (TechnicalException e) {
            LOGGER.error("Cannot fix the 'destinataire' tag value", e);
            throw new TechnicalException("Cannot fix the 'destinataire' tag value", e);
        }
    }
}
