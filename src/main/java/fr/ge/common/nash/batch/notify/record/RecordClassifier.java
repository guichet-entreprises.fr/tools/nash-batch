/**
 * 
 */
package fr.ge.common.nash.batch.notify.record;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Optional;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.classify.Classifier;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.ge.common.nash.batch.common.AbstractSearchQueryBuilder;
import fr.ge.common.nash.batch.common.BatchPattern;
import fr.ge.common.nash.ws.v1.bean.RecordInfoBean;
import fr.ge.tracker.bean.Message;
import fr.ge.tracker.bean.UidNfo;
import fr.ge.tracker.service.v1.IUidRestService;

/**
 * Record notify classifier.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class RecordClassifier implements Classifier<RecordInfoBean, ItemWriter<? super RecordInfoBean>> {

    private static final Logger LOGGER = LoggerFactory.getLogger(RecordClassifier.class);

    /** Target writers. **/
    private SkipRecordWriter skipRecordWriter;
    private RecordWriter notifyFirstTimeWriter;
    private RecordWriter notifyLastTimeWriter;
    private StopRemovalProcedureRecordWriter stopRemovalProcedureWriter;

    private IUidRestService uidRestService;

    private AbstractSearchQueryBuilder searchQueryBuilder;

    /**
     * Mutateur sur l'attribut {@link #notifyFirstTimeWriter}.
     *
     * @param notifyFirstTimeWriter
     *            la nouvelle valeur de l'attribut notifyFirstTimeWriter
     */
    public void setNotifyFirstTimeWriter(final RecordWriter notifyFirstTimeWriter) {
        this.notifyFirstTimeWriter = notifyFirstTimeWriter;
    }

    /**
     * Mutateur sur l'attribut {@link #notifyLastTimeWriter}.
     *
     * @param notifyLastTimeWriter
     *            la nouvelle valeur de l'attribut notifyLastTimeWriter
     */
    public void setNotifyLastTimeWriter(final RecordWriter notifyLastTimeWriter) {
        this.notifyLastTimeWriter = notifyLastTimeWriter;
    }

    /**
     * Mutateur sur l'attribut {@link #stopRemovalProcedureWriter}.
     *
     * @param stopRemovalProcedureWriter
     *            la nouvelle valeur de l'attribut stopRemovalProcedureWriter
     */
    public void setStopRemovalProcedureRecordWriter(final StopRemovalProcedureRecordWriter stopRemovalProcedureWriter) {
        this.stopRemovalProcedureWriter = stopRemovalProcedureWriter;
    }

    /**
     * Mutateur sur l'attribut {@link #skipRecordWriter}.
     *
     * @param skipRecordWriter
     *            la nouvelle valeur de l'attribut skipRecordWriter
     */
    public void setSkipRecordWriter(final SkipRecordWriter skipRecordWriter) {
        this.skipRecordWriter = skipRecordWriter;
    }

    /**
     * Mutateur sur l'attribut {@link #uidRestService}.
     *
     * @param uidRestService
     *            la nouvelle valeur de l'attribut uidRestService
     */
    public void setUidRestService(final IUidRestService uidRestService) {
        this.uidRestService = uidRestService;
    }

    /**
     * Mutateur sur l'attribut {@link #searchQueryBuilder}.
     *
     * @param searchQueryBuilder
     *            la nouvelle valeur de l'attribut searchQueryBuilder
     */
    public void setSearchQueryBuilder(final AbstractSearchQueryBuilder searchQueryBuilder) {
        this.searchQueryBuilder = searchQueryBuilder;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ItemWriter<? super RecordInfoBean> classify(final RecordInfoBean record) {
        // -->Ignoring record containing any payment in progress status
        final ObjectMapper jsonMapper = new ObjectMapper();
        jsonMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        /*
         * if (null != Optional.of(record) // .map(RecordInfoBean::getDetails)
         * // .filter(d -> null != d) // .map(d -> { try { return
         * jsonMapper.readValue(d.toString(),
         * FormSpecificationDescription.class); } catch (IOException e) {
         * LOGGER.warn("Could not map record description for record {}",
         * record.getCode(), e); return null; } }) // .filter(desc -> null !=
         * desc) // .map(FormSpecificationDescription::getSteps) //
         * .map(StepsElement::getElements) // .orElseGet(Collections::emptyList)
         * // .stream() // .filter(step -> step.getId().equals("payment") &&
         * step.getUser().equals(StepUserTypeEnum.USER.getName()) &&
         * step.getStatus().equals(StepStatusEnum.IN_PROGRESS.getStatus())) //
         * .findFirst() // .orElse(null) // ) {
         * LOGGER.warn("The record contains payment step in progress status");
         * return this.skipRecordWriter; }
         */

        // -->Get Tracker messages
        final UidNfo nfo = this.uidRestService.getUid(record.getCode());
        if (null == nfo || CollectionUtils.isEmpty(nfo.getMessages())) {
            LOGGER.warn("Cannot get TRACKER messages for record : {}", record.getCode());
            return this.skipRecordWriter;
        }

        final Message lastRecordMessage = Optional.of(nfo.getMessages()) //
                .filter(CollectionUtils::isNotEmpty) //
                .orElse(new ArrayList<Message>()) //
                .stream() //
                .sorted(Comparator.comparing(Message::getCreated, (m1, m2) -> {
                    return m2.compareTo(m1);
                })) //
                .filter(m -> !BatchPattern.IS_NOTIFY_START.matcher(m.getClient()).matches()) //
                .findFirst() //
                .orElse(null) //
        ;

        if (null == lastRecordMessage) {
            LOGGER.warn("Cannot get last record TRACKER message for record : {}", record.getCode());
            return this.skipRecordWriter;
        }

        final Message lastMessage = Optional.of(nfo.getMessages()) //
                .filter(CollectionUtils::isNotEmpty) //
                .orElse(new ArrayList<Message>()) //
                .stream() //
                .sorted(Comparator.comparing(Message::getCreated, (m1, m2) -> {
                    return m2.compareTo(m1);
                })) //
                .findFirst() //
                .orElse(null) //
        ;

        if (null == lastMessage) {
            LOGGER.warn("Cannot get last TRACKER message for record : {}", record.getCode());
            return this.skipRecordWriter;
        }

        final LocalDate now = LocalDate.now();
        final LocalDate lastRecordCreatedMessage = LocalDateTime.ofInstant(lastRecordMessage.getCreated().toInstant(), lastRecordMessage.getCreated().getTimeZone().toZoneId()).toLocalDate();
        final LocalDate borneInf = now.minusDays(this.searchQueryBuilder.getMinusDaysMin());

        // -->Is removal procedure engaged ??
        final boolean engagedProcedure = (lastRecordCreatedMessage.isBefore(borneInf) || lastRecordCreatedMessage.isEqual(borneInf));

        LOGGER.info("Is last record message date {} is before or equal to {} : {}", lastRecordCreatedMessage, borneInf, engagedProcedure);
        LOGGER.info("Is the removal procedure engaged ? {}", engagedProcedure);

        if (!engagedProcedure) {
            if (lastMessage.getClient().equals(BatchPattern.NOTIFY_STOP)) {
                LOGGER.debug("The removal procedure has been stopped");
                return this.skipRecordWriter;
            }

            LOGGER.debug("The removal procedure is not engaged : stop removal procedure");
            return this.stopRemovalProcedureWriter;
        }

        if (lastMessage.getClient().equals(BatchPattern.NOTIFY_STOP)) {
            LOGGER.debug("The removal procedure has been stopped");
            return this.skipRecordWriter;
        }

        // -->A removal procedure engaged is a record which containing a notify
        // message to alert users
        if (Optional.of(nfo.getMessages()) //
                .filter(CollectionUtils::isNotEmpty) //
                .orElse(new ArrayList<Message>()) //
                .stream() //
                .sorted(Comparator.comparing(Message::getCreated, (m1, m2) -> {
                    return m2.compareTo(m1);
                })) //
                .filter(m -> BatchPattern.IS_NOTIFY_START.matcher(m.getClient()).matches() && m.getCreated().compareTo(lastRecordMessage.getCreated()) >= 0) //
                .count() == 0) {
            LOGGER.debug("Never notify that record : engage removal procedure");
            return this.notifyFirstTimeWriter;
        }

        if (lastMessage.getClient().equals(BatchPattern.NOTIFY_START + this.notifyFirstTimeWriter.getRemainingDays())) {
            // -->Check if the last notification is necessary
            final LocalDate lastCreatedMessage = LocalDateTime.ofInstant(lastMessage.getCreated().toInstant(), lastMessage.getCreated().getTimeZone().toZoneId()).toLocalDate();
            final LocalDate nextRemovalDate = now.plusDays(Long.parseLong(this.notifyLastTimeWriter.getRemainingDays()));
            final Long period = ChronoUnit.DAYS.between(lastCreatedMessage, nextRemovalDate);

            LOGGER.debug("Last notification date : {}", lastCreatedMessage);
            LOGGER.debug("Next removal date : {}", nextRemovalDate);
            LOGGER.debug("Period in days : {}", period);

            if (period >= (Integer.parseInt(this.notifyFirstTimeWriter.getRemainingDays()) - Integer.parseInt(this.notifyLastTimeWriter.getRemainingDays()))) {
                LOGGER.debug("A notification has been sent for the record {} : the batch will sent another one", record.getCode());
                return this.notifyLastTimeWriter;
            }
            // <--
        }

        return this.skipRecordWriter;
    }

}
