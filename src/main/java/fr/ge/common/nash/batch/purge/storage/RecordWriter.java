package fr.ge.common.nash.batch.purge.storage;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.stereotype.Component;

import fr.ge.common.storage.ws.model.StoredFile;
import fr.ge.common.storage.ws.rest.IRecordRestService;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.tracker.service.v1.IUidRestService;

/**
 * Storge record archived batch writer.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@Component(value = "purgeStorageRecordWriter")
public class RecordWriter implements ItemWriter<StoredFile> {

    private static final Logger LOGGER = LoggerFactory.getLogger(RecordWriter.class);

    private IRecordRestService storageRecordRestService;

    private IUidRestService uidRestService;

    /**
     * Mutateur sur l'attribut {@link #uidRestService}.
     *
     * @param uidRestService
     *            la nouvelle valeur de l'attribut uidRestService
     */
    public void setUidRestService(final IUidRestService uidRestService) {
        this.uidRestService = uidRestService;
    }

    /**
     * Mutateur sur l'attribut {@link #storageRecordRestService}.
     *
     * @param storageRecordRestService
     *            la nouvelle valeur de l'attribut storageRecordRestService
     */
    public void setStorageRecordRestService(final IRecordRestService storageRecordRestService) {
        this.storageRecordRestService = storageRecordRestService;
    }

    @Override
    public void write(final List<? extends StoredFile> items) throws Exception {
        final List<String> uids = items.stream() //
                .map(StoredFile::getReferenceId) //
                .collect(Collectors.toList());
        this.uidRestService.remove(uids);
        LOGGER.info("Removing tracker uids : {}", uids);

        this.storageRecordRestService.remove(null, null, Arrays.asList(new SearchQueryFilter("reference:" + uids.stream().collect(Collectors.joining(",")))));
        LOGGER.info("Removing storage uids : {}", uids);
    }
}
