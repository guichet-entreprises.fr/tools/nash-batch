package fr.ge.common.nash.batch.record.report.reader;

import java.net.URI;
import java.net.URISyntaxException;

import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.FileSystemOptions;
import org.apache.commons.vfs2.provider.sftp.SftpFileSystemConfigBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ge.common.nash.batch.utils.vfs.SftpConfiguration;

/**
 * @author Christian Cougourdan
 */
public class SftpResourceItemReader extends VfsResourceItemStreamReader {

    private static final Logger LOGGER = LoggerFactory.getLogger(SftpResourceItemReader.class);

    private SftpConfiguration sftpConfig;

    @Override
    protected URI buildUri(final String path) throws URISyntaxException {
        String userInfo = null;
        if (null != this.sftpConfig.getUsername()) {
            userInfo = this.sftpConfig.getUsername();
            if (null != this.sftpConfig.getPassword()) {
                userInfo += ':' + this.sftpConfig.getPassword();
            }
        }

        return new URI("sftp", userInfo, this.sftpConfig.getHost(), this.sftpConfig.getPort(), path, null, null);
    }

    @Override
    protected FileSystemOptions getOptions() throws FileSystemException {
        final FileSystemOptions opts = super.getOptions();

        final SftpFileSystemConfigBuilder builder = SftpFileSystemConfigBuilder.getInstance();
        builder.setStrictHostKeyChecking(opts, this.sftpConfig.getStrictHostKeyChecking());
        builder.setUserDirIsRoot(opts, this.sftpConfig.isUserDirIsRoot());
        builder.setConnectTimeoutMillis(opts, this.sftpConfig.getConnectTimeout());

        return opts;
    }

    public void setSftpConfiguration(final SftpConfiguration cfg) {
        this.sftpConfig = cfg;
    }

}
