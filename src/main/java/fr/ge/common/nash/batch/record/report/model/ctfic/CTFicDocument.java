package fr.ge.common.nash.batch.record.report.model.ctfic;

import java.util.Collection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import fr.ge.common.nash.batch.record.report.model.arfic.ARFicDocument;
import fr.ge.common.nash.batch.record.report.model.common.Erreur;

@XmlRootElement(name = "GUEN_CTFIC")
@XmlAccessorType(XmlAccessType.NONE)
public class CTFicDocument extends ARFicDocument {

    @XmlElement(name = "etatTraitementFichier", required = true)
    private String etatTraitementFichier;

    @XmlElement(name = "erreur")
    private Collection<Erreur> erreurs;

    /**
     * @return the etatTraitementFichier
     */
    public String getEtatTraitementFichier() {
        return this.etatTraitementFichier;
    }

    /**
     * @return the erreur
     */
    public Collection<Erreur> getErreurs() {
        return this.erreurs;
    }

}
