package fr.ge.common.nash.batch.scan.fix.record;

import java.io.File;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Pattern;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.stereotype.Component;
import org.w3c.dom.Node;

import com.fasterxml.jackson.databind.JsonNode;

import fr.ge.common.nash.batch.utils.ScanFixUtils;
import fr.ge.common.nash.batch.utils.XMLRegentQuery;
import fr.ge.common.nash.batch.validation.ScanFixService;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationMeta;
import fr.ge.common.nash.engine.mapping.form.v1_2.meta.MetaElement;
import fr.ge.common.nash.engine.provider.ZipProvider;
import fr.ge.common.storage.ws.model.StorageTrayEnum;
import fr.ge.common.storage.ws.model.StoredFile;
import fr.ge.common.storage.ws.rest.IRecordRestService;
import fr.ge.common.storage.ws.rest.IStorageRestService;
import fr.ge.common.utils.ZipUtil;
import fr.ge.common.utils.bean.FileEntry;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.common.utils.exception.TechnicalException;
import fr.ge.core.rest.exception.RestResponseException;
import fr.ge.directory.ws.v1.bean.ResponseAuthorityBean;
import fr.ge.directory.ws.v1.service.AuthorityServicesWebService;
import fr.ge.directory.ws.v1.service.IAuthorityService;
import fr.ge.tracker.service.v1.IUidRestService;
import fr.ge.tracker.service.v2.IMessageService;

/**
 * Scan & fix batch writer.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@Component(value = "scanFixMessageWriter")
public class ScanFixWriter implements ItemWriter<StoredFile> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ScanFixWriter.class);

    /** DMTDU pattern. **/
    private static final Pattern PATTERN_DMTDU_FILTER = Pattern.compile("^.{33}TDMTDUPGUEN\\.zip$");

    /** Eddie private url. **/
    private String eddieBaseUrl;

    /** Nash record service. **/
    private IRecordRestService storageRecordRestService;

    /** Scan & fix service. **/
    private ScanFixService scanFixService;

    /** Directory authority service. **/
    private IAuthorityService authorityService;

    /** Directory authority service. **/
    private AuthorityServicesWebService authorityServicesWebService;

    /** Tracker uid service. **/
    private IUidRestService uidRestService;

    /** Storage record service. **/
    private IStorageRestService storageRestService;

    /** Tracker cannot fix reference. **/
    private String trackerCannotFixReference;

    /** Tracker message service. **/
    private IMessageService messageService;

    /**
     * Mutateur sur l'attribut {@link #storageRecordRestService}.
     *
     * @param storageRecordRestService
     *            la nouvelle valeur de l'attribut storageRecordRestService
     */
    public void setStorageRecordRestService(final IRecordRestService storageRecordRestService) {
        this.storageRecordRestService = storageRecordRestService;
    }

    /**
     * Mutateur sur l'attribut {@link #scanFixService}.
     *
     * @param scanFixService
     *            la nouvelle valeur de l'attribut scanFixService
     */
    public void setScanFixService(final ScanFixService scanFixService) {
        this.scanFixService = scanFixService;
    }

    /**
     * Mutateur sur l'attribut {@link #authorityService}.
     *
     * @param authorityService
     *            la nouvelle valeur de l'attribut authorityService
     */
    public void setAuthorityService(final IAuthorityService authorityService) {
        this.authorityService = authorityService;
    }

    /**
     * Mutateur sur l'attribut {@link #authorityServicesWebService}.
     *
     * @param authorityServicesWebService
     *            la nouvelle valeur de l'attribut authorityServicesWebService
     */
    public void setAuthorityServicesWebService(final AuthorityServicesWebService authorityServicesWebService) {
        this.authorityServicesWebService = authorityServicesWebService;
    }

    /**
     * Mutateur sur l'attribut {@link #eddieBaseUrl}.
     *
     * @param eddieBaseUrl
     *            la nouvelle valeur de l'attribut eddieBaseUrl
     */
    public void setEddieBaseUrl(final String eddieBaseUrl) {
        this.eddieBaseUrl = eddieBaseUrl;
    }

    /**
     * Mutateur sur l'attribut {@link #uidRestService}.
     *
     * @param uidRestService
     *            la nouvelle valeur de l'attribut uidRestService
     */
    public void setUidRestService(IUidRestService uidRestService) {
        this.uidRestService = uidRestService;
    }

    /**
     * Mutateur sur l'attribut {@link #storageRestService}.
     *
     * @param storageRestService
     *            la nouvelle valeur de l'attribut storageRestService
     */
    public void setStorageRestService(final IStorageRestService storageRestService) {
        this.storageRestService = storageRestService;
    }

    /**
     * Mutateur sur l'attribut {@link #trackerCannotFixReference}.
     *
     * @param trackerCannotFixReference
     *            la nouvelle valeur de l'attribut trackerCannotFixReference
     */
    public void setTrackerCannotFixReference(final String trackerCannotFixReference) {
        this.trackerCannotFixReference = trackerCannotFixReference;
    }

    /**
     * Mutateur sur l'attribut {@link #messageService}.
     *
     * @param messageService
     *            la nouvelle valeur de l'attribut messageService
     */
    public void setMessageService(final IMessageService messageService) {
        this.messageService = messageService;
    }

    @Override
    public void write(final List<? extends StoredFile> items) throws Exception {
        for (final StoredFile record : items) {
            try {
                // -->Download archived record from STORAGE
                final Response download = this.storageRecordRestService.download(record.getId());
                LOGGER.debug("Download archived record {} from STORAGE", record.getId());

                if (Status.OK.getStatusCode() != download.getStatus()) {
                    throw new TechnicalException("Cannot download storage archived record : " + record.getReferenceId());
                }

                final byte[] archiveAsBytes = download.readEntity(byte[].class);

                // -->Retrieve latest DMTDU as resource as bytes
                LOGGER.debug("Retrieve DMTDU as resource as bytes from archived record : {}", record.getId());
                final FileEntry dmtdu = ZipUtil.entries(archiveAsBytes) //
                        .stream() //
                        .sorted(Comparator.reverseOrder()) //
                        .filter(entry -> PATTERN_DMTDU_FILTER.matcher(entry).matches()) //
                        .findFirst() //
                        .map(name -> ZipUtil.entry(name, archiveAsBytes)) //
                        .orElse(null);

                final byte[] dmtduResourceAsBytes = dmtdu.asBytes();
                if (null == dmtdu || ArrayUtils.isEmpty(dmtduResourceAsBytes)) {
                    throw new TechnicalException("Cannot find any DMTDU file into storage archived record : " + record.getReferenceId());
                }

                // -->Apply fix service
                final byte[] outResourceAsBytes = this.scanFixService.fix(dmtduResourceAsBytes);
                if (ArrayUtils.isEmpty(outResourceAsBytes)) {
                    throw new TechnicalException("Cannot fix the DMTDU file into storage archived record : " + record.getReferenceId());
                }

                if (Arrays.equals(dmtduResourceAsBytes, outResourceAsBytes)) {
                    this.uidRestService.addMessage(this.trackerCannotFixReference, //
                            String.format("[CANNOT.FIX] [%s] Aucune correction appliquée pour ce dossier.", record.getReferenceId()), //
                            "scan-fix" //
                    );
                    LOGGER.warn("The batch detect no errors and does not apply any correction in the file {} from the record {}", dmtdu.name(), record.getReferenceId());
                    continue;
                }

                // -->Send fixed dmtdu via target authority using EDDIE channel
                final FileEntry xmlRegentEntry = ScanFixUtils.getXMLRegentEntry(outResourceAsBytes);
                if (null == xmlRegentEntry) {
                    throw new TechnicalException("Cannot find XML Regent file into DMTDU file from storage archived record : " + record.getReferenceId());
                }

                final Node destinataire = XMLRegentQuery.extractDestinataire(ZipUtil.entry(xmlRegentEntry.name(), outResourceAsBytes));
                if (null == destinataire || (null != destinataire && StringUtils.isEmpty(destinataire.getTextContent()))) {
                    throw new TechnicalException("Cannot find the target authority into the XML Regent file into DMTDU file from storage archived record : " + record.getReferenceId());
                }

                final String codeEdi = destinataire.getTextContent();
                LOGGER.debug("Target authority EDI code : {}", codeEdi);

                // -->Get EDDIE token from DIRECTORY
                final JsonNode details = Optional.ofNullable( //
                        this.authorityService.search(//
                                0L, //
                                1L, //
                                Arrays.asList(new SearchQueryFilter("details.ediCode:" + codeEdi)), //
                                Arrays.asList(new SearchQueryOrder("entityId:asc")), //
                                null) //
                ) //
                        .filter(r -> null != r) //
                        .map(SearchResult::getContent) //
                        .filter(CollectionUtils::isNotEmpty) //
                        .orElse(new ArrayList<ResponseAuthorityBean>()) //
                        .stream() //
                        .findFirst() //
                        .map(ResponseAuthorityBean::getEntityId) //
                        .map(entityId -> {
                            try {
                                LOGGER.debug("Get channel configuration for target authority EDI code : {}", codeEdi);
                                return Optional.ofNullable(this.authorityServicesWebService.computeActiveChannel(entityId)) //
                                        .filter(d -> null != d.get("ftp") && null != d.get("ftp").get("token") && null != d.get("ftp").get("pathFolder")) //
                                        .<TechnicalException>orElseThrow(() -> new TechnicalException(
                                                "Cannot get eddie channel configuratiion for the target authority " + entityId + " for archived record " + record.getReferenceId())) //
                                ;
                            } catch (RestResponseException e) {
                                throw new TechnicalException("Cannot find active channel for the target authority " + entityId + " for archived record " + record.getReferenceId());
                            }
                        }) //
                        .orElse(null);

                if (null == details) {
                    throw new TechnicalException("Cannot find active channel for the target authority " + codeEdi + " for archived record " + record.getReferenceId());
                }

                // -->Get new dmtdu file name (and fini file name)
                final Map<String, String> zipFiles = this.scanFixService.generateFiles(outResourceAsBytes);
                final String zipFileName = zipFiles.get("zip");
                final String zipFiniFileName = zipFiles.get("fini");

                // -->Post both files into EDDIE based on target authority
                // configuration
                this.postEddie(codeEdi, details, outResourceAsBytes, zipFileName);
                this.postEddie(codeEdi, details, "".getBytes(), zipFiniFileName);
                // <--

                LOGGER.info("Fix and send files using eddie channel to target authority {} succeed", codeEdi);
                // <--

                this.archiveRecord(record.getReferenceId(), outResourceAsBytes, zipFileName, archiveAsBytes);

                this.messageService.removeMessages(this.trackerCannotFixReference, record.getReferenceId());
                LOGGER.info("Remove Tracker message record {} into reference {}", this.trackerCannotFixReference, record.getReferenceId());

            } catch (final TechnicalException e) {
                LOGGER.error("An error occured when fixing the archived record with reference : " + record.getReferenceId(), e);
                // -->TODO Post a Tracker message with reference
                // YYYY-MM.SPT.INFERNO
                final String reference = StringUtils.join(DateTimeFormatter.ofPattern("YYYY-MM").format(LocalDate.now()), ".SPT.INFERNO");
                this.uidRestService.addMessage(reference, record.getReferenceId(), "scan-fix");
            }
        }
    }

    /**
     * Post a file into EDDIE using DIRECTORY authority configuration.
     * 
     * @param codeEdi
     * @param details
     * @param resourceAsBytes
     * @param name
     */
    private void postEddie(final String codeEdi, final JsonNode details, final byte[] resourceAsBytes, final String name) {
        final String token = details.get("ftp").get("token").asText();
        final String pathFolder = details.get("ftp").get("pathFolder").asText();

        final String streamName = Optional.ofNullable(pathFolder) //
                .filter(p -> StringUtils.isNotEmpty(p) && !"null".equals(p)) //
                .map(p -> String.join(File.separator, p, name)) //
                .orElse(String.join(File.separator, name));

        final Response upload = ClientBuilder.newClient() //
                .register(com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider.class) //
                .target(this.eddieBaseUrl + "/exchange") //
                .queryParam("uid", token) //
                .queryParam("streamName", streamName) //
                .queryParam("comment", StringUtils.EMPTY) //
                .request() //
                .post(Entity.entity(resourceAsBytes, javax.ws.rs.core.MediaType.APPLICATION_OCTET_STREAM));

        if (null != upload && upload.getStatus() != Status.OK.getStatusCode() && upload.getStatus() != Status.CREATED.getStatusCode()) {
            throw new TechnicalException(String.format("Cannot post file {} using EDDIE channel for the target authority {}", name, codeEdi));
        }
        LOGGER.info("Send file {} using eddie channel to target authority {} succeed", streamName, codeEdi);
    }

    /**
     * Store a record into Storage containing new fixed DTMDU
     * 
     * @param uid
     * @param entryAsBytes
     * @param entryName
     * @param archiveAsBytes
     */
    private void archiveRecord(final String uid, final byte[] entryAsBytes, final String entryName, final byte[] archiveAsBytes) {
        // -->Include new fixed DMTDU
        final Map<String, byte[]> filesToInsert = new HashMap<>();
        filesToInsert.put(entryName, entryAsBytes);
        final byte[] storageResourceAsBytes = ZipUtil.entries(filesToInsert, archiveAsBytes);
        final SpecificationLoader loader = SpecificationLoader.create(new ZipProvider(storageResourceAsBytes));

        // -->Prepare metas
        final List<SearchQueryFilter> metas = new ArrayList<>();
        metas.addAll( //
                Arrays.asList(//
                        new SearchQueryFilter("author", loader.description().getAuthor()), //
                        new SearchQueryFilter("description", loader.description().getDescription()), //
                        new SearchQueryFilter("title", loader.description().getTitle()) //
                ) //
        );

        Optional.of(loader) //
                .filter(l -> null != l) //
                .map(SpecificationLoader::meta) //
                .filter(m -> null != m) //
                .map(FormSpecificationMeta::getMetas) //
                .filter(l -> null != l) //
                .orElse(new ArrayList<MetaElement>()) //
                .forEach(m -> metas.add(new SearchQueryFilter(StringUtils.join(m.getName(), ":", m.getValue()))));

        // -->Store record into STORAGE
        final Response response = this.storageRestService.store( //
                StorageTrayEnum.ARCHIVED.getName().toLowerCase(), //
                new Attachment("storageUploadFile", MediaType.APPLICATION_OCTET_STREAM, storageResourceAsBytes), //
                String.format("%s.zip", loader.description().getRecordUid()), //
                loader.description().getRecordUid(), //
                metas //
        );
        if (Status.OK.getStatusCode() == response.getStatus()) {
            LOGGER.info("Archive record {} to the storage queue {} with success", loader.description().getRecordUid(), StorageTrayEnum.ARCHIVED);
        } else {
            throw new TechnicalException("Cannot store record to storage for record " + loader.description().getRecordUid());
        }
    }
}
