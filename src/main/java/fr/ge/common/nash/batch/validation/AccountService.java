/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.batch.validation;

import java.util.Map;
import java.util.Properties;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Account service.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@Service
public class AccountService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AccountService.class);

    @Autowired
    private Properties appProperties;

    @SuppressWarnings("unchecked")
    public Map<String, String> readUser(final String userId) {
        if (StringUtils.isEmpty(userId)) {
            return null;
        }
        try {
            final Response response = ClientBuilder.newClient() //
                    .register(com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider.class) //
                    .target(this.appProperties.getProperty("account.ge.url.ws.private") + "/private/users/" + userId) //
                    .request() //
                    .get();

            if (null != response && response.getStatus() != Status.OK.getStatusCode()) {
                LOGGER.error("Cannot get information from user {}", userId);
                return null;
            }
            return response.readEntity(Map.class);
        } catch (Exception e) {
            LOGGER.error(String.format("Cannot get information from user %s", userId), e);
            return null;
        }
    }
}
