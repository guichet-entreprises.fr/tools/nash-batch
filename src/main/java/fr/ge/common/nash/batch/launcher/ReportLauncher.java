package fr.ge.common.nash.batch.launcher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.repository.support.JobRepositoryFactoryBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ReportLauncher {

    private final static Logger LOGGER = LoggerFactory.getLogger(ReportLauncher.class);

    public static void main(final String[] args) throws Exception {
        final ApplicationContext context = new ClassPathXmlApplicationContext("classpath:jobs/record-report.xml");
        final Job job = context.getBean("record-report", Job.class);

        final JobRepositoryFactoryBean jobRepositoryFactory = context.getBean(JobRepositoryFactoryBean.class);
        final JobRepository jobRepository = jobRepositoryFactory.getObject();

        final JobParameters jobParameters = new JobParameters();
        final JobExecution jobExecution = jobRepository.createJobExecution("record-report", jobParameters);

        job.execute(jobExecution);
        LOGGER.debug("job finished");
    }

}
