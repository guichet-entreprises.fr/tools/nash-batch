package fr.ge.common.nash.batch.record.report.model.majdu;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * @author Christian Cougourdan
 */
@XmlAccessorType(XmlAccessType.NONE)
public class IdentifiantDossierUnique {

    @XmlElement(name = "codePartenaireEmetteur", required = true)
    private String codePartenaireEmetteur;

    @XmlElement(name = "numeroDossierUnique", required = true)
    private String numeroDossierUnique;

    /**
     * @return the codePartenaireEmetteur
     */
    public String getCodePartenaireEmetteur() {
        return this.codePartenaireEmetteur;
    }

    /**
     * @return the numeroDossierUnique
     */
    public String getNumeroDossierUnique() {
        return this.numeroDossierUnique;
    }

}
