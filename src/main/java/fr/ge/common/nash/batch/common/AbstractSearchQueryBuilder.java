/**
 * 
 */
package fr.ge.common.nash.batch.common;

import java.util.ArrayList;
import java.util.List;

import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;

/**
 * Common search query builder.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public abstract class AbstractSearchQueryBuilder {

    protected List<SearchQueryFilter> filters;

    protected List<SearchQueryOrder> orders;

    protected Integer minusDaysMin;

    protected Integer minusDaysMax;

    public abstract void build();

    public AbstractSearchQueryBuilder() {
        this.filters = new ArrayList<SearchQueryFilter>();
        this.orders = new ArrayList<SearchQueryOrder>();
    }

    /**
     * Accesseur sur l'attribut {@link #filters}.
     *
     * @return List<SearchQueryFilter> filters
     */
    public List<SearchQueryFilter> getFilters() {
        return filters;
    }

    /**
     * Mutateur sur l'attribut {@link #filters}.
     *
     * @param filters
     *            la nouvelle valeur de l'attribut filters
     */
    public void setFilters(final List<SearchQueryFilter> filters) {
        this.filters = filters;
    }

    /**
     * Accesseur sur l'attribut {@link #orders}.
     *
     * @return List<SearchQueryOrder> orders
     */
    public List<SearchQueryOrder> getOrders() {
        return orders;
    }

    /**
     * Mutateur sur l'attribut {@link #orders}.
     *
     * @param orders
     *            la nouvelle valeur de l'attribut orders
     */
    public void setOrders(final List<SearchQueryOrder> orders) {
        this.orders = orders;
    }

    /**
     * Accesseur sur l'attribut {@link #minusDaysMin}.
     *
     * @return Integer minusDaysMin
     */
    public Integer getMinusDaysMin() {
        return minusDaysMin;
    }

    /**
     * Mutateur sur l'attribut {@link #minusDaysMin}.
     *
     * @param minusDaysMin
     *            la nouvelle valeur de l'attribut minusDaysMin
     */
    public void setMinusDaysMin(final Integer minusDaysMin) {
        this.minusDaysMin = minusDaysMin;
    }

    /**
     * Accesseur sur l'attribut {@link #minusDaysMax}.
     *
     * @return Integer minusDaysMax
     */
    public Integer getMinusDaysMax() {
        return minusDaysMax;
    }

    /**
     * Mutateur sur l'attribut {@link #minusDaysMax}.
     *
     * @param minusDaysMax
     *            la nouvelle valeur de l'attribut minusDaysMax
     */
    public void setMinusDaysMax(final Integer minusDaysMax) {
        this.minusDaysMax = minusDaysMax;
    }
}
