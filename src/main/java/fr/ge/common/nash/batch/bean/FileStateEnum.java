/**
 * 
 */
package fr.ge.common.nash.batch.bean;

import java.util.Arrays;

/**
 * The file type enum.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public enum FileStateEnum {

    SCAN_OK("SCAN_OK", "Scan with success"),

    SCAN_KO("SCAN_KO", "Scan with failure"),

    FIX_OK("FIX_OK", "Fix with success"),

    FIX_KO("FIX_KO", "Fix with failure"),

    UNECESSARY_FIX("UNECESSARY_FIX", "Unecessary fix"),

    CANNOT_FIX_FILE("CANNOT_FIX_FILE", "Cannot fix"),

    FILE_NOT_FOUND("FILE_NOT_FOUND", "File not found"),

    SEND_OK("SEND_OK", "Send with success"),

    SEND_KO("SEND_OK", "Send with failure"),

    PREPARE_MASS_MAILING("PREPARE_EMAIL", "Preparing mass mailing");

    private String code;

    private String label;

    FileStateEnum(final String code, final String label) {
        this.code = code;
        this.label = label;
    }

    public void setCode(final String code) {
        this.code = code;
    }

    public String getCode() {
        return this.code;
    }

    public void setLabel(final String label) {
        this.label = label;
    }

    public String getLabel() {
        return this.label;
    }

    /**
     * Get type from code.
     *
     * @param code
     *            the code identifier
     * @return the status enum
     */
    public static FileStateEnum getType(final String code) {
        return Arrays.asList(FileStateEnum.values()) //
                .stream() //
                .filter(s -> s.getCode().equalsIgnoreCase(code)) //
                .findFirst() //
                .orElse(null) //
        ;
    }
}
