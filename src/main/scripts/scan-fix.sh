#!/bin/bash

##################################################################################################################
# Nom du fichier  : scan-fix.sh
# Description     : shell de lancement du batch d'analyse et correction des dossiers en erreur notifiés au GREFFE
#
# Date   : 13/07/2020
#
# Version          : 0.1
##################################################################################################################
#  0.1  : 13/07/2020 : CAP   : creation
##################################################################################################################

##################################################################################################################
# Constantes
##################################################################################################################
#Home directory
#repertoire courant
CURDIR=$( cd -P -- "$(dirname -- "$(command -v -- "$0")")" && pwd -P )
#home directory
BATCH_HOME=$(dirname "$CURDIR")
#repertoire log
LOG_DIR="${BATCH_HOME}/logs"
#Date de log
DATELOGFILE=`date '+%Y%m%d%H%M%S'`
#Nom du job
NOMJOB="scan-fix-error"
CARTE_PARAMS_USAGE="-r XXXX -i AAAA-MM-JJ -s AAAA-MM-JJ"
#Nom du log du shell
LOG_FILE="${LOG_DIR}/${DATELOGFILE}_${NOMJOB}.log"
TODAY=`date +%Y-%m-%d`

export NOMJOB;
export CARTE_PARAMS_USAGE;
export LOG_FILE;

##############################################################################################################################
# Fonctions utilitaires utilisees dans les traitements
##############################################################################################################################
function usage
{
    echo " "
    echo " Usage :"
    echo " $0 $CARTE_PARAMS_USAGE "
	echo " -r (obligatoire) : référence TRACKER pour rechercher les messages"
	echo " -i (non-obligatoire) : borne inférieure sous forme de date (date du jour par défaut)"
	echo " -s (non-obligatoire) : borne supérieure sous forme de date (date du jour par défaut)"
    exit 12
}

function do_classpath
{
	DIR_CONF=${BATCH_HOME}/conf
	DIR_LIB=${BATCH_HOME}/lib

	THE_CLASSPATH=${DIR_CONF}

	for i in `ls $DIR_LIB/*.jar`
	do
	  THE_CLASSPATH=${THE_CLASSPATH}:${i}
	done
}

##############################################################################################################################
# Main
##############################################################################################################################
#Gestion des parametres d'appel
while getopts ":r:i:s:h" option;
do
  case "${option}" in
    r)  TRACKER_REFERENCE=${OPTARG}
        ;;
    i)  BORNE_INF=${OPTARG}
        ;;
    s)  BORNE_SUP=$OPTARG 
        ;;
    h)  usage ;;
    :)  echo "L'option -$OPTARG necessite un argument."; usage ;;
  esac
done
shift $(($OPTIND - 2))

##############################################################################################################################
# JAVA
##############################################################################################################################
JAVA_COMMAND_PATH=java

#Chargement des proprietes de la jvm
. ${BATCH_HOME}/bin/jvm.config

JAVA_OPTS="${JAVA_OPTS_BATCH} -Xms${MIN_HEAPSPACE} -Xmx${MAX_HEAPSPACE}"

#classpath du batch
do_classpath
BATCH_CLASSPATH=$THE_CLASSPATH

export CLASSPATH=${BATCH_CLASSPATH}
##############################################################################################################################

##################################################################################################################
#Lancement du shell de batch
##################################################################################################################
DATELOG=`date '+%d%m%Y-%H%M%S'`
echo "[GE][INFO] - ${DATELOG} - Appel:" >>  ${LOG_FILE}

DATELOG=`date '+%d%m%Y-%H%M%S'`
echo "[GE][INFO] - ${DATELOG} - Lancement du batch ${NOMJOB} avec pour date inférieure ${BORNE_INF} et supérieure ${BORNE_SUP}" >> ${LOG_FILE}

#Execution du batch
${JAVA_COMMAND_PATH} ${JAVA_OPTS} -classpath ${CLASSPATH} org.springframework.batch.core.launch.support.CommandLineJobRunner jobs/scan-fix.xml ${NOMJOB} ref=${TRACKER_REFERENCE} inf=${BORNE_INF} sup=${BORNE_SUP} >> ${LOG_FILE}

#Recuperation du code retour
RET=`echo $?`

#LOG FIN BATCH
DATELOG=`date '+%d%m%Y-%H%M%S'`
echo "[GE][INFO] - ${DATELOG} - Fin de lancement du batch ${NOMJOB} "  >> ${LOG_FILE}

#Gestion du code retour
if [ "$RET" != 0 ]; then
	exit 12;
else
	exit 0;
fi
