package fr.ge.common.nash.batch.validation;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.io.FileUtils;
import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.cfg.Annotations;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;

import fr.ge.common.nash.batch.bean.FileState;
import fr.ge.common.nash.batch.bean.FileStateEnum;
import fr.ge.common.utils.test.AbstractTest;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/batch-context.xml", "classpath*:spring/validator-context.xml" })
public class EddieServiceTest extends AbstractTest {

    /** The Constant ENDPOINT. */
    protected static final String ENDPOINT = "http://localhost:8888/eddie";

    /** The server. */
    protected Server server;

    /** The service. */
    protected ITestService eddieClient;

    public static final String TOKEN = "1234";

    public static final String PATH_FOLDER = "dir";

    @Autowired
    private EddieService service;

    private File inputFile;

    private File outputFile;

    private File outputFileFini;

    /**
     * {@inheritDoc}
     */
    @Before
    public void setUp() throws Exception {
        final String timeStamp = new SimpleDateFormat("yyyy-MM").format(Calendar.getInstance().getTime());
        final String timeStamp1 = new SimpleDateFormat("dd").format(Calendar.getInstance().getTime());

        this.inputFile = new File("target/send/" + timeStamp + "/" + timeStamp1 + "/input.txt");
        this.outputFile = new File("target/send/" + timeStamp + "/" + timeStamp1 + "/output.txt");
        this.outputFileFini = new File("target/send/" + timeStamp + "/" + timeStamp1 + "/output.txt.fini");

        FileUtils.writeByteArrayToFile(this.inputFile, "inputFile".getBytes());
        FileUtils.writeByteArrayToFile(this.outputFile, "outputFileZip".getBytes());
        FileUtils.writeByteArrayToFile(this.outputFileFini, "".getBytes());

        this.eddieClient = Mockito.mock(ITestService.class);

        final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
        final Object jsonProvider = new JacksonJaxbJsonProvider(mapper, new Annotations[] { Annotations.JAXB });

        final JAXRSServerFactoryBean serverFactory = new JAXRSServerFactoryBean();
        serverFactory.setAddress(ENDPOINT);
        serverFactory.setServiceBean(this.eddieClient);
        serverFactory.setProvider(jsonProvider);
        this.server = serverFactory.create();
    }

    @Path("")
    public static interface ITestService {

        @POST
        @Path("/exchange")
        @Consumes(MediaType.APPLICATION_OCTET_STREAM)
        @Produces(MediaType.APPLICATION_JSON)
        Response exchange(@QueryParam("uid") final String uid, @QueryParam("comment") final String comment, @QueryParam("streamName") final String streamName, final byte[] resourceAsBytes);
    }

    @Test
    public void testSendFile() throws Exception {
        // -->prepare
        when(this.eddieClient.exchange(any(), any(), any(), any())).thenReturn(Response.ok().build());

        // -->call
        final FileState state = this.service.uploadToEddie(this.inputFile.getAbsolutePath(), TOKEN, PATH_FOLDER, this.outputFile.getAbsolutePath(), this.outputFileFini.getAbsolutePath());

        // -->verify
        verify(this.eddieClient).exchange(eq(TOKEN), eq(null), eq(PATH_FOLDER + File.separator + "output.txt"), eq("outputFileZip".getBytes()));
        verify(this.eddieClient).exchange(eq(TOKEN), eq(null), eq(PATH_FOLDER + File.separator + "output.txt.fini"), eq("".getBytes()));

        assertThat(state, //
                allOf( //
                        hasProperty("state", equalTo(FileStateEnum.SEND_OK)), //
                        hasProperty("inputFile", equalTo(this.inputFile.getAbsolutePath())), //
                        hasProperty("outputZipFileName", equalTo(this.outputFile.getAbsolutePath())), //
                        hasProperty("outputZipFiniFileName", equalTo(this.outputFileFini.getAbsolutePath())) //
                ) //
        );
    }
}
