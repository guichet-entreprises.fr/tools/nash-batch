package fr.ge.common.nash.batch.validation;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.emptyArray;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.io.FileUtils;
import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.cfg.Annotations;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;

import fr.ge.common.nash.batch.bean.Errors;
import fr.ge.common.nash.batch.validation.ScanFixService.BatchTypeEnum;
import fr.ge.common.nash.batch.validator.ScanFixValidator;
import fr.ge.common.utils.test.AbstractTest;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/batch-context.xml", "classpath*:spring/validator-context.xml", "classpath:spring/data-context.xml" })
public class ScanFixServiceTest extends AbstractTest {

    /** The Constant ENDPOINT. */
    protected static final String NASH_ENDPOINT = "http://localhost:8888/nash";

    /** The Constant ENDPOINT. */
    protected static final String ACCOUNT_ENDPOINT = "http://localhost:8888/account";

    @Autowired
    private ScanFixValidator validator;

    @Autowired
    private ScanFixService service;

    private File scanZip;

    private File scanDir;

    private File outputDir;

    private File scanCsv;

    @Autowired
    private Properties appProperties;

    /** The Account server. */
    protected Server serverAccount;

    /** The Nash server. */
    protected Server serverNash;

    /** The Nash client. */
    protected INashService nashClient;

    /** The Account client. */
    protected IAccountService accountClient;

    public static interface INashService {

        @GET
        @Path("/v1/Record/code/{code}/file")
        @Produces("application/zip")
        Response download(@PathParam("code") String code);
    }

    public static interface IAccountService {

        @GET
        @Path("/private/users/{id}")
        @Produces(MediaType.APPLICATION_JSON)
        Map<String, String> readUser(@PathParam("id") String id);
    }

    /**
     * {@inheritDoc}
     */
    @Before
    public void setUp() throws Exception {
        final String timeStamp = new SimpleDateFormat("yyyy-MM").format(Calendar.getInstance().getTime());
        final String timeStamp1 = new SimpleDateFormat("dd").format(Calendar.getInstance().getTime());

        this.scanZip = new File("target/scan/" + timeStamp + "/" + timeStamp1 + "/input/scan.zip");

        this.scanDir = new File("target/scan/" + timeStamp + "/" + timeStamp1 + "/dir");
        FileUtils.deleteDirectory(this.scanDir);
        this.scanDir.mkdirs();

        this.outputDir = new File("target/scan/" + timeStamp + "/" + timeStamp1 + "/out");
        this.outputDir.mkdirs();

        final URL urlCsv = ScanFixServiceTest.class.getResource(String.format("%s-%s", this.getClass().getSimpleName(), "scan.csv"));
        this.scanCsv = new File(urlCsv.toURI());

        // -->Init nash client
        this.nashClient = Mockito.mock(INashService.class);
        final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
        final Object jsonProvider = new JacksonJaxbJsonProvider(mapper, new Annotations[] { Annotations.JAXB });
        final JAXRSServerFactoryBean serverFactory = new JAXRSServerFactoryBean();
        serverFactory.setAddress(NASH_ENDPOINT);
        serverFactory.setServiceBean(this.nashClient);
        serverFactory.setProvider(jsonProvider);
        this.serverNash = serverFactory.create();

        // -->Init account client
        this.accountClient = Mockito.mock(IAccountService.class);
        final JAXRSServerFactoryBean serverFactoryAccount = new JAXRSServerFactoryBean();
        serverFactoryAccount.setAddress(ACCOUNT_ENDPOINT);
        serverFactoryAccount.setServiceBean(this.accountClient);
        serverFactoryAccount.setProvider(jsonProvider);
        this.serverAccount = serverFactoryAccount.create();

        final Map<String, String> userInfos = new HashMap<>();
        userInfos.put("email", "test@tst.com");
        userInfos.put("firstName", "John");
        userInfos.put("lastName", "Wick");
        userInfos.put("civility", "Monsieur");
        when(this.accountClient.readUser(anyString())).thenReturn(userInfos);

        when(this.nashClient.download(anyString())).thenReturn(Response.ok(this.resourceAsBytes("record.zip")).build());
    }

    /**
     * Clean up.
     * 
     * @throws IOException
     */
    @After
    public void cleanUp() throws IOException {
        this.scanZip.deleteOnExit();

        FileUtils.deleteDirectory(this.scanDir);
        FileUtils.deleteDirectory(this.outputDir);

        this.serverAccount.stop();
        this.serverAccount.destroy();

        this.serverNash.stop();
        this.serverNash.destroy();
    }

    @Test
    public void testScan() throws IOException {
        // -->prepare
        FileUtils.writeByteArrayToFile(this.scanZip, "content".getBytes());

        when(this.validator.validate(any(Properties.class), any(byte[].class))).thenReturn(new Errors());

        // -->call
        final Map<String, String> params = new HashMap<>();
        params.put("t", BatchTypeEnum.SCAN.getCode());
        params.put("f", this.scanZip.getAbsolutePath());
        params.put("d", this.scanDir.getAbsolutePath());
        params.put("o", this.outputDir.getAbsolutePath());

        this.service.run(params);

        // -->verify
        assertThat(this.scanZip.exists(), equalTo(true));
        assertThat(this.scanDir.isDirectory(), equalTo(true));
        verify(this.validator).validate(any(Properties.class), any(byte[].class));
    }

    @Test
    public void testFix() throws IOException {
        // -->prepare
        final byte[] resourceAsBytes = this.resourceAsBytes("scan.zip");
        final byte[] fixedResourceAsBytes = this.resourceAsBytes("fixed.zip");

        when(this.validator.fix(eq(this.appProperties), eq(resourceAsBytes))).thenReturn(fixedResourceAsBytes);

        // -->Create file and output directory
        FileUtils.writeByteArrayToFile(this.scanZip, resourceAsBytes);

        // -->call
        final Map<String, String> params = new HashMap<>();
        params.put("t", BatchTypeEnum.FIX.getCode());
        params.put("f", this.scanZip.getAbsolutePath());
        params.put("d", this.scanDir.getAbsolutePath());
        params.put("o", this.outputDir.getAbsolutePath());
        this.service.run(params);

        // -->verify
        assertThat(this.scanZip.exists(), equalTo(true));

        assertThat(this.scanDir.isDirectory(), equalTo(true));
        assertThat(this.scanDir.list(), emptyArray());

        assertThat(this.outputDir.isDirectory(), equalTo(true));
        assertThat(this.outputDir.list(), not(emptyArray()));

        verify(this.validator).fix(eq(this.appProperties), eq(resourceAsBytes));
    }

    @Test
    public void testScanFromCsvFile() throws IOException {
        this.scanZip = new File("target/scan/scan.zip");

        // -->prepare
        FileUtils.writeByteArrayToFile(this.scanZip, this.resourceAsBytes("scan.zip"));

        when(this.validator.validate(any(Properties.class), any(byte[].class))).thenReturn(new Errors());

        // -->call
        final Map<String, String> params = new HashMap<>();
        params.put("t", BatchTypeEnum.SCAN.getCode());
        params.put("csv", this.scanCsv.getAbsolutePath());
        params.put("o", this.outputDir.getAbsolutePath());
        this.service.run(params);

        // -->verify
        assertThat(this.scanCsv.exists(), equalTo(true));
        assertThat(this.scanZip.exists(), equalTo(true));
        assertThat(this.scanDir.isDirectory(), equalTo(true));
    }

    @Test
    public void testScanFromDirectory() throws IOException {
        this.scanZip = new File(this.scanDir.getAbsolutePath() + File.separator + "scan.zip");

        // -->prepare
        FileUtils.writeByteArrayToFile(this.scanZip, this.resourceAsBytes("scan.zip"));

        when(this.validator.validate(any(Properties.class), any(byte[].class))).thenReturn(new Errors());

        // -->call
        final Map<String, String> params = new HashMap<>();
        params.put("t", BatchTypeEnum.SCAN.getCode());
        params.put("d", this.scanDir.getAbsolutePath());
        params.put("o", this.outputDir.getAbsolutePath());
        this.service.run(params);

        // -->verify
        assertThat(this.scanZip.exists(), equalTo(true));
        assertThat(this.scanDir.isDirectory(), equalTo(true));
        assertThat(this.scanDir.isDirectory(), equalTo(true));
        assertThat(this.scanDir.list(), not(emptyArray()));
    }
}
