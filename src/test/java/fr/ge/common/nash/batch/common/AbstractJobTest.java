package fr.ge.common.nash.batch.common;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.cfg.Annotations;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;

import fr.ge.common.nash.core.util.JaxbFactoryImpl;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationDescription;
import fr.ge.common.nash.ws.v1.bean.RecordInfoBean;
import fr.ge.common.nash.ws.v1.bean.search.RecordSearchResult;
import fr.ge.common.storage.ws.model.StorageTrayEnum;
import fr.ge.common.storage.ws.model.StoredFile;
import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.common.utils.test.AbstractTest;
import fr.ge.core.exception.TechniqueException;
import fr.ge.core.rest.exception.RestResponseException;
import fr.ge.directory.ws.v1.bean.ResponseAuthorityBean;
import fr.ge.exchange.email.ws.v1.bean.EmailSendBean;
import fr.ge.tracker.bean.Message;
import fr.ge.tracker.bean.UidNfo;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

/**
 * Notify job class test.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/batch-core-config-test.xml", "classpath:jobs/purge-record.xml" })
public abstract class AbstractJobTest extends AbstractTest {

    /** TRACKER notify author first alert. */
    protected static final String TRACKER_AUTHOR_NOTIFY_START_15 = "notify-start-15";

    /** TRACKER notify author last alert. */
    protected static final String TRACKER_AUTHOR_NOTIFY_START_5 = "notify-start-5";

    /** TRACKER notify author stop procedure. */
    protected static final String TRACKER_AUTHOR_NOTIFY_STOP = "notify-stop";

    /** The Constant RECORD_UID. */
    protected static final String RECORD_UID = "2020-01-REC-ORD-01";

    /** The Constant RECORD_UID. */
    protected static final String AUTHOR_UID = "2020-01-AUT-HOR-01";

    /** The Constant STORAGE_UID. */
    protected static final String STORAGE_UID = "2020-01-STO-RED-01";

    /** The Constant AUTHORITY_UID. */
    protected static final String AUTHORITY_UID = "2020-01-AUTHO-RITY-01";

    /** The Constant NASH_ENDPOINT. */
    protected static final String NASH_ENDPOINT = "http://localhost:8888/nash";

    protected static int PARTITION_MAX_RESULTS = 100;

    @Autowired
    protected JobLauncherTestUtils jobLauncherTestUtils;

    protected JobExecution jobExecution;

    /** The Nash server. */
    protected Server nashServer;

    /** The Nash service. */
    protected ITestNashService nashClient;

    @Path("/v1/Record")
    protected static interface ITestNashService {

        /**
         * Search for records.
         *
         * @param startIndex
         *            start index
         * @param maxResults
         *            max results per page
         * @param filters
         *            filters as string
         * @param orders
         *            orders as string
         * @return issues search result
         */
        @GET
        @Produces(MediaType.APPLICATION_JSON)
        RecordSearchResult search(@QueryParam("startIndex") @DefaultValue(SearchQuery.DEFAULT_START_INDEX) long startIndex, //
                @QueryParam("maxResults") @DefaultValue(SearchQuery.DEFAULT_MAX_RESULTS) long maxResults, //
                @QueryParam("filters") List<SearchQueryFilter> filters, //
                @QueryParam("orders") List<SearchQueryOrder> orders //
        );

        /**
         * Remove records based on criteria.
         *
         * @param code
         *            the record identifier
         * @return the response
         */
        @DELETE
        @Path("/remove")
        Response remove(@QueryParam("filters") final List<SearchQueryFilter> filters);

        /**
         * Downloads an item.
         *
         * @param code
         *            the item identifier
         * @param resourcePath
         *            the path of the file to download within the item
         * @return the response
         */
        @GET
        @Path("/code/{code}/file/{resourcePath:.*}")
        Response download(@PathParam("code") String code, @PathParam("resourcePath") String resourcePath);
    }

    /** The Constant EXCHANGE_ENDPOINT. */
    protected static final String EXCHANGE_ENDPOINT = "http://localhost:8888/exchange";

    /** The Exchange server. */
    protected Server exchangeServer;

    /** The Exchange service. */
    protected ITestExchangeService exchangeClient;

    protected static interface ITestExchangeService {
        /**
         * REST method for sending an email without any attachment.
         *
         * @param emailSend
         *            Email information
         * @return The response
         * @throws TechniqueException
         *             is any problem preventing the execution to complete
         *             occurs
         */
        @Path("/email/sendWithoutAttachement")
        @POST
        @Consumes(MediaType.APPLICATION_JSON)
        @Produces(MediaType.APPLICATION_JSON)
        Response sendEmail(@QueryParam("") final EmailSendBean emailSend) throws TechniqueException;
    }

    /** The Constant ACCOUNT_ENDPOINT. */
    protected static final String ACCOUNT_ENDPOINT = "http://localhost:8888/account";

    /** The Account server. */
    protected Server accountServer;

    /** The Account service. */
    protected ITestAccountService accountClient;

    protected static interface ITestAccountService {

        @Path("/private/users/{id}")
        @GET
        @Produces(MediaType.APPLICATION_JSON)
        Response readUser(@PathParam("id") final String idOrEmail) throws TechniqueException;
    }

    /** The Constant STORAGE_ENDPOINT. */
    protected static final String STORAGE_ENDPOINT = "http://localhost:8888/storage";

    /** The Storage server. */
    protected Server storageServer;

    /** The Storage service. */
    protected ITestStorageService storageClient;

    @Path("/v1")
    protected static interface ITestStorageService {

        /**
         * Search for records.
         *
         * @param startIndex
         *            start index
         * @param maxResults
         *            max results per page
         * @param filters
         *            filters as string
         * @param orders
         *            orders as string
         * @return issues search result
         */
        @GET
        @Path("/Record")
        @Produces(MediaType.APPLICATION_JSON)
        SearchResult<StoredFile> search(@QueryParam("startIndex") @DefaultValue(SearchQuery.DEFAULT_START_INDEX) final long startIndex, //
                @QueryParam("maxResults") @DefaultValue(SearchQuery.DEFAULT_MAX_RESULTS) final long maxResults, //
                @QueryParam("filters") final List<SearchQueryFilter> filters, //
                @QueryParam("orders") final List<SearchQueryOrder> orders, //
                @QueryParam("q") final String searchTerms);

        /**
         * REST method downloading a file.
         * 
         * @param storageId
         *            The uid storage
         * @return The response
         */
        @GET
        @Path("/Record/code/{storageId}/download")
        @Produces(MediaType.APPLICATION_OCTET_STREAM)
        Response download(@PathParam("storageId") @DefaultValue("") final String storageId);

        /**
         * Remove records based on criteria.
         *
         * @param code
         *            the record identifier
         * @return the response
         */
        @DELETE
        @Path("/Record/remove")
        Response remove(@QueryParam("uids") final List<String> uids, @QueryParam("fromDate") final String fromDate, @QueryParam("filters") final List<SearchQueryFilter> filters);

        @POST
        @Path("/tray/{storageTray}/file")
        @Consumes(MediaType.MULTIPART_FORM_DATA)
        @Produces(MediaType.APPLICATION_JSON)
        Response store( //
                @PathParam("storageTray") final String storageTray, //
                final Attachment storageFile, //
                @QueryParam("storageFilename") final String storageFilename, //
                @QueryParam("storageReferenceId") final String storageReferenceId, //
                @QueryParam("metas") final List<SearchQueryFilter> metas);
    }

    /** The Constant TRACKER_ENDPOINT. */
    protected static final String TRACKER_ENDPOINT = "http://localhost:8888/tracker";

    /** The Exchange server. */
    protected Server trackerServer;

    /** The Exchange service. */
    protected ITestTrackerService trackerClient;

    protected ITestTrackerMessageService trackerMessageClient;

    @Path("/v1/uid")
    protected static interface ITestTrackerService {

        /**
         * Gets all the data for an uid.
         *
         * @param reference
         *            the reference id
         * @return the response
         */
        @GET
        @Path("/{reference}")
        @Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_XML })
        UidNfo getUid( //
                @PathParam("reference") String reference //
        );

        /**
         * Remove messages for uids.
         *
         * @param uids
         *            the uids
         * @return the response
         */
        @DELETE
        @Path("/remove")
        @Operation(summary = "Remove messages for uids.", tags = { "UID REST Services" })
        Response remove( //
                @QueryParam("uids") List<String> uids //
        );

        @POST
        @Path("/{ref}/msg")
        String addMessage( //
                @PathParam("ref") String ref, //
                @QueryParam("content") String content, //
                @QueryParam("author") String author //
        );
    }

    @Path("/v2")
    protected static interface ITestTrackerMessageService {

        @GET
        @Path("/msg")
        @Produces({ MediaType.APPLICATION_JSON })
        @Operation(summary = "Search for messages.", tags = { "Message Services [v2]" })
        SearchResult<Message> searchMessage( //
                @Parameter(description = "First element offset.") //
                @QueryParam("startIndex") @DefaultValue(SearchQuery.DEFAULT_START_INDEX) final long startIndex, //
                @Parameter(description = "Max elements per page.") //
                @QueryParam("maxResults") @DefaultValue(SearchQuery.DEFAULT_MAX_RESULTS) final long maxResults, // //
                @Parameter(description = "Query filters as `<fieldName><op><value>`") //
                @QueryParam("filters") final List<SearchQueryFilter> filters, //
                @Parameter(description = "Sort orders as `<fieldname>:<asc|desc>`") //
                @QueryParam("orders") final List<SearchQueryOrder> orders //
        );

        @DELETE
        @Path("/ref/{key}/msg")
        @Operation(summary = "Delete messages associated to a reference.", tags = { "Message Services [v2]" })
        Response removeMessages( //
                @Parameter(description = "Reference key which messages are attached to.") //
                @PathParam("key") final String key, //
                @Parameter(description = "Terms") //
                @QueryParam("q") final String terms //
        );
    }

    /** The Constant SIGNATURE_ENDPOINT. */
    protected static final String SIGNATURE_ENDPOINT = "http://localhost:8888/signature";

    /** The Signature server. */
    protected Server signatureServer;

    /** The Signature service. */
    protected ITestSignatureService signatureClient;

    @Path("/api/v1/proxy")
    protected static interface ITestSignatureService {
        /**
         * Remove the proxy session.
         * 
         * @param uids
         *            the records identifier
         * @param fromDate
         *            the input date
         * @param filters
         *            the filters
         * @return
         */
        @DELETE
        @Path("/remove")
        Response remove(@QueryParam("fromDate") final String fromDate, @QueryParam("filters") final List<SearchQueryFilter> filters);
    }

    /** The Constant PAYMENT_ENDPOINT. */
    protected static final String PAYMENT_ENDPOINT = "http://localhost:8888/payment";

    /** The Payment server. */
    protected Server paymentServer;

    /** The Payment service. */
    protected ITestPaymentService paymentClient;

    @Path("/v1/cart")
    protected static interface ITestPaymentService {
        /**
         * Remove the proxy session.
         * 
         * @param uids
         *            the records identifier
         * @param fromDate
         *            the input date
         * @param filters
         *            the filters
         * @return
         */
        @DELETE
        @Path("/remove")
        Response remove(@QueryParam("fromDate") final String fromDate, @QueryParam("filters") final List<SearchQueryFilter> filters);
    }

    /** The Constant DIRECTORY_ENDPOINT. */
    protected static final String DIRECTORY_ENDPOINT = "http://localhost:8888/directory";

    /** The Directory server. */
    protected Server directoryServer;

    protected ITestAuthorityService authorityServiceClient;

    @Path("/v1")
    protected static interface ITestAuthorityService {

        @Path("/authority")
        @GET
        @Produces(MediaType.APPLICATION_JSON)
        @Operation(summary = "Search for Authority, paginated, filtered, ranked, on a full text value.", tags = { "Private Authority Services" })
        @ApiResponses({ //
                @ApiResponse( //
                        responseCode = "200", //
                        description = "Successfull operation.", //
                        content = @Content( //
                                mediaType = MediaType.APPLICATION_JSON //
                        ) //
                ), //
        })
        SearchResult<ResponseAuthorityBean> search( //
                @Parameter(description = "Search index") @QueryParam("startIndex") @DefaultValue(SearchQuery.DEFAULT_START_INDEX) long startIndex, //
                @Parameter(description = "Maximum result") @QueryParam("maxResults") @DefaultValue(SearchQuery.DEFAULT_MAX_RESULTS) long maxResults, //
                @Parameter(description = "Query filters") @QueryParam("filters") List<SearchQueryFilter> filters, //
                @Parameter(description = "Query orders") @QueryParam("orders") List<SearchQueryOrder> orders, //
                @Parameter(description = "Terms") @QueryParam("searchedValue") @DefaultValue("") String searchedValue);
    }

    protected ITestAuthorityServicesWebService authorityServicesWebServiceClient;

    protected static interface ITestAuthorityServicesWebService {

        @GET
        @Path("/private/v1/services/authority/{entityId}/channel/active")
        @Produces(MediaType.APPLICATION_JSON)
        @Operation(summary = "Get authority channels.", tags = { "Private Authority Services" })
        @ApiResponses({ //
                @ApiResponse( //
                        responseCode = "200", //
                        description = "Successfull operation.", //
                        content = @Content( //
                                mediaType = MediaType.APPLICATION_JSON, //
                                examples = @ExampleObject("{ \"entityId\": \"2020-01-AUT-HORITY\", \"label\": \"Authority label\", \"path\": null, \"ftp\": { \"state\": \"enabled\", \"token\": \"1234567890\", \"pathFolder\": null, \"ftpMode\": \"delegatedChannel\", \"comment\": null, \"delegationAuthority\": \"Root Authority\" } }") //
                        ) //
                ), ///
        })
        JsonNode computeActiveChannel( //
                @Parameter(description = "Authority identifier", required = true) @PathParam("entityId") String entityId) throws RestResponseException;
    }

    /** The Constant EDDIE_ENDPOINT. */
    protected static final String EDDIE_ENDPOINT = "http://localhost:8888/eddie";

    /** The eddie server. */
    protected Server eddieServer;

    protected ITestEddieService eddieServiceClient;

    public static interface ITestEddieService {

        @POST
        @Path("/exchange")
        @Consumes(MediaType.APPLICATION_OCTET_STREAM)
        @Produces(MediaType.TEXT_PLAIN)
        Response exchange(@QueryParam("uid") String uid, @QueryParam("comment") String comment, @QueryParam("streamName") String streamName, byte[] resourceAsByte);
    }

    /**
     * {@inheritDoc}
     */
    @Before
    public void setUp() throws Exception {
        // -->NASH CLIENT
        this.nashClient = Mockito.mock(ITestNashService.class);

        final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
        final Object jsonProvider = new JacksonJaxbJsonProvider(mapper, new Annotations[] { Annotations.JAXB });

        // -->NASH Client
        final JAXRSServerFactoryBean serverFactory = new JAXRSServerFactoryBean();
        serverFactory.setAddress(NASH_ENDPOINT);
        serverFactory.setServiceBean(this.nashClient);
        serverFactory.setProvider(jsonProvider);
        this.nashServer = serverFactory.create();

        // -->EXCHANGE Client
        this.exchangeClient = Mockito.mock(ITestExchangeService.class);
        final JAXRSServerFactoryBean serverFactoryExchange = new JAXRSServerFactoryBean();
        serverFactoryExchange.setAddress(EXCHANGE_ENDPOINT);
        serverFactoryExchange.setServiceBean(this.exchangeClient);
        serverFactoryExchange.setProvider(jsonProvider);
        this.exchangeServer = serverFactoryExchange.create();

        // -->ACCOUNT Client
        this.accountClient = Mockito.mock(ITestAccountService.class);
        when(this.accountClient.readUser(anyString())).thenReturn(Response.ok(this.getUserInfo()).build());
        final JAXRSServerFactoryBean serverFactoryAccount = new JAXRSServerFactoryBean();
        serverFactoryAccount.setAddress(ACCOUNT_ENDPOINT);
        serverFactoryAccount.setServiceBean(this.accountClient);
        serverFactoryAccount.setProvider(jsonProvider);
        this.accountServer = serverFactoryAccount.create();

        // -->STORAGE Client
        this.storageClient = Mockito.mock(ITestStorageService.class);
        final JAXRSServerFactoryBean serverFactoryStorage = new JAXRSServerFactoryBean();
        serverFactoryStorage.setAddress(STORAGE_ENDPOINT);
        serverFactoryStorage.setServiceBean(this.storageClient);
        serverFactoryStorage.setProvider(jsonProvider);
        this.storageServer = serverFactoryStorage.create();

        // -->TRACKER Client
        this.trackerClient = Mockito.mock(ITestTrackerService.class);
        this.trackerMessageClient = Mockito.mock(ITestTrackerMessageService.class);
        final JAXRSServerFactoryBean serverFactoryTracker = new JAXRSServerFactoryBean();
        serverFactoryTracker.setAddress(TRACKER_ENDPOINT);
        serverFactoryTracker.setServiceBeans(Arrays.asList(this.trackerClient, this.trackerMessageClient));
        serverFactoryTracker.setProvider(jsonProvider);
        this.trackerServer = serverFactoryTracker.create();

        // -->SIGNATURE Client
        this.signatureClient = Mockito.mock(ITestSignatureService.class);
        final JAXRSServerFactoryBean serverFactorySignature = new JAXRSServerFactoryBean();
        serverFactorySignature.setAddress(SIGNATURE_ENDPOINT);
        serverFactorySignature.setServiceBean(this.signatureClient);
        serverFactorySignature.setProvider(jsonProvider);
        this.signatureServer = serverFactorySignature.create();

        // -->PAYMENT Client
        this.paymentClient = Mockito.mock(ITestPaymentService.class);
        final JAXRSServerFactoryBean serverFactoryPayment = new JAXRSServerFactoryBean();
        serverFactoryPayment.setAddress(PAYMENT_ENDPOINT);
        serverFactoryPayment.setServiceBean(this.paymentClient);
        serverFactoryPayment.setProvider(jsonProvider);
        this.paymentServer = serverFactoryPayment.create();

        // -->DIRECTORY Client
        this.authorityServiceClient = Mockito.mock(ITestAuthorityService.class);
        this.authorityServicesWebServiceClient = Mockito.mock(ITestAuthorityServicesWebService.class);
        final JAXRSServerFactoryBean serverFactoryDirectory = new JAXRSServerFactoryBean();
        serverFactoryDirectory.setAddress(DIRECTORY_ENDPOINT);
        serverFactoryDirectory.setServiceBeans(Arrays.asList(this.authorityServiceClient, this.authorityServicesWebServiceClient));
        serverFactoryDirectory.setProvider(jsonProvider);
        this.directoryServer = serverFactoryDirectory.create();

        // -->EDDIE Client
        this.eddieServiceClient = Mockito.mock(ITestEddieService.class);
        final JAXRSServerFactoryBean serverFactoryEddie = new JAXRSServerFactoryBean();
        serverFactoryEddie.setAddress(EDDIE_ENDPOINT);
        serverFactoryEddie.setServiceBean(this.eddieServiceClient);
        serverFactoryEddie.setProvider(jsonProvider);
        this.eddieServer = serverFactoryEddie.create();
    }

    /**
     * Tear down.
     *
     * @throws Exception
     *             exception
     */
    @After
    public void tearDown() throws Exception {
        if (null != this.nashServer) {
            this.nashServer.stop();
            this.nashServer.destroy();
        }
        if (null != this.exchangeServer) {
            this.exchangeServer.stop();
            this.exchangeServer.destroy();
        }
        if (null != this.accountServer) {
            this.accountServer.stop();
            this.accountServer.destroy();
        }
        if (null != this.storageServer) {
            this.storageServer.stop();
            this.storageServer.destroy();
        }
        if (null != this.trackerServer) {
            this.trackerServer.stop();
            this.trackerServer.destroy();
        }
        if (null != this.signatureServer) {
            this.signatureServer.stop();
            this.signatureServer.destroy();
        }
        if (null != this.paymentServer) {
            this.paymentServer.stop();
            this.paymentServer.destroy();
        }
        if (null != this.directoryServer) {
            this.directoryServer.stop();
            this.directoryServer.destroy();
        }
        if (null != this.eddieServer) {
            this.eddieServer.stop();
            this.eddieServer.destroy();
        }
    }

    protected Calendar localTimeToDate(final LocalDate localDate) {
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(localDate.getYear(), localDate.getMonth().getValue() - 1, localDate.getDayOfMonth());
        return calendar;
    }

    private Map<String, String> getUserInfo() {
        final Map<String, String> userInfo = new HashMap<>();
        userInfo.put("civility", "Monsieur");
        userInfo.put("firstName", "John");
        userInfo.put("lastName", "Wick");
        userInfo.put("language", "fr");
        userInfo.put("email", "john.wick@yopmail.com");
        return userInfo;
    }

    protected LocalDate mockRecordSearch(boolean paymentDone) {
        LocalDate date = LocalDate.now();
        date = date.minusYears(1L);

        final RecordSearchResult defaultResult = new RecordSearchResult();
        defaultResult.setTotalResults(500);
        when(this.nashClient.search(eq(Long.valueOf(0)), eq(Long.valueOf(1)), ArgumentMatchers.<SearchQueryFilter>anyList(), ArgumentMatchers.<SearchQueryOrder>anyList())).thenReturn(defaultResult);

        final RecordSearchResult result = new RecordSearchResult();
        final FormSpecificationDescription desc = JaxbFactoryImpl.instance()
                .unmarshal(paymentDone ? this.resourceAsBytes("description-payment-done.xml") : this.resourceAsBytes("description-payment-in-progress.xml"), FormSpecificationDescription.class);
        final JsonNode details = new ObjectMapper().convertValue(desc, JsonNode.class);
        result.setContent(Arrays.asList( //
                new RecordInfoBean().setCode(RECORD_UID).setAuthor(AUTHOR_UID).setCreated(this.localTimeToDate(date)).setUpdated(this.localTimeToDate(date)).setDetails(details) //
        ));
        result.setTotalResults(result.getContent().size());
        when(this.nashClient.search(eq(0L), eq(100L), ArgumentMatchers.<SearchQueryFilter>anyList(), ArgumentMatchers.<SearchQueryOrder>anyList())).thenReturn(result);

        return date;
    }

    protected void mockStorageRecordSearch() {
        LocalDate date = LocalDate.now();
        date = date.minusYears(1L);

        SearchResult<StoredFile> storageRecords = new SearchResult<>();
        storageRecords.setTotalResults(1L);
        when(this.storageClient.search( //
                eq(0L), //
                eq(1L), //
                ArgumentMatchers.<SearchQueryFilter>anyList(), //
                ArgumentMatchers.<SearchQueryOrder>anyList(), //
                eq(null)) //
        ).thenReturn(storageRecords);

        final StoredFile singleFile = new StoredFile();
        singleFile.setId(STORAGE_UID);
        singleFile.setReferenceId(RECORD_UID);
        singleFile.setCreated(this.localTimeToDate(date).getTime());
        singleFile.setUpdated(this.localTimeToDate(date).getTime());
        singleFile.setTray(StorageTrayEnum.ARCHIVED.name());
        storageRecords.setContent(Arrays.asList(singleFile));
        storageRecords.setTotalResults(storageRecords.getContent().size());
        when(this.storageClient.search( //
                eq(Long.valueOf(0)), //
                eq(Long.valueOf(PARTITION_MAX_RESULTS)), //
                ArgumentMatchers.<SearchQueryFilter>anyList(), //
                ArgumentMatchers.<SearchQueryOrder>anyList(), //
                eq(null)) //
        ).thenReturn(storageRecords);
    }

    protected UidNfo buildTrackerHistory(LocalDate date, String... steps) {
        final List<Message> messages = new ArrayList<Message>();
        for (final String step : steps) {
            final Message message = new Message();
            date = date.plusDays(1L);
            message.setCreated(this.localTimeToDate(date));
            message.setUid(RECORD_UID);
            message.setContent(step);
            message.setClient("nash");
            if (step.equals(TRACKER_AUTHOR_NOTIFY_START_15) || step.equals(TRACKER_AUTHOR_NOTIFY_START_5) || step.equals(TRACKER_AUTHOR_NOTIFY_STOP)) {
                message.setClient(step);
            }
            messages.add(message);
        }

        final UidNfo nfo = new UidNfo();
        nfo.setMessages(messages);
        return nfo;
    }

    /**
     * Formats a String to be at the JSON format.
     *
     * @param str
     *            the input string
     * @return the string with the good format
     */
    protected String json(final String str) {
        return str.replaceAll("'", "\"");
    }
}
