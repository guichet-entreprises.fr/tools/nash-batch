package fr.ge.common.nash.batch.jobs;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.Arrays;

import javax.ws.rs.core.Response;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.springframework.batch.core.BatchStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.ge.common.nash.batch.common.AbstractJobTest;
import fr.ge.common.nash.core.util.JaxbFactoryImpl;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationDescription;
import fr.ge.common.nash.ws.v1.bean.RecordInfoBean;
import fr.ge.common.nash.ws.v1.bean.search.RecordSearchResult;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;

/**
 * Notify job class test.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/batch-core-config-test.xml", "classpath:jobs/notify-users.xml" })
public class JobNotifyTest extends AbstractJobTest {

    /** Job name. */
    private static final String BATCH_JOB_NAME = "notify";

    /**
     * Initialisation de la classe de test.
     * 
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void loadSystemProperties() {
        System.setProperty("job", BATCH_JOB_NAME);
    }

    /**
     * Test notify records created not finalized for the first alert.
     * 
     * @throws Exception
     *             : Exception
     */
    @Test
    public void testNotifyRecordWithNoTrackerAlert() throws Exception {
        // -->prepare
        LocalDate date = this.mockRecordSearch(true);

        when(this.trackerClient.getUid(any())).thenReturn(this.buildTrackerHistory(date, "init", "data"));
        when(this.trackerClient.addMessage(anyString(), anyString(), anyString())).thenReturn("ok");
        when(this.exchangeClient.sendEmail(any())).thenReturn(Response.ok().build());

        // ->call
        this.jobExecution = this.jobLauncherTestUtils.launchStep("notify-record.master");

        // -->verify
        verify(this.nashClient, times(7)).search(anyLong(), anyLong(), anyList(), anyList());
        verify(this.trackerClient).getUid(eq(RECORD_UID));
        verify(this.trackerClient).addMessage(eq(RECORD_UID), anyString(), anyString());
        verify(this.accountClient).readUser(eq(AUTHOR_UID));
        verify(this.exchangeClient).sendEmail(any());
        assertThat(this.jobExecution.getStatus(), equalTo(BatchStatus.COMPLETED));
    }

    /**
     * Test notify records with existing TRACER alert.
     * 
     * @throws Exception
     *             : Exception
     */
    @Test
    public void testNotifyRecordWithFirstAlert() throws Exception {
        // -->prepare
        LocalDate date = this.mockRecordSearch(true);
        when(this.trackerClient.getUid(any())).thenReturn(this.buildTrackerHistory(date, "init", "data", TRACKER_AUTHOR_NOTIFY_START_15));

        // ->call
        this.jobExecution = this.jobLauncherTestUtils.launchStep("notify-record.master");

        // -->verify
        verify(this.nashClient, times(7)).search(anyLong(), anyLong(), anyList(), anyList());
        verify(this.trackerClient).getUid(eq(RECORD_UID));
        verify(this.trackerClient).addMessage(eq(RECORD_UID), anyString(), anyString());
        verify(this.accountClient).readUser(eq(AUTHOR_UID));
        verify(this.exchangeClient).sendEmail(any());
        assertThat(this.jobExecution.getStatus(), equalTo(BatchStatus.COMPLETED));
    }

    /**
     * Test notify records with existing TRACER alert.
     * 
     * @throws Exception
     *             : Exception
     */
    @Test
    public void testNotifyRecordWithFirstAndLastAlert() throws Exception {
        // -->prepare
        LocalDate date = this.mockRecordSearch(true);
        when(this.trackerClient.getUid(any())).thenReturn(this.buildTrackerHistory(date, "init", "data", TRACKER_AUTHOR_NOTIFY_START_15, TRACKER_AUTHOR_NOTIFY_START_5));

        // ->call
        this.jobExecution = this.jobLauncherTestUtils.launchStep("notify-record.master");

        // -->verify
        verify(this.nashClient, times(7)).search(anyLong(), anyLong(), anyList(), anyList());
        verify(this.trackerClient).getUid(eq(RECORD_UID));
        verify(this.trackerClient, never()).addMessage(eq(RECORD_UID), anyString(), anyString());
        verify(this.accountClient, never()).readUser(eq(AUTHOR_UID));
        verify(this.exchangeClient, never()).sendEmail(any());
        assertThat(this.jobExecution.getStatus(), equalTo(BatchStatus.COMPLETED));
    }

    /**
     * Test stop notify records with existing TRACKER alert.
     * 
     * @throws Exception
     *             : Exception
     */
    @Test
    public void testStopNotifyRecord() throws Exception {
        // -->prepare
        final RecordSearchResult defaultResult = new RecordSearchResult();
        defaultResult.setTotalResults(500);
        when(this.nashClient.search(eq(Long.valueOf(0)), eq(Long.valueOf(1)), ArgumentMatchers.<SearchQueryFilter>anyList(), ArgumentMatchers.<SearchQueryOrder>anyList())).thenReturn(defaultResult);

        LocalDate date = LocalDate.now();
        final RecordSearchResult result = new RecordSearchResult();
        final FormSpecificationDescription desc = JaxbFactoryImpl.instance().unmarshal(this.resourceAsBytes("description-payment-done.xml"), FormSpecificationDescription.class);
        final JsonNode details = new ObjectMapper().convertValue(desc, JsonNode.class);
        result.setContent(Arrays.asList( //
                new RecordInfoBean().setCode(RECORD_UID).setAuthor(AUTHOR_UID).setCreated(this.localTimeToDate(date)).setUpdated(this.localTimeToDate(date)).setDetails(details) //
        ));
        result.setTotalResults(result.getContent().size());
        when(this.nashClient.search(eq(0L), eq(100L), ArgumentMatchers.<SearchQueryFilter>anyList(), ArgumentMatchers.<SearchQueryOrder>anyList())).thenReturn(result);
        when(this.trackerClient.getUid(any())).thenReturn(this.buildTrackerHistory(date, "init", "data", TRACKER_AUTHOR_NOTIFY_START_15, "payment"));

        // ->call
        this.jobExecution = this.jobLauncherTestUtils.launchStep("notify-record.master");

        // -->verify
        verify(this.nashClient, times(7)).search(anyLong(), anyLong(), anyList(), anyList());
        verify(this.trackerClient).getUid(eq(RECORD_UID));
        verify(this.trackerClient).addMessage(eq(RECORD_UID), anyString(), anyString());
        assertThat(this.jobExecution.getStatus(), equalTo(BatchStatus.COMPLETED));
    }

    /**
     * Test notify records with stop removal procedure message.
     * 
     * @throws Exception
     *             : Exception
     */
    @Test
    public void testNotifyRecordWithStopProcedureMessage() throws Exception {
        // -->prepare
        LocalDate date = this.mockRecordSearch(true);
        when(this.trackerClient.getUid(any())).thenReturn(this.buildTrackerHistory(date, "init", "data", "payment", TRACKER_AUTHOR_NOTIFY_START_15, "finished", TRACKER_AUTHOR_NOTIFY_STOP));

        // ->call
        this.jobExecution = this.jobLauncherTestUtils.launchStep("notify-record.master");

        // -->verify
        verify(this.nashClient, times(7)).search(anyLong(), anyLong(), anyList(), anyList());
        verify(this.trackerClient).getUid(eq(RECORD_UID));
        verify(this.trackerClient, never()).addMessage(eq(RECORD_UID), anyString(), anyString());
        verify(this.accountClient, never()).readUser(eq(AUTHOR_UID));
        verify(this.exchangeClient, never()).sendEmail(any());
        assertThat(this.jobExecution.getStatus(), equalTo(BatchStatus.COMPLETED));
    }
}
