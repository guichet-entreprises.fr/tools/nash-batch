package fr.ge.common.nash.batch.validator;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Properties;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.io.FileUtils;
import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.cfg.Annotations;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;

import fr.ge.common.nash.batch.bean.AuthorityNaming;
import fr.ge.common.nash.batch.bean.Errors;
import fr.ge.common.nash.batch.utils.ScanFixUtils;
import fr.ge.common.nash.batch.utils.XPathBuilder;
import fr.ge.common.storage.ws.model.StorageTrayEnum;
import fr.ge.common.storage.ws.model.StoredFile;
import fr.ge.common.utils.ZipUtil;
import fr.ge.common.utils.bean.FileEntry;
import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.common.utils.exception.TechnicalException;
import fr.ge.common.utils.test.AbstractTest;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/batch-context.xml", "classpath:spring/validator-context.xml" })
public class ScanFixValidatorTest extends AbstractTest {

    /** The Constant NASH_ENDPOINT. */
    protected static final String NASH_ENDPOINT = "http://localhost:8888/nash";

    /** The Constant PAYMENT_ENDPOINT. */
    protected static final String PAYMENT_ENDPOINT = "http://localhost:8888/payment";

    /** The Nash server. */
    protected Server nashServer;

    /** The Nash service. */
    protected ITestNashService nashClient;

    /** The Payment server. */
    protected Server paymentServer;

    /** The Payment service. */
    protected ITestPaymentService paymentClient;

    @Autowired
    private Properties appProperties;

    @Path("/v1/Record")
    public static interface ITestNashService {

        /**
         * Downloads an item.
         *
         * @param code
         *            the item identifier
         * @param resourcePath
         *            the path of the file to download within the item
         * @return the response
         */
        @GET
        @Path("/code/{code}/file/{resourcePath:.*}")
        Response download(@PathParam("code") String code, @PathParam("resourcePath") String resourcePath);
    }

    @Path("/api/v1/cart")
    public static interface ITestPaymentService {

        @GET
        @Path("/{uid}")
        @Produces(MediaType.APPLICATION_JSON)
        Response getCart(@PathParam("uid") String uid);
    }

    /** The Constant STORAGE_ENDPOINT. */
    protected static final String STORAGE_ENDPOINT = "http://localhost:8888/storage";

    /** The Storage server. */
    protected Server storageServer;

    /** The Storage service. */
    protected ITestStorageService storageClient;

    @Path("/v1/Record")
    protected static interface ITestStorageService {
        /**
         * Search for records.
         *
         * @param startIndex
         *            start index
         * @param maxResults
         *            max results per page
         * @param filters
         *            filters as string
         * @param orders
         *            orders as string
         * @return issues search result
         */
        @GET
        @Produces(MediaType.APPLICATION_JSON)
        SearchResult<StoredFile> search(@QueryParam("startIndex") @DefaultValue(SearchQuery.DEFAULT_START_INDEX) final long startIndex, //
                @QueryParam("maxResults") @DefaultValue(SearchQuery.DEFAULT_MAX_RESULTS) final long maxResults, //
                @QueryParam("filters") final List<SearchQueryFilter> filters, //
                @QueryParam("orders") final List<SearchQueryOrder> orders, //
                @QueryParam("searchTerms") final String searchTerms);
    }

    /**
     * {@inheritDoc}
     */
    @Before
    public void setUp() throws Exception {
        this.nashClient = Mockito.mock(ITestNashService.class);
        this.paymentClient = Mockito.mock(ITestPaymentService.class);
        this.storageClient = Mockito.mock(ITestStorageService.class);

        final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
        final Object jsonProvider = new JacksonJaxbJsonProvider(mapper, new Annotations[] { Annotations.JAXB });

        final JAXRSServerFactoryBean serverFactory = new JAXRSServerFactoryBean();
        serverFactory.setAddress(NASH_ENDPOINT);
        serverFactory.setServiceBean(this.nashClient);
        serverFactory.setProvider(jsonProvider);
        this.nashServer = serverFactory.create();

        final JAXRSServerFactoryBean serverFactoryPayment = new JAXRSServerFactoryBean();
        serverFactoryPayment.setAddress(PAYMENT_ENDPOINT);
        serverFactoryPayment.setServiceBean(this.paymentClient);
        serverFactoryPayment.setProvider(jsonProvider);
        this.paymentServer = serverFactoryPayment.create();

        final JAXRSServerFactoryBean serverFactoryStorage = new JAXRSServerFactoryBean();
        serverFactoryStorage.setAddress(STORAGE_ENDPOINT);
        serverFactoryStorage.setServiceBean(this.storageClient);
        serverFactoryStorage.setProvider(jsonProvider);
        this.storageServer = serverFactoryStorage.create();

        when(this.nashClient.download(anyString(), anyString())).thenReturn(Response.ok(this.resourceAsBytes("eligible-greffe-nash-description.xml")).build());
        when(this.paymentClient.getCart(anyString())).thenReturn(Response.ok(this.resourceAsBytes("cart.json")).build());
    }

    /**
     * Tear down.
     *
     * @throws Exception
     *             exception
     */
    @After
    public void tearDown() throws Exception {
        if (null != this.nashServer) {
            this.nashServer.stop();
            this.nashServer.destroy();
        }
        if (null != this.paymentServer) {
            this.paymentServer.stop();
            this.paymentServer.destroy();
        }
        if (null != this.storageServer) {
            this.storageServer.stop();
            this.storageServer.destroy();
        }
    }

    /**
     * Fix.
     *
     * @param resourceName
     *            resource name
     * @return errors
     */
    private byte[] fix(final String resourceName) {
        return this.fix(this.resourceAsBytes(resourceName));
    }

    /**
     * Fix.
     *
     * @param resourceAsBytes
     *            resource as bytes
     * @return errors
     */
    private byte[] fix(final byte[] resourceAsBytes) {
        return new ScanFixValidator().fix(this.appProperties, resourceAsBytes);
    }

    /**
     * Validate.
     *
     * @param resourceName
     *            resource name
     * @return errors
     */
    private Errors validate(final String resourceName) {
        return this.validate(this.resourceAsBytes(resourceName));
    }

    /**
     * Validate.
     *
     * @param resourceAsBytes
     *            resource as bytes
     * @return errors
     */
    private Errors validate(final byte[] resourceAsBytes) {
        return new ScanFixValidator().validate(this.appProperties, resourceAsBytes);
    }

    /**
     * Test missing XMLTC file.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testMissingXMLTC() throws Exception {
        final Errors actual = this.validate("missing-xmltc-file.zip");
        assertThat(actual.hasErrors(), equalTo(true));
    }

    /**
     * Test invalid XMLTC.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testMissingXMLRegent() throws Exception {
        final Errors actual = this.validate("invalid-xmltc-file.zip");
        assertThat(actual.hasErrors(), equalTo(true));
    }

    /**
     * Test invalid XMLTC.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testValidXMLTC() throws Exception {
        final Errors actual = this.validate("valid-xmltc-file.zip");
        assertThat(actual.hasErrors(), equalTo(false));
    }

    /**
     * Test fix emetteur and destinataire tag value.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testFixTagEmetteurDestintaire() throws Exception {
        // -->call
        final byte[] actual = this.fix("wrong-emetteur-destintaire.zip");

        // -->verify
        assertThat(actual, notNullValue());

        try {
            final FileEntry xmltcEntry = ScanFixUtils.getXMLTCEntry(actual);
            final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setFeature("http://xml.org/sax/features/external-general-entities", false);
            factory.setFeature("http://xml.org/sax/features/external-parameter-entities", false);

            final DocumentBuilder builder = factory.newDocumentBuilder();
            final Document document = builder.parse(new ByteArrayInputStream(xmltcEntry.asBytes()));
            final Node emetteur = Optional.of(document) //
                    .map(d -> d.getElementsByTagName(IFunctionalValidator.TAG_EMETTEUR)) //
                    .map(n -> n.item(0)) //
                    .orElse(null) //
            ;
            assertThat(emetteur.getTextContent(), equalTo(IFunctionalValidator.EMETTEUR_VALUE));

            final Node destinataire = Optional.of(document) //
                    .map(d -> d.getElementsByTagName(IFunctionalValidator.TAG_DESTINATAIRE)) //
                    .map(n -> n.item(0)) //
                    .orElse(null) //
            ;
            assertThat(destinataire.getTextContent(), equalTo(IFunctionalValidator.DESTINATAIRE_VALUE));
        } catch (ParserConfigurationException | SAXException | IOException e) {
            throw new TechnicalException(e);
        }
    }

    /**
     * Test fix wrong extension documents to upper case.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testFixDocumentExtension() throws Exception {
        // -->call
        final byte[] actual = this.fix("fix-wrong-extension-pj.zip");

        // -->verify
        assertThat(actual, notNullValue());

        try {
            final FileEntry xmltcEntry = ScanFixUtils.getXMLTCEntry(actual);
            final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setFeature("http://xml.org/sax/features/external-general-entities", false);
            factory.setFeature("http://xml.org/sax/features/external-parameter-entities", false);

            final DocumentBuilder builder = factory.newDocumentBuilder();
            final Document document = builder.parse(new ByteArrayInputStream(xmltcEntry.asBytes()));

            final Element root = document.getDocumentElement();
            final XPathFactory xpf = XPathFactory.newInstance();
            final XPath path = xpf.newXPath();

            final NodeList pieceJointelist = (NodeList) path.evaluate("//dossierUnique/pieceJointe", root, XPathConstants.NODESET);

            for (int idx = 0; idx < pieceJointelist.getLength(); idx++) {
                final Node pieceJointe = pieceJointelist.item(idx);
                final Node formatPieceJointe = (Node) path.evaluate("formatPieceJointe", pieceJointe, XPathConstants.NODE);
                assertThat(IFunctionalValidator.REGEX_EXTENSION_UPPERCASE.matcher(formatPieceJointe.getTextContent()).matches(), equalTo(true));
            }

        } catch (ParserConfigurationException | SAXException | IOException e) {
            throw new TechnicalException(e);
        }
    }

    /**
     * Test fix wrong denomination.
     *
     * @throws Exception
     *             exception
     */
    @Ignore
    @Test
    public void testFixDenomination() throws Exception {
        // -->call
        final byte[] actual = this.fix("fix-wrong-denomination.zip");

        // -->verify
        assertThat(actual, notNullValue());
        final Node denomination = Optional.of(actual) //
                .filter(r -> null != r) //
                .map(r -> ZipUtil.entries(r)) //
                .orElse(new ArrayList<String>()) //
                .stream() //
                .filter(entry -> IFunctionalValidator.REGEX_XMLTC.matcher(entry).matches()) //
                .findFirst() //
                .map(xml -> ZipUtil.entry(xml, actual)) //
                .map(FileEntry::asBytes) //
                .map(b -> XPathBuilder.buildDocument(b)) //
                .map(doc -> XPathBuilder.queryNode(doc.getDocumentElement(), "//GUEN_DMTDU/dossierUnique/identificationDossierUnique/nomDossier")) //
                .orElse(null);
        assertThat(denomination.getTextContent(), equalTo("Dupond JEAN"));
    }

    /**
     * Test duplicate tag 'destinataireDossierCfe'.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testFixDuplicateDestinataireDossierCfeTag() throws Exception {
        // -->call
        final byte[] actual = this.fix("duplicate-tag-destinataireDossierCfe.zip");

        // -->verify
        assertThat(actual, notNullValue());
        final int total = Optional.of(actual) //
                .filter(r -> null != r) //
                .map(r -> ZipUtil.entries(r)) //
                .orElse(new ArrayList<String>()) //
                .stream() //
                .filter(entry -> IFunctionalValidator.REGEX_XMLTC.matcher(entry).matches()) //
                .findFirst() //
                .map(xml -> ZipUtil.entry(xml, actual)) //
                .map(FileEntry::asBytes) //
                .map(b -> XPathBuilder.buildDocument(b)) //
                .map(doc -> XPathBuilder.queryNodeList(doc.getDocumentElement(), "//GUEN_DMTDU/dossierUnique/dossierCfe/destinataireDossierCfe")) //
                .map(nl -> nl.getLength()).orElse(0);
        assertThat(total, equalTo(1));
    }

    /**
     * Test scan CCI file.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testScanCCIFile() throws Exception {
        final Errors actual = this.validate("fix-cci.zip");
        assertThat(actual.hasErrors(), equalTo(true));
    }

    /**
     * Test fix CCI file.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testFixCCIFile() throws Exception {
        // -->call
        final byte[] actual = this.fix("fix-cci.zip");

        // -->verify
        assertThat(actual, notNullValue());

        final boolean matches = Optional.of(actual) //
                .filter(r -> null != r) //
                .map(r -> ZipUtil.entries(r)) //
                .orElse(new ArrayList<String>()) //
                .stream() //
                .allMatch(entry -> AuthorityNaming.CCI.getXmltcPattern().matcher(entry).matches() || AuthorityNaming.CCI.getAttachmentPattern().matcher(entry).matches()
                        || AuthorityNaming.CCI.getXmlRegentPattern().matcher(entry).matches()) //
        ;
        assertThat(matches, equalTo(true));
    }

    /**
     * Test scan tag paiementDossier for GREFFE file.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testScanPaiementDossierTag() throws Exception {
        final Errors actual = this.validate("paiementDossier.zip");
        assertThat(actual.hasErrors(), equalTo(true));
    }

    /**
     * Test fix tag paiementDossier for GREFFE file.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testFixPaiementDossierTag() throws Exception {
        // -->call
        final byte[] actual = this.fix("paiementDossier.zip");

        // -->verify
        assertThat(actual, notNullValue());

        final Node paiementDossier = Optional.of(actual) //
                .filter(r -> null != r) //
                .map(r -> ZipUtil.entries(r)) //
                .orElse(new ArrayList<String>()) //
                .stream() //
                .filter(entry -> IFunctionalValidator.REGEX_XMLTC.matcher(entry).matches()) //
                .findFirst() //
                .map(xml -> ZipUtil.entry(xml, actual)) //
                .map(FileEntry::asBytes) //
                .map(b -> XPathBuilder.buildDocument(b)) //
                .map(doc -> XPathBuilder.queryNode(doc.getDocumentElement(), "//GUEN_DMTDU/dossierUnique/dossierCfe/destinataireDossierCfe/paiementDossier")) //
                .orElse(null);
        assertThat(paiementDossier, nullValue());

    }

    /**
     * Test fix remove tag codePays.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testFixRemoveTagCodePays() throws Exception {
        // -->call
        final byte[] actual = this.fix("fix-codePays.zip");

        // -->verify
        assertThat(actual, notNullValue());

        final Node paiementDossier = Optional.of(actual) //
                .filter(r -> null != r) //
                .map(r -> ZipUtil.entries(r)) //
                .orElse(new ArrayList<String>()) //
                .stream() //
                .filter(entry -> IFunctionalValidator.REGEX_XMLTC.matcher(entry).matches()) //
                .findFirst() //
                .map(xml -> ZipUtil.entry(xml, actual)) //
                .map(FileEntry::asBytes) //
                .map(b -> XPathBuilder.buildDocument(b)) //
                .map(doc -> XPathBuilder.queryNode(doc.getDocumentElement(), "//GUEN_DMTDU/identificationDossierUnique/correspondant/adresseCorrespondant/codePays")) //
                .orElse(null);
        assertThat(paiementDossier, nullValue());

    }

    /**
     * Test scan eligible GREFFE file.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testScanEligible() throws Exception {
        final Errors actual = this.validate("immat-greffe-dmtdu.zip");
        assertThat(actual.hasErrors(), equalTo(false));
    }

    /**
     * Test scan correct URSSAF file.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testScanCorrectUrssafFile() throws Exception {
        assertThat(this.validate("urssaf-ok.zip").hasErrors(), equalTo(false));
    }

    /**
     * Test scan incorrect URSSAF file.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testScanIncorrectUrssafFile() throws Exception {
        assertThat(this.validate("urssaf-malformed.zip").hasErrors(), equalTo(true));
    }

    /**
     * Test scan wrong URSSAF file.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testFixUrssafFile() throws Exception {
        // -->call
        final byte[] actual = this.fix("urssaf-ko.zip");

        // -->verify
        assertThat(actual, notNullValue());

        assertThat(Optional.of(actual) //
                .filter(r -> null != r) //
                .map(r -> ZipUtil.entries(r)) //
                .orElse(new ArrayList<String>()) //
                .stream() //
                .allMatch(entry -> entry.startsWith("C0016A1005L")), equalTo(true));

        final byte[] xmltcAsBytes = Optional.of(actual) //
                .filter(r -> null != r) //
                .map(r -> ZipUtil.entries(r)) //
                .orElse(new ArrayList<String>()) //
                .stream() //
                .filter(entry -> IFunctionalValidator.REGEX_XMLTC.matcher(entry).matches()) //
                .findFirst() //
                .map(xml -> ZipUtil.entry(xml, actual)) //
                .map(FileEntry::asBytes) //
                .orElse(null);
        assertThat(xmltcAsBytes, notNullValue());

        final Node numeroDeLiasse = Optional.of(actual) //
                .filter(r -> null != r) //
                .map(r -> ZipUtil.entries(r)) //
                .orElse(new ArrayList<String>()) //
                .stream() //
                .filter(entry -> IFunctionalValidator.REGEX_XMLTC.matcher(entry).matches()) //
                .findFirst() //
                .map(xml -> ZipUtil.entry(xml, actual)) //
                .map(FileEntry::asBytes) //
                .map(b -> XPathBuilder.buildDocument(b)) //
                .map(doc -> XPathBuilder.queryNode(doc.getDocumentElement(), "//MGUN_DEMAT/formalite/numeroDeLiasse")) //
                .orElse(null);
        assertThat(numeroDeLiasse, notNullValue());
        assertThat(numeroDeLiasse.getTextContent(), equalTo("H10000024947"));
    }

    /**
     * Test fix malformed URSSAF file.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testFixMalformeUrssafFile() throws Exception {
        // -->call
        final byte[] actual = this.fix("urssaf-malformed.zip");

        // -->verify
        assertThat(actual, notNullValue());

        final byte[] xmltcAsBytes = Optional.of(actual) //
                .filter(r -> null != r) //
                .map(r -> ZipUtil.entries(r)) //
                .orElse(new ArrayList<String>()) //
                .stream() //
                .filter(entry -> IFunctionalValidator.REGEX_XMLTC.matcher(entry).matches()) //
                .findFirst() //
                .map(xml -> ZipUtil.entry(xml, actual)) //
                .map(FileEntry::asBytes) //
                .orElse(null);
        assertThat(xmltcAsBytes, notNullValue());

        final Node numeroDeLiasse = Optional.of(actual) //
                .filter(r -> null != r) //
                .map(r -> ZipUtil.entries(r)) //
                .orElse(new ArrayList<String>()) //
                .stream() //
                .filter(entry -> IFunctionalValidator.REGEX_XMLTC.matcher(entry).matches()) //
                .findFirst() //
                .map(xml -> ZipUtil.entry(xml, actual)) //
                .map(FileEntry::asBytes) //
                .map(b -> XPathBuilder.buildDocument(b)) //
                .map(doc -> XPathBuilder.queryNode(doc.getDocumentElement(), "//MGUN_DEMAT/formalite/numeroDeLiasse")) //
                .orElse(null);
        assertThat(numeroDeLiasse, notNullValue());
        assertThat(numeroDeLiasse.getTextContent(), equalTo("H10000024812"));
    }

    /**
     * Test scan incorrect CMA file.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testScanIncorrectCMAFile() throws Exception {
        assertThat(this.validate("fix-wrong-cma.zip").hasErrors(), equalTo(true));
    }

    /**
     * Test fix malformed URSSAF file.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testFixIncorrectCMAFile() throws Exception {
        // -->call
        final byte[] actual = this.fix("fix-wrong-cma.zip");

        // -->verify
        assertThat(actual, notNullValue());

        final byte[] xmltcAsBytes = Optional.of(actual) //
                .filter(r -> null != r) //
                .map(r -> ZipUtil.entries(r)) //
                .orElse(new ArrayList<String>()) //
                .stream() //
                .filter(entry -> IFunctionalValidator.REGEX_XMLTC.matcher(entry).matches()) //
                .findFirst() //
                .map(xml -> ZipUtil.entry(xml, actual)) //
                .map(FileEntry::asBytes) //
                .orElse(null);
        assertThat(xmltcAsBytes, notNullValue());

        final Node paiementDossier = Optional.of(actual) //
                .filter(r -> null != r) //
                .map(r -> ZipUtil.entries(r)) //
                .orElse(new ArrayList<String>()) //
                .stream() //
                .filter(entry -> IFunctionalValidator.REGEX_XMLTC.matcher(entry).matches()) //
                .findFirst() //
                .map(xml -> ZipUtil.entry(xml, actual)) //
                .map(FileEntry::asBytes) //
                .map(b -> XPathBuilder.buildDocument(b)) //
                .map(doc -> XPathBuilder.queryNode(doc.getDocumentElement(), "//MGUN_DEMAT/formalite/destinataires/paiement/paiementCarteBleue")) //
                .orElse(null);
        assertThat(paiementDossier, notNullValue());
    }

    /**
     * Test scan valid 'typeDestinataire' tag value from CMA file.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testScanCMATypeDestinataire() throws Exception {
        Errors actual = this.validate("cma-valid-type-destinataire.zip");
        assertThat(actual.hasErrors(), equalTo(false));

        actual = this.validate("cma-invalid-type-destinataire.zip");
        assertThat(actual.hasErrors(), equalTo(true));
    }

    /**
     * Test fix 'typeDestinataire' tag value from CMA file.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testFixCMATypeDestinataire() throws Exception {
        // -->call
        final byte[] actual = this.fix("cma-invalid-type-destinataire.zip");

        // -->verify
        assertThat(actual, notNullValue());

        final byte[] xmltcAsBytes = Optional.of(actual) //
                .filter(r -> null != r) //
                .map(r -> ZipUtil.entries(r)) //
                .orElse(new ArrayList<String>()) //
                .stream() //
                .filter(entry -> IFunctionalValidator.REGEX_XMLTC.matcher(entry).matches()) //
                .findFirst() //
                .map(xml -> ZipUtil.entry(xml, actual)) //
                .map(FileEntry::asBytes) //
                .orElse(null);
        assertThat(xmltcAsBytes, notNullValue());

        final Node typeDestinataire = Optional.of(actual) //
                .filter(r -> null != r) //
                .map(r -> ZipUtil.entries(r)) //
                .orElse(new ArrayList<String>()) //
                .stream() //
                .filter(entry -> IFunctionalValidator.REGEX_XMLTC.matcher(entry).matches()) //
                .findFirst() //
                .map(xml -> ZipUtil.entry(xml, actual)) //
                .map(FileEntry::asBytes) //
                .map(b -> XPathBuilder.buildDocument(b)) //
                .map(doc -> XPathBuilder.queryNode(doc.getDocumentElement(), "//MGUN_DEMAT/formalite/destinataires/typeDestinataire")) //
                .orElse(null);
        assertThat(typeDestinataire, notNullValue());
        assertThat(typeDestinataire.getTextContent(), equalTo("CFE+AC"));
    }

    /**
     * Test fix ineligible GREFFE file.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testFixIneligible() throws Exception {
        when(this.nashClient.download(anyString(), anyString())).thenReturn(Response.ok(this.resourceAsBytes("ineligible-greffe-nash-description.xml")).build());

        // -->call
        final byte[] actual = this.fix("immat-greffe-dmtdu.zip");

        // -->verify
        assertThat(actual, notNullValue());

        final FileEntry xmlRegentEntry = ScanFixUtils.getXMLRegentEntry(actual);
        assertThat(xmlRegentEntry, notNullValue());
        assertThat(xmlRegentEntry.asBytes(), notNullValue());

        final Node c05 = Optional.of(xmlRegentEntry) //
                .map(FileEntry::asBytes) //
                .map(b -> XPathBuilder.buildDocument(b)) //
                .map(doc -> XPathBuilder.queryNode(doc.getDocumentElement(), "//REGENT-XML/ServiceApplicatif/Liasse/Service/IDF/C05")) //
                .orElse(null);
        assertThat(c05, notNullValue());
        assertThat(c05.getTextContent(), equalTo("C"));
    }

    /**
     * Test scan DMTDU containing png files.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testScanPNG() throws Exception {
        Errors actual = this.validate("png-files.zip");
        assertThat(actual.hasErrors(), equalTo(true));
    }

    /**
     * Test fix DMTDU containing png files.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testFixPNG() throws Exception {
        // -->call
        final byte[] actual = this.fix("png-files.zip");

        // -->verify
        assertThat(actual, notNullValue());

        final boolean containsPDF = Optional.of(actual) //
                .filter(r -> null != r) //
                .map(r -> ZipUtil.entries(r)) //
                .orElse(new ArrayList<String>()) //
                .stream() //
                .allMatch(entry -> !IFunctionalValidator.JPG_JPEG_PNG_EXTENSION.matcher(entry).matches()) //
        ;
        assertThat(containsPDF, equalTo(true));

        try {
            final FileEntry xmltcEntry = ScanFixUtils.getXMLTCEntry(actual);
            final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setFeature("http://xml.org/sax/features/external-general-entities", false);
            factory.setFeature("http://xml.org/sax/features/external-parameter-entities", false);

            final DocumentBuilder builder = factory.newDocumentBuilder();
            final Document document = builder.parse(new ByteArrayInputStream(xmltcEntry.asBytes()));

            final Element root = document.getDocumentElement();
            final XPathFactory xpf = XPathFactory.newInstance();
            final XPath path = xpf.newXPath();

            final NodeList pieceJointelist = (NodeList) path.evaluate("//dossierUnique/pieceJointe", root, XPathConstants.NODESET);

            for (int idx = 0; idx < pieceJointelist.getLength(); idx++) {
                final Node pieceJointe = pieceJointelist.item(idx);
                final Node fichierPieceJointe = (Node) path.evaluate("fichierPieceJointe", pieceJointe, XPathConstants.NODE);
                assertThat(IFunctionalValidator.JPG_JPEG_PNG_EXTENSION.matcher(fichierPieceJointe.getTextContent()).matches(), equalTo(false));
            }

        } catch (ParserConfigurationException | SAXException | IOException e) {
            throw new TechnicalException(e);
        }
    }

    /**
     * Test fix typeDeVoie tag.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testFixTypeDeVoieTag() throws Exception {
        // -->call
        final byte[] actual = this.fix("typeDeVoie.zip");

        // -->verify
        assertThat(actual, notNullValue());

        final FileEntry xmltcEntry = ScanFixUtils.getXMLTCEntry(actual);
        final Document document = XPathBuilder.buildDocument(xmltcEntry.asBytes());

        final Node adresseCorrespondant = XPathBuilder.queryNode(document.getDocumentElement(), IFunctionalValidator.ADRESSE_CORRESPONDANT);
        assertThat(XPathBuilder.queryString(adresseCorrespondant, "libelleDeVoie"), notNullValue());
        assertThat(XPathBuilder.queryString(adresseCorrespondant, "libelleDeVoie"), equalTo("RES Diderot"));
    }

    /**
     * Test scan DMTDU with mgun content for GREFFE authority.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testScanMGUN() throws Exception {
        Errors actual = this.validate("mgun.zip");
        assertThat(actual.hasErrors(), equalTo(true));
    }

    /**
     * Test scan DMTDU with mgun content for GREFFE authority.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testFixMGUN() throws Exception {
        // -->prepare
        LocalDate date = LocalDate.now();
        date = date.minusYears(1L);

        final SearchResult<StoredFile> storageRecords = new SearchResult<>();
        final StoredFile singleFile = new StoredFile();
        singleFile.setId("STORAGE_UID");
        singleFile.setReferenceId("RECORD_UID");
        singleFile.setTray(StorageTrayEnum.ARCHIVED.name());
        storageRecords.setContent(Arrays.asList(singleFile));
        storageRecords.setTotalResults(storageRecords.getContent().size());

        when(this.storageClient.search( //
                eq(0L), //
                eq(1L), //
                eq(Arrays.asList(new SearchQueryFilter("tray:" + StorageTrayEnum.ARCHIVED.getName()))), //
                ArgumentMatchers.<SearchQueryOrder>anyList(), //
                ArgumentMatchers.anyString()) //
        ).thenReturn(storageRecords);

        // -->call
        final byte[] actual = this.fix("mgun.zip");

        // -->verify
        assertThat(actual, notNullValue());

        final FileEntry xmltcEntry = ScanFixUtils.getXMLTCEntry(actual);
        final Document document = XPathBuilder.buildDocument(xmltcEntry.asBytes());
        assertThat(XPathBuilder.queryNode(document.getDocumentElement(), "//MGUN_DEMAT/version"), nullValue());
    }

    /**
     * Test scan typePieceJointe tag.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testScanTypePieceJointe() throws Exception {
        Errors actual = this.validate("typePieceJointe.zip");
        assertThat(actual.hasErrors(), equalTo(true));
    }

    /**
     * Test fix typePieceJointe tag.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testFixTypePieceJointe() throws Exception {
        // -->call
        final byte[] actual = this.fix("typePieceJointe.zip");

        // -->verify
        assertThat(actual, notNullValue());

        final FileEntry xmltcEntry = ScanFixUtils.getXMLTCEntry(actual);
        final Document document = XPathBuilder.buildDocument(xmltcEntry.asBytes());
        assertThat(XPathBuilder.queryNode(document.getDocumentElement(), "//GUEN_DMTDU/dossierUnique/pieceJointe[typePieceJointe='LIASSE_TDR_PDF']"), notNullValue());
        assertThat(XPathBuilder.queryNode(document.getDocumentElement(), "//GUEN_DMTDU/dossierUnique/pieceJointe[typePieceJointe='LIASSE_TDR_EDI']"), notNullValue());
        assertThat(XPathBuilder.queryNode(document.getDocumentElement(), "//GUEN_DMTDU/dossierUnique/pieceJointe[typePieceJointe='LIASSE_CFE_PDF']"), nullValue());
        assertThat(XPathBuilder.queryNode(document.getDocumentElement(), "//GUEN_DMTDU/dossierUnique/pieceJointe[typePieceJointe='LIASSE_CFE_EDI']"), nullValue());
    }

    /**
     * Test scan typePieceJointe tag with RCS value.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testScanTypePieceJointeRCS() throws Exception {
        Errors actual = this.validate("typePieceJointe-rcs.zip");
        assertThat(actual.hasErrors(), equalTo(true));
    }

    /**
     * Test fix typePieceJointe tag with RCS value.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testFixTypePieceJointeRCS() throws Exception {
        // -->call
        final byte[] actual = this.fix("typePieceJointe-rcs.zip");

        // -->verify
        assertThat(actual, notNullValue());

        final FileEntry xmltcEntry = ScanFixUtils.getXMLTCEntry(actual);
        final Document document = XPathBuilder.buildDocument(xmltcEntry.asBytes());
        assertThat(XPathBuilder.queryNode(document.getDocumentElement(), "//GUEN_DMTDU/dossierUnique/pieceJointe[typePieceJointe='RCS-FORMULAIRE_BE']"), nullValue());
        assertThat(XPathBuilder.queryNode(document.getDocumentElement(), "//GUEN_DMTDU/dossierUnique/pieceJointe[typePieceJointe='RCS-FORM_BE_SOCIETE']"), nullValue());
    }

    /**
     * Test scan mandatory payment block.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testScanMandatoryPaymentBlock() throws Exception {
        Arrays.asList("mandatory-payment-block-C10.1.zip", "mandatory-payment-block-C05.zip").forEach(file -> { //
            Errors actual = this.validate(file);
            assertThat(actual.hasErrors(), equalTo(true));
        });
    }

    /**
     * Test fix mandatory payment block.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testFixMandatoryPaymentBlock() throws Exception {
        // -->call
        Arrays.asList("mandatory-payment-block-C10.1.zip", "mandatory-payment-block-C05.zip").forEach(file -> { //

            final byte[] actual = this.fix(file);
            // -->verify
            assertThat(actual, notNullValue());

            try {
                FileUtils.writeByteArrayToFile(new File("C:\\GE\\tmp\\result-" + file), actual);
            } catch (IOException e) {
                e.printStackTrace();
            }

            final Node paiementDossier = Optional.of(actual) //
                    .filter(r -> null != r) //
                    .map(r -> ZipUtil.entries(r)) //
                    .orElse(new ArrayList<String>()) //
                    .stream() //
                    .filter(entry -> IFunctionalValidator.REGEX_XMLTC.matcher(entry).matches()) //
                    .findFirst() //
                    .map(xml -> ZipUtil.entry(xml, actual)) //
                    .map(FileEntry::asBytes) //
                    .map(b -> XPathBuilder.buildDocument(b)) //
                    .map(doc -> XPathBuilder.queryNode(doc.getDocumentElement(), "//GUEN_DMTDU/dossierUnique/dossierCfe/destinataireDossierCfe/paiementDossier"))
                    //
                    .orElse(null);
            assertThat(paiementDossier, notNullValue());
        });
    }
}
