package fr.ge.common.nash.batch.jobs;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.matches;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.w3c.dom.Document;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.ge.common.nash.batch.common.AbstractJobTest;
import fr.ge.common.nash.batch.utils.ScanFixUtils;
import fr.ge.common.nash.batch.utils.XPathBuilder;
import fr.ge.common.storage.ws.model.StorageTrayEnum;
import fr.ge.common.storage.ws.model.StoredFile;
import fr.ge.common.utils.ZipUtil;
import fr.ge.common.utils.bean.FileEntry;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.core.rest.exception.RestResponseException;
import fr.ge.directory.ws.v1.bean.ResponseAuthorityBean;
import fr.ge.tracker.bean.Message;

/**
 * Scan and fix job class test.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/batch-core-config-test.xml", "classpath:jobs/scan-fix.xml" })
public class JobScanFixTest extends AbstractJobTest {

    /** Job name. */
    private static final String BATCH_JOB_NAME = "scan-fix-error";

    private static final ObjectMapper MAPPER = new ObjectMapper();

    /**
     * Initialisation de la classe de test.
     * 
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void loadSystemProperties() {
        System.setProperty("job", BATCH_JOB_NAME);
    }

    /**
     * Test scan fix job.
     * 
     * @throws Exception
     *             : Exception
     * @throws RestResponseException
     */
    @Test
    public void testScanFix() throws Exception, RestResponseException {
        // -->prepare
        final LocalDate now = LocalDate.now();

        final SearchResult<Message> result = new SearchResult<Message>();
        result.setMaxResults(1L);
        result.setStartIndex(0L);
        result.setTotalResults(1L);
        final Message message = new Message();
        message.setUid(RECORD_UID);
        message.setContent(String.format("[CRE_DOS_REJETEE_SES] [%s] Rejet du dossier de création par le SES : dossier non conforme", RECORD_UID));
        message.setCreated(this.localTimeToDate(LocalDate.now()));
        result.setContent(Arrays.asList(message));

        when(this.trackerMessageClient.searchMessage(anyLong(), anyLong(), anyList(), anyList())).thenReturn(result);

        final SearchResult<StoredFile> storageRecords = new SearchResult<>();
        storageRecords.setTotalResults(1L);

        final StoredFile singleFile = new StoredFile();
        singleFile.setId(STORAGE_UID);
        singleFile.setReferenceId(RECORD_UID);
        singleFile.setTray(StorageTrayEnum.ARCHIVED.name());
        storageRecords.setContent(Arrays.asList(singleFile));
        storageRecords.setTotalResults(storageRecords.getContent().size());
        when(this.storageClient.search(anyLong(), anyLong(), anyList(), anyList(), anyString())).thenReturn(storageRecords);
        when(this.storageClient.download(eq(STORAGE_UID))).thenReturn(Response.ok(this.resourceAsBytes("storage-archived.zip")).build());
        when(this.nashClient.download(RECORD_UID, "description.xml")).thenReturn(Response.ok(ZipUtil.entry("description.xml", this.resourceAsBytes("storage-archived.zip")).asBytes()).build());

        final SearchResult<ResponseAuthorityBean> authorityResult = new SearchResult<ResponseAuthorityBean>();
        authorityResult.setMaxResults(1L);
        authorityResult.setStartIndex(0L);
        authorityResult.setTotalResults(1L);
        final ResponseAuthorityBean authority = new ResponseAuthorityBean();
        authority.setId(42L);
        authority.setEntityId(AUTHORITY_UID);
        authority.setLabel("LABEL");
        authorityResult.setContent(Arrays.asList(authority));

        when(this.authorityServiceClient.search(anyLong(), anyLong(), anyList(), anyList(), anyString())).thenReturn(authorityResult);
        when(this.authorityServicesWebServiceClient.computeActiveChannel(eq(AUTHORITY_UID))).thenReturn(MAPPER.readTree(this.json("{ 'entityId': '" + AUTHORITY_UID
                + "', 'label': 'AUTHORITY', 'path': null, 'ftp': { 'state': 'enabled', 'token': '12345678901234567890', 'pathFolder': 'HOMEDIR', 'ftpMode': 'delegatedChannel', 'comment': null, 'delegationAuthority': 'ROOT' } }")));
        when(this.eddieServiceClient.exchange(anyString(), anyString(), anyString(), any(byte[].class))).thenReturn(Response.ok().build());
        when(this.storageClient.store(anyString(), any(Attachment.class), anyString(), anyString(), anyList())).thenReturn(Response.ok().build());

        when(this.trackerMessageClient.removeMessages(anyString(), anyString())).thenReturn(Response.ok().build());
        // ->call
        this.jobExecution = this.jobLauncherTestUtils.launchStep("scan-fix-error.master", //
                new JobParametersBuilder() //
                        .addString("ref", "{yyyy-MM}.SPT.ERROR") //
                        .addString("inf", DateTimeFormatter.ofPattern("YYYY-MM-dd").format(now)) //
                        .addString("sup", DateTimeFormatter.ofPattern("YYYY-MM-dd").format(now)) //
                        .toJobParameters() //
        );

        // -->verify
        verify(this.trackerMessageClient, times(2)).searchMessage(anyLong(), anyLong(), anyList(), anyList());
        verify(this.authorityServiceClient).search( //
                eq(0L), //
                eq(1L), //
                eq(Arrays.asList(new SearchQueryFilter("details.ediCode:G7801"))), //
                eq(Arrays.asList(new SearchQueryOrder("entityId:asc"))), //
                eq("") //
        );
        verify(this.storageClient).search(anyLong(), anyLong(), anyList(), anyList(), anyString());

        final ArgumentCaptor<byte[]> dmtduAsBytes = ArgumentCaptor.forClass(byte[].class);
        verify(this.eddieServiceClient).exchange(eq("12345678901234567890"), eq(StringUtils.EMPTY), matches("(.*)\\.zip"), dmtduAsBytes.capture());
        assertThat(dmtduAsBytes.getValue(), notNullValue());

        final FileEntry xmltcEntry = ScanFixUtils.getXMLTCEntry(dmtduAsBytes.getValue());
        final Document document = XPathBuilder.buildDocument(xmltcEntry.asBytes());
        assertThat(XPathBuilder.queryNode(document.getDocumentElement(), "//GUEN_DMTDU/dossierUnique/pieceJointe[typePieceJointe='RCS-FORMULAIRE_BE']"), nullValue());
        assertThat(XPathBuilder.queryNode(document.getDocumentElement(), "//GUEN_DMTDU/dossierUnique/pieceJointe[typePieceJointe='RCS-FORM_BE_SOCIETE']"), nullValue());

        verify(this.eddieServiceClient).exchange(eq("12345678901234567890"), eq(StringUtils.EMPTY), matches("(.*).zip.fini"), any(byte[].class));
        verify(this.trackerClient, never()).addMessage(anyString(), anyString(), anyString());
        verify(this.storageClient).store(anyString(), any(Attachment.class), anyString(), anyString(), anyList());

        assertThat(this.jobExecution.getStatus(), equalTo(BatchStatus.COMPLETED));
    }

    @Test
    public void testCannotFix() throws Exception, RestResponseException {
        // -->prepare
        final LocalDate now = LocalDate.now().minusYears(1L);

        final SearchResult<Message> result = new SearchResult<Message>();
        result.setMaxResults(1L);
        result.setStartIndex(0L);
        result.setTotalResults(1L);
        final Message message = new Message();
        message.setUid(RECORD_UID);
        message.setContent(String.format("[CRE_DOS_REJETEE_SES] [%s] Rejet du dossier de création par le SES : dossier non conforme", RECORD_UID));
        message.setCreated(this.localTimeToDate(LocalDate.now()));
        result.setContent(Arrays.asList(message));

        when(this.trackerMessageClient.searchMessage(anyLong(), anyLong(), anyList(), anyList())).thenReturn(result);

        final SearchResult<StoredFile> storageRecords = new SearchResult<>();
        storageRecords.setTotalResults(1L);

        final StoredFile singleFile = new StoredFile();
        singleFile.setId(STORAGE_UID);
        singleFile.setReferenceId(RECORD_UID);
        singleFile.setTray(StorageTrayEnum.ARCHIVED.name());
        storageRecords.setContent(Arrays.asList(singleFile));
        storageRecords.setTotalResults(storageRecords.getContent().size());
        when(this.storageClient.search(anyLong(), anyLong(), anyList(), anyList(), anyString())).thenReturn(storageRecords);
        when(this.storageClient.download(eq(STORAGE_UID))).thenReturn(Response.ok(this.resourceAsBytes("storage-no-fix.zip")).build());
        when(this.nashClient.download(RECORD_UID, "description.xml")).thenReturn(Response.ok(ZipUtil.entry("description.xml", this.resourceAsBytes("storage-no-fix.zip")).asBytes()).build());

        // ->call
        this.jobExecution = this.jobLauncherTestUtils.launchStep("scan-fix-error.master", //
                new JobParametersBuilder() //
                        .addString("ref", "{yyyy-MM}.SPT.ERROR") //
                        .addString("inf", DateTimeFormatter.ofPattern("YYYY-MM-dd").format(now))
                        //
                        .addString("sup", DateTimeFormatter.ofPattern("YYYY-MM-dd").format(now)) //
                        .toJobParameters() //
        );

        // -->verify
        verify(this.trackerMessageClient, times(2)).searchMessage(anyLong(), anyLong(), anyList(), anyList());
        verify(this.storageClient).search(anyLong(), anyLong(), anyList(), anyList(), anyString());
        verify(this.trackerClient).addMessage( //
                eq("SPT.CANNOT.FIX"), //
                eq(String.format("[CANNOT.FIX] [%s] Aucune correction appliquée pour ce dossier.", RECORD_UID)), //
                eq("scan-fix"));
        verify(this.eddieServiceClient, never()).exchange(anyString(), anyString(), anyString(), any(byte[].class));
    }
}
