package fr.ge.common.nash.batch.jobs;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;

import javax.ws.rs.core.Response;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.core.BatchStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.nash.batch.common.AbstractJobTest;
import fr.ge.common.utils.bean.search.SearchQueryFilter;

/**
 * Purge job class test.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/batch-core-config-test.xml", "classpath:jobs/purge-record.xml" })
public class JobPurgeTest extends AbstractJobTest {

    /** Job name. */
    private static final String BATCH_JOB_NAME = "purge";

    /**
     * Initialisation de la classe de test.
     * 
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void loadSystemProperties() {
        System.setProperty("job", BATCH_JOB_NAME);
    }

    /**
     * Test purge records containing no tracker alert.
     * 
     * @throws Exception
     *             : Exception
     */
    @Test
    public void testPurgeRecordWithNoAlert() throws Exception {
        // -->prepare
        LocalDate date = this.mockRecordSearch(true);
        when(this.trackerClient.getUid(any())).thenReturn(this.buildTrackerHistory(date, "data", "attachment", "review", "computeDestinataire", "payment", "finished"));

        // ->call
        this.jobExecution = this.jobLauncherTestUtils.launchStep("purge-record.master");

        // -->verify
        verify(this.trackerClient).getUid(eq(RECORD_UID));
        verify(this.nashClient, times(7)).search(anyLong(), anyLong(), anyList(), anyList());
        verify(this.nashClient, never()).remove(eq(Arrays.asList(new SearchQueryFilter("uid", ":", Arrays.asList(RECORD_UID)))));
        verify(this.trackerClient, never()).remove(eq(Arrays.asList(RECORD_UID)));
        assertThat(this.jobExecution.getStatus(), equalTo(BatchStatus.COMPLETED));
    }

    /**
     * Test purge records containing one tracker alert.
     * 
     * @throws Exception
     *             : Exception
     */
    @Test
    public void testPurgeRecordWithOneAlert() throws Exception {
        // -->prepare
        LocalDate date = this.mockRecordSearch(true);
        when(this.trackerClient.getUid(any())).thenReturn(this.buildTrackerHistory(date, "data", "attachment", "review", "computeDestinataire", "payment", "finished", TRACKER_AUTHOR_NOTIFY_START_15));
        // ->call
        this.jobExecution = this.jobLauncherTestUtils.launchStep("purge-record.master");

        // -->verify
        verify(this.trackerClient).getUid(eq(RECORD_UID));
        verify(this.nashClient, times(7)).search(anyLong(), anyLong(), anyList(), anyList());
        verify(this.nashClient, never()).remove(eq(Arrays.asList(new SearchQueryFilter("uid", ":", Arrays.asList(RECORD_UID)))));
        assertThat(this.jobExecution.getStatus(), equalTo(BatchStatus.COMPLETED));
    }

    /**
     * Test purge records containing two tracker alerts.
     * 
     * @throws Exception
     *             : Exception
     */
    @Test
    public void testPurgeRecordWithTwoAlert() throws Exception {
        // -->prepare
        LocalDate date = this.mockRecordSearch(true);
        when(this.trackerClient.getUid(any())).thenReturn(
                this.buildTrackerHistory(date, "data", "attachment", "review", "computeDestinataire", "payment", "finished", TRACKER_AUTHOR_NOTIFY_START_15, TRACKER_AUTHOR_NOTIFY_START_5));
        when(this.nashClient.remove(eq(Arrays.asList(new SearchQueryFilter("uid", ":", Arrays.asList(RECORD_UID)))))).thenReturn(Response.ok().build());

        // ->call
        this.jobExecution = this.jobLauncherTestUtils.launchStep("purge-record.master");

        // -->verify
        verify(this.trackerClient).getUid(eq(RECORD_UID));
        verify(this.nashClient, times(7)).search(anyLong(), anyLong(), anyList(), anyList());
        verify(this.nashClient).remove(eq(Arrays.asList(new SearchQueryFilter("uid", ":", Arrays.asList(RECORD_UID)))));
        verify(this.accountClient).readUser(eq(AUTHOR_UID));
        verify(this.exchangeClient).sendEmail(any());
        assertThat(this.jobExecution.getStatus(), equalTo(BatchStatus.COMPLETED));
    }

    /**
     * Test purge storage records.
     * 
     * @throws Exception
     *             : Exception
     */
    @Test
    public void testStorageRecord() throws Exception {
        // -->prepare
        this.mockStorageRecordSearch();

        when(this.storageClient.remove(eq(Arrays.asList(STORAGE_UID)), eq(null), eq(null))).thenReturn(Response.ok().build());
        when(this.trackerClient.remove(eq(Arrays.asList(RECORD_UID)))).thenReturn(Response.ok().build());

        // ->call
        this.jobExecution = this.jobLauncherTestUtils.launchStep("purge-storage.master");

        // -->verify
        verify(this.storageClient, times(2)).search(anyLong(), anyLong(), anyList(), anyList(), eq(null));
        verify(this.storageClient).remove(eq(Arrays.asList()), eq(null), eq(Arrays.asList(new SearchQueryFilter("reference:" + RECORD_UID))));
        verify(this.trackerClient).remove(eq(Arrays.asList(RECORD_UID)));
        assertThat(jobExecution.getStatus(), equalTo(BatchStatus.COMPLETED));
    }

    /**
     * Test purge proxy sessions.
     * 
     * @throws Exception
     *             : Exception
     */
    @Test
    public void testProxySessions() throws Exception {
        // -->prepare
        when(this.signatureClient.remove(anyString(), anyList())).thenReturn(Response.ok().build());
        when(this.paymentClient.remove(anyString(), anyList())).thenReturn(Response.ok().build());

        // ->call
        this.jobExecution = this.jobLauncherTestUtils.launchStep("purge-proxy.master");

        // -->verify
        verify(this.signatureClient).remove(eq(LocalDate.now().minusDays(365).toString()), eq(new ArrayList<>()));
        verify(this.paymentClient).remove(eq(LocalDate.now().minusDays(365).toString()), eq(new ArrayList<>()));
        assertThat(jobExecution.getStatus(), equalTo(BatchStatus.COMPLETED));
    }
}
