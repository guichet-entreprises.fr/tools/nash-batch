# Scan & Fix BATCH

## Installation structure

```
${ROOT_DIR}
│   README.md
│   nash-batch-${VERSION}-scan-fix.jar
│
└───config
│   │   application.properties
```

## How to execute the batch ?

Below is the command line to execute the BATCH :

```sh
java -jar ${DIR}/nash-batch-${VERSION}-scan-fix.jar fr.ge.common.nash.batch.launcher.ScanFixLauncher -t ${TYPE} -csv ${CSV} -f ${FILE} -d ${DIRECTORY} -o ${OUTPUT} -token ${TOKEN} -pathFolder ${PATHFOLDER} > ${LOGFILE}
```

where :

* DIR refers the target directory where the executable will be stored
* VERSION refers the executable version
* TYPE refers the executable type to launch : scan, fix or send (in lower case) 
* CSV refers the input CSV absolute file path containing files to scan, fix or send to EDDIE
* FILE refers the input absolute file path to scan or fix
* DIRECTORY refers the input absolute directory path to scan or fix
* OUTPUT refers the output absolute directory path to store fixed files
* LOGFILE refers the output absolute directory path to store log files
* TOKEN refers the EDDIE token to send fixed files
* PATHFOLDER refers the EDDIE sub path to send fixed files


## What is a validator ?

A Scan&Fix validator is a Java class which implements the interface `fr.ge.common.nash.batch.validator.IFunctionalValidator` and must implements the following methods

#### validateConditions
Here is the method to determine conditions to execute the validator (presence of the XML Regent and/or XMLTC file, the target authority, etc...)

#### validate
Here is the method to apply scan controls from the input zip file.

#### fix
Here is the method to apply multiple corrections from the input zip file.


## Describing the batch options
## Scan option

#### Description
This option will apply multiple controls from a zip file.
These controls are splitted in 2 parts :
* Consistency controls
* Functional controls

##### Consistency controls
This is about controlling the entries from the zip file :
* Presence of XML Regent and XMLTC files

##### Functional controls
This is about controlling the XMLTC content inside the zip file
* Presence of mandatory tags
* Controlling the attachment inside the zip file and also declared in the XMLTC content

#### Output
This option will generate a report file (in .csv format) describing the scan result.

#### Example
```sh
java -jar ${DIR}/nash-batch-${VERSION}-scan-fix.jar fr.ge.common.nash.batch.launcher.ScanFixLauncher -t scan -f ${FILE} -o ${OUTPUT} > ${LOGFILE}
```

```sh
java -jar ${DIR}/nash-batch-${VERSION}-scan-fix.jar fr.ge.common.nash.batch.launcher.ScanFixLauncher -t scan -d ${DIRECTORY} -o ${OUTPUT} > ${LOGFILE}
```

## Fix option

#### Description
Based on the scan option results, the fix option will apply multiple corrections from a zip file.

#### Output
This option will generate a report file (in .csv format) describing the fix result

#### Example
```sh
java -jar ${DIR}/nash-batch-${VERSION}-scan-fix.jar fr.ge.common.nash.batch.launcher.ScanFixLauncher -t fix -f ${FILE} -o ${OUTPUT} > ${LOGFILE}
```

```sh
java -jar ${DIR}/nash-batch-${VERSION}-scan-fix.jar fr.ge.common.nash.batch.launcher.ScanFixLauncher -t fix -d ${DIRECTORY} -o ${OUTPUT} > ${LOGFILE}
```

## Send option

#### Description
Based on a fix report file with a EDDIE token (and a path folder), the send option will send output files (.zip/.fini) to EDDIE.

#### Output
This option will generate a report file (in .csv format) describing the send result

#### Example
```sh
java -jar ${DIR}/nash-batch-${VERSION}-scan-fix.jar fr.ge.common.nash.batch.launcher.ScanFixLauncher -t send -csv ${CSV} -o ${OUTPUT} > ${LOGFILE}
```

