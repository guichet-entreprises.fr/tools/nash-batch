# Courriel purge - Dossier non finalisé

*Courriel envoyé au déclarant ayant créé un dossier sans le finaliser au bout de 2 mois et 15 jours*

**[OBJET : Suppression de votre dossier n° <numéro dossier>]**

Bonjour,

Vous avez entrepris la constitution du dossier n° [numéro dossier] sur le site www.guichet-entreprises.fr le [date création dossier]. Nous vous invitons à le finaliser.

Sans action de votre part pour valider votre dossier, il sera supprimé sous 15 jours à compter de la réception de ce courriel.

Votre espace personnel restera accessible pour toute autre démarche.

Pour toute question relative à votre dossier, vous pouvez contacter le service Guichet Entreprises à l’adresse suivante : [adresse support].

Merci de votre confiance,

L’équipe Guichet Entreprises.

Ce message est automatique, veuillez ne pas y répondre.