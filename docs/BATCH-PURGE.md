# Batch de purge

## Contexte métier

### Notification :
Notifier le déclarant par courriel si et seulement si : 
* un dossier a été crée et est non finalisé dont **la date de modification est antérieure à 2 mois et 15 jours**. 
* un dossier a été crée et est finalisé dont **la date de modification est antérieure à 2 mois et 15 jours**.
* dont **la date de dernière connexion est antérieure à 11 mois et 15 jours**.

>NB : Le wording sera validé par l'équipe TILT et appliqué pour chaque point mentionné ci-dessus.

### Suppression physique des dossiers :
* crées et non finalisés dont **la date de modification est antérieure à 3 mois**.
* crées et finalisés dont **la date de modification est antérieure à 3 mois**.
* crées et finalisés dont **la date de modification est antérieure à 12 mois**.

### Suppression physique des comptes Guichet-Entreprises :
> Concerne tous les comptes Guichet-Entreprises dont **la date de dernière connexion** est antérieure à 12 mois.

## FAQ :

### Qu'est-ce qu'un dossier non finalisé ?
> Un dossier dans lequel **au moins une étape utilisateur est au statut "A faire" ou "En cours"**.

### Qu'est-ce qu'un dossier finalisé ?
> Un dossier **présent dans la pile "Archive" de Storage**.

### Quelles sont les données d'un dossier déclarant à supprimer dans le SI Guichet-Entreprises ?

| Application | Dossier déclarant non finalisé vieux de 3 mois | Dossier déclarant finalisé vieux de 3 mois | Dossier déclarant vieux de 12 mois (finalisé ou non) |
|------|------|------|------|
| Proxy (signature, payment) | Non | Non | Oui |
| Tracker | Oui | Non | Oui |
| Nash-web | Oui | Oui | Non applicable |
| Storage | Non applicable  | Non | Oui |
| Eddie | Non applicable | Oui | Non applicable |
| Account | Non | Non | Oui |

### Quelles sont les applications non concernées par le batch de purge ?
> Côté Guichet-Entreprises : 
* Clicker (données conservées 36 mois à des fins statistiques conformément au code du Commerce)
* Nash-profiler
* Welcome

> Côté Guichet-Partenaires : 
* Nash-support
* Nash-backoffice
* Nash-directory
* Exchange
* Markov
* Welcome

## Pré-requis
* Disposer des **templates** pour les notifications par courriel :
  * pour les dossiers crées et non finalisés dont la date de modification est antérieure à 3 mois.
  * pour les dossiers crées et finalisés dont la date de modification est antérieure à 3 mois.
  * pour les comptes utilisateurs dont la date de dernière connexion est antérieure à 11 mois et 15 jours.
  
* Disposer de l'**API de sélection** :
  * de dossier dans NASH-WEB (avec filtre sur une période de date et le statut et l'identifiant des étapes)
  * de dossier dans Storage (avec filtre sur une période de date et sur la pile)

* Disposer des **API de suppression physique de données**

## Impact pour les formalités dédiées au Support
* Rejeu total et partiel (revoir le développement)
* Recherche dossier
 