# Courriel purge - Dossier finalisé

Courriel envoyé au déclarant ayant finalisé un dossier il y a 11 mois et 15 jours

**[OBJET : Suppression de votre dossier n° <numéro dossier>]**

Bonjour,

Vous avez créé le dossier n° [numéro dossier] sur le site www.guichet-entreprises.fr le [date création dossier].

Conformément à la réglementation en vigueur, les données personnelles renseignées et les fichiers joints à votre dossier seront effacés dans 15 jours.

Votre espace personnel restera accessible pour toute nouvelle démarche.

Pour toute question relative à votre dossier, vous pouvez vous rapprocher du centre de formalités des entreprises (CFE) compétent ou contacter le service Guichet Entreprises à l’adresse suivante : [adresse support]. Pour télécharger votre dossier, contactez le service Guichet Entreprises à la même adresse.

Merci de votre confiance,

L’équipe Guichet Entreprises.

Ce message est automatique, veuillez ne pas y répondre.