# Changelog
Ce fichier contient les modifications techniques du module.

## [2.15.4.0] - 2020-08-28

###  Ajout
- Ajout du bloc paiement dans le XMLTC pour les personnes morales

## [2.15.3.0] - 2020-08-20

###  Ajout
- Suppression de la balise typeDeVoie dans le XMLTC

- __Properties__ :

| fichier   |      nom      |  description | valeur |
|-----------|:-------------:|-------------:|-------:|
|application.properties|NASH GE-GQ private URL to download records|ws.nash.url|${NASH-GE-GQ-WS}/api|

### Suppression

- __Properties__ :

| fichier   |      nom      |
|-----------|:-------------:|
|application.properties|nash.ge.url.ws.private|

## [2.15.2.1] - 2020-07-13
###  Ajout
Revue de l'impl�mentation du batch Scan&Fix (spring-batch, utilisation de TRACKER et de STORAGE comme source de donn�es) 

## [2.15.2.0] - 2020-07-07
###  Ajout
- Correction du libell� du type de pi�ce jointe RCS-FORMULAIRE_BE et RCS-FORM_BE_SOCIETE

## [2.14.5.0] - 2020-05-18
###  Ajout
- Correction lors de la conversion des fichiers en PDF

## [2.14.4.1] - 2020-04-21

- Projet : [Nash-batch](https://tools.projet-ge.fr/gitlab/tools/nash-batch.git)

###  Ajout
- Correction de la balise typePieceJointe

## [2.14.4.0] - 2020-04-21

- Projet : [Nash-batch](https://tools.projet-ge.fr/gitlab/tools/nash-batch.git)

###  Ajout
- Revue de la gestion des exceptions

## [2.14.3.1] - 2020-04-15

- Projet : [Nash-batch](https://tools.projet-ge.fr/gitlab/tools/nash-batch.git)

###  Ajout
- Correction de la balise "typeDeVoie"
- Transformer un flux MGUN en flux PGUEN dans le XMLTC à destination du GREFFE

- __Properties__ :

###  Ajout
| fichier   |      nom      |  description | valeur |
|-----------|:-------------:|-------------:|-------:|
|application.properties|Storage GP private URL|mas.storage.service.url|${STORAGE-GP-WS}/api|

## [2.14.3.0] - 2020-04-14

- Projet : [Nash-batch](https://tools.projet-ge.fr/gitlab/tools/nash-batch.git)

###  Ajout
- Analyse et correction des noms des fichiers avec pour extension .png
- Notification par courriel et purge des dossiers
- Construction d'un fichier "shaded" pour le batch de scan & fix d'un fichier "shaded" pour le batch de notification et de purge

- __Properties__ :

###  Ajout
| fichier   |      nom      |  description | valeur |
|-----------|:-------------:|-------------:|-------:|
|application.properties|Exchange GP private URL|mas.exchange.service.url|${EXCHANGE-GP-WS}/api|
|application.properties|Storage GP private URL|mas.storage.service.url|${STORAGE-GP-WS}/api|
|application.properties|Tracker GE private URL|mas.tracker.service.url|${TRACKER-GE-WS}/api|
|application.properties|Stamper GE private URL|stamper.ge.url.ws.private|${STAMPER-GE-WS}/api|

###  Modification
| fichier   |      nom      |  description | anicenne valeur | nouvelle valeur |
|-----------|:-------------:|-------------:|-------:|-------:|
|application.properties|Payment GE private URL|payment.ge.url.ws.private|${PAYMENT-GE-WS}/private/api|${PAYMENT-GE-WS}/private|

## [2.14.1.0] - 2020-03-19

- Projet : [Nash-batch](https://tools.projet-ge.fr/gitlab/tools/nash-batch.git)

###  Ajout
- Analyse et correction des noms des fichiers inclus dans le DMTDU à destination de : URSSAF, CCI, CMA
- Analyse et correction de la balise <C05> dans le XML Regent en se basant sur le modèle de formalité

- __Properties__ :
Aucune modification

## [2.13.5.1] - 2020-03-04

- Projet : [Nash-batch](https://tools.projet-ge.fr/gitlab/tools/nash-batch.git)

###  Ajout
- Construction d'un fichier CSV éligible pour le mass mailing
- Ajout de la référence de paiement dans les fichiers CSV de sortie pour les dossiers avec paiement
- Supprimer le bloc paiement dont le montant est égal à 0,00 euros dans le XMLTC
- Correction du contenu de la balise 'typeDestinataire' pour les destinataires autre que le GREFFE

- __Properties__ :

| fichier   |      nom      |  description | valeur |
|-----------|:-------------:|-------------:|-------:|
|application.properties|Account GE private URL to get user information|account.ge.url.ws.private|${ACCONT-GE-WS}|
|application.properties|Payment GE private URL|payment.ge.url.ws.private|${PAYMENT-GE-WS}/private/api|

### Modification


### Suppression

## [2.13.5.0] - 2020-02-21

- Projet : [Nash-batch](https://tools.projet-ge.fr/gitlab/tools/nash-batch.git)

###  Ajout
- Mise en place du batch de Scan, fix and send pour les dossiers en souffrance transmis aux autorités (GREFFE, CCI, URSSAF)

- __Properties__ :

| fichier   |      nom      |  description | valeur |
|-----------|:-------------:|-------------:|-------:|
|application.properties|EDDIE private URL to send files|eddie.gp.url.ws.private|${EDDIE-WS}/api|
|application.properties|NASH GQ-GQ private URL to download records|nash.ge.url.ws.private|${NASH-GE-GQ-WS}/api|

### Modification


### Suppression
